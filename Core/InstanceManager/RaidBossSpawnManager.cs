﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Core.Model;
using Core.Model.CalculateStats;
using Core.Model.Npc.Type;
using Core.Model.Spawn;
using DataBase.Interfaces;
using L2Logger;
using Microsoft.Extensions.DependencyInjection;

namespace Core.InstanceManager
{
    public sealed class RaidBossSpawnManager
    {
        private readonly ConcurrentDictionary<int, L2RaidBoss> _bosses;
        private readonly IRaidBossSpawnListRepository _raidBossSpawnListRepository;
        private readonly IdFactory _idFactory;
        private readonly Calculator[] _calculators;

        public RaidBossSpawnManager(IServiceProvider serviceProvider)
        {
            _bosses = new ConcurrentDictionary<int, L2RaidBoss>();
            _raidBossSpawnListRepository = serviceProvider.GetService<IUnitOfWork>()?.RaidBossSpawnList;
            _idFactory = serviceProvider.GetService<IdFactory>();
            _calculators = Formulas.GetStandardNpcCalculators();
            Task.Run(Initialize);
        }

        private async Task Initialize()
        {
            try
            {
                LoggerManager.Info(GetType().Name + ": Loading Raid Bosses...");
                var list = _raidBossSpawnListRepository.GetAllAsync();
                foreach (var raidBossSpawn in list.Result)
                {
                    var raidBossTemplate = Initializer.NpcTableService().GetTemplate(raidBossSpawn.BossId);
                    SpawnData spawnData = new SpawnData(raidBossTemplate, _idFactory, _calculators)
                    {
                        X = raidBossSpawn.LocX,
                        Y = raidBossSpawn.LocY,
                        Z = raidBossSpawn.LocZ,
                        Heading = raidBossSpawn.Heading,
                        RespawnMinDelay = raidBossSpawn.RespawnMinDelay,
                        RespawnMaxDelay = raidBossSpawn.RespawnMaxDelay,
                    };
                    Initializer.SpawnInit().AddNewSpawn(spawnData);

                    var raidBoss = (L2RaidBoss) await spawnData.DoSpawn();
                    await raidBoss.Status.SetCurrentHpAsync(raidBossSpawn.CurrentHp);
                    raidBoss.Status.SetCurrentMp(raidBossSpawn.CurrentMp);
                    _bosses.TryAdd(spawnData.Id, raidBoss);
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Error(GetType().Name + ": " + ex.Message);
            }
            LoggerManager.Info("RaidBossSpawnManager: Loaded " + _bosses.Count + " instances.");
        }
    }
}