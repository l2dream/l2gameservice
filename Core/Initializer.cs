﻿using System;
using Config;
using Core.Cache;
using Core.InstanceManager;
using Core.Model;
using Core.Model.CalculateStats;
using Core.Model.Items;
using Core.Model.Items.Handlers;
using Core.Model.Npc;
using Core.Model.Player.Handlers;
using Core.Model.Player.Template;
using Core.Model.Skills;
using Core.Model.Skills.Handlers;
using Core.Model.Spawn;
using Core.Model.Teleport;
using Core.Model.World;
using Core.Model.World.RegionData;
using Core.Model.Zones;
using L2Logger;
using Microsoft.Extensions.DependencyInjection;

namespace Core
{
    public class Initializer
    {
        public static IServiceProvider ServiceProvider { get; private set; }
        
        public Initializer(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }
        
        public static GameConfig Config()
        {
            return ServiceProvider.GetService<GameConfig>();
        }

        public static GameTimeController TimeController()
        {
            return ServiceProvider.GetService<GameTimeController>();
        }

        public static WorldInit WorldInit()
        {
            return ServiceProvider.GetService<WorldInit>();
        }

        public static ZoneInit ZoneService()
        {
            return ServiceProvider.GetService<ZoneInit>();
        }

        public static MapRegionInit MapRegionService()
        {
            return ServiceProvider.GetService<MapRegionInit>();
        }

        public static SpawnInit SpawnInit()
        {
            return ServiceProvider.GetService<SpawnInit>();
        }
        
        public static RaidBossSpawnManager RaidBossSpawnInit()
        {
            return ServiceProvider.GetService<RaidBossSpawnManager>();
        }

        public static NpcTableInit NpcTableService()
        {
            return ServiceProvider.GetService<NpcTableInit>();
        }
        
        public static ItemInit ItemService()
        {
            return ServiceProvider.GetService<ItemInit>();
        }

        public static SkillInit SkillService()
        {
            return ServiceProvider.GetService<SkillInit>();
        }
        
        public static SkillTreeInit SkillTreeService()
        {
            return ServiceProvider.GetService<SkillTreeInit>();
        }

        public static SkillHandlerInit SkillHandlerService()
        {
            return ServiceProvider.GetService<SkillHandlerInit>();
        }

        public static ItemHandlerInit ItemHandlerService()
        {
            return ServiceProvider.GetService<ItemHandlerInit>();
        }

        public static PlayerHandlerInit PlayerHandlerService()
        {
            return ServiceProvider.GetService<PlayerHandlerInit>();
        }
        
        public static BonusStatsInit BonusStatsService()
        {
            return ServiceProvider.GetService<BonusStatsInit>();
        }

        public static ExperienceDataInit ExperienceDataService()
        {
            return ServiceProvider.GetService<ExperienceDataInit>();
        }
        
        public static HtmlCacheInit HtmlCacheService()
        {
            return ServiceProvider.GetService<HtmlCacheInit>();
        }

        public static TeleportLocationInit TeleportLocationService()
        {
            return ServiceProvider.GetService<TeleportLocationInit>();
        }
        
        public static SkillSpellBookInit SkillSpellBookService()
        {
            return ServiceProvider.GetService<SkillSpellBookInit>();
        }
        
        public void Load()
        {
            LoggerManager.Info("----Html Cache----");
            ServiceProvider.GetService<HtmlCacheInit>();
            LoggerManager.Info("----Json Teleports----");
            ServiceProvider.GetService<TeleportLocationInit>();
            LoggerManager.Info("----Players----");
            ServiceProvider.GetService<PlayerTemplateInit>();
            LoggerManager.Info("----Bonus Stats----");
            ServiceProvider.GetService<BonusStatsInit>();
            LoggerManager.Info("----Items----");
            ServiceProvider.GetService<ItemInit>();
            ServiceProvider.GetService<ItemHandlerInit>();
            ServiceProvider.GetService<PlayerHandlerInit>();
            //LoggerManager.Info("----Parser XML----");
            //ServiceProvider.GetService<Parser>();
            LoggerManager.Info("----Skills----");
            ServiceProvider.GetService<SkillInit>();
            ServiceProvider.GetService<SkillTreeInit>();
            ServiceProvider.GetService<SkillHandlerInit>();
            ServiceProvider.GetService<SkillSpellBookInit>();
            LoggerManager.Info("----World----");
            ServiceProvider.GetService<WorldInit>();
            ServiceProvider.GetService<MapRegionInit>();
            ServiceProvider.GetService<ExperienceDataInit>();
            LoggerManager.Info("----Npc----");
            ServiceProvider.GetService<NpcTableInit>();
            ServiceProvider.GetService<NpcWalkerRouteDataInit>();
            LoggerManager.Info("----Spawn List----");
            ServiceProvider.GetService<SpawnInit>();
            ServiceProvider.GetService<RaidBossSpawnManager>();
            LoggerManager.Info("----Zone----");
            ServiceProvider.GetService<ZoneInit>();
        }
    }
}