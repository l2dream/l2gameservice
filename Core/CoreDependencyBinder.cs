﻿using Core.Cache;
using Core.InstanceManager;
using Core.Model.CalculateStats;
using Core.Model.Items;
using Core.Model.Items.Handlers;
using Core.Model.Npc;
using Core.Model.Player.Handlers;
using Core.Model.Player.Template;
using Core.Model.Skills;
using Core.Model.Skills.Handlers;
using Core.Model.Spawn;
using Core.Model.Teleport;
using Core.Model.World;
using Core.Model.World.RegionData;
using Core.Model.Zones;
using Core.Network;
using Microsoft.Extensions.DependencyInjection;

namespace Core
{
    public static class CoreDependencyBinder
    {
        public static void Bind(IServiceCollection services)
        {

            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = Initializer.Config().ServerConfig.RedisConnection;
                options.InstanceName = "Interlude_";
            });
            //services.AddSingleton<Parser>();
            services.AddSingleton<NetworkWriter>();
            services.AddSingleton<HtmlCacheInit>();
            services.AddSingleton<TeleportLocationInit>();
            services.AddSingleton<GameTimeController>();
            services.AddTransient<GameServiceController>();
            services.AddTransient<ClientManager>();
            services.AddSingleton<PlayerTemplateInit>();
            services.AddSingleton<BonusStatsInit>();
            services.AddSingleton<ItemInit>();
            services.AddSingleton<ItemHandlerInit>();
            services.AddSingleton<NpcTableInit>();
            services.AddSingleton<NpcWalkerRouteDataInit>();
            services.AddSingleton<WorldInit>();
            services.AddSingleton<MapRegionInit>();
            services.AddSingleton<ExperienceDataInit>();
            services.AddSingleton<ZoneInit>();
            services.AddSingleton<SpawnInit>();
            services.AddSingleton<RaidBossSpawnManager>();
            services.AddSingleton<SkillInit>();
            services.AddSingleton<SkillSpellBookInit>();
            services.AddSingleton<SkillTreeInit>();
            services.AddSingleton<SkillHandlerInit>();
            services.AddSingleton<PlayerHandlerInit>();
        }
    }
}