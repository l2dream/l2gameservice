﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Helpers;
using L2Logger;

namespace Core.TaskManager
{
    internal sealed class ThreadPoolManager
    {
        private static volatile ThreadPoolManager _instance;
        
        public static ThreadPoolManager Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;
                if (_instance == null)
                    _instance = new ThreadPoolManager();
                return _instance;
            }
        }

        public void Schedule(Action action, int delay)
        {
            try
            {
                Task.Run(async () =>
                {
                    await Task.Delay(delay);
                    action.Invoke();
                });
            }
            catch (Exception ex)
            {
                LoggerManager.Error(GetType().Name + " "  + ex.Message);
            }
        }

        public Task ScheduleAtFixed(Action action, int delay, CancellationToken token)
        {
            return Task.Run( async () =>
            {
                await Task.Delay(delay, token);
                action.Invoke();
            }, token);
        }
        
        public void Schedule(Func<Task> func, int delay)
        {
            Task.Run(async () =>
            {
                await Task.Delay(delay);
                await func.Invoke();
            });
        }
        
        public async Task Schedule(Action action, int delay, int period)
        {
            using var timer = new TaskTimer(period).Start(delay);
            foreach (var task in timer)
            {
                await task;
                action.Invoke();
            }
        }

        public Task ScheduleAtFixedRate(Action action, int delay, int period, CancellationToken token)
        {
            try
            {
                return Task.Run(async () =>
                {
                    try
                    {
                        using var timer = new TaskTimer(period).CancelWith(token).Start(delay);
                        foreach (var task in timer)
                        {
                            await task;
                            action.Invoke();
                        }
                    }
                    catch (TaskCanceledException)
                    {

                    }
                }, token);
            }
            catch (Exception ex)
            {
                LoggerManager.Error(GetType().Name + " " + ex.Message);
                throw;
            }
        }
        
        public Task ScheduleAtFixedRate(Action action, int delay, int period)
        {
            try
            {
                return Task.Run(async () =>
                {
                    try
                    {
                        using var timer = new TaskTimer(period).Start(delay);
                        foreach (var task in timer)
                        {
                            await task;
                            action.Invoke();
                        }
                    }
                    catch (TaskCanceledException)
                    {

                    }
                });
            }
            catch (Exception ex)
            {
                LoggerManager.Error(GetType().Name + " " + ex.Message);
                throw;
            }
        }
        
        public Task ScheduleAtFixedRate(Func<Task> action, int delay, int period)
        {
            try
            {
                return Task.Run(async () =>
                {
                    try
                    {
                        using var timer = new TaskTimer(period).Start(delay);
                        foreach (var task in timer)
                        {
                            await task;
                            await action.Invoke();
                        }
                    }
                    catch (TaskCanceledException)
                    {

                    }
                });
            }
            catch (Exception ex)
            {
                LoggerManager.Error(GetType().Name + " " + ex.Message);
                throw;
            }
        }
    }
}