﻿using System.Collections.Concurrent;
using Core.Model.Actor;

namespace Core.TaskManager
{
    internal sealed class AttackableThinkTaskManager
    {
        private static volatile AttackableThinkTaskManager _instance;
        
        private readonly ConcurrentDictionary<int, Attackable> _attackables;
        private bool _working;
        
        public static AttackableThinkTaskManager Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;
                if (_instance == null)
                    _instance = new AttackableThinkTaskManager();
                return _instance;
            }
        }

        private AttackableThinkTaskManager()
        {
            _attackables = new ConcurrentDictionary<int, Attackable>();
            ThreadPoolManager.Instance.ScheduleAtFixedRate(Run, 1000, 1000);
        }

        private void Run()
        {
            if (_working)
            {
                return;
            }
            _working = true;
            foreach (var attackable in _attackables.Values)
            {
                if (attackable.AI != null)
                {
                    var ai = attackable.AI;
                    if (ai != null)
                    {
                        ai.OnEvtThinkAsync();
                    }
                    else
                    {
                        Remove(attackable);
                    }
                }
                else
                {
                    Remove(attackable);
                }
            }
            _working = false;
        }

        public void Add(Attackable attackable)
        {
            _attackables.TryAdd(attackable.ObjectId, attackable);
        }
        
        public void Remove(Attackable attackable)
        {
            _attackables.TryRemove(attackable.ObjectId, out _);
        }
    }
}