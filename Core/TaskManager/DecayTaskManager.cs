﻿using System;
using System.Collections.Concurrent;
using Core.Model.Actor;
using Core.Model.Npc.Type;
using Helpers;
using L2Logger;

namespace Core.TaskManager
{
    internal sealed class DecayTaskManager
    {
        private static volatile DecayTaskManager _instance;
        private readonly ConcurrentDictionary<Character, long> _decayTasks;
        
        public static DecayTaskManager Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;
                if (_instance == null)
                    _instance = new DecayTaskManager();
                return _instance;
            }
        }

        private DecayTaskManager()
        {
            _decayTasks =  new ConcurrentDictionary<Character, long>();
            ThreadPoolManager.Instance.ScheduleAtFixedRate(Run,10000, 5000);
        }
        
        public void AddDecayTask(Character actor)
        {
            _decayTasks.TryAdd(actor, DateTimeHelper.CurrentUnixTimeMillis());
        }
        
        public void AddDecayTask(Character actor, int interval)
        {
            _decayTasks.TryAdd(actor, DateTimeHelper.CurrentUnixTimeMillis() + interval);
        }
        
        public void CancelDecayTask(Character actor)
        {
            try
            {
                _decayTasks.TryRemove(actor, out _);
            }
            catch (Exception ex)
            {
                LoggerManager.Error(GetType().Name + " " + ex.Message);
            }
        }

        private void Run()
        {
            long current = DateTimeHelper.CurrentUnixTimeMillis();
            try
            {
                if (_decayTasks == null) return;
                foreach (var (actor, value) in _decayTasks)
                {
                    var delay = actor is L2RaidBoss ? 30000 : 8500;
                    if ((current - value) <= delay) continue;
                    actor.OnDecay();
                    _decayTasks.TryRemove(actor, out _);
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Error(GetType().Name + " " + ex.Message);
            }
        }
    }
}