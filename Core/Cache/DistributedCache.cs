﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace Core.Cache
{
    public static class DistributedCache
    {
        public static async Task SetDataAsync<T>(this IDistributedCache cache, 
            string keyId, 
            T data,
            TimeSpan? absoluteExpireTime = null,
            TimeSpan? unusedExpireTime = null
            )
        {
            var options = new DistributedCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = absoluteExpireTime ?? TimeSpan.FromDays(1),
                SlidingExpiration = unusedExpireTime
            };
            var jsonData = JsonConvert.SerializeObject(data);
            await cache.SetStringAsync(keyId, jsonData, options);
        }
        
        public static async Task<T> GetDataAsync<T>(this IDistributedCache cache, string keyId)
        {
            var jsonData = await cache.GetStringAsync(keyId);
            return jsonData is null ? default(T) : JsonConvert.DeserializeObject<T>(jsonData);
        }

        public static async Task SetDataAsync(this IDistributedCache cache, string keyId, 
            string data,
            TimeSpan? absoluteExpireTime = null,
            TimeSpan? unusedExpireTime = null)
        {
            var options = new DistributedCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = absoluteExpireTime ?? TimeSpan.FromDays(1),
                SlidingExpiration = unusedExpireTime
            };
            await cache.SetStringAsync(keyId, data, options);
        }

        public static async Task<string> GetDataAsync(this IDistributedCache cache, string keyId)
        {
            var jsonData = await cache.GetStringAsync(keyId);
            return jsonData;
        }
    }
}