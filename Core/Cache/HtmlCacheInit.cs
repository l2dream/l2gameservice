﻿using System;
using System.Collections.Generic;
using System.IO;
using L2Logger;

namespace Core.Cache
{
    public class HtmlCacheInit
    {
        private readonly Dictionary<int, string> _cache;
        private int _loadedFiles;
        private long _bytesBuffLen;
        private readonly string _basePath;

        public HtmlCacheInit()
        {
            _cache = new Dictionary<int, string>();
            _basePath = Initializer.Config().ServerConfig.StaticData + "/Html";
            Init();
        }

        private void Init()
        {
            LoggerManager.Info("Html cache start...");
            LoadDirectory(_basePath);
            LoggerManager.Info("Cache[HTML]: " + $"{GetMemoryUsage():F3}" + " megabytes on " + _loadedFiles + " files loaded");
        }

        private void LoadDirectory(string path)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            if (!dirInfo.Exists)
            {
                throw new Exception("Html static does not exist");
            }

            foreach (FileSystemInfo file in dirInfo.EnumerateFileSystemInfos())
            {
                if (file is FileInfo fileInfo)
                {
                    LoadFile(fileInfo);
                }
                else
                {
                    LoadDirectory(file.FullName);
                }
            }
        }

        private string LoadFile(FileInfo fileInfo)
        {
            string content = "";
            try
            {
                using (StreamReader sr = new StreamReader(fileInfo.FullName))
                {
                    content = new string(sr.ReadToEnd());
                    content = content.Replace("\r\n", "\n");
                    content = content.Replace("(?s)<!--.*?-->", ""); // Remove html comments

                    string relativePath = fileInfo.FullName.Substring(_basePath.Length);
                    var newStr = relativePath.Replace("\\", "/");
                    int hashCode = newStr.GetHashCode();
                    _cache.Add(hashCode, content);

                    _loadedFiles++;
                    _bytesBuffLen += fileInfo.Length;
                }
                return content;
            }
            catch (FileNotFoundException e)
            {
                LoggerManager.Error(e.Message);
            }

            return content;
        }
        
        public String GetHtm(string path)
        {
            if (_cache.ContainsKey(path.GetHashCode()))
            {
                return _cache[path.GetHashCode()];
            }
            return LoadFile(new FileInfo(_basePath + path));
        }
        
        public String GetHtmForce(string path)
        {
            String content = GetHtm(path);
            if (content == null)
            {
                content = "<html><body>My text is missing:<br>" + path + "</body></html>";
                LoggerManager.Warn("Cache[HTML]: Missing HTML page: " + path);
            }
            return content;
        }

        private double GetMemoryUsage()
        {
            return (float) _bytesBuffLen / 1048576;
        }
    }
}