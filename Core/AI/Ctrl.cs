﻿using Core.Model.Actor;

namespace Core.AI
{
    public interface Ctrl
    {
        /**
	     * @return the character this AI serves
	     */
        Character GetActor();
        
        /**
		 * @return the current intention.
		 */
        CtrlIntention GetIntention();
        
        /**
		 * @return the current attack target.
		 */
        Character GetAttackTarget();
        
        /**
		 * Set general state/intention for AI, with optional data
		 * @param intention
		 */
        void SetIntention(CtrlIntention intention);
	
        void SetIntention(CtrlIntention intention, object arg0);
	
        void SetIntention(CtrlIntention intention, object arg0, object arg1);
        
        /**
		 * Event, that notifies about previous step result, or user command, that does not change current general intention
		 * @param evt
		 */
        void NotifyEvent(CtrlEvent evt);
	
        void NotifyEvent(CtrlEvent evt, object arg0);
	
        void NotifyEvent(CtrlEvent evt, object arg0, object arg1);
    }
}