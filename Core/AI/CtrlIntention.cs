﻿namespace Core.AI
{
	/**
	 * Enumeration of generic intentions of an NPC/PC, an intention may require several steps to be completed
	 */
    public enum CtrlIntention
    {
        /** Do nothing, disconnect AI of NPC if no players around */
        AiIntentionIdle,
	
        /** Alerted state without goal : scan attackable targets, random walk, etc */
        AiIntentionActive,
	
        /** Rest (sit until attacked) */
        AiIntentionRest,
	
        /**
		 * Attack target (cast combat magic, go to target, combat), may be ignored, if target is locked on another character or a peacefull zone and so on
		 */
        AiIntentionAttack,
	
        /** Cast a spell, depending on the spell - may start or stop attacking */
        AiIntentionCast,
	
        /** Just move to another location */
        AiIntentionMoveTo,
	
        /** Like move, but check target's movement and follow it */
        AiIntentionFollow,
	
        /** PickUp and item, (got to item, pickup it, become idle */
        AiIntentionPickUp,
	
        /** Move to target, then interact */
        AiIntentionInteract,
	
        /** Move to another location in a boat */
        AiIntentionMoveToInABoat
    }
}