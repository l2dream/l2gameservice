﻿using System;
using System.Threading.Tasks;
using Core.Model;
using Core.Model.Actor;
using Core.Model.Npc.Type;
using Core.Model.Player;
using Core.Model.Skills;
using Core.Model.World;
using Core.Network.GameServicePackets.ServerPackets;
using Core.TaskManager;

namespace Core.AI
{
    /// <summary>
    /// Mother class of all objects AI in the world
    /// </summary>
    public abstract class AbstractAi : Ctrl
    {
        /** The creature that this AI manages */
        protected readonly Character _actor;
        
        /** An accessor for private methods of the actor */
        
        /** Current long-term intention */
        private CtrlIntention _intention = CtrlIntention.AiIntentionIdle;
        /** Current long-term intention parameter */
        private object _intentionArg0 = null;
        /** Current long-term intention parameter */
        private object _intentionArg1 = null;
        
        /** Flags about client's state, in order to know which messages to send */
        protected bool _clientMoving;
        /** Flags about client's state, in order to know which messages to send */
        private bool _clientAutoAttacking;
        /** Flags about client's state, in order to know which messages to send */
        protected int _clientMovingToPawnOffset;
        
        /** Different targets this AI maintains */
        private WorldObject _target;
        private Character _castTarget;
        private Character _attackTarget;

        /** Different internal state flags */
        private int _moveToPawnTimeout;

        protected AbstractAi(Character character)
        {
            _actor = character;
        }

        /// <summary>
        /// Return the Creature managed by this Accessor AI
        /// </summary>
        /// <returns></returns>
        public Character GetActor() => _actor;

        public Character Actor => _actor;

        /// <summary>
        /// Set the Intention of this AbstractAI
        /// This method is USED by AI classes
        /// </summary>
        /// <param name="intention">The new Intention to set to the AI</param>
        /// <param name="arg0">The first parameter of the Intention</param>
        /// <param name="arg1">The second parameter of the Intention</param>
        public virtual void ChangeIntention(CtrlIntention intention, object arg0, object arg1)
        {
            lock (this)
            {
                _intention = intention;
                _intentionArg0 = arg0;
                _intentionArg1 = arg1;
            }
        }

        protected WorldObject Target
        {
            set
            {
                lock (this)
                {
                    _target = value;
                }
            }
            get => _target;
        }

        protected Character AttackTarget
        {
            set
            {
                lock (this)
                {
                    _attackTarget = value;
                }
            }
            get => _attackTarget;
        }

        protected Character CastTarget
        {
            get => _castTarget;
            set
            {
                lock (this)
                {
                    _castTarget = value;
                }
            }
        }

        public Character FollowTarget { get; set; }

        public CtrlIntention GetIntention()
        {
            return _intention;
        }

        public Character GetAttackTarget()
        {
            return _attackTarget;
        }

        /// <summary>
        /// Launch the L2CharacterAI onIntention method corresponding to the new Intention
        /// </summary>
        /// <param name="intention">The new Intention to set to the AI</param>
        public void SetIntention(CtrlIntention intention)
        {
            SetIntention(intention, null, null);
        }

        /// <summary>
        /// Launch the L2CharacterAI onIntention method corresponding to the new Intention
        /// </summary>
        /// <param name="intention">The new Intention to set to the AI</param>
        /// <param name="arg0">The first parameter of the Intention (optional target)</param>
        public void SetIntention(CtrlIntention intention, object arg0)
        {
            SetIntention(intention, arg0, null);
        }

        /// <summary>
        /// Launch the L2CharacterAI onIntention method corresponding to the new Intention
        /// </summary>
        /// <param name="intention">The new Intention to set to the AI</param>
        /// <param name="arg0">The first parameter of the Intention (optional target)</param>
        /// <param name="arg1">The second parameter of the Intention (optional target)</param>
        public void SetIntention(CtrlIntention intention, object arg0, object arg1)
        {
            if (!_actor.IsVisible())
            {
                return;
            }
		
            // Stop the follow mode if necessary
            if ((intention != CtrlIntention.AiIntentionFollow) && (intention != CtrlIntention.AiIntentionAttack))
            {
                StopFollow();
            }
            
            // Launch the onIntention method of the CreatureAI corresponding to the new Intention
            switch (intention)
            {
                case CtrlIntention.AiIntentionIdle:
                    OnIntentionIdleAsync();
                    break;
                case CtrlIntention.AiIntentionActive:
                    OnIntentionActiveAsync();
                    break;
                case CtrlIntention.AiIntentionRest:
                    OnIntentionRest();
                    break;
                case CtrlIntention.AiIntentionAttack:
                    OnIntentionAttackAsync((Character) arg0);
                    break;
                case CtrlIntention.AiIntentionCast:
                    OnIntentionCast((Skill) arg0, (WorldObject) arg1);
                    break;
                case CtrlIntention.AiIntentionMoveTo:
                    OnIntentionMoveToAsync((Location) arg0);
                    break;
                case CtrlIntention.AiIntentionFollow:
                    OnIntentionFollowAsync((Character) arg0);
                    break;
                case CtrlIntention.AiIntentionPickUp:
                    OnIntentionPickUpAsync((WorldObject) arg0);
                    break;
                case CtrlIntention.AiIntentionInteract:
                    OnIntentionInteractAsync((WorldObject) arg0);
                    break;
                case CtrlIntention.AiIntentionMoveToInABoat:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(intention), intention, null);
            }
        }

        /// <summary>
        /// Launch the L2CharacterAI onEvt method corresponding to the Event
        /// The current general intention won't be change (ex : If the character attack and is stunned, he will attack again after the stunned periode)
        /// </summary>
        /// <param name="evt">The event whose the AI must be notified</param>
        public void NotifyEvent(CtrlEvent evt)
        {
            NotifyEvent(evt, null, null);
        }

        /// <summary>
        /// Launch the L2CharacterAI onEvt method corresponding to the Event
        /// The current general intention won't be change (ex : If the character attack and is stunned, he will attack again after the stunned periode)
        /// </summary>
        /// <param name="evt">The event whose the AI must be notified</param>
        /// <param name="arg0">The first parameter of the Event (optional target)</param>
        public void NotifyEvent(CtrlEvent evt, object arg0)
        {
            NotifyEvent(evt, arg0, null);
        }

        
        /// <summary>
        /// Launch the L2CharacterAI onEvt method corresponding to the Event
        /// The current general intention won't be change (ex : If the character attack and is stunned, he will attack again after the stunned periode)
        /// </summary>
        /// <param name="evt">The event whose the AI must be notified</param>
        /// <param name="arg0">The first parameter of the Event (optional target)</param>
        /// <param name="arg1">The second parameter of the Event (optional target)</param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public virtual void NotifyEvent(CtrlEvent evt, object arg0, object arg1)
        {
            switch (evt)
            {
                case CtrlEvent.EvtThink:
                    Task.Run(OnEvtThinkAsync);
                    break;
                case CtrlEvent.EvtAttacked:
                    Task.Run(() => OnEvtAttackedAsync((Character) arg0));
                    break;
                case CtrlEvent.EvtAggression:
                    break;
                case CtrlEvent.EvtArrivedRevalidate:
                    Task.Run(OnEvtArrivedRevalidate);
                    break;
                case CtrlEvent.EvtStunned:
                    break;
                case CtrlEvent.EvtSleeping:
                    break;
                case CtrlEvent.EvtRooted:
                    break;
                case CtrlEvent.EvtReadyToAct:
                    Task.Run(OnEvtReadyToAct);
                    break;
                case CtrlEvent.EvtUserCmd:
                    break;
                case CtrlEvent.EvtArrived:
                    Task.Run(OnEvtArrivedAsync);
                    break;
                case CtrlEvent.EvtArrivedBlocked:
                    Task.Run(() => OnEvtArrivedBlockedAsync((Location) arg0));
                    break;
                case CtrlEvent.EvtForgetObject:
                    break;
                case CtrlEvent.EvtCancel:
                    break;
                case CtrlEvent.EvtDead:
                    Task.Run(OnEvtDeadAsync);
                    break;
                case CtrlEvent.EvtFakeDeath:
                    break;
                case CtrlEvent.EvtConfused:
                    break;
                case CtrlEvent.EvtMuted:
                    break;
                case CtrlEvent.EvtAffraid:
                    break;
                case CtrlEvent.EvtFinishCasting:
                    break;
                case CtrlEvent.EvtBetrayed:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(evt), evt, null);
            }
        }

        /// <summary>
        /// Cancel action client side by sending Server->Client packet ActionFailed to the PlayerInstance actor
        /// Low level function, used by AI subclasses
        /// </summary>
        protected async Task ClientActionFailedAsync()
        {
            if (_actor is PlayerInstance playerInstance)
            {
                await playerInstance.SendActionFailedPacketAsync();
            }
        }

        public async Task MoveToAsync(int x, int y, int z)
        {
            if (_actor.Status.IsMovementDisabled())
            {
                await _actor.SendActionFailedPacketAsync();
            }
            else
            {
                // Set AI movement data
                _clientMoving = true;
                _clientMovingToPawnOffset = 0;

                // Calculate movement data for a move to location action and add the actor to movingObjects of GameTimeController
                _actor.Movement().MoveToLocation(x, y, z, 0);
                // Send a Server->Client packet CharMoveToLocation to the actor and all L2Player in its _knownPlayers
                
                if (_actor is L2Monster l2Monster)
                {
                    //l2Monster.NotifyMoving();
                }
                
                await _actor.SendBroadcastPacketAsync(new CharMoveToLocation(_actor));
            }
        }

        protected void MoveToInABoat(Location destination, Location origin)
        {
            throw new NotImplementedException();
        }

        
        /// <summary>
        /// Stop the actor movement server side AND client side by sending Server->Client packet StopMove/StopRotation <i>(broadcast)</i>.
        /// Low level function, used by AI subclasses
        /// </summary>
        /// <param name="pos"></param>
        protected async Task ClientStopMovingAsync(Location pos)
        {
            // Stop movement of the Creature
            if (_actor.IsMoving)
            {
                await _actor.Movement().StopMoveAsync(pos);
            }
            
            _clientMovingToPawnOffset = 0;

            if (!_clientMoving && (pos == null)) return;
            _clientMoving = false;
            // Send a Server->Client packet StopMove to the actor and all PlayerInstance in its _knownPlayers
            await _actor.SendBroadcastPacketAsync(new StopMove(_actor));

            if (pos == null) return;
            // Send a Server->Client packet StopRotation to the actor and all PlayerInstance in its _knownPlayers
            StopRotation stopRotation = new StopRotation(_actor, pos.GetHeading(), 0);
            await _actor.SendPacketAsync(stopRotation);
            await _actor.SendBroadcastPacketAsync(stopRotation);
        }

        /// <summary>
        /// Client has already arrived to target, no need to force StopMove packet
        /// </summary>
        protected async Task ClientStoppedMovingAsync()
        {
            if (_clientMovingToPawnOffset > 0) // movetoPawn needs to be stopped
            {
                _clientMovingToPawnOffset = 0;
                await _actor.SendBroadcastPacketAsync(new StopMove(_actor));
            }
            _clientMoving = false;
        }

        /// <summary>
        /// Kill the actor client side by sending Server->Client packet AutoAttackStop, StopMove/StopRotation, Die (broadcast)
        /// </summary>
        protected virtual async Task ClientNotifyDeadAsync()
        {
            // Send a Server->Client packet Die to the actor and all PlayerInstance in its _knownPlayers
            await _actor.SendBroadcastPacketAsync(new Die(_actor));
            
            SetIntention(CtrlIntention.AiIntentionIdle);
            Target = null;
            AttackTarget = null;
            CastTarget = null;
            
            // Cancel the follow task if necessary
            StopFollow();
        }

        
        protected abstract Task OnIntentionIdleAsync();
        protected abstract Task OnIntentionActiveAsync();
        protected abstract void OnIntentionRest();
        protected abstract Task OnIntentionAttackAsync(Character target);
        protected abstract void OnIntentionCast(Skill skill, WorldObject target);
        protected abstract Task OnIntentionMoveToAsync(Location destination);
        protected abstract void OnIntentionMoveToInABoat(Location destination, Location origin);
        protected abstract Task OnIntentionFollowAsync(Character target);
        protected abstract Task OnIntentionPickUpAsync(WorldObject item);
        protected abstract Task OnIntentionInteractAsync(WorldObject worldObject);
        public abstract Task OnEvtThinkAsync();
        protected abstract Task OnEvtAttackedAsync(Character attacker);
        protected abstract void OnEvtAggression(Character target, int aggro);
        protected abstract void OnEvtStunned(Character attacker);
        protected abstract void OnEvtSleeping(Character attacker);
        protected abstract void OnEvtRooted(Character attacker);
        protected abstract void OnEvtConfused(Character attacker);
        protected abstract void OnEvtMuted(Character attacker);
        protected abstract void OnEvtReadyToAct();
        protected abstract void OnEvtUserCmd(object arg0, object arg1);
        protected abstract Task OnEvtArrivedAsync();
        protected abstract void OnEvtArrivedRevalidate();
        protected abstract Task OnEvtArrivedBlockedAsync(Location location);
        protected abstract void OnEvtForgetObject(Character worldObject);
        protected abstract void OnEvtCancel();
        protected abstract Task OnEvtDeadAsync();
        protected abstract void OnEvtFakeDeath();
        protected abstract void OnEvtFinishCasting();

        public async Task MoveToPawnAsync(WorldObject pawn, int offset)
        {
            // Check if actor can move
            if (!_actor.Status.IsMovementDisabled())
            {
                if (offset < 10)
                {
                    offset = 10;
                }
                bool sendPacket = true;
                
                if (_clientMoving && (Target == pawn))
                {
                    if (_clientMovingToPawnOffset == offset)
                    {
                        if (Initializer.TimeController().GetGameTicks() < _moveToPawnTimeout)
                        {
                            return;
                        }
                        sendPacket = false;
                    }
                }
                
                // Set AI movement data
                _clientMoving = true;
                _clientMovingToPawnOffset = offset;

                Target = pawn;
                
                _moveToPawnTimeout = Initializer.TimeController().GetGameTicks();
                _moveToPawnTimeout += /* 1000 */ 200 / Initializer.TimeController().MillisInTick;
                
                if ((pawn == null))
                {
                    return;
                }
                // Calculate movement data for a move to location action and add the actor to movingObjects of GameTimeController
                _actor.Movement().MoveToLocation(pawn.GetX(), pawn.GetY(), pawn.GetZ(), offset);
                if (!_actor.IsMoving)
                {
                    await _actor.SendActionFailedPacketAsync();
                    return;
                }
                
                // Send a Server->Client packet MoveToPawn/CharMoveToLocation to the actor and all PlayerInstance in its _knownPlayers
                if (pawn is Character)
                {
                    if (_actor.Movement().IsOnGeoDataPath())
                    {
                        await _actor.SendBroadcastPacketAsync(new CharMoveToLocation(_actor));
                        _clientMovingToPawnOffset = 0;
                    } 
                    else if (sendPacket)
                    {
                        await _actor.SendBroadcastPacketAsync(new MoveToPawn(_actor, (Character) pawn, offset));
                    }
                }
                else
                {
                    await _actor.SendBroadcastPacketAsync(new CharMoveToLocation(_actor));
                }
            }
            else
            {
                await _actor.SendActionFailedPacketAsync();
            }
        }
        
        /// <summary>
        /// Stop an AI Follow Task
        /// </summary>
        /// <returns></returns>
        public void StopFollow()
        {
            CharacterFollowTaskManager.Instance.Remove(_actor);
            FollowTarget = null;
        }
        
        public async Task StartFollowAsync(Character target, int range)
        {
            StopFollow();
            FollowTarget = target;
            if (range == -1)
            {
                await CharacterFollowTaskManager.Instance.AddNormalFollow(_actor, range);
            }
            else
            {
                await CharacterFollowTaskManager.Instance.AddAttackFollow(_actor, range);
            }
        }
        
        public async Task StartFollowAsync(Character target)
        {
            await StartFollowAsync(target, -1);
        }


        public async Task ClientStartAutoAttackAsync()
        {
            if (!_clientAutoAttacking)
            {
                // Send a Server->Client packet AutoAttackStart to the actor and all PlayerInstance in its _knownPlayers
                await _actor.SendBroadcastPacketAsync(new AutoAttackStart(_actor.ObjectId));
                SetAutoAttacking(true);
            }
        }
        
        public async Task ClientStopAutoAttackAsync()
        {
            if (_clientAutoAttacking)
            {
                await _actor.SendBroadcastPacketAsync(new AutoAttackStop(_actor.ObjectId));
                SetAutoAttacking(false);
            }
            AttackStanceTaskManager.Instance.AddAttackStanceTask(_actor);
        }
        
        public void SetAutoAttacking(bool isAutoAttacking)
        {
            _clientAutoAttacking = isAutoAttacking;
        }
    }
}