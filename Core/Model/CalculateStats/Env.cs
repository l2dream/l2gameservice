﻿using Core.Model.Actor;
using Core.Model.Items;
using Core.Model.Player;
using Core.Model.Skills;

namespace Core.Model.CalculateStats
{
    public class Env
    {
        public Character Player { get; set; }
        public Character Target { get; set; }
        
        public ItemInstance Item { get; set; }
        public Skill Skill { get; set; }
        public double Value { get; set; }
        public double BaseValue { get; set; }
        public bool skillMastery = false;

    }
}