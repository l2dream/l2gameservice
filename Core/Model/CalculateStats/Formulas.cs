﻿using System;
using Core.Model.Actor;
using Core.Model.CalculateStats.Functions;
using Core.Model.Items.Type;
using Core.Model.Npc;
using Core.Model.Player;
using Core.Model.Skills;
using Core.Model.Skills.Effects;
using Core.Model.Zones;
using Helpers;

namespace Core.Model.CalculateStats
{
    public static class Formulas
    {
        private static int HP_REGENERATE_PERIOD = 3000; // 3 secs
        
        public static Calculator[] GetStandardNpcCalculators()
        {
            Calculator[] std = new Calculator[Enum.GetNames(typeof(CharacterStatId)).Length];
            std[(int)CharacterStatId.MaxHp] = new Calculator();
            std[(int)CharacterStatId.MaxHp].AddFunc(new FuncMaxHpMul(CharacterStatId.MaxHp, 0x20, null));
            
            std[(int)CharacterStatId.MaxMp] = new Calculator();
            std[(int)CharacterStatId.MaxMp].AddFunc(new FuncMaxMpMul(CharacterStatId.MaxMp, 0x20, null));
            
            std[(int)CharacterStatId.PowerAttack] = new Calculator();
            std[(int)CharacterStatId.PowerAttack].AddFunc(new FuncPAtkMod(CharacterStatId.PowerAttack, 0x20, null));
            
            std[(int)CharacterStatId.MagicAttack] = new Calculator();
            std[(int)CharacterStatId.MagicAttack].AddFunc(new FuncMAtkMod(CharacterStatId.MagicAttack, 0x20, null));
            
            std[(int)CharacterStatId.PowerDefence] = new Calculator();
            std[(int)CharacterStatId.PowerDefence].AddFunc(new FuncPDefMod(CharacterStatId.PowerDefence, 0x20, null));
            
            std[(int)CharacterStatId.MagicDefence] = new Calculator();
            std[(int)CharacterStatId.MagicDefence].AddFunc(new FuncMDefMod(CharacterStatId.MagicDefence, 0x20, null));
            
            std[(int)CharacterStatId.CriticalRate] = new Calculator();
            std[(int)CharacterStatId.CriticalRate].AddFunc(new FuncAtkCritical(CharacterStatId.CriticalRate, 0x09, null));
            
            std[(int)CharacterStatId.MCriticalRate] = new Calculator();
            std[(int)CharacterStatId.MCriticalRate].AddFunc(new FuncMAtkCritical(CharacterStatId.MCriticalRate, 0x30, null));
            
            std[(int)CharacterStatId.AccuracyCombat] = new Calculator();
            std[(int)CharacterStatId.AccuracyCombat].AddFunc(new FuncAtkAccuracy(CharacterStatId.AccuracyCombat, 0x10, null));
            
            std[(int)CharacterStatId.EvasionRate] = new Calculator();
            std[(int)CharacterStatId.EvasionRate].AddFunc(new FuncAtkEvasion(CharacterStatId.EvasionRate, 0x10, null));
            
            std[(int)CharacterStatId.PowerAttackSpeed] = new Calculator();
            std[(int)CharacterStatId.PowerAttackSpeed].AddFunc(new FuncPAtkSpeed(CharacterStatId.PowerAttackSpeed, 0x20, null));
            
            std[(int)CharacterStatId.MagicAttackSpeed] = new Calculator();
            std[(int)CharacterStatId.MagicAttackSpeed].AddFunc(new FuncMAtkSpeed(CharacterStatId.MagicAttackSpeed, 0x20, null));
            
            std[(int)CharacterStatId.RunSpeed] = new Calculator();
            std[(int)CharacterStatId.RunSpeed].AddFunc(new FuncMoveSpeed(CharacterStatId.RunSpeed, 0x30, null));
            
            return std;
        }

        public static void AddFuncsToNewPlayer(PlayerInstance playerInstance)
        {
            playerInstance.AddStatFunc(new FuncMaxHpAdd(CharacterStatId.MaxHp, 0x10, null));
            playerInstance.AddStatFunc(new FuncMaxHpMul(CharacterStatId.MaxHp, 0x20, null));
            playerInstance.AddStatFunc(new FuncMaxCpAdd(CharacterStatId.MaxCp, 0x10, null));
            playerInstance.AddStatFunc(new FuncMaxCpMul(CharacterStatId.MaxCp, 0x20, null));
            playerInstance.AddStatFunc(new FuncMaxMpAdd(CharacterStatId.MaxMp, 0x10, null));
            playerInstance.AddStatFunc(new FuncMaxMpMul(CharacterStatId.MaxMp, 0x20, null));
            playerInstance.AddStatFunc(new FuncBowAtkRange(CharacterStatId.PowerAttackRange, 0x10, null));
            playerInstance.AddStatFunc(new FuncPAtkMod(CharacterStatId.PowerAttack, 0x30, null));
            playerInstance.AddStatFunc(new FuncMAtkMod(CharacterStatId.MagicAttack, 0x20, null));
            playerInstance.AddStatFunc(new FuncPDefMod(CharacterStatId.PowerDefence, 0x20, null));
            playerInstance.AddStatFunc(new FuncMDefMod(CharacterStatId.MagicDefence, 0x20, null));
            playerInstance.AddStatFunc(new FuncAtkCritical(CharacterStatId.CriticalRate, 0x09, null));
            playerInstance.AddStatFunc(new FuncMAtkCritical(CharacterStatId.MCriticalRate, 0x30, null));
            playerInstance.AddStatFunc(new FuncAtkAccuracy(CharacterStatId.AccuracyCombat, 0x10, null));
            playerInstance.AddStatFunc(new FuncAtkEvasion(CharacterStatId.EvasionRate, 0x10, null));
            playerInstance.AddStatFunc(new FuncPAtkSpeed(CharacterStatId.PowerAttackSpeed, 0x20, null));
            playerInstance.AddStatFunc(new FuncMAtkSpeed(CharacterStatId.MagicAttackSpeed, 0x20, null));
            playerInstance.AddStatFunc(new FuncMoveSpeed(CharacterStatId.RunSpeed, 0x30, null));
        }
        
        public static int CalcPAtkSpd(Character attacker, Character target, double rate)
        {
            if (rate < 2)
            {
                return 2700;
            }
            return (int) (470000 / rate);
        }
        
        public static bool CalcHitMiss(Character attacker, Character target)
        {
            int chance = (80 + (2 * (attacker.Stat.GetAccuracy() - target.Stat.GetEvasionRate(attacker)))) * 10;
            // Get additional bonus from the conditions when you are attacking
            
            //chance *= hitConditionBonus.getConditionBonus(attacker, target); TODO
            chance = Math.Max(chance, 200);
            chance = Math.Min(chance, 980);
            return chance < Rnd.Next(1000);
        }
        
        public static bool CalcCrit(double rate)
        {
            return rate > Rnd.Next(1000);
        }
        
        public static bool CalcMagicCrit(double mRate)
        {
            return mRate > Rnd.Next(1000);
        }
        
        public static double CalcCpRegen(Character character)
        {
            double init = character.Template.Stat.BaseHpReg;
            double cpRegenMultiplier = 1;
            double cpRegenBonus = 0;
            if (character is PlayerInstance playerInstance)
            {
                // Calculate correct baseHpReg value for certain level of PC
                init += playerInstance.Stat.Level > 10 ? (playerInstance.Stat.Level - 1) / 10.0 : 0.5;
			
                // Calculate Movement bonus
                if (playerInstance.PlayerAction().IsSitting)
                {
                    cpRegenMultiplier *= 1.5; // Sitting
                }
                else if (!playerInstance.IsMoving)
                {
                    cpRegenMultiplier *= 1.1; // Staying
                }
                else if (playerInstance.Movement().IsRunning())
                {
                    cpRegenMultiplier *= 0.7; // Running
                }
            }
            else // Calculate Movement bonus
            if (!character.IsMoving)
            {
                cpRegenMultiplier *= 1.1; // Staying
            }
            else if (character.Movement().IsRunning())
            {
                cpRegenMultiplier *= 0.7; // Running
            }
		
            // Apply CON bonus
            init *= character.LevelMod * 3; //character.Stat.GetCon().CalcBonus(); TODO
            if (init < 1)
            {
                init = 1;
            }
            return ((character.Stat.GetCalc().CalcStat(CharacterStatId.RegenerateCpRate, (int) init, null, null) * cpRegenMultiplier) + cpRegenBonus);
        }
        
        public static int GetRegeneratePeriod(Character character)
        {
            return HP_REGENERATE_PERIOD; // 3s
        }

        public static int CalcHpRegen(Character character)
        {
            double init = character.Template.Stat.BaseHpReg;
            double hpRegenMultiplier = 1;
            double hpRegenBonus = 0;
            
            if (character is PlayerInstance playerInstance)
            {
                // Calculate correct baseHpReg value for certain level of PC
                init += (playerInstance.Stat.Level > 10) ? ((playerInstance.Stat.Level - 1) / 10.0) : 0.5;
                
                // Mother Tree effect is calculated at last
                if (playerInstance.Zone().IsInsideZone(ZoneId.MotherTree))
                {
                    hpRegenBonus += 3;
                }
                
                // Calculate Movement bonus
                if (playerInstance.PlayerAction().IsSitting)
                {
                    hpRegenMultiplier *= 1.5; // Sitting
                }
                else if (!playerInstance.IsMoving)
                {
                    hpRegenMultiplier *= 1.1; // Staying
                }
                else if (playerInstance.Movement().IsRunning())
                {
                    hpRegenMultiplier *= 0.7; // Running
                }
                
                // Add CON bonus
                init *= playerInstance.LevelMod * 3; //character.Stat.GetCon().CalcBonus; TODO
            }
            
            if (init < 1)
            {
                init = 1;
            }
            
            return (int) ((character.Stat.GetCalc().CalcStat(CharacterStatId.RegenerateHpRate, (int) init, null, null) * hpRegenMultiplier) + hpRegenBonus);
        }

        public static int CalcMpRegen(Character character)
        {
            double init = character.Template.Stat.BaseMpReg;
            double mpRegenMultiplier = 1;
            double mpRegenBonus = 0;
            if (character is PlayerInstance playerInstance)
            {
                // Calculate correct baseMpReg value for certain level of PC
                init += 0.3 * ((playerInstance.Stat.Level - 1) / 10.0);
                // Mother Tree effect is calculated at last
                if (playerInstance.Zone().IsInsideZone(ZoneId.MotherTree))
                {
                    mpRegenBonus += 1;
                }
                
                // Calculate Movement bonus
                if (playerInstance.PlayerAction().IsSitting)
                {
                    mpRegenMultiplier *= 1.5; // Sitting
                }
                else if (!playerInstance.IsMoving)
                {
                    mpRegenMultiplier *= 1.1; // Staying
                }
                else if (playerInstance.Movement().IsRunning())
                {
                    mpRegenMultiplier *= 0.7; // Running
                }
            }
            
            if (init < 1)
            {
                init = 1;
            }
            return (int) ((character.Stat.GetCalc().CalcStat(CharacterStatId.RegenerateMpRate, (int) init, null, null) * mpRegenMultiplier) + mpRegenBonus);
        }
        
        public static double CalcPhysDam(Character attacker, Character target, Skill skill, bool shld,
            bool crit, bool dual, bool ss)
        {
            double damage = attacker.Stat.GetPAtk(target);
            double defence = target.Stat.GetPDef(attacker);
            if (ss)
            {
                damage *= 2;
            }
            if (skill != null)
            {
                double skillpower = skill.GetPower(attacker);
                double ssboost = skill.SSBoost;
                if (ssboost <= 0)
                {
                    damage += skillpower;
                }
                else if (ssboost > 0)
                {
                    if (ss)
                    {
                        skillpower *= ssboost;
                        damage += skillpower;
                    }
                    else
                    {
                        damage += skillpower;
                    }
                }
            }
            
            // defence modifier depending of the attacker weapon
            Weapon weapon = attacker.GetActiveWeaponItem();
            Stat stat = null;

            if (weapon != null)
            {
                switch (weapon.WeaponType.Id)
                {
                    case WeaponTypeId.Sword:
                        stat = Stat.SwordWpnVuln;
                        break;
                    case WeaponTypeId.Blunt:
                        stat = Stat.BluntWpnVuln;
                        break;
                    case WeaponTypeId.Dagger:
                        stat = Stat.DaggerWpnVuln;
                        break;
                    case WeaponTypeId.Bow:
                        stat = Stat.BowWpnVuln;
                        break;
                    case WeaponTypeId.Pole:
                        stat = Stat.PoleWpnVuln;
                        break;
                    case WeaponTypeId.Etc:
                        stat = Stat.EtcWpnVuln;
                        break;
                    case WeaponTypeId.Fist:
                        stat = Stat.FistWpnVuln;
                        break;
                    case WeaponTypeId.Dual:
                        stat = Stat.DualWpnVuln;
                        break;
                    case WeaponTypeId.DualFist:
                        stat = Stat.DualfistWpnVuln;
                        break;
                    case WeaponTypeId.BigSword:
                        stat = Stat.BigswordWpnVuln;
                        break;
                    case WeaponTypeId.BigBlunt:
                        stat = Stat.BigbluntWpnVuln;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            
            
            if (crit)
            {
                // Finally retail like formula
                double cAtkMultiplied = damage + attacker.Stat.GetCalc().CalcStat(CharacterStatId.CriticalDamage, (int)damage, target, skill);
                double cAtkVuln = target.Stat.GetCalc().CalcStat(CharacterStatId.CritVuln, 1, target, null);
                double improvedDamageByCriticalMulAndVuln = cAtkMultiplied * cAtkVuln;
                double improvedDamageByCriticalMulAndAdd = improvedDamageByCriticalMulAndVuln + attacker.Stat.GetCalc().CalcStat(CharacterStatId.CriticalDamageAdd, 0, target, skill);
                damage = improvedDamageByCriticalMulAndAdd;
            }
            
            //TODO Config ALT_GAME_SHIELD_BLOCKS
            if (shld) 
            {
                defence += target.Stat.GetShieldDef();
            }

            damage = (70 * damage) / defence;
            if (stat != null)
            {
                // get the vulnerability due to skills (buffs, passives, toggles, etc)
                damage = target.Stat.GetCalc().CalcStat(stat.CharacterStatId, (int) damage, target, null);
                if (target is NpcInstance npcInstance)
                {
                    // get the natural vulnerability for the template
                    //damage *= npcInstance.Template.GetVulnerability(stat);
                }
            }
            
            damage += (Rnd.NextDouble() * damage) / 10;
            /*
             TODO Config
            if (shld && Config.ALT_GAME_SHIELD_BLOCKS)
            {
                damage -= target.getShldDef();
                if (damage < 0)
                {
                    damage = 0;
                }
            }
            */
            return damage;
        }

        public static int CalcMAtkSpd(Character attacker, Skill skill, double skillTime)
        {
            if (skill.IsMagic)
            {
                return (int) ((skillTime * 333) / attacker.Stat.GetMAtkSpd());
            }
            return (int) ((skillTime * 333) / attacker.Stat.GetPAtkSpd());
        }
        
        public static bool CalcSkillMastery(Character actor)
        {
            if (actor == null)
            {
                return false;
            }
		
            double val = actor.Stat.GetCalc().CalcStat(CharacterStatId.SkillMastery, 0, null, null);
            var baseStat = Initializer.BonusStatsService();
            if (actor is PlayerInstance playerInstance)
            {
                if (playerInstance.IsMageClass)
                {
                    val *= baseStat.Intelligence.CalcBonus(actor);
                }
                else
                {
                    val *= baseStat.Strength.CalcBonus(actor);
                }
            }
		
            return Rnd.Next(100) < val;
        }

        public static bool CalcEffectSuccess(Character attacker, Character target, EffectTemplate effect, Skill skill,
            bool ss, bool sps, bool bss)
        {
            if (attacker == null)
            {
                return false;
            }
		
            if ((target.Stat.GetCalc().CalcStat(CharacterStatId.DebuffImmunity, 0, null, skill) > 0) && skill.IsDeBuff())
            {
                return false;
            }
            SkillType type = effect.EffectType;
            int value = (int) effect.EffectPower;
            if (type == null)
            {
                return Rnd.Next(100) < value;
            }
            else if (type.Equals(SkillType.Cancel))
            {
                return true;
            }
            double statModifier = CalcSkillStatModifier(skill, target);
            
            // Calculate BaseRate.
            double rate = (int) (value * statModifier);
            
            // Add Matk/Mdef Bonus
            double mAtkModifier = 0;
            int ssModifier = 0;
            if (skill.IsMagic)
            {
                mAtkModifier = target.Stat.GetMDef(target, skill);
			
                // Add Bonus for Sps/SS
                if (bss)
                {
                    ssModifier = 4;
                }
                else if (sps)
                {
                    ssModifier = 2;
                }
                else
                {
                    ssModifier = 1;
                }
			
                mAtkModifier = (14 * Math.Sqrt(ssModifier * attacker.Stat.GetMAtk(target, skill))) / mAtkModifier;
                rate = (int) (rate * mAtkModifier);
            }
            // Resists
            double vulnModifier = CalcSkillTypeVulnerability(1, target, type);
            double res = vulnModifier;
            double resMod = 1;
            if (res != 0)
            {
                if (res < 0)
                {
                    resMod = 1 - (0.075 * res);
                    resMod = 1 / resMod;
                }
                else
                {
                    double x_factor = 1.3;
                    resMod = res * x_factor;
                    if (resMod > 1)
                    {
                        resMod = res;
                    }
                }
			
                if (resMod > 0.9)
                {
                    resMod = 0.9;
                }
                else if (resMod < 0.5)
                {
                    resMod = 0.5;
                }
                rate *= resMod;
            }
            
            // lvl modifier.
            int deltaMod = CalcLvlDependModifier(attacker, target, skill);
            rate += deltaMod;
            if (rate > skill.MaxChance)
            {
                rate = skill.MaxChance;
            }
            else if (rate < skill.MinChance)
            {
                rate = skill.MinChance;
            }
            // physics configuration addons
            //float physicsMult = GetChanceMultiplier(skill); TODO
            int physicsMult = 1;
            rate *= physicsMult;
            return (Rnd.Next(100) < rate);
        }
        
        /// <summary>
        /// TODO
        /// </summary>
        /// <param name="skill"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static double CalcSkillStatModifier(Skill skill, Character target)
        {
            return 1;
        }

        public static double CalcSkillTypeVulnerability(double multiplier, Character target, SkillType type)
        {
            if (type != null)
            {
                if (type == SkillType.Bleed)
                {
                    multiplier = target.Stat.GetCalc().CalcStat(CharacterStatId.BleedVuln, (int)multiplier, target, null);
                } 
                else if (type == SkillType.Poison)
                {
                    multiplier = target.Stat.GetCalc().CalcStat(CharacterStatId.PoisonVuln, (int)multiplier, target, null);
                }
                else if (type == SkillType.Stun)
                {
                    multiplier = target.Stat.GetCalc().CalcStat(CharacterStatId.StunVuln, (int)multiplier, target, null);
                }
                else if (type == SkillType.Paralyze)
                {
                    multiplier = target.Stat.GetCalc().CalcStat(CharacterStatId.ParalyzeVuln, (int)multiplier, target, null);
                }
                else if (type == SkillType.Root)
                {
                    multiplier = target.Stat.GetCalc().CalcStat(CharacterStatId.RootVuln, (int)multiplier, target, null);
                }
                else if (type == SkillType.Sleep)
                {
                    multiplier = target.Stat.GetCalc().CalcStat(CharacterStatId.SleepVuln, (int)multiplier, target, null);
                }
                else if (type == SkillType.Root || type == SkillType.Mute || type == SkillType.Fear ||
                         type == SkillType.Betray || type == SkillType.AggDeBuff || type == SkillType.Erase)
                {
                    multiplier = target.Stat.GetCalc().CalcStat(CharacterStatId.DerangementVuln, (int)multiplier, target, null);
                }
                else if (type == SkillType.Confusion || type == SkillType.ConfuseMobOnly)
                {
                    multiplier = target.Stat.GetCalc().CalcStat(CharacterStatId.ConfusionVuln, (int)multiplier, target, null);
                }
                else if (type == SkillType.DeBuff)
                {
                    multiplier = target.Stat.GetCalc().CalcStat(CharacterStatId.DebuffVuln, (int)multiplier, target, null);
                }
                else if (type == SkillType.Buff)
                {
                    multiplier = target.Stat.GetCalc().CalcStat(CharacterStatId.BuffVuln, (int)multiplier, target, null);
                }
                else if (type == SkillType.Cancel)
                {
                    multiplier = target.Stat.GetCalc().CalcStat(CharacterStatId.CancelVuln, (int)multiplier, target, null);
                }
            }
            return multiplier;
        }

        public static int CalcLvlDependModifier(Character attacker, Character target, Skill skill)
        {
            if (attacker == null)
            {
                return 0;
            }
            if (skill.LevelDepend == 0)
            {
                return 0;
            }
            int attackerMod;
            if (skill.MagicLevel > 0)
            {
                attackerMod = skill.MagicLevel;
            }
            else
            {
                attackerMod = attacker.Stat.Level;
            }
            int delta = attackerMod - target.Stat.Level;
            int deltaMod = delta / 5;
            deltaMod *= 5;
            if (deltaMod != delta)
            {
                if (delta < 0)
                {
                    deltaMod -= 5;
                }
                else
                {
                    deltaMod += 5;
                }
            }
            return deltaMod;
        }
        
        public static double CalcMagicDam(Character attacker, Character target, Skill skill, bool ss,
            bool bss, bool mCrit)
        {
            double damage = 1;
            // Add Matk/Mdef Bonus
            int ssModifier = 1;
            if (bss)
            {
                ssModifier = 4;
            }
            else if (ss)
            {
                ssModifier = 2;
            }
            
            if (attacker is PlayerInstance playerInstance)
            {
                double mAtk = playerInstance.Stat.GetMAtk(target, skill);
                double mDef = target.Stat.GetMDef(attacker, skill);
                // apply ss bonus
                mAtk *= ssModifier;
                
                damage = ((91 * Math.Sqrt(mAtk)) / mDef) * skill.GetPower(attacker) * CalcSkillVulnerability(target, skill);
                if (mCrit)
                {
                    damage *= 3;
                }
            }
            return damage;
        }

        //TODO
        public static double CalcSkillVulnerability(Character target, Skill skill)
        {
            double multiplier = 1; // initialize...
            return multiplier;
        }

        /// <summary>
        /// true if shield defence successfull
        /// </summary>
        /// <param name="character"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static bool CalcShieldUse(Character character, Character target)
        {
            Weapon weapon = character.GetActiveWeaponItem();
            var baseStat = Initializer.BonusStatsService();
            double shieldRate = target.Stat.GetCalc().CalcStat(CharacterStatId.ShieldRate, 0, character, null) * baseStat.Dexterity.CalcBonus(target);
            if (shieldRate == 0.0)
            {
                return false;
            }
            // if attacker use bow and target wear shield, shield block rate is multiplied by 1.3 (30%)
            if ((weapon != null) && (weapon.WeaponType.Id == WeaponTypeId.Bow))
            {
                shieldRate *= 1.3;
            }
            return shieldRate > Rnd.Next(100);
        }
    }
}