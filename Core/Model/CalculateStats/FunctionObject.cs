﻿using Core.Model.CalculateStats.Conditions;
using Microsoft.Extensions.DependencyInjection;
namespace Core.Model.CalculateStats
{
    /// <summary>
    /// A Func object is a component of a Calculator created to manage and dynamically calculate the effect of a character property (ex : MAX_HP, REGENERATE_HP_RATE...). In fact, each calculator is a table of Func object in which each Func represents a mathematics function
    /// When the calc method of a calculator is launched, each mathematics function is called according to its priority <b>_order</b>. Indeed, Func with lowest priority order is executed first and Funcs with the same order are executed in unspecified order.
    /// The result of the calculation is stored in the value property of an Env class instance.
    /// </summary>
    public abstract class FunctionObject
    {
        public readonly CharacterStatId StatId;

        public int Order { get; set; } = 0;
        
        public object FunctionOwner { get; set; }
        protected BonusStatsInit BaseStat { get; }
        public Condition Condition { get; set; }

        protected FunctionObject(CharacterStatId statId, int order, object owner)
        {
            StatId = statId;
            Order = order;
            FunctionOwner = owner;
            BaseStat = Initializer.ServiceProvider.GetService<BonusStatsInit>();
        }
        
        /// <summary>
        /// Run the mathematics function of the Func
        /// </summary>
        /// <param name="env"></param>
        public abstract void Calc(Env env);
    }
}