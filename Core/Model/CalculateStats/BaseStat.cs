﻿using Core.Model.CalculateStats.BaseStats;

namespace Core.Model.CalculateStats
{
    public class BaseStat
    {
        protected float[] WitBonus { get; set; } = new float[MaxStatValue];
        protected float[] StrBonus { get; set; } = new float[MaxStatValue];
        protected float[] IntBonus { get; set; } = new float[MaxStatValue];
        protected float[] MenBonus { get; set; } = new float[MaxStatValue];
        protected float[] DexBonus { get; set; } = new float[MaxStatValue];
        protected float[] ConBonus { get; set; } = new float[MaxStatValue];
        
        protected const int MaxStatValue = 100;
        public Strength Strength => new Strength(StrBonus, MaxStatValue);
        public Dexterity Dexterity => new Dexterity(DexBonus, MaxStatValue);
        public Mental Mental => new Mental(MenBonus, MaxStatValue);
        public Intelligence Intelligence => new Intelligence(IntBonus, MaxStatValue);
        public Wit Wit => new Wit(WitBonus, MaxStatValue);
        public Constitution Constitution => new Constitution(ConBonus, MaxStatValue);

        

        
    }

    
}