﻿using System.Collections.Generic;
using System.Linq;

namespace Core.Model.CalculateStats
{
    public class Calculator
    {
        private readonly FunctionObject[] _emptyFunctions;
        private FunctionObject[] _functions;

        public Calculator()
        {
            _emptyFunctions = new FunctionObject[10];
            _functions = _emptyFunctions;
        }

        public Calculator(Calculator c)
        {
            _functions = c._functions;
        }
        
        /// <summary>
        /// Check if 2 calculators are equals
        /// </summary>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <returns></returns>
        public static bool EqualsCals(Calculator c1, Calculator c2)
        {
            if (c1 == c2)
            {
                return true;
            }
		
            if ((c1 == null) || (c2 == null))
            {
                return false;
            }
		
            FunctionObject[] funcs1 = c1._functions;
            FunctionObject[] funcs2 = c2._functions;
            if (funcs1 == funcs2)
            {
                return true;
            }
		
            if (funcs1.Length != funcs2.Length)
            {
                return false;
            }
		
            if (funcs1.Length == 0)
            {
                return true;
            }
            return !funcs1.Where((t, i) => t != funcs2[i]).Any();
        }
        
        /// <returns>the number of Funcs in the Calculator</returns>
        public int Size()
        {
            return _functions.Length;
        }
        
        /// <summary>
        /// Add a Func to the Calculator
        /// </summary>
        /// <param name="f"></param>
        public void AddFunc(FunctionObject f)
        {
            lock (this)
            {
                FunctionObject[] funcs = _functions;
                FunctionObject[] tmp = new FunctionObject[funcs.Length + 1];
                int order = f.Order;
                int i;
                for (i = 0; (i < funcs.Length) && (order >= funcs[i]?.Order); i++)
                {
                    tmp[i] = funcs[i];
                }
		
                tmp[i] = f;
                for (; i < funcs.Length; i++)
                {
                    tmp[i + 1] = funcs[i];
                }
                _functions = tmp;
            }
        }
        
        /// <summary>
        /// Remove a Func from the Calculator
        /// </summary>
        /// <param name="f"></param>
        public void RemoveFunc(FunctionObject f)
        {
            if (f == null)
            {
                return;
            }
	
            List<FunctionObject> tmp = new List<FunctionObject>();
            tmp.AddRange(_functions);
	
            if (tmp.Contains(f))
            {
                tmp.Remove(f);
            }
            _functions = tmp.ToArray();
        }
        
        /// <summary>
        /// Remove each Func with the specified owner of the Calculator
        /// </summary>
        /// <param name="owner"></param>
        /// <returns></returns>
        public List<CharacterStatId> RemoveOwner(object owner)
        {
            List<FunctionObject> funcs = _functions.ToList();
            List<CharacterStatId> modifiedStats = new List<CharacterStatId>();
            foreach (FunctionObject func in funcs.Where(func => func != null && func.FunctionOwner == owner))
            {
                modifiedStats.Add(func.StatId);
                RemoveFunc(func);
            }
            return modifiedStats;
        }
        
        /// <summary>
        /// Run each Func of the Calculator
        /// </summary>
        /// <param name="env"></param>
        public void Calc(Env env)
        {
            List<FunctionObject> funcs = _functions.ToList();
            foreach (var func in funcs.Where(func => func != null))
            {
                func.Calc(env);
            }
        }
    }
}