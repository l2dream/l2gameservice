﻿using Core.Model.Skills;

namespace Core.Model.CalculateStats.Conditions
{
    internal sealed class ConditionSkillStats : Condition
    {
        private CharacterStatId _characterStatId;

        public ConditionSkillStats(CharacterStatId characterStatId)
        {
            _characterStatId = characterStatId;
        }
        protected override bool Impl(Env env)
        {
            if (env.Skill == null)
            {
                return false;
            }

            return false;
        }
    }
}