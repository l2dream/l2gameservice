﻿using System;
using System.Linq;

namespace Core.Model.CalculateStats.Conditions
{
    public class ConditionLogicAnd : Condition
    {
        private readonly Condition[] _emptyConditions;
        public Condition[] Conditions { get; set; }

        public ConditionLogicAnd()
        {
            _emptyConditions = new Condition[0];
            Conditions = _emptyConditions;
        }
        
        public void Add(Condition condition)
        {
            if (condition == null)
            {
                return;
            }
            if (GetListener() != null)
            {
                condition.SetListener(this);
            }
            int len = Conditions.Length;
            Condition[] tmp = new Condition[len + 1];
            Array.Copy(Conditions, 0, tmp, 0, len);
            tmp[len] = condition;
            Conditions = tmp;
        }
        
        protected override bool Impl(Env env)
        {
            return Conditions.All(c => c.Test(env));
        }
    }
}