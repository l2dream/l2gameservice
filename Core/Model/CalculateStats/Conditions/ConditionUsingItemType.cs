﻿using Core.Model.Items;
using Core.Model.Items.Type;
using Core.Model.Player;

namespace Core.Model.CalculateStats.Conditions
{
    public sealed class ConditionUsingItemType : Condition
    {
        private readonly bool _armor;
        private readonly int _mask;

        public ConditionUsingItemType(int mask)
        {
            _mask = mask;
            _armor = (_mask & (ArmorType.GetArmorTypeId("magic").Mask() | ArmorType.GetArmorTypeId("light").Mask() |
                               ArmorType.GetArmorTypeId("heavy").Mask())) != 0;
        }
        protected override bool Impl(Env env)
        {
            if (!(env.Player is PlayerInstance))
            {
                return false;
            }
            Inventory inv = ((PlayerInstance) env.Player).PlayerInventory();
            if (_armor)
            {
                // Get the itemMask of the weared chest (if exists)
                ItemInstance chest = inv.GetPaperdollItem(Inventory.PaperdollChest);
                if (chest == null)
                {
                    return false;
                }
                int chestMask = chest.Item.GetItemMask();
			
                // If chest armor is different from the condition one return false
                if ((_mask & chestMask) == 0)
                {
                    return false;
                }
			
                // So from here, chest armor matches conditions
			
                ItemSlotId chestBodyPart =  chest.Item.BodyPart;
                // return True if chest armor is a Full Armor
                if (chestBodyPart == ItemSlotId.SlotFullArmor)
                {
                    return true;
                }
			
                ItemInstance legs = inv.GetPaperdollItem(Inventory.PaperdollLegs);
                if (legs == null)
                {
                    return false;
                }
                int legMask = legs.Item.GetItemMask();
                // return true if legs armor matches too
                return (_mask & legMask) != 0;
            }
            return (_mask & inv.GetWearedMask()) != 0;
        }
    }
}