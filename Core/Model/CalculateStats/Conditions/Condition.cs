﻿namespace Core.Model.CalculateStats.Conditions
{
    public abstract class Condition : IConditionListener
    {
        private IConditionListener _listener;
        public string Message { get; set; }
        public bool Result { get; set; }
        
        protected internal virtual void SetListener(IConditionListener listener)
        {
            _listener = listener;
            NotifyChanged();
        }
        
        protected abstract bool Impl(Env env);
        
        protected IConditionListener GetListener()
        {
            return _listener;
        }
        
        public bool Test(Env env)
        {
            bool res = Impl(env);
            if ((_listener == null) || (res == Result)) return res;
            Result = res;
            NotifyChanged();
            return res;
        }
        
        public void NotifyChanged()
        {
            _listener?.NotifyChanged();
        }
    }
}