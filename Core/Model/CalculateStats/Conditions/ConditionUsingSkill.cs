﻿namespace Core.Model.CalculateStats.Conditions
{
    internal sealed class ConditionUsingSkill : Condition
    {
        private readonly int _skillId;

        public ConditionUsingSkill(int skillId)
        {
            _skillId = skillId;            
        }
        protected override bool Impl(Env env)
        {
            if (env.Skill == null)
            {
                return false;
            }
            return env.Skill.Id == _skillId;
        }
    }
}