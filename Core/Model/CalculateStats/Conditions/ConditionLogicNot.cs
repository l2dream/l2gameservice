﻿namespace Core.Model.CalculateStats.Conditions
{
    internal sealed class ConditionLogicNot : Condition
    {
        private readonly Condition _condition;
        public ConditionLogicNot(Condition condition)
        {
            _condition = condition;
            if (GetListener() != null)
            {
                _condition.SetListener(this);
            }
        }

        protected internal override void SetListener(IConditionListener listener)
        {
            if (listener != null)
            {
                _condition.SetListener(this);
            }
            else
            {
                _condition.SetListener(null);
            }
            base.SetListener(listener);
        }

        protected override bool Impl(Env env)
        {
            return !_condition.Test(env);
        }
    }
}