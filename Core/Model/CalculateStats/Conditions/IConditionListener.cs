﻿namespace Core.Model.CalculateStats.Conditions
{
    public interface IConditionListener
    {
        void NotifyChanged();
    }
}