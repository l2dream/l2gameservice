﻿using System;
using System.Threading.Tasks;
using L2Logger;

namespace Core.Model.CalculateStats
{
    internal sealed class EffectTask
    {
        private readonly int _delay;
        private readonly int _rate;
        private readonly Effect _effect;
        
        public EffectTask(int pDelay, int pRate, Effect effect)
        {
            _delay = pDelay;
            _rate = pRate;
            _effect = effect;
        }

        public int GetDelay()
        {
            return _delay;
        }
        
        public int GetRate()
        {
            return _rate;
        }

        public void Run()
        {
            try
            {
                if (_effect.PeriodFirstTime == 0)
                {
                    _effect.PeriodStartTicks = Initializer.TimeController().GetGameTicks();
                }
                else
                {
                    _effect.PeriodFirstTime = 0;
                }
                Task.Run(() => _effect.ScheduleEffect());
            }
            catch (Exception ex)
            {
                LoggerManager.Warn(ex.Message);
            }
        }
    }
}