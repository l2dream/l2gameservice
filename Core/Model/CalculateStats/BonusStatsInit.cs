﻿using System;
using System.Globalization;
using System.Xml;
using L2Logger;

namespace Core.Model.CalculateStats
{
    public sealed class BonusStatsInit : BaseStat
    {
        public BonusStatsInit()
        {
            Initialize();
        }

        private void Initialize()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(@"StaticData/Xml/Stats/StatBonus.xml");
                if (doc.DocumentElement == null)
                {
                    return;
                }
                
                XmlNodeList nodes = doc.DocumentElement.SelectNodes("/list");

                foreach (XmlNode node in nodes)
                {
                    XmlNodeList childNodes = node.ChildNodes;

                    foreach (XmlElement childNode in childNodes)
                    {
                        if (childNode.Name.Equals("STR"))
                        {
                            foreach (var statChildNode in childNode.ChildNodes)
                            {
                                if (!(statChildNode is XmlElement element)) continue;
                                var valueBonusStat = GetValueBonusStat(element);
                                StrBonus[valueBonusStat.Value] = valueBonusStat.Bonus;
                            }
                        } else if (childNode.Name.Equals("INT"))
                        {
                            foreach (var statChildNode in childNode.ChildNodes)
                            {
                                if (!(statChildNode is XmlElement element)) continue;
                                var valueBonusStat = GetValueBonusStat(element);
                                IntBonus[valueBonusStat.Value] = valueBonusStat.Bonus;
                            }
                        } else if (childNode.Name.Equals("CON"))
                        {
                            foreach (var statChildNode in childNode.ChildNodes)
                            {
                                if (!(statChildNode is XmlElement element)) continue;
                                var valueBonusStat = GetValueBonusStat(element);
                                ConBonus[valueBonusStat.Value] = valueBonusStat.Bonus;
                            }
                        } else if (childNode.Name.Equals("MEN"))
                        {
                            foreach (var statChildNode in childNode.ChildNodes)
                            {
                                if (!(statChildNode is XmlElement element)) continue;
                                var valueBonusStat = GetValueBonusStat(element);
                                MenBonus[valueBonusStat.Value] = valueBonusStat.Bonus;
                            }
                        } else if (childNode.Name.Equals("DEX"))
                        {
                            foreach (var statChildNode in childNode.ChildNodes)
                            {
                                if (!(statChildNode is XmlElement element)) continue;
                                var valueBonusStat = GetValueBonusStat(element);
                                DexBonus[valueBonusStat.Value] = valueBonusStat.Bonus;
                            }
                        } else if (childNode.Name.Equals("WIT"))
                        {
                            foreach (var statChildNode in childNode.ChildNodes)
                            {
                                if (!(statChildNode is XmlElement element)) continue;
                                var valueBonusStat = GetValueBonusStat(element);
                                WitBonus[valueBonusStat.Value] = valueBonusStat.Bonus;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Error(ex.Message);
            }
        }

        private ValueBonus GetValueBonusStat(XmlElement element)
        {
            XmlNamedNodeMap attrs = element.Attributes;
            int value = Convert.ToInt32(attrs.GetNamedItem("value").Value);
            float bonus = float.Parse(attrs.GetNamedItem("bonus").Value, CultureInfo.InvariantCulture.NumberFormat);

            return new ValueBonus
            {
                Value = value,
                Bonus = bonus
            };
        }
        
        private class ValueBonus
        {
            public int Value { get; set; }
            public float Bonus { get; set; }
        }
    }
}