﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Core.Model.Actor;
using Core.Model.CalculateStats.Functions;
using Core.Model.Player;
using Core.Model.Skills;
using Core.Model.Skills.Effects;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;
using Core.TaskManager;
using Helpers;
using L2Logger;

namespace Core.Model.CalculateStats
{
    public abstract class Effect
    {
        private static readonly FunctionObject[] _emptyFunctionSet = new FunctionObject[0];
        public Character Effector { get; set; }
        public Character Effected { get; set; }
        // the skill that was used.
        public Skill Skill { get; set; }
        // the value of an update
        private Lambda _lambda;
        // the current state
        public EffectStateEnum State { get; set; }
        // period, seconds
        public int Period { get; set; }
        public int PeriodStartTicks { get; set; }
        public long PeriodStartTime { get; set; }
        public int PeriodFirstTime { get; set; }
	
        // function templates
        private readonly FuncTemplate[] _funcTemplates;
	
        // initial count
        public int TotalCount { get; set; }
        // counter
        public int Count { get; set; }
	
        // abnormal effect mask
        private readonly int _abnormalEffect;
	
        public bool PreventExitUpdate { get; set; }
	
        private bool _cancelEffect = false;
        /** The Identifier of the stack group */
        public string StackType { get; set; }
        
        private Task _currentFuture;
        private EffectTask _currentTask;
        
        /** The position of the effect in the stack group */
        public float StackOrder { get; set; }
	
        private readonly EffectTemplate _template;
	
        public bool InUse { get; set; }
        private CancellationTokenSource _cts;
        public abstract Task<bool> OnActionTime();
        
        protected Effect(Env env, EffectTemplate template)
        {
	        _template = template;
	        State = EffectStateEnum.Created;
	        Skill = env.Skill;
	        // _item = env._item == null ? null : env._item.getItem();
	        Effected = env.Target;
	        Effector = env.Player;
	        _lambda = template.Lambda;
	        _funcTemplates = template.FuncTemplates;
	        Count = template.Counter;
	        TotalCount = Count;
	        int temp = template.Period;
	        if (env.skillMastery)
	        {
		        temp *= 2;
	        }
	        Period = temp;
	        _abnormalEffect = template.AbnormalEffect;
	        StackType = template.StackType;
	        StackOrder = template.StackOrder;
	        PeriodStartTicks = Initializer.TimeController().GameTicks;
	        PeriodStartTime = DateTimeHelper.CurrentUnixTimeMillis();
	        PeriodFirstTime = 0;
	        Task.Run(ScheduleEffect);
        }
        public abstract EffectTypeEnum GetEffectType();
        
        public virtual int GetLevel()
        {
	        return Skill.Level;
        }

        private int GetDelay()
        {
	        return (int) ((Period * 1000) - (DateTimeHelper.CurrentUnixTimeMillis() - PeriodStartTime));
        }
        
        public FunctionObject[] GetStatFuncs()
        {
	        if (_funcTemplates == null)
	        {
		        return _emptyFunctionSet;
	        }
	        List<FunctionObject> funcs = new List<FunctionObject>();
	        foreach (FuncTemplate t in _funcTemplates)
	        {
		        Env env = new Env();
		        env.Player = Effector;
		        env.Target = Effected;
		        env.Skill = Skill;
		        FunctionObject  f = t.GetFunc(env, this); // effect is owner
		        if (f != null)
		        {
			        funcs.Add(f);
		        }
	        }
	        if (funcs.Count == 0)
	        {
		        return _emptyFunctionSet;
	        }
	        return funcs.ToArray();
        }
        
        public bool IsSelfEffect()
        {
	        return Skill.EffectTemplatesSelf != null;
        }
        
        public bool IsHerbEffect()
        {
	        return Skill.Name.Contains("Herb");
        }
        
        public double Calc()
        {
	        Env env = new Env();
	        env.Player = Effector;
	        env.Target = Effected;
	        env.Skill = Skill;
	        return _lambda.Calc(env);
        }
        
        /// <summary>
        /// Notify started
        /// </summary>
        public virtual void OnStart()
        {
	        if (_abnormalEffect != 0)
	        {
		        Effected.StartAbnormalEffect(_abnormalEffect);
	        }
        }
        
        /// <summary>
        /// Cancel the effect in the the abnormal effect map of the effected Creature.
        /// </summary>
        public virtual void OnExit()
        {
	        if (_abnormalEffect != 0)
	        {
		        Effected.StopAbnormalEffect(_abnormalEffect);
	        }
        }

        public async Task ScheduleEffect()
        {
	        try
	        {
		        if (State == EffectStateEnum.Created)
		        {
			        State = EffectStateEnum.Acting;
			        OnStart();
			        if (Period > 0)
			        {
				        await StartEffectTaskAsync(Period * 1000);
				        return;
			        }
		        }

		        if (State == EffectStateEnum.Acting)
		        {
			        if (Count-- > 0)
			        {
				        if (InUse)
				        { // effect has to be in use
					        if (await OnActionTime())
					        {
						        return; // false causes effect to finish right away
					        }
				        }
				        else if (Count > 0)
				        {
					        return;
				        }
			        }
			        State = EffectStateEnum.Finishing;
		        }
	        
		        if (State == EffectStateEnum.Finishing)
		        {
			        // Cancel the effect in the the abnormal effect map of the Creature
			        OnExit();
			
			        // If the time left is equal to zero, send the message
			        if ((Effected is PlayerInstance) && _template.ShowIcon && !Effected.Status.IsDead())
			        {
				        // Like L2OFF message S1_HAS_BEEN_ABORTED for toggle skills
				        if (Skill.IsToggle())
				        {
					        SystemMessage smsg3 = new SystemMessage(SystemMessageId.S1HasBeenAborted);
					        smsg3.AddString(Skill.Name);
					        await Effected.SendPacketAsync(smsg3);
				        }
				        else if (_cancelEffect)
				        {
					        SystemMessage smsg3 = new SystemMessage(SystemMessageId.EffectS1Disappeared);
					        smsg3.AddString(Skill.Name);
					        await Effected.SendPacketAsync(smsg3);
				        }
				        else if (Count == 0)
				        {
					        SystemMessage smsg3 = new SystemMessage(SystemMessageId.S1HasWornOff);
					        smsg3.AddString(Skill.Name);
					        await Effected.SendPacketAsync(smsg3);
				        }
			        }
			
			        // Stop the task of the Effect, remove it and update client magic icon.
			        await StopEffectTaskAsync();
		        }
	        }
	        catch (Exception ex)
	        {
		        LoggerManager.Error(GetType().Name + " " + _template + ex.Message);
	        }
        }
        
        public async Task StopEffectTaskAsync()
        {
	        if (_currentFuture != null)
	        {
		        if (!_currentFuture.IsCanceled)
		        {
			        _cts.Cancel();
		        }
		        _currentFuture = null;
		        _currentTask = null;
		        
		        // To avoid possible NPE caused by player crash
		        if (Effected != null)
		        {
			        await Effected.Effects.RemoveEffectAsync(this);
		        }
		        else
		        {
			        LoggerManager.Warn("Effected is null for skill " + Skill.Id + " on effect " + GetEffectType());
		        }
	        }
        }

        public void Exit(bool cancelEffect)
        {
	        Exit(false, cancelEffect);
        }

        public void Exit(bool preventUpdate, bool cancelEffect)
        {
	        PreventExitUpdate = preventUpdate;
	        State = EffectStateEnum.Finishing;
	        _cancelEffect = cancelEffect;
	        Task.Run(ScheduleEffect);
        }

        public void AddIcon(MagicEffectIcons mi, Effect effect)
        {
	        EffectTask task = _currentTask;
	        Task future = _currentFuture;

	        if ((task == null) || (future == null))
	        {
		        return;
	        }
	        if ((State == EffectStateEnum.Finishing) || (State == EffectStateEnum.Created))
	        {
		        return;
	        }
	        if (!_template.ShowIcon)
	        {
		        return;
	        }
	        
	        if (task.GetRate() > 0)
	        {
		        if (Skill.IsPotion)
		        {
			        mi.AddEffect(Skill.Id, Skill.Level, Skill.BuffDuration - (GetTaskTime() * 1000), false);
		        }
		        else if (!Skill.IsToggle())
		        {
			        mi.AddEffect(Skill.Id, Skill.Level, (Count * Period) * 1000, Skill.IsDeBuff());
		        }
		        else
		        {
			        mi.AddEffect(Skill.Id, Skill.Level, -1, true);
		        }
	        }
	        else
	        {
		        int remaining = GetDelay() + 1000;

		        if (remaining >= 0)
		        {
			        mi.AddEffect(Skill.Id, Skill.Level, remaining , Skill.GetSkillType() == SkillType.DeBuff);
		        }
	        }
        }
        
        public int GetTaskTime()
        {
	        if (Count == TotalCount)
	        {
		        return 0;
	        }
	        return (Math.Abs((Count - TotalCount) + 1) * Period) + GetTime() + 1;
        }
        
        public int GetTime()
        {
	        return (Initializer.TimeController().GetGameTicks() - PeriodStartTicks) / Initializer.TimeController().TicksPerSecond;
        }
        
        private async Task StartEffectTaskAsync(int duration)
        {
	        await StopEffectTaskAsync();
	        _currentTask = new EffectTask(duration, -1, this);
	        _cts = new CancellationTokenSource();
	        _currentFuture = ThreadPoolManager.Instance.ScheduleAtFixed(_currentTask.Run, duration, _cts.Token);
	        if (State == EffectStateEnum.Acting)
	        {
		        // To avoid possible NPE caused by player crash
		        if (Effected != null)
		        {
			        Effected.Effects.AddEffect(this);
		        }
		        else
		        {
			        LoggerManager.Warn("Effected is null for skill " + Skill.Id + " on effect " + GetEffectType());
		        }
	        }
        }
    }
}