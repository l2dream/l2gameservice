﻿using Core.Model.Actor;

namespace Core.Model.CalculateStats.BaseStats
{
    /// <summary>
    /// Max. MP
    /// MP Recovery
    /// M. Resistance
    /// Magic Cancel for incoming damage
    /// Resistance to M. debuffs (Sleep, Hold, etc.)
    /// </summary>
    public class Mental : IBaseStat
    {
        private readonly float[] _menBonus;
        private readonly int _maxStatValue;
        public Mental(float[] menBonus, int maxStatValue)
        {
            _menBonus = menBonus;
            _maxStatValue = maxStatValue;
        }
        public double CalcBonus(Character actor)
        {
            if (actor.Stat.GetMen() > _maxStatValue)
            {
                return _menBonus[_maxStatValue];
            }
            return _menBonus[actor.Stat.GetMen()];
        }
    }
}