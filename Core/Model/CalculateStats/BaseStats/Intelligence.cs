﻿using Core.Model.Actor;

namespace Core.Model.CalculateStats.BaseStats
{
    /// <summary>
    /// M. Atk.
    /// M. Critical Damage
    /// </summary>
    public class Intelligence : BaseStat, IBaseStat
    {
        private readonly float[] _intBonus;
        private readonly int _maxStatValue;
        public Intelligence(float[] intBonus, int maxStatValue)
        {
            _intBonus = intBonus;
            _maxStatValue = maxStatValue;
        }
        public double CalcBonus(Character actor)
        {
            if (actor.Stat.GetInt() > _maxStatValue)
            {
                return _intBonus[_maxStatValue];
            }
            return _intBonus[actor.Stat.GetInt()];
        }
    }
}