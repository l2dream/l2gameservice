﻿using Core.Model.Actor;

namespace Core.Model.CalculateStats.BaseStats
{
    /// <summary>
    /// Max. HP
    /// HP Regeneration
    /// Weight Limit
    /// Lung capacity
    /// Shield Defense
    /// </summary>
    public class Constitution : BaseStat, IBaseStat
    {
        private readonly float[] _conBonus;
        private readonly int _maxStatValue;
        public Constitution(float[] conBonus, int maxStatValue)
        {
            _conBonus = conBonus;
            _maxStatValue = maxStatValue;
        }
        public double CalcBonus(Character actor)
        {
            if (actor.Stat.GetCon() > _maxStatValue)
            {
                return _conBonus[_maxStatValue];
            }
            return _conBonus[actor.Stat.GetCon()];
        }
    }
}