﻿using Core.Model.Actor;

namespace Core.Model.CalculateStats.BaseStats
{
    /// <summary>
    /// P. Accuracy
    /// P. Evasion
    /// Atk. Spd.
    /// P. Critical Damage
    /// </summary>
    public class Dexterity : IBaseStat
    {
        private readonly float[] _dexBonus;
        private readonly int _maxStatValue;
        public Dexterity(float[] dexBonus, int maxStatValue)
        {
            _dexBonus = dexBonus;
            _maxStatValue = maxStatValue;
        }
        public double CalcBonus(Character actor)
        {
            if (actor.Stat.GetDex() > _maxStatValue)
            {
                return _dexBonus[_maxStatValue];
            }
            return _dexBonus[actor.Stat.GetDex()];
        }
    }
}