﻿using Core.Model.Actor;

namespace Core.Model.CalculateStats.BaseStats
{
    public interface IBaseStat
    {
        double CalcBonus(Character actor);
    }
}