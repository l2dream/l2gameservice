﻿using Core.Model.Actor;

namespace Core.Model.CalculateStats.BaseStats
{
    public class Wit : IBaseStat
    {
        private readonly float[] _witBonus;
        private readonly int _maxStatValue;

        public Wit(float[] witBonus, int maxStatValue)
        {
            _witBonus = witBonus;
            _maxStatValue = maxStatValue;
        }
        public double CalcBonus(Character actor)
        {
            if (actor.Stat.GetWit() > _maxStatValue)
            {
                return _witBonus[_maxStatValue];
            }
            return _witBonus[actor.Stat.GetWit()];
        }
    }
}