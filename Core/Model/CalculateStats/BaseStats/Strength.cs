﻿using Core.Model.Actor;

namespace Core.Model.CalculateStats.BaseStats
{
    /// <summary>
    /// P. Atk
    /// P. Critical Damage
    /// </summary>
    public class Strength : IBaseStat
    {
        private readonly float[] _strBonus;
        private readonly int _maxStatValue;
        public Strength(float[] strBonus, int maxStatValue)
        {
            _strBonus = strBonus;
            _maxStatValue = maxStatValue;
        }
        public double CalcBonus(Character actor)
        {
			
            if (actor.Stat.GetStr() > _maxStatValue)
            {
                return _strBonus[_maxStatValue];
            }
            return _strBonus[actor.Stat.GetStr()];
        }
    }
}