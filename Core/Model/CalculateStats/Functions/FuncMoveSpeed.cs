﻿namespace Core.Model.CalculateStats.Functions
{
    public class FuncMoveSpeed : FunctionObject
    {
        public FuncMoveSpeed(CharacterStatId statId, int order, object owner) : base(statId, order, owner)
        {
        }

        public override void Calc(Env env)
        {
            env.Value *= BaseStat.Dexterity.CalcBonus(env.Player);
        }
    }
}