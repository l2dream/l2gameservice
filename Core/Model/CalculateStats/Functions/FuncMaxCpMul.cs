﻿namespace Core.Model.CalculateStats.Functions
{
    internal sealed class FuncMaxCpMul : FunctionObject
    {
        public FuncMaxCpMul(CharacterStatId statId, int order, object owner) : base(statId, order, owner)
        {
        }

        public override void Calc(Env env)
        {
            env.Value *= BaseStat.Constitution.CalcBonus(env.Player);
        }
    }
}