﻿namespace Core.Model.CalculateStats.Functions
{
    internal sealed class FuncSub : FunctionObject
    {
        private readonly Lambda _lambda;
        
        public FuncSub(CharacterStatId statId, int order, object owner, Lambda lambda) : base(statId, order, owner)
        {
            _lambda = lambda;
        }

        public override void Calc(Env env)
        {
            if ((Condition is null) || Condition.Test(env))
            {
                env.Value -= _lambda.Calc(env);
            }
        }
    }
}