﻿namespace Core.Model.CalculateStats.Functions
{
    public class FuncMaxHpMul : FunctionObject 
    {
        public FuncMaxHpMul(CharacterStatId statId, int order, object owner) : base(statId, order, owner)
        {
            
        }

        public override void Calc(Env env)
        {
            env.Value *= BaseStat.Constitution.CalcBonus(env.Player);
        }
    }
}