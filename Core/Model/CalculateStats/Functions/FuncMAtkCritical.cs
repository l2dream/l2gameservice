﻿using Core.Model.Actor;

namespace Core.Model.CalculateStats.Functions
{
    public class FuncMAtkCritical : FunctionObject
    {
        public FuncMAtkCritical(CharacterStatId statId, int order, object owner) : base(statId, order, owner)
        {
        }

        public override void Calc(Env env)
        {
            Character p = env.Player;
            env.Value *= BaseStat.Wit.CalcBonus(p);
        }
    }
}