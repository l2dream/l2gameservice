﻿using System;
using System.Reflection;
using Core.Model.CalculateStats.Conditions;
using Core.Model.Skills;
using L2Logger;

namespace Core.Model.CalculateStats.Functions
{
    public sealed class FuncTemplate
    {
        public Condition AttachCond { get; set; }
        public Condition ApplyCond { get; set; }
        public Type FuncInstance { get; set; }
        public CharacterStatId CharacterStatId { get; }
        public int order;
        public Lambda lambda;
        
        public ConstructorInfo Constructor { get; set; }

        public FuncTemplate(Condition pAttachCond, Condition pApplyCond, Type pFunc, CharacterStatId pStat, int pOrder,
            Lambda pLambda)
        {
            AttachCond = pAttachCond;
            ApplyCond = pApplyCond;
            CharacterStatId = pStat;
            order = pOrder;
            lambda = pLambda;
            FuncInstance = pFunc;
            FunctionInstance();
        }

        private void FunctionInstance()
        {
            try
            {
                //_funcInstance = (FunctionObject) Activator.CreateInstance(pFunc, parameters);
                Constructor = FuncInstance.GetConstructor(new[] {typeof(CharacterStatId), typeof(int), typeof(object), typeof(Lambda)});
            }
            catch (Exception ex)
            {
                LoggerManager.Error(GetType().Name + " " + ex.Message);
            }
        }

        public FunctionObject GetFunc(Env env, object owner)
        {
            if ((AttachCond != null) && !AttachCond.Test(env))
            {
                return null;
            }
            try
            {
                Object[] parameters = new object[]
                {
                    CharacterStatId,
                    order,
                    owner,
                    lambda
                };
                FunctionObject func = (FunctionObject)Constructor.Invoke(parameters);
                if (ApplyCond != null)
                {
                    func.Condition = ApplyCond;
                }
                return func;
                /*
                FunctionObject f = (FunctionObject) _funcInstance.newInstance(stat, order, owner, lambda);
                if (_applyCond != null)
                {
                    f.Condition = _applyCond;
                }
                return f;
                */
            }
            catch (Exception ex)
            {
                LoggerManager.Error(GetType().Name + " " + ex.Message);
            }
            return null;
        }
    }
}