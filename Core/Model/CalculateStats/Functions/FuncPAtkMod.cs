﻿using Core.Model.Player;

namespace Core.Model.CalculateStats.Functions
{
    public class FuncPAtkMod : FunctionObject 
    {
        public FuncPAtkMod(CharacterStatId statId, int order, object owner) : base(statId, order, owner)
        {
        }

        public override void Calc(Env env)
        {
            if (env.Player is PlayerInstance playerInstance)
            {
                double intb = BaseStat.Strength.CalcBonus(playerInstance);
                double lvlb = env.Player.LevelMod;
                env.Value *= (lvlb * lvlb) * (intb * intb);
                //env.Value *= BaseStat.Strength.CalcBonus(playerInstance) * playerInstance.LevelMod;
            }
            else
            {
                env.Value *= BaseStat.Strength.CalcBonus(env.Player) * env.Player.LevelMod;
            }
        }
    }
}