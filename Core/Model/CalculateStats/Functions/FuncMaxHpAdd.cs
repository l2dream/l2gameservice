﻿using Core.Model.Actor.Stat;
using Core.Model.Player;
using Core.Model.Player.Template;
using Core.Model.Skills;

namespace Core.Model.CalculateStats.Functions
{
    internal sealed class FuncMaxHpAdd : FunctionObject
    {
        public FuncMaxHpAdd(CharacterStatId statId, int order, object owner) : base(statId, order, owner)
        {
        }

        public override void Calc(Env env)
        {
            PlayerInstance playerInstance = (PlayerInstance)env.Player;
            PlayerTemplate t = playerInstance.Template;
            int lvl = playerInstance.Stat.Level - t.Stat.ClassBaseLevel;
            double hpMod = t.Stat.LevelHpMod * lvl;
            double hpMax = (t.Stat.LevelHpAdd + hpMod) * lvl;
            double hpMin = (t.Stat.LevelHpAdd * lvl) + hpMod;
            env.Value += (hpMax + hpMin) / 2;
        }
    }
}