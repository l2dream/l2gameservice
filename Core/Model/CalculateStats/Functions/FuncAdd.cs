﻿namespace Core.Model.CalculateStats.Functions
{
    public class FuncAdd : FunctionObject
    {
        private readonly Lambda _lambda;
        public FuncAdd(CharacterStatId statId, int order, object owner, Lambda lambda) : base(statId, order, owner)
        {
            _lambda = lambda;
        }

        public override void Calc(Env env)
        {
            if ((Condition == null) || Condition.Test(env))
            {
                env.Value += _lambda.Calc(env);
            }
        }
    }
}