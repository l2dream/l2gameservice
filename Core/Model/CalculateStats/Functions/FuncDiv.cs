﻿namespace Core.Model.CalculateStats.Functions
{
    internal sealed class FuncDiv : FunctionObject
    {
        private readonly Lambda _lambda;
        public FuncDiv(CharacterStatId statId, int order, object owner, Lambda lambda) : base(statId, order, owner)
        {
            _lambda = lambda;
        }

        public override void Calc(Env env)
        {
            if ((Condition is null) || Condition.Test(env))
            {
                env.Value /= _lambda.Calc(env);
            }
        }
    }
}