﻿using Core.Model.CalculateStats.Conditions;
using Core.Model.Items.Type;

namespace Core.Model.CalculateStats.Functions
{
    internal sealed class FuncBowAtkRange : FunctionObject
    {
        public FuncBowAtkRange(CharacterStatId statId, int order, object owner) : base(statId, order, owner)
        {
            Condition = new ConditionUsingItemType(WeaponType.GetWeaponTypeId("bow").Mask());
        }

        public override void Calc(Env env)
        {
            if (!Condition.Test(env))
            {
                return;
            }
            env.Value += 460;
        }
    }
}