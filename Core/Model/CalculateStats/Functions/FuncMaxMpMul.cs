﻿namespace Core.Model.CalculateStats.Functions
{
    public class FuncMaxMpMul : FunctionObject
    {
        public FuncMaxMpMul(CharacterStatId statId, int order, object owner) : base(statId, order, owner)
        {
        }

        public override void Calc(Env env)
        {
            env.Value *= BaseStat.Mental.CalcBonus(env.Player);
        }
    }
}