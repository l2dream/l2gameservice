﻿using System;
using Core.Model.Actor;
using Core.Model.Player;

namespace Core.Model.CalculateStats.Functions
{
    public class FuncAtkEvasion : FunctionObject
    {

        public FuncAtkEvasion(CharacterStatId statId, int order, object owner) : base(statId, order, owner)
        {
        }

        public override void Calc(Env env)
        {
            int level;
            if (env.Player is PlayerInstance playerInstance)
            {
                level = playerInstance.Stat.Level;
                env.Value += Math.Sqrt(playerInstance.Stat.GetDex()) * 6;
            }
            else
            {
                level = env.Player.Stat.Level;
                env.Value += Math.Sqrt(env.Player.Stat.GetDex()) * 6;
            }
            
            Character p = env.Player;
            
            env.Value += level;
            if (level > 77)
            {
                env.Value += (level - 77);
            }
            if (level > 69)
            {
                env.Value += (level - 69);
            }
        }
    }
}