﻿using Core.Model.Actor.Stat;
using Core.Model.Player;
using Core.Model.Player.Template;

namespace Core.Model.CalculateStats.Functions
{
    internal sealed class FuncMaxMpAdd : FunctionObject
    {
        public FuncMaxMpAdd(CharacterStatId statId, int order, object owner) : base(statId, order, owner)
        {
        }

        public override void Calc(Env env)
        {
            PlayerInstance playerInstance = (PlayerInstance)env.Player;
            PlayerTemplate t = playerInstance.Template;
            int lvl = playerInstance.Stat.Level - t.Stat.ClassBaseLevel;
            double mpMod = t.Stat.LevelMpMod * lvl;
            double mpMax = (t.Stat.LevelMpAdd + mpMod) * lvl;
            double mpMin = (t.Stat.LevelMpAdd * lvl) + mpMod;
            env.Value += (mpMax + mpMin) / 2;
        }
    }
}