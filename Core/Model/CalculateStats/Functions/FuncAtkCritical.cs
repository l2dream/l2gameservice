﻿using Core.Model.Actor;
using Core.Model.Npc.Type;

namespace Core.Model.CalculateStats.Functions
{
    public class FuncAtkCritical : FunctionObject
    {
        public FuncAtkCritical(CharacterStatId statId, int order, object owner) : base(statId, order, owner)
        {
        }

        public override void Calc(Env env)
        {
            env.Value *= BaseStat.Dexterity.CalcBonus(env.Player);
			
            Character p = env.Player;
            if (!(p is L2Pet))
            {
                env.Value *= 10;
            }
			
            env.BaseValue = env.Value;
        }
    }
}