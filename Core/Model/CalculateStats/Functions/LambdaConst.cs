﻿namespace Core.Model.CalculateStats.Functions
{
    public class LambdaConst : Lambda
    {
        private readonly double _value;

        public LambdaConst(double value)
        {
            _value = value;
        }
        public override double Calc(Env env)
        {
            return _value;
        }
    }
}