﻿namespace Core.Model.CalculateStats.Functions
{
    internal sealed class FuncMAtkMod : FunctionObject
    {
        public FuncMAtkMod(CharacterStatId statId, int order, object owner) : base(statId, order, owner)
        {
        }

        public override void Calc(Env env)
        {
            double intb = BaseStat.Intelligence.CalcBonus(env.Player);
            double lvlb = env.Player.LevelMod;
            env.Value *= (lvlb * lvlb) * (intb * intb);
        }
    }
}