﻿using Core.Model.Items;
using Core.Model.Player;

namespace Core.Model.CalculateStats.Functions
{
    public class FuncPDefMod : FunctionObject
    {
        public FuncPDefMod(CharacterStatId statId, int order, object owner) : base(statId, order, owner)
        {
        }

        /// <summary>
        /// Function P.Def
        /// </summary>
        /// <param name="env"></param>
        public override void Calc(Env env)
        {
            if (env.Player is PlayerInstance playerInstance)
            {
                bool hasMagePDef = (playerInstance.Template.Stat.ClassId.IsMageClass || (playerInstance.Template.Stat.ClassId.Id == ClassIds.OrcMystic)); // orc mystics are a special case
                if (playerInstance.PlayerInventory().GetPaperdollItem(Inventory.PaperdollHead) != null)
                {
                    env.Value -= 12;
                }
                ItemInstance chest = playerInstance.PlayerInventory().GetPaperdollItem(Inventory.PaperdollChest);
                if (chest != null)
                {
                    env.Value -= hasMagePDef ? 15 : 31;
                }
                if ((playerInstance.PlayerInventory().GetPaperdollItem(Inventory.PaperdollLegs) != null) || ((chest != null) && (chest.Item.BodyPart == ItemSlotId.SlotFullArmor)))
                {
                    env.Value -= hasMagePDef ? 8 : 18;
                }
                if (playerInstance.PlayerInventory().GetPaperdollItem(Inventory.PaperdollGloves) != null)
                {
                    env.Value -= 8;
                }
                if (playerInstance.PlayerInventory().GetPaperdollItem(Inventory.PaperdollFeet) != null)
                {
                    env.Value -= 7;
                }
            }
            env.Value *= env.Player.LevelMod;
        }
    }
}