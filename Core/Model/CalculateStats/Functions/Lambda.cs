﻿namespace Core.Model.CalculateStats.Functions
{
    public abstract class Lambda
    {
        public abstract double Calc(Env env);
    }
}