﻿namespace Core.Model.CalculateStats.Functions
{
    public class FuncMAtkSpeed : FunctionObject
    {
        public FuncMAtkSpeed(CharacterStatId statId, int order, object owner) : base(statId, order, owner)
        {
        }

        public override void Calc(Env env)
        {
            env.Value *= BaseStat.Wit.CalcBonus(env.Player);
        }
    }
}