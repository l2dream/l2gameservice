﻿namespace Core.Model.CalculateStats.Functions
{
    public class FuncMDefMod : FunctionObject
    {
        public FuncMDefMod(CharacterStatId statId, int order, object owner) : base(statId, order, owner)
        {
        }

        /// <summary>
        /// TODO Inventory
        /// </summary>
        /// <param name="env"></param>
        public override void Calc(Env env)
        {
            env.Value *= BaseStat.Mental.CalcBonus(env.Player) * env.Player.LevelMod;
        }
    }
}