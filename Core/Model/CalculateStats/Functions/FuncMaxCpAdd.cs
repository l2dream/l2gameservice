﻿using Core.Model.Player;
using Core.Model.Player.Template;

namespace Core.Model.CalculateStats.Functions
{
    internal sealed class FuncMaxCpAdd : FunctionObject
    {
        public FuncMaxCpAdd(CharacterStatId statId, int order, object owner) : base(statId, order, owner)
        {
        }

        public override void Calc(Env env)
        {
            PlayerInstance playerInstance = (PlayerInstance)env.Player;
            PlayerTemplate t = playerInstance.Template;
            int lvl = playerInstance.Stat.Level - t.Stat.ClassBaseLevel;
            double cpMod = t.Stat.LevelCpMod * lvl;
            double cpMax = (t.Stat.LevelCpAdd + cpMod) * lvl;
            double cpMin = (t.Stat.LevelCpAdd * lvl) + cpMod;
            env.Value += (cpMax + cpMin) / 2;
        }
    }
}