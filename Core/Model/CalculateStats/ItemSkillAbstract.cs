﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using Core.Model.Actor;
using Core.Model.CalculateStats.Conditions;
using Core.Model.CalculateStats.Functions;
using Core.Model.Items;
using Core.Model.Items.Type;
using Core.Model.Skills;
using Core.Model.Skills.Effects;
using L2Logger;

namespace Core.Model.CalculateStats
{
    public abstract class ItemSkillAbstract
    {
        protected readonly IDictionary<string, string[]> _tables;
        protected SkillDataHolder _currentSkill;
        protected readonly IDictionary<string, Type> _initEffects;

        protected ItemSkillAbstract()
        {
            _tables = new Dictionary<string, string[]>();
            _initEffects = new Dictionary<string, Type>();
        }
        
        protected string GetValue(string value, object template)
        {
            // is it a table?
            if (value.StartsWith("#"))
            {
                if (template is Skill)
                {
                    return GetTableValue(value);
                }
                else if (template is int)
                {
                    return GetTableValue(value, ((int) template));
                }
            }
            return value;
        }
        
        protected string GetTableValue(string name)
        {
            try
            {
                return _tables[name][_currentSkill.CurrentLevel];
            }
            catch (Exception ex)
            {
                LoggerManager.Error("Error in table: " + name + " of Skill Id " + _currentSkill.Id + " " + ex.Message);
                return "";
            }
        }
        
        protected string GetTableValue(string name, int idx)
        {
            try
            {
                return _tables[name][idx - 1];
            }
            catch (Exception ex)
            {
                LoggerManager.Error("wrong level count in skill Id " + _currentSkill.Id + " " + ex.Message);
                return "";
            }
        }
        
        protected Lambda GetLambda(XmlNode node, object template)
        {
            var xmlNode = node.Attributes?.GetNamedItem("val");
            if (xmlNode != null)
            {
                string val = xmlNode.Value;
                if (val.StartsWith("#"))
                {
                    return new LambdaConst(Convert.ToDouble(GetTableValue(val), CultureInfo.InvariantCulture.NumberFormat));
                }
                return new LambdaConst(Convert.ToDouble(val, CultureInfo.InvariantCulture.NumberFormat));
            }
            throw new Exception("GetLambda xmlNode is Null please check code");
        }
        
        protected void AttachFunc(XmlNode node, object template, Type typeName, Condition attachCond)
        {
            var attrs = node.Attributes;
            var statValue = attrs.GetNamedItem("stat").Value;
            var statOrder = attrs.GetNamedItem("order").Value;
            CharacterStatId statId = Stat.GetStatByValue(statValue);
            int order = Convert.ToInt32(statOrder, 16);
            Lambda lambda = GetLambda(node, template);
            Condition applyCondition = ParseCondition(node.FirstChild, template);
            FuncTemplate ft = new FuncTemplate(attachCond, applyCondition, typeName, statId, order, lambda);
            
            switch (template)
            {
                case Item item:
                    item.Attach(ft);
                    break;
                case Skill skill:
                    skill.Attach(ft);
                    break;
                case EffectTemplate effectTemplate:
                    effectTemplate.Attach(ft);
                    break;
            }
        }

        protected void ParseTemplate(XmlNode node, object template)
        {
            Condition condition = null;
            var firstChild = node.FirstChild;
            if (firstChild is null)
            {
                return;
            }

            foreach (XmlLinkedNode element in node)
            {
                switch (element.Name)
                {
                    case "add":
                        AttachFunc(element, template, typeof(FuncAdd), condition);
                        break;
                    case "sub":
                        AttachFunc(element, template, typeof(FuncSub), condition);
                        break;
                    case "mul":
                        AttachFunc(element, template, typeof(FuncMul), condition);
                        break;
                    case "basemul":
                        AttachFunc(element, template, typeof(FuncBaseMul), condition);
                        break;
                    case "div":
                        AttachFunc(element, template, typeof(FuncDiv), condition);
                        break;
                    case "set":
                        AttachFunc(element, template, typeof(FuncSet), condition);
                        break;
                    case "enchant":
                        //TODO Enchant 
                        ///AttachFunc(element, template, typeof(FuncEnchant), condition);
                        break;
                    case "skill":
                        //AttachSkill(element, template, condition);
                        break;
                    case "effect":
                        AttachEffect(element, template, condition);
                        break;
                    case "cond":
                        condition = ParseCondition(node.FirstChild, template);
                        if (node.Attributes != null)
                        {
                            var msg = node.Attributes.GetNamedItem("msg");
                            if (condition != null && msg != null)
                            {
                                condition.Message = msg.Value;
                            }
                        }
                        break;
                    case "#comment":
                    case "player":
                    case "using":
                    case "and":
                    case "target":
                    case "game":
                        break;
                    default:
                        LoggerManager.Error("Skill Init ParseTemplate: " + element.Name + " not implemented");
                        break;
                }
            }
            
        }
        
        protected Condition JoinAnd(Condition cond, Condition c)
        {
            if (cond == null)
            {
                return c;
            }
            if (cond is ConditionLogicAnd conditionLogicAnd)
            {
                conditionLogicAnd.Add(c);
                return cond;
            }
            ConditionLogicAnd and = new ConditionLogicAnd();
            and.Add(cond);
            and.Add(c);
            return and;
        }
        
        protected void ParseBeanSet(XmlNode n, StatSet set, int level)
        {
            if (n.Attributes != null)
            {
                string name = n.Attributes.GetNamedItem("name").Value.Trim();
                string value = n.Attributes.GetNamedItem("val").Value.Trim();
                char ch = value.Length == 0 ? ' ' : value[0];
                if ((ch == '#') || (ch == '-') || char.IsDigit(ch))
                {
                    set.Add(name, GetValue(value, level));
                }
                else
                {
                    set.Add(name, value);
                }
            }
        }
        
        protected Condition ParseSkillCondition(XmlNode node)
        {
            var attrs = node.Attributes;
            if (attrs != null)
            {
                CharacterStatId statId = Stat.GetStatByValue(attrs.GetNamedItem("stat").Value);
                return new ConditionSkillStats(statId);
            }

            return null;
        }
        
        protected Condition ParseUsingCondition(XmlNode node)
        {
            Condition cond = null;
            var attrs = node.Attributes;
            if (attrs != null)
            {
                foreach (XmlNode a in attrs)
                {
                    if (a.Name.Equals("kind"))
                    {
                        int mask = 0;
                        var st = a.Value.Split(",");
                        foreach (var s in st)
                        {
                            var weaponType = WeaponType.GetWeaponTypeId(s.ToLower());
                            if (weaponType != null)
                            {
                                mask |= weaponType.Mask();
                            }

                            var armorType = ArmorType.GetArmorTypeId(s.ToLower());
                            if (armorType != null)
                            {
                                mask |= armorType.Mask();
                            }
                        }
                        cond = JoinAnd(cond, new ConditionUsingItemType(mask));
                    } 
                    else if (a.Name.Equals("skill"))
                    {
                        int id = Convert.ToInt32(a.Value);
                        cond = JoinAnd(cond, new ConditionUsingSkill(id));
                    } 
                    else if (a.Name.Equals("slotitem"))
                    {
                        var st = a.Value.Split(";");
                        //int id = Convert.ToInt32()
                    }
                }
            }
            return cond;
        }
        
        protected Condition ParseLogicNot(XmlNode node, object template)
        {
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType == XmlNodeType.Element)
                {
                    return new ConditionLogicNot(ParseCondition(n, template));
                }
            }
            throw new NullReferenceException();
        }
        
        protected Condition ParseLogicAnd(XmlNode node, object template)
        {
            ConditionLogicAnd cond = new ConditionLogicAnd();
            if (node?.ChildNodes is null)
                return cond;
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType == XmlNodeType.Element)
                {
                    cond.Add(ParseCondition(n, template));
                }
            }
            return cond;
        }
        
        protected Condition ParseCondition(XmlNode node, object template)
        {
            if (node is null)
                return null;
            if (node.Name.Equals("and"))
            {
                return ParseLogicAnd(node, template);
            }
            if (node.Name.Equals("not"))
            {
                return ParseLogicNot(node, template);
            }
            if (node.Name.Equals("player"))
            {
                //return ParsePlayerCondition(node);
            }
            if (node.Name.Equals("using"))
            {
                return ParseUsingCondition(node);
            }
            if (node.Name.Equals("skill"))
            {
                return ParseSkillCondition(node);
            }
            return null;
        }
        
        protected void AttachEffect(XmlNode node, object template, Condition attachCond)
        {
            try
            {
                var attrs = node.Attributes;
                int time;
                int count = 1;
                int showIcon = 0;
                string name = attrs?.GetNamedItem("name").Value;
                if (attrs?.GetNamedItem("noicon") != null)
                {
                    showIcon = Convert.ToInt32(GetValue(attrs.GetNamedItem("noicon").Value, template));
                }

                if (attrs?.GetNamedItem("count") != null)
                {
                    count = Convert.ToInt32(GetValue(attrs.GetNamedItem("count").Value, template), 16);
                }

                if (attrs?.GetNamedItem("time") != null)
                {
                    time = int.Parse(GetValue(attrs.GetNamedItem("time").Value, template));
                }
                else
                {
                    time = ((Skill) template).BuffDuration / 1000 / count;
                }

                bool self = false;
                if (attrs?.GetNamedItem("self") != null)
                {
                    self = (int.Parse(attrs.GetNamedItem("self").Value) == 1);
                }

                Lambda lambda = GetLambda(node, template);
                
                Condition applyCond = ParseCondition(node.FirstChild, template);
                
                int abnormal = 0;
                if (attrs?.GetNamedItem("abnormal") != null)
                {
                    string abn = attrs.GetNamedItem("abnormal").Value;
                    if (abn.Equals("poison"))
                    {
                        abnormal = CharacterAbnormalEffect.AbnormalEffectPoison;
                    } 
                    else if (abn.Equals("bleeding"))
                    {
                        abnormal = CharacterAbnormalEffect.AbnormalEffectBleeding;
                    } 
                    else if (abn.Equals("flame"))
                    {
                        abnormal = CharacterAbnormalEffect.AbnormalEffectFlame;
                    }
                    else if (abn.Equals("bighead"))
                    {
                        abnormal = CharacterAbnormalEffect.AbnormalEffectBigHead;
                    }
                    else if (abn.Equals("stealth"))
                    {
                        abnormal = CharacterAbnormalEffect.AbnormalEffectStealth;
                    }
                    else if (abn.Equals("float"))
                    {
                        abnormal = CharacterAbnormalEffect.AbnormalEffectFloatingRoot;
                    }
                }
                
                float stackOrder = 0;
                string stackType = "none";

                if (attrs?.GetNamedItem("stackType") != null)
                {
                    stackType = attrs.GetNamedItem("stackType").Value;
                }
                if (attrs?.GetNamedItem("stackOrder") != null)
                {
                    var value = GetValue(attrs.GetNamedItem("stackOrder").Value, template);
                    stackOrder = Convert.ToSingle(value, CultureInfo.InvariantCulture.NumberFormat);
                }
                double effectPower = -1;
                if (attrs?.GetNamedItem("effectPower") != null)
                {
                    var value = GetValue(attrs.GetNamedItem("effectPower").Value, template);
                    effectPower = Convert.ToDouble(value, CultureInfo.InvariantCulture.NumberFormat);
                }

                SkillType skillType = null;
                if (attrs?.GetNamedItem("effectType") != null)
                {
                    string typeName = GetValue(attrs.GetNamedItem("effectType").Value, template);
                    skillType = SkillType.ValueOf(typeName);
                }

                Type classType = _initEffects["Effect" + name];
                EffectTemplate lt = new EffectTemplate(attachCond, applyCond, classType, lambda, count, time, abnormal, stackType, stackOrder, showIcon, skillType, effectPower);
                ParseTemplate(node, lt);
                if (template is Item item)
                {
                    item.Attach(lt);
                }
                else if ((template is Skill notSelfSkill) && !self)
                {
                    notSelfSkill.Attach(lt);
                }
                else if ((template is Skill selfSkill) && self)
                {
                    selfSkill.AttachSelf(lt);
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Error(node.Name + " " + ex.Message);
            }
        }
        
        protected void ParseTable(XmlNode node)
        {
            try
            {
                var attrs = node.Attributes;
                string name = attrs?.GetNamedItem("name").Value;
                var res = node.FirstChild.Value.Split(" ");
                SetTable(name, res);
            }
            catch (Exception ex)
            {
                LoggerManager.Error(ex.Message);
            }
        }
        
        protected void SetTable(string name, string[] table)
        {
            _tables.TryAdd(name, table);
        }
    }
}