﻿using System;
using System.Threading;
using DataBase.Interfaces;
using L2Logger;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Model
{
    public class IdFactory
    {
        private int _currentId = 1;
        private const int IdMin = 0x10000000;
        private const int IdMax = 0x7FFFFFFF;

        private IServiceProvider _serviceProvider;
        private readonly ICharacterRepository _characterRepository;

        public IdFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _characterRepository = _serviceProvider.GetService<IUnitOfWork>().Characters;
        }
        
        public int NextId()
        {
            return Interlocked.Increment(ref _currentId);
        }

        public void Init()
        {
            _currentId = GetMaxObjectId();
            LoggerManager.Info($"Used IDs {_currentId}.");
        }

        private int GetMaxObjectId()
        {
            int objectId = _characterRepository.GetMaxObjectId();
            if (objectId < IdMin)
                objectId = IdMin;
            return objectId;
        }
    }
}