﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using L2Logger;
using Newtonsoft.Json;

namespace Core.Model
{
    public class Parser
    {
        private class Item
        {
            public int ItemId { get; set; }
            public int Price { get; set; }
            public string Name { get; set; }
            public int Count { get; set; }
        }

        private readonly Dictionary<int, List<Item>> _listItems;


        private class SetItem
        {
            private readonly IDictionary<string, int> _sets;

            public SetItem()
            {
                _sets = new Dictionary<string, int>();
            }
            public void AddToSet(string name, int value)
            {
                _sets.Add(name, value);
            }

            public int GetDataByField(string name)
            {
                return _sets[name];
            }

            public void Clear()
            {
                _sets.Clear();
            }
        }
        
        public Parser()
        {
            _listItems = new Dictionary<int, List<Item>>();
            Init();
        }

        private void Init()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(@"StaticData/Xml/buylist.xml");

                XmlNodeList nodes = doc.DocumentElement.SelectNodes("/table_data/row");
                
                foreach (XmlNode node in nodes)
                {
                    SetItem setItem = new SetItem();
                    foreach (XmlElement child in node.ChildNodes)
                    {
                        var name = child.Attributes["name"].InnerText;
                        var value = child.InnerText;
                        if (name == "savetimer")
                            continue;
                        setItem.AddToSet(name, int.Parse(value));
                    }

                    var tmpItem = Initializer.ItemService().GetItemById(setItem.GetDataByField("item_id"));

                    var item = new Item
                    {
                        ItemId = setItem.GetDataByField("item_id"),
                        Price = setItem.GetDataByField("price"),
                        Name = tmpItem?.Name,
                        Count =  setItem.GetDataByField("count")
                    };
                    int shopId = setItem.GetDataByField("shop_id");

                    if (_listItems.ContainsKey(shopId))
                    {
                        _listItems[shopId].Add(item);
                    }
                    else
                    {
                        List<Item> lst = new List<Item> {item};
                        _listItems.Add(shopId, lst);
                    }
                    setItem.Clear();
                }
                LoggerManager.Info("Count " + _listItems.Count);
                PrepareJson();
            }
            catch (Exception xmlException)
            {
                LoggerManager.Error(xmlException.Message);
            }
        }

        private class JsonClass
        {
            public int ShopId { get; set; }
            public List<Item> Items { get; set; }
        }

        private void PrepareJson()
        {
            var path = Initializer.Config().ServerConfig.StaticData;
            try
            {
                foreach (var listItem in _listItems)
                {
                    // serialize JSON directly to a file
                    using StreamWriter file = File.CreateText(path + "/Json/Merchant/" + listItem.Key + ".json");
                    var jsonClass = new JsonClass
                    {
                        ShopId = listItem.Key,
                        Items = listItem.Value
                    };
                    string json = JsonConvert.SerializeObject(jsonClass, Newtonsoft.Json.Formatting.Indented);
                    file.Write(json);
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Error(ex.Message);
            }
        }
    }
}