﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Core.Model.Actor;
using Core.Model.Player;
using Core.Model.Skills;

namespace Core.Model.Items.Handlers
{
    internal sealed class Potions : IItemHandler
    {
        private readonly int[] _itemIds =
        {
            65,
            725,
            726,
            727,
            728,
            733,
            734,
            735,
            1060,
            1061,
            1062,
            1073,
            1374,
            1375,
            1539,
            1540,
            4667,
            4679,
            4680,
            5283,
            5591,
            5592,
            6035,
            6036,
            6652,
            6653,
            6654,
            6655,
            8193,
            8194,
            8195,
            8196,
            8197,
            8198,
            8199,
            8200,
            8201,
            8202,
            8600,
            8601,
            8602,
            8603,
            8604,
            8605,
            8606,
            8607,
            8608,
            8609,
            8610,
            8611,
            8612,
            8613,
            8614,
            // elixir of life
            8622,
            8623,
            8624,
            8625,
            8626,
            8627,
            // elixir of Strength
            8628,
            8629,
            8630,
            8631,
            8632,
            8633,
            // elixir of cp
            8634,
            8635,
            8636,
            8637,
            8638,
            8639,
            // primavel potions
            8786,
            8787
        };
        
        private readonly IDictionary<int, PotionsSkills> _potions;
        public Potions()
        {
            _potions = new Dictionary<int, PotionsSkills>();
            LoadPotions();
        }

        private void LoadPotions()
        {
            System.Type potions = typeof(PotionsSkills);
            var potionSkills = potions.GetFields(BindingFlags.Static | BindingFlags.NonPublic);
            foreach (FieldInfo fieldInfo in potionSkills)
            {
                PotionsSkills potionSkill = (PotionsSkills) fieldInfo.GetValue(null);
                _potions.Add(potionSkill.PotionId, potionSkill);
            }
        }

        public PotionsSkills GetSkillsForPotion(int potionId)
        {
            return _potions[potionId];
        }
        
        public async Task UseItemAsync(Playable playable, ItemInstance item)
        {
            if (playable is PlayerInstance playerInstance)
            {
                int itemId = item.ItemId;
                switch (itemId)
                {
                    case 734: // quick_step_potion, xml: 2011
                    {
                        await UsePotionAsync(playerInstance, 2011, 1);
                        break;
                    }
                    case 735: // swift_attack_potion, xml: 2012
                    {
                        await UsePotionAsync(playerInstance, 2012, 1);
                        break;
                    }
                    case 1060: // lesser_healing_potion,
                    case 1073: // beginner's potion, xml: 2031
                    {
                        await UsePotionAsync(playerInstance, 2031, 1);
                        break;
                    }
                    case 6035: // Magic Haste Potion, xml: 2169
                    {
                        await UsePotionAsync(playerInstance, 2169, 1);
                        break;
                    }
                    case 8600: // Herb of Life
                    {
                        await UsePotionAsync(playerInstance, 2278, 1);
                        break;
                    }
                    case 8601: // Greater Herb of Life
                    {
                        await UsePotionAsync(playerInstance, 2278, 2);
                        break;
                    }
                    case 8602: // Superior Herb of Life
                    {
                        await UsePotionAsync(playerInstance, 2278, 3);
                        break;
                    }
                    case 8603: // Herb of Mana
                    {
                        await UsePotionAsync(playerInstance, 2279, 1);
                        break;
                    }
                    case 8604: // Greater Herb of Mane
                    {
                        await UsePotionAsync(playerInstance, 2279, 2);
                        break;
                    }
                    case 8605: // Superior Herb of Mane
                    {
                        await UsePotionAsync(playerInstance, 2279, 3);
                        break;
                    }
                    case 8606: // Herb of Strength
                    {
                        await UsePotionAsync(playerInstance, 2280, 1);
                        break;
                    }
                    case 8607: // Herb of Magic
                    {
                        await UsePotionAsync(playerInstance, 2281, 1);
                        break;
                    }
                    case 8608: // Herb of Atk. Spd.
                    {
                        await UsePotionAsync(playerInstance, 2282, 1);
                        break;
                    }
                    case 8609: // Herb of Casting Spd.
                    {
                        await UsePotionAsync(playerInstance, 2283, 1);
                        break;
                    }
                    case 8610: // Herb of Critical Attack
                    {
                        await UsePotionAsync(playerInstance, 2284, 1);
                        break;
                    }
                    case 8611: // Herb of Speed
                    {
                        await UsePotionAsync(playerInstance, 2285, 1);
                        break;
                    }
                    case 8612: // Herb of Warrior
                    {
                        await UsePotionAsync(playerInstance, 2280, 1);
                        await UsePotionAsync(playerInstance, 2282, 1);
                        await UsePotionAsync(playerInstance, 2284, 1);
                        break;
                    }
                    case 8613: // Herb of Mystic
                    {
                        await UsePotionAsync(playerInstance, 2281, 1);
                        await UsePotionAsync(playerInstance, 2283, 1);
                        break;
                    }
                    case 8614: // Herb of Warrior
                    {
                        await UsePotionAsync(playerInstance, 2278, 3);
                        await UsePotionAsync(playerInstance, 2279, 3);
                        break;
                    }
                }
            }
        }

        private async Task UsePotionAsync(PlayerInstance playerInstance, int magicId, int level)
        {
            Skill skill = Initializer.SkillService().GetSkill(magicId, level);
            await playerInstance.MagicCast().DoCastAsync(skill);
            
            // only for Heal potions
            if ((magicId == 2031) || (magicId == 2032) || (magicId == 2037))
            {
                playerInstance.Effects.ShortBuffStatusUpdate(magicId, level, 15);
            }
        }

        public int[] GetItemIds()
        {
            return _itemIds;
        }
    }

}