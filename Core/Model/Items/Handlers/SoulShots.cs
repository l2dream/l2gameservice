﻿using System.Threading.Tasks;
using Core.Model.Actor;
using Core.Model.Items.Type;
using Core.Model.Player;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Items.Handlers
{
    internal sealed class SoulShots : IItemHandler
    {
        // All the item IDs that this handler knows.
        private readonly int[] _itemIds =
        {
            5789,
            1835,
            1463,
            1464,
            1465,
            1466,
            1467
        };
        private readonly int[] _skillIds =
        {
            2039,
            2150,
            2151,
            2152,
            2153,
            2154
        };
        public async Task UseItemAsync(Playable playable, ItemInstance item)
        {
            PlayerInstance player = (PlayerInstance) playable;
            ItemInstance weaponInst = player.GetActiveWeaponInstance();
            if (weaponInst is null)
                return;
            
            Weapon weaponItem = player.GetActiveWeaponItem();
            int itemId = item.ItemId;
            
            // Check if SoulShot is already active
            if (weaponItem.ChargedSoulShot != Charged.ChargedNone)
            {
                return;
            }
            
            // Check for correct grade
            CrystalId weaponGrade = weaponItem.CrystalType;
            if (((weaponGrade == CrystalId.CrystalNone) && (itemId != 5789) && (itemId != 1835)) ||
                ((weaponGrade == CrystalId.CrystalD) && (itemId != 1463)) ||
                ((weaponGrade == CrystalId.CrystalC) && (itemId != 1464)) ||
                ((weaponGrade == CrystalId.CrystalB) && (itemId != 1465)) ||
                ((weaponGrade == CrystalId.CrystalA) && (itemId != 1466)) ||
                ((weaponGrade == CrystalId.CrystalS) && (itemId != 1467)))
            {
                await player.SendPacketAsync(new SystemMessage(SystemMessageId.SoulshotsGradeMismatch));
                return;
            }

            // Charge SoulShot
            weaponItem.ChargedSoulShot = Charged.ChargedSoulShot;
            // Send message to client
            await player.SendPacketAsync(new SystemMessage(SystemMessageId.EnabledSoulshot));
            PlayerClientServer.ToSelfAndKnownPlayersInRadius(player, new MagicSkillUse(player, player, _skillIds[(int)weaponGrade], 1, 0, 0), 360000);
        }

        public int[] GetItemIds()
        {
            return _itemIds;
        }
    }
}