﻿using System.Threading.Tasks;
using Core.Model.Actor;
using Core.Model.CalculateStats;
using Core.Model.Player;
using Core.Model.Skills;
using Core.Model.Skills.Handlers;
using Core.Model.World;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Items.Handlers
{
    internal sealed class ManaHeal : ISkillHandler
    {
        private readonly SkillType[] _skillTypes =
        {
            SkillType.ManaHeal,
            SkillType.ManaRecharge,
            SkillType.ManaHealPercent
        };
        public async Task UseSkillAsync(Character character, Skill skill, WorldObject[] targets)
        {
            foreach (Character target in (Character[]) targets)
            {
                double mp = skill.GetPower();
                if (skill.GetSkillType() == SkillType.ManaHealPercent)
                {
                    mp = (target.Stat.GetMaxMp() * mp) / 100.0;
                }
                else
                {
                    mp = (skill.GetSkillType() == SkillType.ManaRecharge) ? target.Stat.GetCalc().CalcStat(CharacterStatId.RechargeMpRate, (int)mp, null, null) : mp;
                }
                target.Status.SetCurrentMp(mp + target.Status.GetCurrentMp());
                StatusUpdate sump = new StatusUpdate(target.ObjectId);
                sump.AddAttribute(StatusUpdate.CurMp, (int) target.Status.GetCurrentMp());
                await target.SendPacketAsync(sump);
			
                if ((character is PlayerInstance) && (character != target))
                {
                    SystemMessage sm = new SystemMessage(SystemMessageId.S2MpRestoredByS1);
                    sm.AddString(character.CharacterName);
                    sm.AddNumber((int) mp);
                    await target.SendPacketAsync(sm);
                }
                else
                {
                    SystemMessage sm = new SystemMessage(SystemMessageId.S1MpRestored);
                    sm.AddNumber((int) mp);
                    await target.SendPacketAsync(sm);
                }
            }
        }

        public SkillType[] GetSkillIds()
        {
            return _skillTypes;
        }
    }
}