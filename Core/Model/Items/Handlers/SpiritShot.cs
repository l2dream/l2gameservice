﻿using System.Threading.Tasks;
using Core.Model.Actor;
using Core.Model.Items.Type;
using Core.Model.Player;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Items.Handlers
{
    internal sealed class SpiritShot : IItemHandler
    {
        // All the item IDs that this handler knows.
        private readonly int[] _itemIds =
        {
            5790,
            2509,
            2510,
            2511,
            2512,
            2513,
            2514
        };
        private readonly int[] _skillIds =
        {
            2061,
            2155,
            2156,
            2157,
            2158,
            2159
        };
        
        public async Task UseItemAsync(Playable playable, ItemInstance item)
        {
            PlayerInstance player = (PlayerInstance) playable;
            ItemInstance weaponInst = player.GetActiveWeaponInstance();
            if (weaponInst is null)
                return;

            Weapon weaponItem = player.GetActiveWeaponItem();
            int itemId = item.ItemId;
            
            // Check if SpiritShot is already active
            if (weaponItem.ChargedSpiritShot != Charged.ChargedNone)
            {
                return;
            }
            
            // Check for correct grade
            CrystalId weaponGrade = weaponItem.CrystalType;
            if (((weaponGrade == CrystalId.CrystalNone) && (itemId != 5790) && (itemId != 2509)) ||
                ((weaponGrade == CrystalId.CrystalD) && (itemId != 2510)) ||
                ((weaponGrade == CrystalId.CrystalC) && (itemId != 2511)) ||
                ((weaponGrade == CrystalId.CrystalB) && (itemId != 2512)) ||
                ((weaponGrade == CrystalId.CrystalA) && (itemId != 2513)) ||
                ((weaponGrade == CrystalId.CrystalS) && (itemId != 2514)))
            {
                await player.SendPacketAsync(new SystemMessage(SystemMessageId.SpiritshotsGradeMismatch));
                return;
            }

            // Charge SpiritShot
            weaponItem.ChargedSpiritShot = Charged.ChargedSpiritShot;
            // Send message to client
            await player.SendPacketAsync(new SystemMessage(SystemMessageId.EnabledSpiritshot));
            PlayerClientServer.ToSelfAndKnownPlayersInRadius(player, new MagicSkillUse(player, player, _skillIds[(int)weaponGrade], 1, 0, 0), 360000);
        }

        public int[] GetItemIds()
        {
            return _itemIds;
        }
    }
}