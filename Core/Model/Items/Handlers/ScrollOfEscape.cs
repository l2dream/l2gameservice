﻿using System.Threading.Tasks;
using Core.AI;
using Core.Model.Actor;
using Core.Model.Player;
using Core.Model.Skills;
using Core.Model.World.RegionData;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;
using Core.TaskManager;

namespace Core.Model.Items.Handlers
{
    internal sealed class ScrollOfEscape : IItemHandler
    {
        
        // All the items ids that this handler knows
        private static readonly int[] ItemIds =
        {
            736,
            1830,
            1829,
            1538,
            3958,
            5858,
            5859,
            7117,
            7118,
            7119,
            7120,
            7121,
            7122,
            7123,
            7124,
            7125,
            7126,
            7127,
            7128,
            7129,
            7130,
            7131,
            7132,
            7133,
            7134,
            7135,
            7554,
            7555,
            7556,
            7557,
            7558,
            7559,
            7618,
            7619
        };

        private PlayerInstance _playerInstance;
        private int ItemId { get; set; }
        
        public async Task UseItemAsync(Playable playable, ItemInstance item)
        {
            if (playable is PlayerInstance playerInstance)
            {
                _playerInstance = playerInstance;
                if (CheckConditions(playerInstance))
                {
                    return;
                }
                playerInstance.AI.SetIntention(CtrlIntention.AiIntentionIdle);
                int itemId = item.ItemId;
                SystemMessage sm3 = new SystemMessage(SystemMessageId.UseS1);
                sm3.AddItemName(itemId);
                await playerInstance.SendPacketAsync(sm3);
                
                int escapeSkill =
                    (item.ItemId == 1538) || (item.ItemId == 5858) || (item.ItemId == 5859) || (item.ItemId == 3958) ||
                    (item.ItemId == 10130)
                        ? 2036
                        : 2013;

                ItemId = item.ItemId;
                Skill skill = Initializer.SkillService().GetSkill(escapeSkill, 1);
                await playerInstance.SendBroadcastPacketAsync(new MagicSkillUse(playerInstance, playerInstance, escapeSkill, skill.Level, skill.HitTime, 0));
                await playerInstance.SendPacketAsync(new SetupGauge(0, skill.HitTime));
                // End SoE Animation section
                playerInstance.Target.RemoveTargetAsync();
                
                SystemMessage sm = new SystemMessage(SystemMessageId.S1HasDisappeared);
                sm.AddItemName(itemId);
                await playerInstance.SendPacketAsync(sm);
                ThreadPoolManager.Instance.Schedule(Run, skill.HitTime);
            }
        }

        private async Task Run()
        {
            if (ItemId < 7117)
            {
                await _playerInstance.Teleport().TeleToLocationAsync(TeleportWhereType.Town);                
            }
            else
            {
                switch (ItemId)
                {
                    case 7117:
                        await _playerInstance.Teleport().TeleToLocationAsync(-84318, 244579, -3730, true); // Talking Island
                        break;
                    case 7554:
                        await _playerInstance.Teleport().TeleToLocationAsync(-84318, 244579, -3730, true); // Talking Island quest scroll
                        break;
                    case 7118:
                        await _playerInstance.Teleport().TeleToLocationAsync(46934, 51467, -2977, true); // Elven Village
                        break;
                    case 7555:
                        await _playerInstance.Teleport().TeleToLocationAsync(46934, 51467, -2977, true); // Elven Village quest scroll
                        break;
                    case 7619:
                        await _playerInstance.Teleport().TeleToLocationAsync(108275, -53785, -2524, true); // Varka Silenos Village
                        break;
                    default:
                        await _playerInstance.Teleport().TeleToLocationAsync(TeleportWhereType.Town);
                        break;
                }
            }
        }

        private bool CheckConditions(PlayerInstance playerInstance)
        {
            return playerInstance.IsStunned || playerInstance.IsSleeping || playerInstance.IsParalyzed ||
                   playerInstance.IsAlikeDead || playerInstance.Status.IsAllSkillsDisabled();
        }

        public int[] GetItemIds()
        {
            return ItemIds;
        }
    }
}