﻿using System.Threading.Tasks;
using Core.Model.Actor;
using Core.Model.Items.Type;
using Core.Model.Player;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Items.Handlers
{
    internal sealed class BlessedSpiritShot : IItemHandler
    {
        // All the items ids that this handler knows
        private readonly int[] _itemIds =
        {
            3947,
            3948,
            3949,
            3950,
            3951,
            3952
        };
        private readonly int[] _skillIds =
        {
            2061,
            2160,
            2161,
            2162,
            2163,
            2164
        };
        
        public async Task UseItemAsync(Playable playable, ItemInstance item)
        {
            PlayerInstance player = (PlayerInstance) playable;
            ItemInstance weaponInst = player.GetActiveWeaponInstance();
            Weapon weaponItem = player.GetActiveWeaponItem();
            int itemId = item.ItemId;
            
            // Check if SpiritShot is already active
            if (weaponItem.ChargedSpiritShot != Charged.ChargedNone)
            {
                return;
            }
            // Check for correct grade
            CrystalId weaponGrade = weaponItem.CrystalType;
            if (((weaponGrade == CrystalId.CrystalNone) && (itemId != 3947)) ||
                ((weaponGrade == CrystalId.CrystalD) && (itemId != 3948)) ||
                ((weaponGrade == CrystalId.CrystalC) && (itemId != 3949)) ||
                ((weaponGrade == CrystalId.CrystalB) && (itemId != 3950)) ||
                ((weaponGrade == CrystalId.CrystalA) && (itemId != 3951)) ||
                ((weaponGrade == CrystalId.CrystalS) && (itemId != 3952)))
            {
                await player.SendPacketAsync(new SystemMessage(SystemMessageId.SpiritshotsGradeMismatch));
                return;
            }
            // Charge SoulShot
            weaponItem.ChargedSpiritShot = Charged.ChargedBlessedSpiritShot;
            // Send message to client
            await player.SendPacketAsync(new SystemMessage(SystemMessageId.EnabledSpiritshot));
            PlayerClientServer.ToSelfAndKnownPlayersInRadius(player, new MagicSkillUse(player, player, _skillIds[(int)weaponGrade], 1, 0, 0), 360000);
        }

        public int[] GetItemIds()
        {
            return _itemIds;
        }
    }
}