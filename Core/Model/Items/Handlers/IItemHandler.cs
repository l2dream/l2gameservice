﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Model.Actor;

namespace Core.Model.Items.Handlers
{
    public interface IItemHandler
    {
        Task UseItemAsync(Playable playable, ItemInstance item);
        int[] GetItemIds();
    }
}