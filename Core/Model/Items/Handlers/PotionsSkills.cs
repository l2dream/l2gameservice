﻿using System;
using System.Collections.Generic;

namespace Core.Model.Items.Handlers
{
    public sealed class PotionsSkills
    {
        private static readonly PotionsSkills ManaDrug = new PotionsSkills("ManaDrug", 726, 2003, 1);
        private static readonly PotionsSkills HerbOfLife = new PotionsSkills("HerbOfLife", 8600, 2278, 1);
        private static readonly PotionsSkills GreaterHerbOfLife = new PotionsSkills("GreaterHerbOfLife", 8601, 2278, 2);
        private static readonly PotionsSkills SuperiorHerbOfLife = new PotionsSkills("SuperiorHerbOfLife", 8602, 2278, 3);
        private static readonly PotionsSkills HerbOfMana = new PotionsSkills("HerbOfMana", 8603, 2279, 1);
        private static readonly PotionsSkills GreaterHerbOfMana = new PotionsSkills("GreaterHerbOfMana", 8604, 2279, 2);
        private static readonly PotionsSkills SuperiorHerbOfMana = new PotionsSkills("SuperiorHerbOfMana", 8605, 2279, 3);
        private static readonly PotionsSkills HerbOfStrength = new PotionsSkills("HerbOfStrength", 8606, 2280, 1);
        private static readonly PotionsSkills HerbOfMagic = new PotionsSkills("HerbOfMagic", 8607, 2281, 1);
        private static readonly PotionsSkills HerbOfAtkSpd = new PotionsSkills("HerbOfAtkSpd", 8608, 2282, 1);
        private static readonly PotionsSkills HerbOfCastingSpd = new PotionsSkills("HerbOfCastingSpd", 8609, 2283, 1);
        private static readonly PotionsSkills HerbOfCriticalAttack = new PotionsSkills("HerbOfCriticalAttack", 8610, 2284, 1);
        private static readonly PotionsSkills HerbOfSpeed = new PotionsSkills("HerbOfSpeed", 8611, 2285, 1);
        private static readonly PotionsSkills HerbOfWarrior =
            new PotionsSkills("HerbOfWarrior", 8612, new[] {2280, 2282, 2284}, new[] {1, 1, 1});
        private static readonly PotionsSkills HerbOfMystic = 
            new PotionsSkills("HerbOfMystic", 8613, new [] {2281, 2283}, new [] {1, 1});
        private static readonly PotionsSkills HerbOfRecovery =
            new PotionsSkills("HerbOfRecovery", 8614, new[] {2278, 2279}, new[] {1, 1});
        
        public string PotionType { get; }
        public int PotionId { get; }
        private readonly IDictionary<int, int> _skills = new Dictionary<int, int>();

        private PotionsSkills(string potionType, int potionItem, int skillIdentifier, int skillLevel)
        {
            PotionType = potionType;
            _skills.Add(skillIdentifier, skillLevel);
            PotionId = potionItem;
        }

        private PotionsSkills(string potionType, int potionItem, int[] skillIdentifiers, int[] skillLevels)
        {
            PotionType = potionType;
            for (int i = 0; i < skillIdentifiers.Length; i++)
            {
                _skills.Add(skillIdentifiers[i], skillLevels[i]); // each skill of a particular potion can have just 1 level, not more
            }
            PotionId = potionItem;
        }
    }
}