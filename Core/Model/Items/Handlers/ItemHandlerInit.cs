﻿using System;
using System.Collections.Generic;
using L2Logger;

namespace Core.Model.Items.Handlers
{
    public class ItemHandlerInit
    {
        private readonly IDictionary<int, IItemHandler> _handlers;

        public ItemHandlerInit()
        {
            try
            {
                _handlers = new Dictionary<int, IItemHandler>();
                RegisterItemHandler(new Potions());
                RegisterItemHandler(new ScrollOfEscape());
                RegisterItemHandler(new SoulShots());
                RegisterItemHandler(new SpiritShot());
                RegisterItemHandler(new BlessedSpiritShot());
                LoggerManager.Info("ItemHandler: Loaded " + _handlers.Count + " handlers.");
            }
            catch (Exception ex)
            {
                LoggerManager.Error(ex.Message);
            }
        }

        private void RegisterItemHandler(IItemHandler handler)
        {
            // Get all ID corresponding to the item type of the handler
            int[] ids = handler.GetItemIds();
            // Add handler for each ID found
            foreach (int id in ids)
            {
                _handlers.Add(id, handler);
            }
        }
        
        public IItemHandler GetItemHandler(int itemId)
        {
            return _handlers[itemId];
        }
    }
}