﻿namespace Core.Model.Items
{
    public class ItemHolder
    {
        public int Id { get; }
        public long Count { get; }
	
        public ItemHolder(int id, long count)
        {
            Id = id;
            Count = count;
        }
    }
}