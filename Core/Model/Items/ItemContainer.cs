﻿using System.Collections.Generic;
using System.Linq;
using Core.Model.Actor;
using Core.Model.Player;
using Core.Model.World;

namespace Core.Model.Items
{
    public abstract class ItemContainer
    {
        protected List<ItemInstance> Items { get; }
        protected abstract Character GetOwner();
        protected abstract ItemLocation GetBaseLocation();
        
        protected ItemContainer()
        {
            Items = new List<ItemInstance>();
        }

        /// <summary>
        /// Adds item to inventory
        /// </summary>
        /// <param name="process"></param>
        /// <param name="item"></param>
        /// <param name="actor"></param>
        /// <param name="reference"></param>
        /// <returns></returns>
        public virtual ItemInstance AddItem(ItemAction process, ItemInstance item, PlayerInstance actor, WorldObject reference)
        {
            ItemInstance oldItem = GetItemByItemId(item.ItemId);
            // If stackable item is found in inventory just add to current quantity
            if ((oldItem != null) && oldItem.IsStackable())
            {
                int count = item.Count;
                oldItem.ChangeCount(count);
                oldItem.LastChange = ItemInstance.Modified;
                
                // And destroys the item
                //ItemTable.getInstance().destroyItem(process, item, actor, reference);
                item.ItemModel.UpdateDatabase();
                item = oldItem;
                
                // Updates database
                if ((item.ItemId == 57) && (count < (10000 * 1.0)))
                {
                    // Small adena changes won't be saved to database all the time
                    if ((Initializer.TimeController().GameTicks % 5) == 0)
                    {
                        item.ItemModel.UpdateDatabase();
                    }
                }
                else
                {
                    item.ItemModel.UpdateDatabase();
                }
            }
            // If item hasn't be found in inventory, create new one
            else
            {
                item.SetOwnerId(GetOwnerId());
                item.SetLocation(GetBaseLocation());
                item.LastChange = ItemInstance.Added;
			
                // Add item in inventory
                AddItem(item);
                // Updates database
                item.ItemModel.UpdateDatabase();
            }
            return item;
        }

        /// <summary>
        /// Adds item to inventory
        /// </summary>
        /// <param name="process"></param>
        /// <param name="itemId"></param>
        /// <param name="count"></param>
        /// <param name="actor"></param>
        /// <param name="reference"></param>
        /// <returns></returns>
        public virtual ItemInstance AddItem(ItemAction process, int itemId, int count, PlayerInstance actor, WorldObject reference)
        {
            ItemInstance itemInstance = GetItemByItemId(itemId);
            if (itemInstance != null && itemInstance.IsStackable())
            {
                itemInstance.ChangeCount(count);
                itemInstance.LastChange = ItemInstance.Modified;
                
                // And destroys the item
                //ItemTable.getInstance().destroyItem(process, item, actor, reference);
                itemInstance.ItemModel.UpdateDatabase();
                
                // Updates database
                if ((itemInstance.ItemId == 57) && (count < (10000 * 1.0)))
                {
                    // Small adena changes won't be saved to database all the time
                    if ((Initializer.TimeController().GameTicks % 5) == 0)
                    {
                        itemInstance.ItemModel.UpdateDatabase();
                    }
                }
                else
                {
                    itemInstance.ItemModel.UpdateDatabase();
                }
                return itemInstance;
            }

            // If item hasn't be found in inventory, create new one
            var itemService = Initializer.ItemService();
            Item item = itemService.GetItemById(itemId);
            itemInstance = itemService.CreateItem(process, itemId, item.Stackable ? count : 1, actor, reference);
            itemInstance.SetOwnerId(GetOwnerId());
            itemInstance.SetLocation(GetBaseLocation());
            itemInstance.LastChange = ItemInstance.Added;
            
            // Add item in inventory
            AddItem(itemInstance);
            // Updates database
            itemInstance.ItemModel.UpdateDatabase();

            return itemInstance;
        }
        
        public int GetOwnerId()
        {
            return GetOwner().ObjectId;
        }

        public ItemInstance GetItemByItemId(int itemId)
        {
            return Items?.FirstOrDefault(i => i.ItemId == itemId);
        }

        public ItemInstance GetItemByObjectId(int objectId)
        {
            return Items?.FirstOrDefault(i => i.ObjectId == objectId);
        }
        
        public int GetSize()
        {
            return Items.Count;
        }
        
        protected virtual void AddItem(ItemInstance item)
        {
            lock (Items)
            {
                Items.Add(item);
            }
        }
        
        protected virtual void RemoveItem(ItemInstance item)
        {
            lock (Items)
            {
                Items.Remove(item);
            }
        }
        
        public virtual bool ValidateCapacity(int slots)
        {
            return true;
        }
	
        public virtual bool ValidateWeight(int weight)
        {
            return true;
        }
        
        public List<ItemInstance> GetItems()
        {
            return Items;
        }
        
        /// <summary>
        /// Get warehouse adena
        /// </summary>
        /// <returns></returns>
        public virtual int GetAdena()
        {
            return Items.FirstOrDefault(item => item.ItemId == 57).Count;
        }
        
        public virtual ItemInstance DestroyItem(ItemAction process, int objectId, int count, PlayerInstance actor, WorldObject reference)
        {
            ItemInstance item = GetItemByObjectId(objectId);
            if (item == null)
            {
                return null;
            }
		
            // Adjust item quantity
            if (item.Count > count)
            {
                item.ChangeCount(-count);
                item.LastChange = ItemInstance.Modified;
                item.ItemModel.UpdateDatabase();
                return item;
            }
            // Directly drop entire item
            return DestroyItem(process, item, actor, reference);
        }

        public ItemInstance DestroyItem(ItemAction process, ItemInstance item, PlayerInstance actor, WorldObject reference)
        {
            // check if item is present in this container
            if (!Items.Contains(item))
            {
                return null;
            }
			
            RemoveItem(item);
            Initializer.ItemService().DestroyItem(process, item, actor, reference);
            item.ItemModel.UpdateDatabase();
            return item;
        }
    }
}