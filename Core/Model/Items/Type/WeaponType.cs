﻿using System.Collections.Generic;
using System.Linq;

namespace Core.Model.Items.Type
{
    public sealed class WeaponType
    {
        public WeaponTypeId Id { get; set; }
        public string TypeName { get; set; }
        
        private static readonly WeaponType None = new WeaponType(WeaponTypeId.None, "none");
        private static readonly WeaponType Shield = new WeaponType(WeaponTypeId.None, "shield");
        private static readonly WeaponType Blunt = new WeaponType(WeaponTypeId.Blunt, "blunt");
        private static readonly WeaponType Sword = new WeaponType(WeaponTypeId.Sword, "sword");
        private static readonly WeaponType Dagger = new WeaponType(WeaponTypeId.Dagger, "dagger");
        private static readonly WeaponType Bow = new WeaponType(WeaponTypeId.Bow, "bow");
        private static readonly WeaponType Pole = new WeaponType(WeaponTypeId.Pole, "pole");
        private static readonly WeaponType Etc = new WeaponType(WeaponTypeId.Etc, "etc");
        private static readonly WeaponType Fist = new WeaponType(WeaponTypeId.Fist, "fist");
        private static readonly WeaponType DualSword = new WeaponType(WeaponTypeId.Dual, "dual");
        private static readonly WeaponType DualFist = new WeaponType(WeaponTypeId.DualFist, "dualfist");
        private static readonly WeaponType BigSword = new WeaponType(WeaponTypeId.BigSword, "bigsword");
        private static readonly WeaponType Pet = new WeaponType(WeaponTypeId.Pet, "pet");
        private static readonly WeaponType Rod = new WeaponType(WeaponTypeId.Rod, "rod");
        private static readonly WeaponType BigBlunt = new WeaponType(WeaponTypeId.BigBlunt, "bigblunt");

        public WeaponType(WeaponTypeId weaponTypeId, string weaponTypeName)
        {
            Id = weaponTypeId;
            TypeName = weaponTypeName;
        }

        private static IEnumerable<WeaponType> Values
        {
            get
            {
                yield return None;
                yield return Shield;
                yield return Blunt;
                yield return Sword;
                yield return Dagger;
                yield return Bow;
                yield return Pole;
                yield return Etc;
                yield return Fist;
                yield return DualSword;
                yield return DualFist;
                yield return BigSword;
                yield return Pet;
                yield return Rod;
                yield return BigBlunt;
            }
        }
        
        public static WeaponType GetWeaponTypeId(string type)
        {
            return Values.FirstOrDefault(w => w.TypeName == type);
        }
        
        public static WeaponType GetWeaponTypeIdByValue(string type)
        {
            return Values.FirstOrDefault(w => w.Id.ToString() == type);
        }
        
        public int Mask()
        {
            return 1 << ((int)Id);
        }
    }
}


