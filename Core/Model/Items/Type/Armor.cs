﻿using System;
using System.Collections.Generic;
using Core.Model.Actor;
using Core.Model.CalculateStats;
using Core.Model.CalculateStats.Functions;

namespace Core.Model.Items.Type
{
    public class Armor : Item 
    {
        public ArmorType ArmorType { get; }
        public int AvoidModifier { get; }
        public int PDef { get; }
        public int MDef { get; }
        public int MpBonus { get; }
        public int HpBonus { get; }

        public Armor(StatSet set, ItemSlot slot) : base(set, slot)
        {
            ArmorType = ArmorType.GetArmorTypeId(set.GetString("armor_type"));
            AvoidModifier = set.GetInt("avoid_modify");
            PDef = set.GetInt("p_def");
            MDef = set.GetInt("m_def");
            MpBonus = set.GetInt("mp_bonus");
            HpBonus = set.GetInt("hp_bonus");

            if (BodyPart == ItemSlotId.SlotNeck ||
                BodyPart == ItemSlotId.SlotHair ||
                BodyPart == ItemSlotId.SlotFace ||
                BodyPart == ItemSlotId.SlotDHair ||
                BodyPart == ItemSlotId.SlotLEar ||
                BodyPart == ItemSlotId.SlotLFinger)
            {
                Type1 = Type1.Type1WeaponRingEarringNecklace;
                Type2 = Type2.Type2Accessory;
            }
            else
            {
                Type1 = Type1.Type1ShieldArmor;
                Type2 = Type2.Type2ShieldArmor;
            }
        }

        public override int GetItemMask()
        {
            return ArmorType.Mask();
        }
        
        public override FunctionObject[] GetStatFuncs(ItemInstance instance, Character character)
        {
            List<FunctionObject> funcs = new List<FunctionObject>();
            if (_funcTemplates != null)
            {
                foreach (FuncTemplate t in _funcTemplates)
                {
                    Env env = new Env();
                    env.Player = character;
                    env.Item = instance;
                    FunctionObject f = t.GetFunc(env, instance);
                    if (f != null)
                    {
                        funcs.Add(f);
                    }
                }
            }
            return funcs.ToArray();
        }
    }
}