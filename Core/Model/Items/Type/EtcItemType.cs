﻿using System.Collections.Generic;
using System.Linq;

namespace Core.Model.Items.Type
{
    public sealed class EtcItemType
    {
        public EtcItemTypeId Id { get; }
        public string TypeName { get; }
        
        private static readonly EtcItemType Arrow = new EtcItemType(EtcItemTypeId.Arrow, "arrow");
        private static readonly EtcItemType Material = new EtcItemType(EtcItemTypeId.Material, "material");
        private static readonly EtcItemType PetCollar = new EtcItemType(EtcItemTypeId.PetCollar, "pet_collar");
        private static readonly EtcItemType Potion = new EtcItemType(EtcItemTypeId.Potion, "potion");
        private static readonly EtcItemType Receipe = new EtcItemType(EtcItemTypeId.Receipe, "receipe");
        private static readonly EtcItemType Scroll = new EtcItemType(EtcItemTypeId.Scroll, "scroll");
        private static readonly EtcItemType Quest = new EtcItemType(EtcItemTypeId.Quest, "quest");
        private static readonly EtcItemType Money = new EtcItemType(EtcItemTypeId.Money, "money");
        private static readonly EtcItemType Other = new EtcItemType(EtcItemTypeId.Other, "other");
        private static readonly EtcItemType SpellBook = new EtcItemType(EtcItemTypeId.SpellBook, "spellbook");
        private static readonly EtcItemType Seed = new EtcItemType(EtcItemTypeId.Seed, "seed");
        private static readonly EtcItemType Shot = new EtcItemType(EtcItemTypeId.Shot, "shot");
        private static readonly EtcItemType Herb = new EtcItemType(EtcItemTypeId.Herb, "herb");
        

        public EtcItemType(EtcItemTypeId etcItemTypeId, string etcItemTypeName)
        {
            Id = etcItemTypeId;
            TypeName = etcItemTypeName;
        }
        
        private static IEnumerable<EtcItemType> Values
        {
            get
            {
                yield return Arrow;
                yield return Material;
                yield return PetCollar;
                yield return Potion;
                yield return Scroll;
                yield return Quest;
                yield return Money;
                yield return Other;
                yield return SpellBook;
                yield return Seed;
                yield return Shot;
                yield return Herb;
            }
        }
        
        public static EtcItemType GetEtcItemTypeId(string type)
        {
            return Values.FirstOrDefault(w => w.TypeName == type);
        }
        
        public int Mask()
        {
            return 1 << ((int)Id + 21);
        }
    }
}