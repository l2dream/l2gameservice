﻿using System;
using System.Collections.Generic;
using Core.Model.Actor;
using Core.Model.CalculateStats;
using Core.Model.CalculateStats.Functions;

namespace Core.Model.Items.Type
{
    public class Weapon : Item
    {
        public int SoulShotCount { get; }
        public int SpiritShotCount { get; }
        public int PDam { get; }
        public int MDam { get; }
        public int RndDam { get; }
        public int Critical { get; }
        public double HitModifier { get; }
        public int AvoidModifier { get; }
        public int ShieldDef { get; }
        public int ShieldDefRate { get; }
        public int AtkSpeed { get; }
        public int AtkReuse { get; }
        public int MpConsume { get; }
        public WeaponType WeaponType { get; }
        public Charged ChargedSoulShot { get; set; }
        public Charged ChargedSpiritShot { get; set; }
        
        public Weapon(StatSet set, ItemSlot slot) : base(set, slot)
        {
            WeaponType = WeaponType.GetWeaponTypeId(set.GetString("weapon_type"));
            SoulShotCount = set.GetInt("soulshots");
            SpiritShotCount = set.GetInt("spiritshots");
            PDam = set.GetInt("p_dam");
            RndDam = set.GetInt("rnd_dam");
            Critical = set.GetInt("critical");
            HitModifier = set.GetDouble("hit_modify");
            AvoidModifier = set.GetInt("avoid_modify");
            ShieldDef = set.GetInt("shield_def");
            ShieldDefRate = set.GetInt("shield_def_rate");
            AtkSpeed = set.GetInt("atk_speed");
            AtkReuse = set.GetInt("atk_reuse", (WeaponType.Id == WeaponTypeId.Bow)? 1500 : 0);
            MpConsume = set.GetInt("mp_consume");
            MDam = set.GetInt("m_dam");

            if (WeaponType == WeaponType.GetWeaponTypeId("none"))
            {
                Type1 = Type1.Type1ShieldArmor;
                Type2 = Type2.Type2ShieldArmor;
            }
            else
            {
                Type1 = Type1.Type1WeaponRingEarringNecklace;
                Type2 = Type2.Type2Weapon;
            }
        }
        
        public override int GetItemMask()
        {
            return WeaponType.Mask();
        }

        public override FunctionObject[] GetStatFuncs(ItemInstance instance, Character character)
        {
            List<FunctionObject> funcs = new List<FunctionObject>();
            if (_funcTemplates != null)
            {
                foreach (FuncTemplate t in _funcTemplates)
                {
                    Env env = new Env();
                    env.Player = character;
                    env.Item = instance;
                    FunctionObject f = t.GetFunc(env, instance);
                    if (f != null)
                    {
                        funcs.Add(f);
                    }
                }
            }
            return funcs.ToArray();
        }
    }
}