﻿namespace Core.Model.Items.Type
{
    public enum EtcItemTypeId
    {
        Arrow,
        Material,
        PetCollar,
        Potion,
        Receipe,
        Scroll,
        Quest,
        Money,
        Other,
        SpellBook,
        Seed,
        Shot,
        Herb
    }
}