﻿using System.Collections.Generic;
using System.Linq;

namespace Core.Model.Items.Type
{
    public sealed class ArmorType
    {
        public ArmorTypeId Id { get; }
        public string TypeName { get; }
        
        private static readonly ArmorType None = new ArmorType(ArmorTypeId.None, "none");
        private static readonly ArmorType Light = new ArmorType(ArmorTypeId.Light, "light");
        private static readonly ArmorType Heavy = new ArmorType(ArmorTypeId.Heavy, "heavy");
        private static readonly ArmorType Magic = new ArmorType(ArmorTypeId.Magic, "magic");
        private static readonly ArmorType Pet = new ArmorType(ArmorTypeId.Pet, "pet");

        public ArmorType(ArmorTypeId armorTypeId, string armorTypeName)
        {
            Id = armorTypeId;
            TypeName = armorTypeName;
        }
        
        private static IEnumerable<ArmorType> Values
        {
            get
            {
                yield return None;
                yield return Light;
                yield return Heavy;
                yield return Magic;
                yield return Pet;
            }
        }
        
        public static ArmorType GetArmorTypeId(string type)
        {
            return Values.FirstOrDefault(w => w.TypeName == type);
        }
        
        public int Mask()
        {
            return 1 << ((int)Id + 16);
        }
    }
}