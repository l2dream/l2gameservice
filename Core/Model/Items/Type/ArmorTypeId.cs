﻿namespace Core.Model.Items.Type
{
    public enum ArmorTypeId
    {
        None = 1,
        Light = 2,
        Heavy = 3,
        Magic = 4,
        Pet = 5
    }
}