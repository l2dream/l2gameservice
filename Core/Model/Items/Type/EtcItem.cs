﻿namespace Core.Model.Items.Type
{
    public class EtcItem : Item
    {
        public EtcItemType EtcItemType { get; }
        
        public EtcItem(object type, StatSet set, ItemSlot slot) : base(set, slot)
        {
            EtcItemType = (EtcItemType) type;

            Type1 = Type1.Type1ItemQuestItemAdena;
            Type2 = Type2.Type2Other;
        }
        
        public override int GetItemMask()
        {
            return EtcItemType.Mask();
        }
    }
}