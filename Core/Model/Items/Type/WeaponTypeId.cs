﻿namespace Core.Model.Items.Type
{
    public enum WeaponTypeId
    {
        None,
        Shield,
        Sword,
        Blunt,
        Dagger,
        Bow,
        Pole,
        Etc,
        Fist,
        Dual,
        DualFist,
        BigSword,
        Pet,
        Rod,
        BigBlunt
    }
}