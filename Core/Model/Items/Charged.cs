﻿namespace Core.Model.Items
{
    public enum Charged
    {
        ChargedNone,
        ChargedSoulShot,
        ChargedSpiritShot,
        ChargedBlessedSoulShot,
        ChargedBlessedSpiritShot
    }
}