﻿namespace Core.Model.Items
{
    public enum ItemAction
    {
        Pickup,
        Loot,
        Consume,
        Init,
        Restore,
        Buy,
        Sell,
        Destroy
    }
}