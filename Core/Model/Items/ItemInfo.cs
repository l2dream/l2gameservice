﻿namespace Core.Model.Items
{
    public class ItemInfo
    {
        public int ObjectId { get; set; }
        public Item Item { get; set; }
        public int Enchant { get; set; }
        public int AugmentationBonus { get; set; }
        public int Count { get; set; }
        public int Price { get; set; }
        public int CustomType1 { get; set; }
        public int CustomType2 { get; set; }
        public int Equipped { get; set; }
        public int Change { get; set; } // 1=ADD, 2=MODIFY, 3=REMOVE
        public int Mana { get; set; }

        public ItemInfo(ItemInstance item)
        {
            // Get the action to do clientside
            switch (item.LastChange)
            {
                case ItemInstance.Added:
                {
                    Change = 1;
                    break;
                }
                case ItemInstance.Modified:
                {
                    Change = 2;
                    break;
                }
                case ItemInstance.Removed:
                {
                    Change = 3;
                    break;
                }
            }

            Init(item);
        }

        public ItemInfo(ItemInstance item, int change)
        {
            Change = change;
            Init(item);
        }

        private void Init(ItemInstance item)
        {
            // Get the Identifier of the ItemInstance
            ObjectId = item.ObjectId;

            // Get the Item of the ItemInstance
            Item = item.Item;

            // Get the enchant level of the ItemInstance
            Enchant = item.EnchantLevel;
            AugmentationBonus = 0; //TODO need add logic

            // Get the quantity of the ItemInstance
            Count = item.Count;
            // Get custom item types (used loto, race tickets)
            CustomType1 = item.CustomType1;
            CustomType2 = item.CustomType2;

            // Verify if the ItemInstance is equipped
            Equipped = item.IsEquipped() ? 1 : 0;

            // Get shadow item mana
            Mana = item.Mana;
        }
    }
}