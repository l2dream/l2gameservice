﻿namespace Core.Model.Items
{
    public enum Type2
    {
        Type2Weapon = 0,
        Type2ShieldArmor = 1,
        Type2Accessory = 2,
        Type2Quest = 3,
        Type2Money = 4,
        Type2Other = 5,
        Type2PetWolf = 6,
        Type2PetHatchLing = 7,
        Type2PetStrider = 8,
        Type2PetBaby = 9
    }
}