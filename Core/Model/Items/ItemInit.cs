﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Core.Model.CalculateStats;
using Core.Model.Items.Type;
using Core.Model.Player;
using Core.Model.World;
using L2Logger;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Model.Items
{
    public sealed class ItemInit : ItemSkillAbstract
    {
        private readonly IDictionary<string, System.Type> _types;
        private ItemDataHolder _currentItem;
        private List<Item> _itemsInFile;
        
        private readonly IDictionary<int, Armor> _armors;
        private readonly IDictionary<int, Weapon> _weapons;
        private readonly IDictionary<int, EtcItem> _etcItems;

        private readonly ItemSlot _itemSlot;
        private readonly string _basePath;

        private int tmpItemId;
        
        public IDictionary<int, Item> Items { get; }

        public ItemInit()
        {
            Items = new Dictionary<int, Item>();
            _itemSlot = new ItemSlot();
            _weapons = new Dictionary<int, Weapon>();
            _armors = new Dictionary<int, Armor>();
            _etcItems = new Dictionary<int, EtcItem>();
            _types = new Dictionary<string, System.Type>
                {{"Weapon", typeof(Weapon)}, {"Armor", typeof(Armor)}, {"EtcItem", typeof(EtcItem)}};
            _itemsInFile = new List<Item>();
            _basePath = Initializer.Config().ServerConfig.StaticData + "/Xml/Stats/Items/";
            Initialize();
        }

        private void Initialize()
        {
            XmlDocument doc = new XmlDocument();
            string[] xmlFilesArray = Directory.GetFiles(_basePath);

            try
            {
                foreach (string i in xmlFilesArray)
                {
                    doc.Load(i);
                    XmlNodeList nodes = doc.DocumentElement?.SelectNodes("/list/item");
                    if (nodes == null)
                    {
                        continue;
                    }

                    foreach (XmlNode node in nodes)
                    {
                        XmlElement ownerElement = node.Attributes?[0].OwnerElement;
                        if (ownerElement != null && node.Attributes != null && ownerElement.Name == "item")
                        {
                            _currentItem = new ItemDataHolder();
                            ParseItem(node);
                            _itemsInFile.Add(_currentItem.Item);
                            _tables.Clear();
                        }
                    }
                }
                LoggerManager.Info($"Loaded {Items.Count} Items");
                LoggerManager.Info($"Loaded {_weapons.Count} Weapons");
                LoggerManager.Info($"Loaded {_armors.Count} Armors");
                LoggerManager.Info($"Loaded {_etcItems.Count} EtcItems");
            } catch (Exception ex)
            {
                LoggerManager.Error($"Message: '{ex.Message}' {tmpItemId} ");
                throw;
            }
        }

        private void ParseItem(XmlNode node)
        {
            XmlNamedNodeMap attrs = node.Attributes;
            int itemId = int.Parse(attrs.GetNamedItem("id").Value);
            tmpItemId = itemId;
            string itemName = attrs.GetNamedItem("name").Value;
            string itemType = attrs.GetNamedItem("type").Value;
            
            _currentItem.Id = itemId;
            _currentItem.Name = itemName;
            _currentItem.Set = new StatSet();
            _currentItem.Set.Add("item_id", itemId.ToString());
            _currentItem.Set.Add("name", itemName);
            
            /*
            StatSet set = new StatSet();
            set.Add("item_id", itemId.ToString());
            set.Add("name", itemName);
            */
            
            foreach (XmlNode innerData in node.ChildNodes)
            {
                if (innerData.Name.Equals("set"))
                {
                    var attrsInner = innerData.Attributes;
                    if (attrsInner?["name"] == null || attrsInner["val"] == null) continue;
                    //string value = innerData.Attributes["val"].Value;
                    //string name = innerData.Attributes["name"].Value;
                    ParseBeanSet(innerData, _currentItem.Set, 1);
                    //set.Add(name, value);
                }
            }

            if (itemType.Equals("Weapon"))
            {
                ParseWeapon();
            } 
            else if (itemType.Equals("Armor"))
            {
                ParseArmor();
            }
            else
            {
                ParseEtcItems();
            }

            foreach (XmlNode innerData in node.ChildNodes)
            {
                if (innerData.Name.Equals("for"))
                {
                    MakeItem();
                    ParseTemplate(innerData, _currentItem.Item);
                }
            }

            MakeItem();
        }

        private void MakeItem()
        {
            if (_currentItem.Item != null)
            {
                return;
            }
            //var item  = (Item)Activator.CreateInstance(_types[itemType], _currentItem.Set, _itemSlot);
            //Items.Add(itemId, item);

            switch (_currentItem.Type)
            {
                case WeaponType weapon:
                    _currentItem.Item = new Weapon(_currentItem.Set, _itemSlot);
                    _weapons.Add(_currentItem.Id, (Weapon) _currentItem.Item);
                    break;
                case ArmorType armor:
                    _currentItem.Item = new Armor(_currentItem.Set, _itemSlot);
                    _armors.Add(_currentItem.Id, (Armor) _currentItem.Item);
                    break;
                case EtcItemType etcItem:
                    _currentItem.Item = new EtcItem( _currentItem.Type, _currentItem.Set, _itemSlot);
                    _etcItems.Add(_currentItem.Id, (EtcItem) _currentItem.Item);
                    break;
            }
            Items.Add(_currentItem.Id, _currentItem.Item);
        }
        
        private void ParseWeapon()
        {
            try
            {
                int bodypart = (int) _itemSlot.Slots[_currentItem.Set.GetString("bodypart")];
                _currentItem.Type = WeaponType.GetWeaponTypeId(_currentItem.Set.GetString("weapon_type"));

                // lets see if this is a shield
                if (_currentItem.Type == WeaponType.GetWeaponTypeId("none"))
                {
                    _currentItem.Set.Add("type1", Type1.Type1ShieldArmor.ToString());
                    _currentItem.Set.Add("type2", Type2.Type2ShieldArmor.ToString());
                }
                else
                {
                    _currentItem.Set.Add("type1", Type1.Type1WeaponRingEarringNecklace.ToString());
                    _currentItem.Set.Add("type2", Type2.Type2Weapon.ToString());
                }

                if (_currentItem.Type == WeaponType.GetWeaponTypeId("pet"))
                {
                    _currentItem.Set.Add("type1", Type1.Type1WeaponRingEarringNecklace.ToString());

                    switch (_currentItem.Set.GetString("bodypart"))
                    {
                        case "wolf":
                        {
                            _currentItem.Set.Add("type2", Type2.Type2PetWolf.ToString());
                            break;
                        }
                        case "hatchling":
                        {
                            _currentItem.Set.Add("type2", Type2.Type2PetHatchLing.ToString());
                            break;
                        }
                        case "babypet":
                        {
                            _currentItem.Set.Add("type2", Type2.Type2PetBaby.ToString());
                            break;
                        }
                        default:
                        {
                            _currentItem.Set.Add("type2", Type2.Type2PetStrider.ToString());
                            break;
                        }
                    }

                    //bodypart = (int) ItemSlotId.SlotRHand;
                }

                //_currentItem.Set.Delete("bodypart");
                //_currentItem.Set.Add("bodypart", bodypart.ToString());
            }
            catch (Exception ex)
            {
                LoggerManager.Error(ex.Message);
            }
        }

        private void ParseArmor()
        {
            try
            {
                int bodypart = (int) _itemSlot.Slots[_currentItem.Set.GetString("bodypart")];
                _currentItem.Type = ArmorType.GetArmorTypeId(_currentItem.Set.GetString("armor_type"));
                
                if ((bodypart == (int)ItemSlotId.SlotNeck) || 
                    (bodypart == (int)ItemSlotId.SlotHair) || 
                    (bodypart == (int)ItemSlotId.SlotFace) || 
                    (bodypart == (int)ItemSlotId.SlotDHair) || ((bodypart & (int)ItemSlotId.SlotLEar) != 0) || ((bodypart & (int)ItemSlotId.SlotLFinger) != 0))
                {
                    _currentItem.Set.Add("type1", Type1.Type1WeaponRingEarringNecklace.ToString());
                    _currentItem.Set.Add("type2", Type2.Type2Accessory.ToString());
                }
                else
                {
                    _currentItem.Set.Add("type1", Type1.Type1ShieldArmor.ToString());
                    _currentItem.Set.Add("type2", Type2.Type2ShieldArmor.ToString());
                }
			
                if (_currentItem.Type == ArmorType.GetArmorTypeId("pet"))
                {
                    _currentItem.Set.Add("type1", Type1.Type1ShieldArmor.ToString());
				
                    switch (_currentItem.Set.GetString("bodypart"))
                    {
                        case "wolf":
                        {
                            _currentItem.Set.Add("type2", Type2.Type2PetWolf.ToString());
                            break;
                        }
                        case "hatchling":
                        {
                            _currentItem.Set.Add("type2", Type2.Type2PetHatchLing.ToString());
                            break;
                        }
                        case "babypet":
                        {
                            _currentItem.Set.Add("type2", Type2.Type2PetBaby.ToString());
                            break;
                        }
                        default:
                        {
                            _currentItem.Set.Add("type2", Type2.Type2PetStrider.ToString());
                            break;
                        }
                    }
                    //bodypart = (int)ItemSlotId.SlotChest;
                }
                //_currentItem.Set.Delete("bodypart");
                //_currentItem.Set.Add("bodypart", bodypart.ToString());
            }
            catch (Exception ex)
            {
                LoggerManager.Error(ex.Message);
            }
        }

        private void ParseEtcItems()
        {
            try
            {
                _currentItem.Set.Add("type1", Type1.Type1ItemQuestItemAdena.ToString());
                _currentItem.Set.Add("type2", Type2.Type2Other.ToString());
                
                string itemType = _currentItem.Set.GetString("item_type");
                switch (itemType)
                {
                    case "none":
                    {
                        _currentItem.Type = EtcItemType.GetEtcItemTypeId("other"); // only for default
                        break;
                    }
                    case "castle_guard":
                    {
                        _currentItem.Type = EtcItemType.GetEtcItemTypeId("scroll"); // dummy
                        break;
                    }
                    case "pet_collar":
                    {
                        _currentItem.Type = EtcItemType.GetEtcItemTypeId("pet_collar");
                        break;
                    }
                    case "potion":
                    {
                        _currentItem.Type = EtcItemType.GetEtcItemTypeId("potion");
                        break;
                    }
                    case "recipe":
                    {
                        _currentItem.Type = EtcItemType.GetEtcItemTypeId("receipe");
                        break;
                    }
                    case "scroll":
                    {
                        _currentItem.Type = EtcItemType.GetEtcItemTypeId("scroll");
                        break;
                    }
                    case "seed":
                    {
                        _currentItem.Type = EtcItemType.GetEtcItemTypeId("seed");
                        break;
                    }
                    case "shot":
                    {
                        _currentItem.Type = EtcItemType.GetEtcItemTypeId("shot");
                        break;
                    }
                    case "spellbook":
                    {
                        _currentItem.Type = EtcItemType.GetEtcItemTypeId("spellbook"); // Spellbook, Amulet, Blueprint
                        break;
                    }
                    case "herb":
                    {
                        _currentItem.Type = EtcItemType.GetEtcItemTypeId("herb");
                        break;
                    }
                    case "arrow":
                    {
                        _currentItem.Type = EtcItemType.GetEtcItemTypeId("arrow");
                        _currentItem.Set.Add("bodypart", "lhand");
                        break;
                    }
                    case "quest":
                    {
                        _currentItem.Type = EtcItemType.GetEtcItemTypeId("quest");
                        _currentItem.Set.Add("type2", Type2.Type2Quest.ToString());
                        break;
                    }
                    case "lure":
                    {
                        _currentItem.Type = EtcItemType.GetEtcItemTypeId("other");
                        _currentItem.Set.Add("bodypart", "lhand");
                        break;
                    }
                    default:
                    {
                        _currentItem.Type = EtcItemType.GetEtcItemTypeId("other");
                        break;
                    }
                }
                
                string consume = _currentItem.Set.GetString("consume_type");
                switch (consume)
                {
                    case "asset":
                    {
                        _currentItem.Type = EtcItemType.GetEtcItemTypeId("money");
                        _currentItem.Set.Add("stackable", "true");
                        _currentItem.Set.Add("type2", Type2.Type2Money.ToString());
                        break;
                    }
                    case "stackable":
                    {
                        _currentItem.Set.Add("stackable", "true");
                        break;
                    }
                    default:
                    {
                        _currentItem.Set.Add("stackable", "false");
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Error(ex.Message);
            }
        }

        public Item GetItemById(int itemId)
        {
            return Items[itemId];
        }

        public ItemInstance CreateItem(ItemAction process, int itemId, int count, PlayerInstance actor, WorldObject reference)
        {
            // Create and Init the ItemInstance corresponding to the Item Identifier
            var idFactory = Initializer.ServiceProvider.GetService<IdFactory>();
            var world = Initializer.WorldInit();
            ItemInstance item = new ItemInstance(idFactory.NextId(), itemId);
            // create loot schedule also if autoloot is enabled
            //TODO
            
            // Add the ItemInstance object to _allObjects of L2world
            world.StoreObject(item);
            // Set Item parameters
            if (item.IsStackable() && (count > 1))
            {
                item.Count = count;
            }
            return item;
        }
        
        public ItemInstance CreateDummyItem(int itemId)
        {
            try
            {
                Item item = GetItemById(itemId);
                if (item == null)
                {
                    throw new Exception(GetType().Name + $": ItemId{itemId} not found");
                }
                return new ItemInstance(0, itemId);
            }
            catch (Exception e)
            {
                // this can happen if the item templates were not initialized
                LoggerManager.Error(GetType().Name + " " + e.Message);
                throw;
            }
        }

        public void DestroyItem(ItemAction process, ItemInstance item, PlayerInstance actor, WorldObject reference)
        {
            item.Count = 0;
            item.SetOwnerId(0);
            item.SetLocation(ItemLocation.Void);
            item.LastChange = ItemInstance.Removed;
			
            Initializer.WorldInit().RemoveObject(item);
        }

    }
}