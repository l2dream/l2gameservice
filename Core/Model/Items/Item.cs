﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Model.Actor;
using Core.Model.CalculateStats;
using Core.Model.CalculateStats.Functions;
using Core.Model.Skills;
using Core.Model.Skills.Effects;

namespace Core.Model.Items
{
    public abstract class Item
    {
	
		private int[] _crystalItemId =
		{
			0,
			1458,
			1459,
			1460,
			1461,
			1462
		};
		
		private int[] _crystalEnchantBonusArmor =
		{
			0,
			11,
			6,
			11,
			19,
			25
		};
		private int[] _crystalEnchantBonusWeapon =
		{
			0,
			90,
			45,
			67,
			144,
			250
		};
		
		public int ItemId { get; set; }
		public object TypeId { get; set; }
		public string Name { get; set; }
		public string Icon { get; set; }
		public Type1 Type1 { get; set; } // needed for item list (inventory)
		public Type2 Type2 { get; set; } // different lists for armor, weapon, etc
		public int Weight { get; set; }
		public bool Crystallizable { get; set; }
		public bool Stackable { get; set; }
		public CrystalId CrystalType { get; set; } // default to none-grade
		public int Duration { get; set; }
		public ItemSlotId BodyPart { get; set; }
		public int ReferencePrice { get; set; }
		public int CrystalCount { get; set; }
		public bool Sellable { get; set; }
		public bool Dropable { get; set; }
		public bool Destroyable { get; set; }
		public bool Tradeable { get; set; }

		public virtual bool Consumable { get; } = false;
		
		protected FuncTemplate[] _funcTemplates;
		protected EffectTemplate[] EffectTemplates { get; set; }
		protected Skill[] _skills;
	
		private static FunctionObject[] _emptyFunctionSet = new FunctionObject[0];
		protected static Effect[] _emptyEffectSet = new Effect[0];

		public Item(StatSet set, ItemSlot slot)
		{
			ItemId = set.GetInt("item_id");
			Name = set.GetString("name");
			Icon = set.GetString("icon");
			//Type1 = Convert.ToInt32(set["type1"]);
			//Type2 = Convert.ToInt32(set["type2"]);

			Weight = set.GetInt("weight");
			Crystallizable = set.GetBoolean("crystallizable");
			Stackable = set.GetBoolean("stackable");
			Duration =  set.GetInt("duration", -1);
			var bodyPart = set.GetString("bodypart");
			BodyPart = bodyPart == string.Empty ? ItemSlotId.SlotNone : slot.Slots[bodyPart];
			ReferencePrice = set.GetInt("price"); 
			CrystalCount = set.GetInt("crystal_count");
			Sellable = set.GetBoolean("sellable", true);
			Dropable = set.GetBoolean("dropable", true);
			Destroyable = set.GetBoolean("destroyable", true);
			Tradeable = set.GetBoolean("tradeable", true);

			var crystalType = set.GetString("crystal_type");
			CrystalType = crystalType switch
			{
				"d" => CrystalId.CrystalD,
				"c" => CrystalId.CrystalC,
				"b" => CrystalId.CrystalB,
				"a" => CrystalId.CrystalA,
				"s" => CrystalId.CrystalS,
				_ => CrystalId.CrystalNone
			};
		}
		
		public abstract int GetItemMask();

		public void Attach(EffectTemplate effect)
		{
			if (EffectTemplates == null)
			{
				EffectTemplates = new EffectTemplate[]
				{
					effect
				};
			}
			else
			{
				int len = EffectTemplates.Length;
				EffectTemplate[] tmp = new EffectTemplate[len + 1];
				// Definition : arraycopy(array source, begins copy at this position of source, array destination, begins copy at this position in dest,
				// number of components to be copied)
				Array.Copy(EffectTemplates, 0, tmp, 0, len);
				tmp[len] = effect;
				EffectTemplates = tmp;
			}
		}
		
		public void Attach(FuncTemplate f)
		{
			// If _functTemplates is empty, create it and add the FuncTemplate f in it
			if (_funcTemplates == null)
			{
				_funcTemplates = new FuncTemplate[]
				{
					f
				};
			}
			else
			{
				int len = _funcTemplates.Length;
				FuncTemplate[] tmp = new FuncTemplate[len + 1];
				// Definition : arraycopy(array source, begins copy at this position of source, array destination, begins copy at this position in dest, number of components to be copied)
				Array.Copy(_funcTemplates, 0, tmp, 0, len);
				tmp[len] = f;
				_funcTemplates = tmp;
			}
		}
		
		public virtual FunctionObject[] GetStatFuncs(ItemInstance instance, Character creature)
		{
			if (_funcTemplates == null)
			{
				return _emptyFunctionSet;
			}
			List<FunctionObject> funcs = new List<FunctionObject>();
			foreach (FuncTemplate t in _funcTemplates)
			{
				Env env = new Env();
				env.Player = creature;
				env.Target = creature;
				env.Item = instance;
				FunctionObject f = t.GetFunc(env, this); // skill is owner
				if (f != null)
				{
					funcs.Add(f);
				}
			}
			if (funcs.Count == 0)
			{
				return _emptyFunctionSet;
			}
			return funcs.ToArray();
		}
    }
}