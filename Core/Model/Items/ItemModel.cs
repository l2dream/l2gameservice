﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataBase.Entities;
using DataBase.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Model.Items
{
    public class ItemModel
    {
        private readonly ItemInstance _itemInstance;

        public ItemModel()
        {
            
        }
        
        public ItemModel(ItemInstance itemInstance)
        {
            _itemInstance = itemInstance;
        }

        public void UpdateDatabase()
        {
            if (_itemInstance.Wear)
            {
                return;
            }
            if (_itemInstance.ExistsInDb)
            {
                if ((_itemInstance.GetOwnerId() == 0) 
                    || (_itemInstance.ItemLocation == ItemLocation.Void) 
                    || ((_itemInstance.Count == 0) && (_itemInstance.ItemLocation != ItemLocation.Lease)))
                {
                    RemoveFromDb();
                }
                else
                {
                    UpdateInDb();
                }
            }
            else
            {
                if ((_itemInstance.Count == 0) && (_itemInstance.ItemLocation != ItemLocation.Lease))
                {
                    return;
                }
                if ((_itemInstance.ItemLocation == ItemLocation.Void) || (_itemInstance.GetOwnerId() == 0))
                {
                    return;
                }
                InsertIntoDb();
            }
        }

        /// <summary>
        /// Insert the item in database
        /// </summary>
        private void InsertIntoDb()
        {
            IItemRepository itemRepository =
                Initializer.ServiceProvider.GetService<IUnitOfWork>()?.Items;
             if (itemRepository != null) itemRepository.CreateItemAsync(ConvertToEntity());

            _itemInstance.ExistsInDb = true;
            _itemInstance.StoredInDb = true;
        }

        private ItemEntity ConvertToEntity()
        {
            ItemEntity itemEntity = new ItemEntity
            {
                OwnerId = _itemInstance.GetOwnerId(),
                ItemId = _itemInstance.ItemId,
                Count = _itemInstance.Count,
                Loc = _itemInstance.ItemLocation.ToString(),
                LocData = _itemInstance.LocData,
                EnchantLevel = _itemInstance.EnchantLevel,
                PriceSell = _itemInstance.PriceSell,
                PriceBuy = _itemInstance.PriceBuy,
                ObjectId = _itemInstance.ObjectId,
                CustomType1 = _itemInstance.CustomType1,
                CustomType2 = _itemInstance.CustomType2,
                ManaLeft = _itemInstance.Mana
            };
            return itemEntity;
        }
        
        /// <summary>
        /// Update the database with values of the item
        /// </summary>
        private void UpdateInDb()
        {
            if (_itemInstance.StoredInDb)
            {
                return;
            }
            IItemRepository itemRepository =
                Initializer.ServiceProvider.GetService<IUnitOfWork>()?.Items;

            if (itemRepository != null) itemRepository.UpdateItemAsync(ConvertToEntity());
            _itemInstance.ExistsInDb = true;
            _itemInstance.StoredInDb = true;
        }

        /// <summary>
        /// Delete item from database
        /// </summary>
        private void RemoveFromDb()
        {
            if (_itemInstance.StoredInDb)
            {
                return;
            }
            IItemRepository itemRepository =
                Initializer.ServiceProvider.GetService<IUnitOfWork>()?.Items;

            if (itemRepository is null)
            {
                throw new Exception("Inventory is Empty");
            }
            itemRepository.DeleteAsync(_itemInstance.ObjectId);
            _itemInstance.ExistsInDb = false;
            _itemInstance.StoredInDb = false;
        }

        public List<ItemEntity> GetItemsByOwnerIdAndLocId(int ownerId, string baseLocation, string equipLocation)
        {
            IItemRepository itemRepository =
                Initializer.ServiceProvider.GetService<IUnitOfWork>()?.Items;
            var item = itemRepository.GetInventoryItemsByOwnerIdAndLocId(ownerId, baseLocation, equipLocation);
            return item.Result;
        }
    }
}