﻿using System.Collections.Generic;

namespace Core.Model.Items
{
    public class ItemSlot
    {
        public IDictionary<string, ItemSlotId> Slots { get; }

        public ItemSlot()
        {
            Slots = new Dictionary<string, ItemSlotId>
            {
                {"chest", ItemSlotId.SlotChest},
                {"fullarmor", ItemSlotId.SlotFullArmor},
                {"head", ItemSlotId.SlotHead},
                {"hair", ItemSlotId.SlotHair},
                {"face", ItemSlotId.SlotFace},
                {"dhair", ItemSlotId.SlotDHair},
                {"underwear", ItemSlotId.SlotUnderwear},
                {"back", ItemSlotId.SlotBack},
                {"neck", ItemSlotId.SlotNeck},
                {"legs", ItemSlotId.SlotLegs},
                {"feet", ItemSlotId.SlotFeet},
                {"gloves", ItemSlotId.SlotGloves},
                {"chest,legs", ItemSlotId.SlotChest | ItemSlotId.SlotLegs},
                {"rhand", ItemSlotId.SlotRHand},
                {"lhand", ItemSlotId.SlotLHand},
                {"lrhand", ItemSlotId.SlotLRHand},
                {"rear,lear", ItemSlotId.SlotREar | ItemSlotId.SlotLEar},
                {"rfinger,lfinger", ItemSlotId.SlotRFinger | ItemSlotId.SlotLFinger},
                {"none", ItemSlotId.SlotNone},
                {"wolf", ItemSlotId.SlotWolf},
                {"hatchling", ItemSlotId.SlotHatchLing},
                {"strider", ItemSlotId.SlotStrider},
                {"babypet", ItemSlotId.SlotBabyPet}
            };
            // for wolf
            // for hatchling
            // for strider
            // for babypet
        }
    }
}