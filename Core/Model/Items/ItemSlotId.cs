﻿using System;

namespace Core.Model.Items
{
    [Flags]
    public enum ItemSlotId
    {
        SlotNone = 0x0000,
        SlotUnderwear = 0x0001,
        SlotREar = 0x0002,
        SlotLEar = 0x0004,
        SlotNeck = 0x0008,
        SlotRFinger = 0x0010,
        SlotLFinger = 0x0020,
        SlotHead = 0x0040,
        SlotRHand = 0x0080,
        SlotLHand = 0x0100,
        SlotGloves = 0x0200,
        SlotChest = 0x0400,
        SlotLegs = 0x0800,
        SlotFeet = 0x1000,
        SlotBack = 0x2000,
        SlotLRHand = 0x4000,
        SlotFullArmor = 0x8000,
        SlotHair = 0x010000,
        SlotWolf = 0x020000,
        SlotHatchLing = 0x100000,
        SlotStrider = 0x200000,
        SlotBabyPet = 0x400000,
        SlotFace = 0x040000,
        SlotDHair = 0x080000
    }
}