﻿using System.Collections.Generic;
using System.IO;
using Core.Model.Skills;
using L2Logger;
using Newtonsoft.Json;

namespace Core.Model.Items
{
    public sealed class SkillSpellBookInit
    {
        private readonly string _basePath;
        private readonly IDictionary<int, int> _skillSpellBooks;

        public SkillSpellBookInit()
        {
            _basePath = Initializer.Config().ServerConfig.StaticData + "/Json";
            _skillSpellBooks = new Dictionary<int, int>();
            Init();
        }

        private void Init()
        {
            try
            {
                using (StreamReader sr = new StreamReader(_basePath + "/" + "SkillSpellBooks.json"))
                {
                    var skillSpellBookEntity = JsonConvert.DeserializeObject<SkillSpellBookEntity>(sr.ReadToEnd());
                    skillSpellBookEntity?.Items.ForEach(e =>
                    {
                        _skillSpellBooks.Add(e.SkillId, e.ItemId);
                    });
                }
                LoggerManager.Info("SkillSpellBookTable: Loaded " + _skillSpellBooks.Count + " SpellBooks");
            }
            catch (JsonException e)
            {
                LoggerManager.Error(GetType().Name + ": " + e.Message);
            }
        }

        private int GetBookForSkill(int skillId, int level)
        {
            if ((skillId == SkillStat.SkillDivineInspiration) && (level != -1))
            {
                return level switch
                {
                    1 => 8618 // Ancient Book - Divine Inspiration (Modern Language Version)
                    ,
                    2 => 8619 // Ancient Book - Divine Inspiration (Original Language Version)
                    ,
                    3 => 8620 // Ancient Book - Divine Inspiration (Manuscript)
                    ,
                    4 => 8621 // Ancient Book - Divine Inspiration (Original Version)
                    ,
                    _ => -1
                };
            }
            if (!_skillSpellBooks.ContainsKey(skillId))
            {
                return -1;
            }
            return _skillSpellBooks[skillId];
        }
        
        public int GetBookForSkill(Skill skill)
        {
            return GetBookForSkill(skill.Id, -1);
        }
	
        public int GetBookForSkill(Skill skill, int level)
        {
            return GetBookForSkill(skill.Id, level);
        }
    }
}