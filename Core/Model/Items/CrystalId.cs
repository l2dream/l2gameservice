﻿namespace Core.Model.Items
{
    public enum CrystalId
    {
        CrystalNone = 0x00,
        CrystalD = 0x01,
        CrystalC = 0x02,
        CrystalB = 0x03,
        CrystalA = 0x04,
        CrystalS = 0x05,
    }
}