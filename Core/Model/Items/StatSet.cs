﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Core.Model.Items
{
    public class StatSet
    {
        private readonly IDictionary<string, string> _set;

        public StatSet()
        {
            _set = new Dictionary<string, string>();
        }

        public void Add(string field, string value)
        {
            _set.TryAdd(field, value);
        }

        public void Delete(string field)
        {
            _set.Remove(field);
        }

        public string GetSet(string field)
        {
            return _set[field];
        }

        public int GetInt(string field, int defaultValue = 0)
        {
            _set.TryGetValue(field, out var value);
            int toReturn;
            return value == null ? defaultValue : int.TryParse(value, out toReturn) ? toReturn : defaultValue;
        }

        public double GetDouble(string field, double defaultValue = 0)
        {
            _set.TryGetValue(field, out var value);
            return value == null ? defaultValue : Convert.ToDouble(value, CultureInfo.InvariantCulture.NumberFormat);
        }

        public bool GetBoolean(string field, bool defaultValue = false)
        {
            _set.TryGetValue(field, out var value);
            bool toReturn;
            return value == null ? defaultValue : bool.TryParse(value, out toReturn) ? toReturn : defaultValue;
        }

        public string GetString(string field, string defaultValue = "")
        {
            _set.TryGetValue(field, out var value);
            return value ?? defaultValue;
        }

        public bool HasKey(string key)
        {
            return _set.ContainsKey(key);
        }

        public void Clear()
        {
            _set.Clear();
        }
    }
}