﻿using System.Threading.Tasks;
using Core.AI;
using Core.Model.Actor;
using Core.Model.CalculateStats;
using Core.Model.Items.Type;
using Core.Model.Player;
using Core.Model.World;
using Core.Model.World.RegionData;
using Core.Network.GameServicePackets.ServerPackets;
using Helpers;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Model.Items
{
    public class ItemInstance : WorldObject
    {
        public const int Unchanged = 0;
        public const int Added = 1;
        public const int Removed = 3;
        public const int Modified = 2;
        
        public bool ExistsInDb { get; set; } // if a record exists in DB.
        public bool StoredInDb { get; set; } // if DB data is up-to-date.
        
        public ItemModel ItemModel { get; } 
        public int ItemId { get; }
        public Item Item { get; }
        public int Count { get; set; }
        public int PriceBuy { get; set; }
        public int CustomType1 { get; set; }
        public int CustomType2 { get; set; }
        private int _ownerId;
        private int _enchantLevel;
        private int _priceSell;
        public long DropTime { get; set; }
        public ItemLocation ItemLocation { get; set; }
        public int LocData { get; set; }
        public int LastChange { get; set; }
        public int Mana { get; set; }
        public bool Wear { get; set; }
        public override bool IsItem => true;
        
        public ItemInstance(int objectId, int itemId) : base(objectId)
        {
            var itemInit = Initializer.ItemService();
            var item = itemInit.GetItemById(itemId);
            ItemId = item.ItemId;
            Item = item;
            Count = 1;
            ItemLocation = ItemLocation.Void;
            Mana = item.Duration;
            ItemModel = new ItemModel(this);
        }
        
        public void SetOwnerId(int ownerId)
        {
            if (ownerId == _ownerId)
            {
                return;
            }
            _ownerId = ownerId;
            StoredInDb = false;
        }
        
        public int GetOwnerId()
        {
            return _ownerId;
        }
        
        public int EnchantLevel
        {
            get => _enchantLevel;
            set
            {
                if (_enchantLevel == value)
                {
                    return;
                }
                _enchantLevel = value;
                StoredInDb = false;
            }
        }

        public int PriceSell
        {
            get
            {
                if (Item.Consumable)
                {
                    return (int) (_priceSell * 1.0);
                }
                else
                {
                    return _priceSell;
                }
            }
            set
            {
                _priceSell = value;
                StoredInDb = false;
            }
        }
        
        public bool IsEquippable()
        {
            return ((Item.BodyPart != 0) && !(Item is EtcItem));
        }
        
        public bool IsEquipped()
        {
            return (ItemLocation == ItemLocation.PaperDoll) || (ItemLocation == ItemLocation.PetEquip);
        }
        
        public void SetLocation(ItemLocation loc, int locData = 0)
        {
            if ((loc == ItemLocation) && (locData == LocData))
            {
                return;
            }
            ItemLocation = loc;
            LocData = locData;
            StoredInDb = false;
        }
        
        public void ChangeCount(int count)
        {
            if (count == 0)
            {
                return;
            }
            if ((count > 0) && (Count > (int.MaxValue - count)))
            {
                Count = int.MaxValue;
            }
            else
            {
                Count += count;
            }
            if (Count < 0)
            {
                Count = 0;
            }
            StoredInDb = false;
        }
        
        public bool IsStackable()
        {
            return Item.Stackable;
        }

        public override Task OnActionAsync(PlayerInstance player)
        {
            return Task.Run(() => player.AI.SetIntention(CtrlIntention.AiIntentionPickUp, this));
        }
        
        public FunctionObject[] GetStatFuncs(Character character)
        {
            return Item.GetStatFuncs(this, character);
        }

        /// <summary>
        /// Init a dropped ItemInstance and add it in the world as a visible object
        /// </summary>
        /// <param name="dropper"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void DropMe(Character dropper, int x, int y, int z)
        {
            lock (this)
            {
                // Set the x,y,z position of the ItemInstance dropped and update its _worldregion
                SetVisible(true);
                WorldObjectPosition().SetWorldPosition(x, y, z);
                WorldObjectPosition().SetWorldRegion(Initializer.WorldInit().GetRegion(WorldObjectPosition().GetWorldPosition()));
			
                // Add the ItemInstance dropped to _visibleObjects of its WorldRegion
                WorldObjectPosition().GetWorldRegion().AddVisibleObject(this);
            }

            DropTime = DateTimeHelper.CurrentUnixTimeMillis();
            Initializer.WorldInit().AddVisibleObject(this, WorldObjectPosition().GetWorldRegion(), dropper);
        }

        /// <summary>
        /// Remove a ItemInstance from the world and send server->client GetItem packets
        /// </summary>
        /// <param name="playerInstance"></param>
        public async Task PickupMeAsync(PlayerInstance playerInstance)
        {
            WorldRegionData oldRegion = WorldObjectPosition().GetWorldRegion();
            // Create a server->client GetItem packet to pick up the ItemInstance
            await playerInstance.SendBroadcastPacketAsync(new GetItem(this, playerInstance.ObjectId));
            
            SetVisible(false);
            WorldObjectPosition().SetWorldRegion(null);
            
            // Remove the ItemInstance from the world
            Initializer.WorldInit().RemoveVisibleObject(this, oldRegion);
        }
    }
}