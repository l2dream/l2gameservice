﻿using System.Collections.Generic;

namespace Core.Model.Items
{
    public class SkillSpellBookEntity
    {
        public string Table { get; set; }
        public List<SkillSpellBook> Items { get; set; }
    }
    
    public sealed class SkillSpellBook
    {
        public int SkillId { get; set; }
        public int ItemId { get; set; }
    }
}