﻿using Core.Model.World;

namespace Core.Model.Items
{
    public enum ItemLocation
    {
        Void,
        Inventory,
        PaperDoll,
        Warehouse,
        ClanWh,
        Pet,
        PetEquip,
        Lease,
        Freight
    }
}