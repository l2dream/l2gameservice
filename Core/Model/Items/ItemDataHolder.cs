﻿using Core.Model.Items.Type;

namespace Core.Model.Items
{
    public class ItemDataHolder
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public object Type { get; set; }
        public StatSet Set { get; set; }
        public int CurrentLevel { get; set; }
        public Item Item { get; set; }
    }
}