﻿using System.Threading.Tasks;
using Core.Model.Player.ActionUse;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Player.Handlers
{
    public class UpDown : IActionUseHandler
    {
        public async Task DoActionAsync(PlayerInstance playerInstance)
        {
            if (playerInstance.PlayerAction().IsSitting)
            {
                playerInstance.PlayerAction().StandUp();
                await playerInstance.SendBroadcastPacketAsync(new ChangeWaitType(playerInstance, 1));
                return;
            }
            playerInstance.PlayerAction().SitDown();
            await playerInstance.SendBroadcastPacketAsync(new ChangeWaitType(playerInstance, 0));
        }
    }
}