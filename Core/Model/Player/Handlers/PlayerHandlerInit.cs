﻿using System;
using System.Collections.Generic;
using Core.Model.Player.ActionUse;
using L2Logger;

namespace Core.Model.Player.Handlers
{
    public class PlayerHandlerInit
    {
        private readonly IDictionary<int, IActionUseHandler> _handlers;

        public PlayerHandlerInit()
        {
            _handlers = new Dictionary<int, IActionUseHandler>();
            InitHandlers();
        }
        
        private void InitHandlers()
        {
            _handlers.Add(0, new UpDown());
        }
        
        public IActionUseHandler GetActionHandler(int actionId)
        {
            try
            {
                return _handlers[actionId];
            }
            catch (KeyNotFoundException ex)
            {
                LoggerManager.Error(GetType().Name + " no handler with action id " + actionId + " " + ex);
                throw;
            }
        }
    }
}