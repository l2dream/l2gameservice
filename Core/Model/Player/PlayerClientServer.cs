﻿using System.Threading.Tasks;
using Core.Helper;
using Core.Model.Actor;
using Core.Network;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Player
{
    public class PlayerClientServer : CharacterClientServer
    {
        private readonly PlayerInstance _playerInstance;
        public PlayerClientServer(PlayerInstance playerInstance) : base(playerInstance)
        {
            _playerInstance = playerInstance;
        }

        public override async Task BroadcastStatusUpdateAsync()
        {
            StatusUpdate su = new StatusUpdate(_playerInstance);
            await _playerInstance.SendPacketAsync(su);
        }
        
        public override void UpdateAndBroadcastStatus(int broadcastType)
        {
            //refreshOverloaded(); TODO
            //refreshExpertisePenalty();
            // Send a Server->Client packet UserInfo to this PlayerInstance and CharInfo to all PlayerInstance in its _KnownPlayers (broadcast)
            if (broadcastType == 1)
            {
                _playerInstance.SendPacketAsync(new UserInfo(_playerInstance));
            }
		
            if (broadcastType == 2)
            {
                SendBroadcastUserInfo();
            }
        }

        public override void SendBroadcastUserInfo()
        {
            _playerInstance.SendPacketAsync(new UserInfo(_playerInstance));
            //Broadcast.toKnownPlayers(this, new CharInfo(this)); TODO
        }
        
        public static void ToSelfAndKnownPlayersInRadius(PlayerInstance playerInstance, ServerPacket mov, long radiusSq)
        {
            if (radiusSq < 0)
            {
                radiusSq = 360000;
            }
            playerInstance.SendPacketAsync(mov);
		
            foreach (var player in playerInstance.GetKnownList().GetKnownPlayers().Values)
            {
                if ((player != null) && (CalculateRange.GetDistanceSq(player, playerInstance) <= radiusSq))
                {
                    player.SendPacketAsync(mov);
                }
            }
        }
        
    }
}