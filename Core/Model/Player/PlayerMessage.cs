﻿using System.Threading.Tasks;
using Core.Model.Actor;
using Core.Model.Items;
using Core.Model.Skills;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Player
{
    public class PlayerMessage
    {
        private readonly PlayerInstance _playerInstance;
        public PlayerMessage(PlayerInstance playerInstance)
        {
            _playerInstance = playerInstance;
        }
        
        public async Task SendMessageAsync(string message)
        {
            SystemMessage sm = new SystemMessage(SystemMessageId.S1);
            sm.AddString(message);
            await _playerInstance.SendPacketAsync(sm);
        }

        public void SendDamageMessageAsync(Character target, int damage, bool mcrit, bool pcrit = false, bool miss = false)
        {
            // Check if hit is missed
            if (miss)
            {
                _playerInstance.SendPacketAsync(new SystemMessage(SystemMessageId.MissedTarget));
                return;
            }
            // Check if hit is critical
            if (pcrit)
            {
                _playerInstance.SendPacketAsync(new SystemMessage(SystemMessageId.CriticalHit));
            }
            if (mcrit)
            {
                _playerInstance.SendPacketAsync(new SystemMessage(SystemMessageId.CriticalHitMagic));
            }
            if (_playerInstance != target)
            {
                SystemMessage sm = new SystemMessage(SystemMessageId.YouDidS1Dmg);
                sm.AddNumber(damage);
                _playerInstance.SendPacketAsync(sm);
            }
        }

        public void SendMessageToPlayerByNpc(int npcId, int damage)
        {
            SystemMessage sm = new SystemMessage(SystemMessageId.S1GaveYouS2Dmg);
            sm.AddNpcName(npcId);
            sm.AddNumber(damage);
            _playerInstance.SendPacketAsync(sm);		
        }
        
        public async Task SendMessageToPlayerAsync(Skill skill, int magicId)
        {
            SystemMessage sm = new SystemMessage(SystemMessageId.UseS1);
            switch (magicId)
            {
                case 2005:
                    sm.AddItemName(728);
                    break;
                case 2003:
                    sm.AddItemName(726);
                    break;
                case 2166 when (skill.Level == 2):
                    sm.AddItemName(5592);
                    break;
                case 2166 when (skill.Level == 1):
                    sm.AddItemName(5591);
                    break;
                default:
                    sm.AddSkillName(magicId, skill.Level);
                    break;
            }
            await _playerInstance.SendPacketAsync(sm);
        }
        
        public async Task SendMessageToPlayerAsync(ItemInstance item, SystemMessageId systemMessageId)
        {
            SystemMessage sm = new SystemMessage(systemMessageId);
            sm.AddString(item.Item.Name);
            await _playerInstance.SendPacketAsync(sm);
        }
    }
}