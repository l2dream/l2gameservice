﻿using System.Collections.Concurrent;
using Core.Model.Actor;

namespace Core.Model.Player
{
    public class PlayerReward
    {
        private struct RewardInfo
        {
            private Character _attacker;
            private long _dmg;

            public RewardInfo(Character pAttacker, long pDmg)
            {
                _dmg = 0;
                _attacker = pAttacker;
            }
            
            public void AddDamage(long pDmg)
            {
                _dmg += pDmg;
            }
            
            /*
            public override bool Equals(object obj)
            {
                if (this == obj)
                {
                    return true;
                }
			
                if (obj is RewardInfo rewardInfo)
                {
                    return rewardInfo._attacker == _attacker;
                }
			
                return false;
            }
            */
        }


        private PlayerInstance _playerInstance;
        public PlayerReward(PlayerInstance playerInstance)
        {
            _playerInstance = playerInstance;
        }

        public void CalculateRewards(Character lastAttacker)
        {
            
        }
    }
}