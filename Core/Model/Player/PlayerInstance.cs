﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Core.AI;
using Core.Model.Actor;
using Core.Model.Actor.KnownList;
using Core.Model.Actor.Stat;
using Core.Model.Actor.Stat.Data;
using Core.Model.CalculateStats;
using Core.Model.Items;
using Core.Model.Items.Type;
using Core.Model.Npc.Type;
using Core.Model.Player.Template;
using Core.Network;
using Core.Network.GameServicePackets.ServerPackets;
using L2Logger;

namespace Core.Model.Player
{
    public sealed class PlayerInstance : Playable
    {
        private static PlayerLoader _playerLoader;
        public new IPlayerStat Stat { get; }
        public Weapon FistsWeaponItem { get; set; }
        public override double LevelMod => ((100.0 - 11) + Stat.Level) / 100.0;

        public new readonly PlayerTemplate Template;
        public override bool IsPlayer => true;
        public bool IsMageClass => Template.Stat.ClassId.IsMageClass;
        public long TimerToAttack { get; set; }
        public string AccountName { get; }
        public int ClientX { get; set; }
        public int ClientY { get; set; }
        public int ClientZ { get; set; }
        public int ClientHeading { get; set; }
        
        public bool IsStunned => Status.IsStunned;
        public bool IsConfused => Status.IsConfused;
        public bool IsParalyzed => Status.IsParalyzed;
        public bool IsSleeping => Status.IsSleeping;
        public bool IsAlikeDead => Status.IsAlikeDead();
        public L2Trainer LastTrainerNpc { get; set; }

        public GameServiceController Controller { get; set; }
        private readonly PlayerEnter _playerEnter;
        private readonly PlayerAction _playerAction;
        private readonly PlayerReward _playerReward;
        private readonly PlayerModel _playerModel;
        private readonly PlayerInventory _playerInventory;
        private readonly PlayerStatusUpdate _playerStatusUpdate;
        private readonly PlayerAppearance _playerAppearance;
        private readonly PlayerSkill _skill;
        private readonly PlayerSkillMagic _skillMagicUse;
        private readonly PlayerSkillReuseTime _skillReuseTime;
        private readonly PlayerShortCut _shortCut;
        private readonly PlayerMessage _playerMessage;
        private readonly PlayerUseItem _playerUseItem;
        private readonly PlayerSoulShot _playerSoulShot;
        private readonly PlayerMoveToLocation _toLocation; 

        public PlayerInstance(int objectId, PlayerTemplate template, string accountName, string characterName, PlayerAppearance app) : base(objectId, template)
        {
            CharacterName = characterName;
            AccountName = accountName;
            PlayerInstance = this;
            Template = template;
            _playerAppearance = app;
            _playerInventory = new PlayerInventory(this);
            _playerStatusUpdate = new PlayerStatusUpdate(this);
            _playerModel = new PlayerModel(this);
            _playerAction = new PlayerAction(this);
            _playerEnter = new PlayerEnter(this);
            _playerReward = new PlayerReward(this);
            _playerMessage = new PlayerMessage(this);
            _playerUseItem = new PlayerUseItem(this);
            _playerSoulShot = new PlayerSoulShot(this);
            _toLocation = new PlayerMoveToLocation(this);
            AI = new PlayerAi(this);
            Target = new PlayerTarget(this);
            Stat = new PlayerStat(this);
            _skill = new PlayerSkill(this);
            _skillReuseTime = new PlayerSkillReuseTime(this);
            _skillMagicUse = new PlayerSkillMagic(this);
            _shortCut = new PlayerShortCut(this);
            CharacterBroadcast = new PlayerClientServer(this);
            Calculators = new Calculator[Skills.Stat.Values.Count()];
            Formulas.AddFuncsToNewPlayer(this);
        }
        public PlayerEnter PlayerEnter()
        {
            return _playerEnter;
        }
        public PlayerAction PlayerAction()
        {
            return _playerAction;
        }
        public PlayerReward PlayerReward()
        {
            return _playerReward;
        }
        public PlayerModel PlayerModel()
        {
            return _playerModel;
        }
        public PlayerInventory PlayerInventory()
        {
            return _playerInventory;
        }
        public PlayerStatusUpdate PlayerStatusUpdate()
        {
            return _playerStatusUpdate;
        }
        public PlayerAppearance PlayerAppearance()
        {
            return _playerAppearance;
        }
        public PlayerSkill PlayerSkill()
        {
            return _skill;
        }
        public PlayerSkillMagic PlayerSkillMagicUse()
        {
            return _skillMagicUse;
        }
        public PlayerSkillReuseTime PlayerSkillReuseTime()
        {
            return _skillReuseTime;
        }
        public PlayerShortCut PlayerShortCut()
        {
            return _shortCut;
        }
        public PlayerMessage PlayerMessage()
        {
            return _playerMessage;
        }
        public PlayerUseItem PlayerUseItem()
        {
            return _playerUseItem;
        }
        public PlayerSoulShot PlayerSoulShot()
        {
            return _playerSoulShot;
        }
        public PlayerMoveToLocation Location()
        {
            return _toLocation;
        }

        public override ItemInstance GetActiveWeaponInstance()
        {
            return PlayerInventory().GetPaperdollItem(Inventory.PaperdollRhand);
        }

        public override Weapon GetActiveWeaponItem()
        {
            ItemInstance weapon = GetActiveWeaponInstance();
            if (weapon == null)
            {
                return FistsWeaponItem;
            }
            return (Weapon) weapon.Item;
        }

        public static PlayerInstance Load(int objectId)
        {
            return PlayerLoader().Load(objectId);
        }

        public static PlayerLoader PlayerLoader()
        {
            if (_playerLoader == null)
            {
                _playerLoader = new PlayerLoader();
            }
            return _playerLoader;
        }

        //TODO need refactor and finalize method
        public override async Task OnActionAsync(PlayerInstance player)
        {
            // Check if the player already target this PlayerInstance
            if (player.Target.GetTarget() != this)
            {
                // Set the target of the player
                player.Target.SetTarget(this);
			
                // Send a Server->Client packet MyTargetSelected to the player
                // The color to display in the select window is White
                await player.SendPacketAsync(new MyTargetSelected(ObjectId, 0));
                if (player != this)
                {
                    await player.SendPacketAsync(new ValidateLocation(this));
                }
            } 
            else 
            {
                if (player != this)
                {
                    await player.SendPacketAsync(new ValidateLocation(this));
                }

                if (Zone().IsInsideRadius2D(player, 50))
                {
                    player.AI.SetIntention(CtrlIntention.AiIntentionInteract, this);
                }
                
                player.AI.SetIntention(CtrlIntention.AiIntentionFollow, this);
            }
        }

        public override WorldObjectKnownList GetKnownList()
        {
            if (!(base.GetKnownList() is PlayerKnownList))
            {
                SetKnownList(new PlayerKnownList(this));
            }
            return (PlayerKnownList) base.GetKnownList();
        }

        /// <summary>
        /// Stop the HP/MP/CP Regeneration task (scheduled tasks)
        /// </summary>
        public void StopAllTimers()
        {
            Status.StopHpMpRegeneration();
        }

        public override async Task BroadcastStatusUpdateAsync()
        {
            await CharacterBroadcast.BroadcastStatusUpdateAsync();
        }

        public void SendBroadcastUserInfo()
        {
            CharacterBroadcast.SendBroadcastUserInfo();
        }

        public void UpdateAndBroadcastStatus(int broadcastType)
        {
            CharacterBroadcast.UpdateAndBroadcastStatus(broadcastType);
        }


        public override async Task SendActionFailedPacketAsync()
        {
            await Controller.SendPacketAsync(new ActionFailed());
        }

        public override Task SendPacketAsync(ServerPacket serverPacket)
        {
            if (Controller is null)
                return Task.CompletedTask;
            return Controller.SendPacketAsync(serverPacket);
        }

        public override void OnTeleported()
        {
            base.OnTeleported();
            // Force a revalidation
            Zone().RevalidateZone();
            SendBroadcastUserInfo();
        }

        public override async Task DoDieAsync(Character killer)
        {
            // Kill the PlayerInstance
            await base.DoDieAsync(killer);
        }

        public async Task DoPickupItemAsync(ItemInstance itemInstance)
        {
            var pickUpInstance = new PlayerPickup(this, itemInstance);
            await pickUpInstance.DoPickupItemAsync();
        }
        
        public override void UpdateAbnormalEffect()
        {
            SendBroadcastUserInfo();
        }

        public void SubscribeMonster(L2Monster l2Monster)
        {
            if (!l2Monster.IsEventMonster())
            {
                l2Monster.EventMonster += L2MonsterOnEventMonster;
            }
        }

        private void L2MonsterOnEventMonster(object sender, CustomEventArgs e)
        {
            var l2Monster = sender as L2Monster;
            LoggerManager.Info($"{CharacterName} received this Action: {e.MonsterAction}");
        }

        public void UnSubscribeMonster(L2Monster l2Monster)
        {
            l2Monster.EventMonster -= L2MonsterOnEventMonster;
        }
    }
}