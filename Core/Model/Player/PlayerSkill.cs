﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Model.Skills;
using Core.Network.GameServicePackets.ServerPackets;
using DataBase.Entities;
using DataBase.Interfaces;
using L2Logger;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Model.Player
{
    public class PlayerSkill
    {
        private readonly PlayerInstance _playerInstance;
        private readonly ConcurrentDictionary<int, Skill> _skills;
        private readonly ICharacterSkillRepository _characterSkillRepository;
        private readonly SkillInit _skillInit;
        public Location CurrentSkillWorldPosition { get; set; }
        
        public PlayerSkill(PlayerInstance playerInstance)
        {
            _playerInstance = playerInstance;
            _characterSkillRepository = Initializer.ServiceProvider.GetService<IUnitOfWork>()?.CharacterSkill;
            _skills = new ConcurrentDictionary<int, Skill>();
            _skillInit = Initializer.ServiceProvider.GetService<SkillInit>();
        }

        public void AddSkill(Skill newSkill, bool store = false)
        {
            // Add or update a PlayerInstance skill in the character_skills table of the database
            try
            {
                if (store)
                {
                    SaveSkill(newSkill);
                }
                _skills.TryAdd(newSkill.Id, newSkill);
                _playerInstance.AddStatFuncs(newSkill.GetStatFunctions(null, _playerInstance));
            }
            catch (Exception ex)
            {
                LoggerManager.Error(GetType().Name + ": " + ex);
            }
        }

        private void SaveSkill(Skill newSkill)
        {
            var skill = GetAllSkills().FirstOrDefault(s => s.Id == newSkill.Id);
            var characterSkillEntity = new CharacterSkillEntity
            {
                CharacterObjectId = _playerInstance.ObjectId,
                SkillId = newSkill.Id,
                ClassIndex = (int) _playerInstance.Template.Stat.ClassId.Id,
                SkillLevel = newSkill.Level,
                SkillName = newSkill.Name
            };
            if (skill != null)
            {
                _characterSkillRepository.UpdateAsync(characterSkillEntity);
            }
            else
            {
                _characterSkillRepository.AddAsync(characterSkillEntity);
            }
        }

        private List<Skill> GetAllSkills()
        {
            return _skills.Values.ToList();
        }

        public async Task SendSkillListAsync()
        {
            SkillList sl = new SkillList();
            foreach (var skill in GetAllSkills().Where(skill => skill.Id <= 9000))
            {
                sl.AddSkill(skill.Id, skill.Level, skill.IsChance ? skill.IsChance : skill.IsPassive);
            }
            await _playerInstance.SendPacketAsync(sl);
        }

        public void RestoreSkills()
        {
            try
            {
                var skills = PlayerSkills().Where(s => s.Value.Id <= 9000);
                foreach (var skill in skills)
                {
                    // Create a Skill object for each record
                    AddSkill(skill.Value);
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Error(GetType().Name + ": " + ex);
            }
        }

        public Dictionary<int, Skill> PlayerSkills()
        {
            int classId = (int) _playerInstance.Template.Stat.ClassId.Id;
            var list = _characterSkillRepository.GetSkillsByCharObjectAndClassIndex(_playerInstance.ObjectId, classId);
            Dictionary<int, Skill> skills = new Dictionary<int, Skill>();
            list.Result.ForEach(s =>
            {
                skills.Add(s.SkillId, _skillInit.GetSkill(s.SkillId, s.SkillLevel));
            });
            return skills;
        }
        
        /// <summary>
        /// Return the level of a skill owned by the Character.
        /// </summary>
        /// <param name="skillId">The identifier of the Skill whose level must be returned</param>
        /// <returns>The level of the Skill identified by skillId</returns>
        public int GetSkillLevel(int skillId)
        {
            if (_skills.ContainsKey(skillId))
            {
                Skill skill = _skills[skillId];
                return skill?.Level ?? 0;
            }
            return 0;
        }
    }
}