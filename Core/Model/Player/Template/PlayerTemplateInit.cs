﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using Core.Model.Items;
using L2Logger;

namespace Core.Model.Player.Template
{
    public sealed class PlayerTemplateInit : PlayerTemplateData
    {
        private List<ItemHolder> _items;

        public PlayerTemplateInit()
        {
            Initialize();
        }

        private void Initialize()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(@"StaticData/Xml/Stats/PlayerTemplates.xml");
                if (doc.DocumentElement == null)
                {
                    return;
                }
                XmlNodeList nodes = doc.DocumentElement.SelectNodes("/list/class");
                if (nodes != null)
                {
                    foreach (XmlNode node in nodes)
                    {
                        XmlElement ownerElement = node.Attributes?[0].OwnerElement;
                        if (ownerElement == null || node.Attributes == null || !ownerElement.Name.Equals("class"))
                        {
                            continue;
                        }
                        XmlNamedNodeMap attrs = node.Attributes;
                        
                        AddTemplate(new PlayerTemplate(PrepareData(attrs)));
                    }
                }
                LoggerManager.Info($"Loaded {GetAllTemplates().Count} player templates");
            }
            catch (Exception ex)
            {
                LoggerManager.Error(ex.Message);
            }
        }

        private PlayerTemplateStat PrepareData(XmlNamedNodeMap attrs)
        {
            var items = attrs.GetNamedItem("items").Value.Split(";");
            _items = new List<ItemHolder>();
            foreach (string split in items)
            {
                var item = split.Split(",");
                _items.Add(new ItemHolder(int.Parse(item[0]), int.Parse(item[1])));
            }
            
            var playerTemplateStat = new PlayerTemplateStat
            {
                ClassId = ClassId.GetClass(Convert.ToInt32(attrs.GetNamedItem("id").Value)),
                Race = ClassId.GetClass(Convert.ToInt32(attrs.GetNamedItem("id").Value)).ClassRace,
                ClassName = attrs.GetNamedItem("name").Value,
                BaseStr = Convert.ToInt32(attrs.GetNamedItem("baseSTR").Value),
                BaseCon = Convert.ToInt32(attrs.GetNamedItem("baseCON").Value),
                BaseDex = Convert.ToInt32(attrs.GetNamedItem("baseDEX").Value),
                BaseInt = Convert.ToInt32(attrs.GetNamedItem("baseINT").Value),
                BaseWit = Convert.ToInt32(attrs.GetNamedItem("baseWIT").Value),
                BaseMen = Convert.ToInt32(attrs.GetNamedItem("baseMEN").Value),
                BaseHpMax = (int) double.Parse(attrs.GetNamedItem("baseHpMax").Value, CultureInfo.InvariantCulture.NumberFormat),
                BaseCpMax = (int) double.Parse(attrs.GetNamedItem("baseCpMax").Value, CultureInfo.InvariantCulture.NumberFormat),
                BaseMpMax = (int) double.Parse(attrs.GetNamedItem("baseMpMax").Value, CultureInfo.InvariantCulture.NumberFormat),
                BasePAtk = Convert.ToInt32(attrs.GetNamedItem("basePAtk").Value),
                BaseMAtk = Convert.ToInt32(attrs.GetNamedItem("baseMAtk").Value),
                BasePDef = Convert.ToInt32(attrs.GetNamedItem("basePDef").Value),
                BaseMDef = Convert.ToInt32(attrs.GetNamedItem("baseMDef").Value),
                BasePAtkSpd = Convert.ToInt32(attrs.GetNamedItem("basePAtkSpd").Value),
                BaseMAtkSpd = Convert.ToInt32(attrs.GetNamedItem("baseMAtkSpd").Value),
                BaseCritRate = Convert.ToInt32(attrs.GetNamedItem("baseCritRate").Value) / 10,
                BaseMagicCritRate = (attrs.GetNamedItem("baseMagicCritRate") == null? 5:  Convert.ToInt32(attrs.GetNamedItem("baseMagicCritRate").Value)),
                BaseMReuseRate = (attrs.GetNamedItem("baseMReuseDelay") == null? 1:  Convert.ToInt32(attrs.GetNamedItem("baseMReuseDelay").Value)),
                BaseRunSpd = Convert.ToInt32(attrs.GetNamedItem("baseRunSpd").Value),
                BaseLevel = Convert.ToInt32(attrs.GetNamedItem("baseLevel").Value),
                SpawnX = Convert.ToInt32(attrs.GetNamedItem("spawnX").Value),
                SpawnY = Convert.ToInt32(attrs.GetNamedItem("spawnY").Value),
                SpawnZ = Convert.ToInt32(attrs.GetNamedItem("spawnZ").Value),
                ClassBaseLevel = Convert.ToInt32(attrs.GetNamedItem("baseLevel").Value),
                LevelHpAdd = float.Parse(attrs.GetNamedItem("levelHpAdd").Value, CultureInfo.InvariantCulture.NumberFormat),
                LevelHpMod = float.Parse(attrs.GetNamedItem("levelHpMod").Value, CultureInfo.InvariantCulture.NumberFormat),
                LevelCpAdd = float.Parse(attrs.GetNamedItem("levelCpAdd").Value, CultureInfo.InvariantCulture.NumberFormat),
                LevelCpMod = float.Parse(attrs.GetNamedItem("levelCpMod").Value, CultureInfo.InvariantCulture.NumberFormat),
                LevelMpAdd = float.Parse(attrs.GetNamedItem("levelMpAdd").Value, CultureInfo.InvariantCulture.NumberFormat),
                LevelMpMod = float.Parse(attrs.GetNamedItem("levelMpMod").Value, CultureInfo.InvariantCulture.NumberFormat),
                Items = _items,
                
                // Geometry
                CollisionHeight = float.Parse(attrs.GetNamedItem("collision_height").Value, CultureInfo.InvariantCulture.NumberFormat),
                CollisionRadius = float.Parse(attrs.GetNamedItem("collision_radius").Value, CultureInfo.InvariantCulture.NumberFormat),
            };

            return playerTemplateStat;
        }
    }
}
