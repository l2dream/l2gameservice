﻿using System.Collections.Generic;

namespace Core.Model.Player.Template
{
    public class PlayerTemplateData
    {
        private readonly Dictionary<int, PlayerTemplate> _templates;

        public PlayerTemplateData()
        {
            _templates = new Dictionary<int, PlayerTemplate>();
        }

        public void AddTemplate(PlayerTemplate playerTemplate)
        {
            _templates.Add((int)playerTemplate.Stat.ClassId.Id, playerTemplate);
        }

        public ICollection<PlayerTemplate> GetAllTemplates()
        {
            return _templates.Values;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public PlayerTemplate GetTemplateByClassId(ClassIds ids)
        {
            return _templates[(int)ids];
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="classId"></param>
        /// <returns></returns>
        public PlayerTemplate GetTemplateByClassId(int classId)
        {
            return _templates[classId];
        }
    }
}
