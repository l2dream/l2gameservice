﻿using Core.Model.Actor;

namespace Core.Model.Player.Template
{
    public class PlayerTemplate : CharacterTemplate
    {
        public PlayerTemplate(PlayerTemplateStat stat) : base(stat)
        {
        }
    }
}