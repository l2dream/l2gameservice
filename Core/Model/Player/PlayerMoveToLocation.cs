﻿using System.Threading.Tasks;
using Core.AI;
using Core.Model.Player.MoveToLocation;
using Core.Model.Zones;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Player
{
    public sealed class PlayerMoveToLocation
    {
        private readonly PlayerInstance _playerInstance;
        private readonly IPlayerMoveToLocationValidator _validator;
        public PlayerMoveToLocation(PlayerInstance playerInstance)
        {
            _playerInstance = playerInstance;
            _validator = new GeneralPlayerMoveToLocationValidator(playerInstance);
        }

        public async Task MoveToLocationAsync(int targetX, int targetY, int targetZ, int originX, int originY, int originZ)
        {
            if (!_validator.IsValid())
            {
                await _playerInstance.SendActionFailedPacketAsync();
                return;
            }
            if ((targetX == originX) && (targetY == originY) && (targetZ == originZ))
            {
                await _playerInstance.SendPacketAsync(new StopMove(_playerInstance));
                await _playerInstance.SendActionFailedPacketAsync();
            }

            double dx = targetX - _playerInstance.GetX();
            double dy = targetY - _playerInstance.GetY();

            if (((dx * dx) + (dy * dy)) > 98010000)
            {
                await _playerInstance.SendActionFailedPacketAsync();
                return;
            }

            _playerInstance.AI.SetIntention(CtrlIntention.AiIntentionMoveTo,
                new Location(targetX, targetY, targetZ));
        }

        public async Task ValidatePositionAsync(int x, int y, int z, int heading)
        {
            if (!_validator.IsValid())
            {
                await _playerInstance.SendActionFailedPacketAsync();
                return;
            }
            int realX = _playerInstance.GetX();
            int realY = _playerInstance.GetY();
            int realZ = _playerInstance.GetZ();

            if ((x == 0) && (y == 0) && (realX != 0))
            {
                return;
            }

            int dx = x - realX;
            int dy = y - realY;
            int dz = z - realZ;
            double diffSq = ((dx * dx) + (dy * dy));

            if (_playerInstance.Zone().IsInsideZone(ZoneId.Water))
            {
                _playerInstance.SetXYZ(realX, realY, z);
                if (diffSq > 90000)
                {
                    await _playerInstance.SendPacketAsync(new ValidateLocation(_playerInstance));
                }
            }
            else if (diffSq < 360000) // if too large, messes observation
            {
                _playerInstance.SetXYZ(realX, realY, z);
                return;
            }

            _playerInstance.ClientX = x;
            _playerInstance.ClientY = y;
            _playerInstance.ClientZ = z;
            _playerInstance.ClientHeading = heading; // No real need to validate heading.
        }
    }
}