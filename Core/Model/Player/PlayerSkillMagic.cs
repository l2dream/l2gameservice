﻿using System.Threading.Tasks;
using Core.AI;
using Core.Model.Player.SkillMagic;
using Core.Model.Skills;
using Core.Model.World;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Player
{
    public class PlayerSkillMagic
    {
        private readonly PlayerInstance _playerInstance;
        private IPlayerSkillMagicValidator Validator { get; }
        public PlayerSkillMagic(PlayerInstance playerInstance)
        {
            _playerInstance = playerInstance;
            Validator = new GeneralPlayerSkillMagicValidator();
        }

        /// <summary>
        /// Check if the active Skill can be casted.
        /// </summary>
        /// <param name="skill">The Skill to use</param>
        /// <param name="forceUse">used to force ATTACK on players</param>
        /// <param name="dontMove">used to prevent movement, if not in range</param>
        public async Task UseMagicAsync(Skill skill, bool forceUse, bool dontMove)
        {
            if (!await Validator.IsValid(_playerInstance, skill))
            {
                await _playerInstance.SendActionFailedPacketAsync();
                return;
            }
            // Get the target for the skill
            WorldObject target = GetTarget(skill);
            if (target is null)
            {
                await _playerInstance.SendPacketAsync(new SystemMessage(SystemMessageId.TargetCantFound));
                await _playerInstance.SendActionFailedPacketAsync();
                return;
            }
            // Notify the AI with AI_INTENTION_CAST and target
            _playerInstance.AI.SetIntention(CtrlIntention.AiIntentionCast, skill, target);
        }

        /// <summary>
        /// Get the target for the skill
        /// </summary>
        /// <param name="skill"></param>
        /// <returns></returns>
        private WorldObject GetTarget(Skill skill)
        {
            switch (skill.TargetType)
            {
                case SkillTargetType.TargetAura: // AURA, SELF should be cast even if no target has been found
                    return _playerInstance; //TODO
                case SkillTargetType.TargetParty:
                case SkillTargetType.TargetAlly:
                case SkillTargetType.TargetClan:
                case SkillTargetType.TargetGround:
                case SkillTargetType.TargetSelf:
                {
                    return _playerInstance;
                }
                default:
                {
                    // Get the first target of the list
                    return _playerInstance.Target.GetTarget();
                }
            }
        }
    }
}