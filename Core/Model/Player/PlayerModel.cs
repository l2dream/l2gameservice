﻿using System.Threading.Tasks;
using DataBase.Entities;
using DataBase.Interfaces;
using Helpers;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Model.Player
{
    public class PlayerModel
    {
        private readonly PlayerInstance _playerInstance;
        private bool _isOnline = false;
        public int BaseClass { get; set; }
        public bool NewBie { get; set; }
        public int ClanId { get; set; } = 0;
        public PlayerModel(PlayerInstance playerInstance)
        {
            _playerInstance = playerInstance;
        }
        public CharacterEntity ConvertToEntity()
        {

            CharacterEntity characterEntity = new CharacterEntity
                {
                    AccountName = _playerInstance.AccountName,
                    ObjectId = _playerInstance.ObjectId,
                    Name = _playerInstance.CharacterName,
                    Level = _playerInstance.Stat.Level, //get stat
                    MaxHp = _playerInstance.Stat.GetMaxHp(), //get stat
                    CurHp = (int)_playerInstance.Status.GetCurrentHp(), //get status,
                    MaxCp = _playerInstance.Stat.GetMaxCp(), //get stat
                    CurCp = (int)_playerInstance.Status.GetCurrentCp(), //get status
                    MaxMp = _playerInstance.Stat.GetMaxMp(), //get stat
                    CurMp = (int)_playerInstance.Status.GetCurrentMp(), //get status
                    Accuracy = _playerInstance.Stat.GetAccuracy(), //get stat
                    CriticalHit = _playerInstance.Stat.GetCriticalHit(), //get stat
                    EvasionRate = _playerInstance.Stat.GetEvasionRate(), //get stat
                    X = _playerInstance.GetX(),
                    Y = _playerInstance.GetY(),
                    Z = _playerInstance.GetZ(),
                    MAtk = 1, //get stat
                    MDef = 1, //get stat
                    MAtkSpd = 1, //get stat
                    PAtk = 1, //get stat
                    PDef = 1, //get stat
                    PAtkSpd = 1, //get stat
                    RunSpeed = 1, //get stat
                    WalkSpeed = 1, //get stat
                    Str = _playerInstance.Stat.GetStr(),//get stat
                    Con = _playerInstance.Stat.GetCon(),//get stat
                    Dex = _playerInstance.Stat.GetDex(),//get stat
                    Int = _playerInstance.Stat.GetInt(),//get stat
                    Men = _playerInstance.Stat.GetMen(),//get stat
                    Wit = _playerInstance.Stat.GetWit(),//get stat
                    Face = _playerInstance.PlayerAppearance().Face, //getAppearance().getFace()
                    HairStyle = _playerInstance.PlayerAppearance().HairStyle, //getAppearance().getHairStyle()
                    HairColor = _playerInstance.PlayerAppearance().HairColor, //getAppearance().getHairColor()
                    Sex = _playerInstance.PlayerAppearance().Female? 1 : 0,
                    MovementMultiplier = 1,
                    AttackSpeedMultiplier = 1,
                    CollisionRadius = _playerInstance.Template.Stat.CollisionRadius, //getTemplate().getCollisionRadius()
                    CollisionHeight = _playerInstance.Template.Stat.CollisionHeight, //getTemplate().getCollisionHeight()
                    Exp = _playerInstance.Stat.Exp, //get stat
                    Sp = _playerInstance.Stat.Sp, //get stat
                    Karma = _playerInstance.Stat.Karma,
                    PvpKills = _playerInstance.Stat.PvpKills,
                    PkKills = _playerInstance.Stat.PkKills,
                    ClanId = 0,
                    MaxLoad = 1, //dynamic, need calculate
                    Race = (int) _playerInstance.Template.Stat.ClassId.ClassRace, //race id
                    ClassId = (int) _playerInstance.Template.Stat.ClassId.Id, //class Id
                    CanCraft = 0,
                    Title = "",
                    AccessLevel = 0,
                    Online = 1,
                    In7SDungeon = 0,
                    ClanPrivileges = 0,
                    WantsPeace = 0,
                    BaseClass = BaseClass,
                    Newbie = 1,
                    Noble = 0,
                    LastRecomDate = DateTimeHelper.CurrentUnixTimeMillis(),
                    NameColor = "0xFFFFFF",
                    TitleColor = "0xFFFF77",
                    PowerGrade = 0,
                    Aio = 0,
                    AioEnd = 0
                };
            return characterEntity;
        }
        
        public ClassRace GetRace()
        {
            return _playerInstance.Template.Stat.ClassId.ClassRace;
        }
        
        public ClassIds GetClassId()
        {
            return _playerInstance.Template.Stat.ClassId.Id;
        }

        public void SetOnlineStatus(bool isOnline)
        {
            _isOnline = isOnline;
        }

        public bool IsOnline()
        {
            return _isOnline;
        }

        public async Task CreateCharacter(int classId)
        {
            try
            {
                BaseClass = classId;
                NewBie = true;
                
                await _playerInstance.Status.SetCurrentCpAsync(0);
                await _playerInstance.Status.SetCurrentHpAsync(_playerInstance.Stat.GetMaxHp());
                _playerInstance.Status.SetCurrentMp(_playerInstance.Stat.GetMaxMp());
                
                _playerInstance.SetXYZ(_playerInstance.Template.Stat.SpawnX, _playerInstance.Template.Stat.SpawnY, _playerInstance.Template.Stat.SpawnZ);
                
                ICharacterRepository characterRepository =
                    Initializer.ServiceProvider.GetService<IUnitOfWork>()?.Characters;

                await characterRepository.CreateCharacterAsync(ConvertToEntity());
            }
            catch
            {
                throw;
            }
            
        }

        public void Store()
        {
            StoreCharBase();
        }

        private void StoreCharBase()
        {
            ICharacterRepository characterRepository =
                Initializer.ServiceProvider.GetService<IUnitOfWork>()?.Characters;
            
            characterRepository.UpdateCharacterAsync(ConvertToEntity());
        }
    }
}