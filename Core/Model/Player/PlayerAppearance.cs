﻿namespace Core.Model.Player
{
    public class PlayerAppearance
    {
        public bool Invisible { get; } = false;

        /** The hexadecimal Color of players name (white is 0xFFFFFF) */
        public int NameColor { get; } = 0xFFFFFF;

        /** The hexadecimal Color of players name (white is 0xFFFFFF) */
        public int TitleColor { get; } = 0xFFFF77;

        public PlayerAppearance(byte face, byte hColor, byte hStyle, bool sex)
        {
            Face = face;
            HairColor = hColor;
            HairStyle = hStyle;
            Female = sex;
        }
        
        public byte Face { get; set; }
        public byte HairColor { get; set; }
        public byte HairStyle { get; set; }
        public bool Female { get; set; }
        
    }
}