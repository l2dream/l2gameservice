﻿using Core.Model.Player.Template;
using DataBase.Entities;
using DataBase.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Model.Player
{
    public class PlayerLoader
    {
        private readonly ICharacterRepository _characterRepository;
        private readonly PlayerTemplateInit _templateInit;

        public PlayerLoader()
        {
            _characterRepository = Initializer.ServiceProvider.GetService<IUnitOfWork>()?.Characters;
            _templateInit = Initializer.ServiceProvider.GetService<PlayerTemplateInit>();
        }
        
        public PlayerInstance Load(int objectId)
        {
            return Restore(objectId);
        }

        private PlayerInstance Restore(int objectId)
        {
            CharacterEntity characterEntity = _characterRepository.GetCharacterByObjectIdAsync(objectId).Result;
            int activeClassId = characterEntity.ClassId;
            bool female = characterEntity.Sex != 0;
            PlayerTemplate template = _templateInit.GetTemplateByClassId(activeClassId);
            PlayerAppearance app = new PlayerAppearance((byte)characterEntity.Face, (byte)characterEntity.HairColor, (byte)characterEntity.HairStyle, female);
            
            PlayerInstance player = new PlayerInstance(objectId, template, characterEntity.AccountName, characterEntity.Name, app);
            player.Stat.Exp = characterEntity.Exp;
            player.Stat.ExpBeforeDeath = characterEntity.ExpBeforeDeath;
            player.Stat.Level = characterEntity.Level;
            player.Stat.Sp = characterEntity.Sp;
            player.Stat.WantsPeace = characterEntity.WantsPeace;
            player.Stat.Heading = characterEntity.Heading;
            player.Stat.Karma = characterEntity.Karma;
            player.Stat.PvpKills = characterEntity.PvpKills;
            player.Stat.PkKills = characterEntity.PkKills;
            player.Stat.OnlineTime = characterEntity.OnlineTime;
            player.Stat.Newbie = (characterEntity.Newbie == 1);
            player.Stat.Noble = (characterEntity.Noble == 1);
            player.Stat.PowerGrade = characterEntity.PowerGrade;
            player.Stat.Title = characterEntity.Title;
            
            // once restored all the skill status, update current CP, MP and HP
            player.Status.CurrCp = characterEntity.CurCp;
            player.Status.CurrHp = characterEntity.CurHp;
            player.Status.CurrMp = characterEntity.CurMp;
            //player.Status.SetCurrentHpDirect(characterEntity.CurHp);
            //player.Status.SetCurrentCpDirect(characterEntity.CurCp);
            //player.Status.SetCurrentMpDirect(characterEntity.CurMp);

            player.WorldObjectPosition().WorldPosition().SetXYZ(characterEntity.X, characterEntity.Y, characterEntity.Z);

            RestoreCharacterData(player);
            
            return player;
        }

        private void RestoreCharacterData(PlayerInstance player)
        {
            player.PlayerInventory().RestoreInventory();
            player.PlayerSkill().RestoreSkills();
            player.PlayerShortCut().RestoreShortCuts();
        }
    }
}