﻿namespace Core.Model.Player.MoveToLocation
{
    public interface IPlayerMoveToLocationValidator
    {
        bool IsValid();
    }
}