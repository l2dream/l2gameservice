﻿using System;
using System.Threading.Tasks;
using L2Logger;

namespace Core.Model.Player
{
    public class PlayerEnter
    {
        private readonly PlayerInstance _playerInstance;
        public PlayerEnter(PlayerInstance playerInstance)
        {
            _playerInstance = playerInstance;
        }

        public async Task DeleteMeAsync()
        {
            try
            {
                //check if already deleted
                if (!_playerInstance.IsVisible())
                {
                    return;
                }
                _playerInstance.StopAllTimers();
                await _playerInstance.Attack().AbortAttackAsync();
                _playerInstance.Target.RemoveTargetAsync();
                if (_playerInstance.GetWorldRegion() != null)
                {
                    _playerInstance.GetWorldRegion().RemoveFromZones(_playerInstance);
                }
                // Remove the PlayerInstance from the world
                _playerInstance.DecayMe();
                _playerInstance.GetKnownList().RemoveAllKnownObjects();
                // Remove WorldObject object from _allObjects of World
                Initializer.WorldInit().RemoveFromAllPlayers(_playerInstance);
            }
            catch (Exception ex)
            {
                LoggerManager.Info("deleteMe()" + ex.Message);
            }
        }
    }
}