﻿using System.Threading.Tasks;
using Core.AI;
using Core.Model.Actor;
using Core.Model.Items;
using Core.Model.Skills;
using Core.Model.World;
using L2Logger;

namespace Core.Model.Player
{
    public class PlayerAi : CharacterAi
    {
        private bool _thinking; // to prevent recursive thinking
        public PlayerAi(PlayerInstance playerInstance) : base(playerInstance)
        {
        }
        
        protected override void OnEvtArrivedRevalidate()
        {
            _actor?.GetKnownList().UpdateKnownObjects();
            base.OnEvtArrivedRevalidate();
        }

        public override async Task OnEvtThinkAsync()
        {
            try
            {
                if (_thinking || _actor.Status.IsAllSkillsDisabled())
                {
                    return;
                }
                _thinking = true;

                if (GetIntention() == CtrlIntention.AiIntentionAttack)
                {
                    await ThinkAttackAsync();
                }
                else if (GetIntention() == CtrlIntention.AiIntentionCast)
                {
                    await ThinkCastAsync();
                }
                else if (GetIntention() == CtrlIntention.AiIntentionPickUp)
                {
                    await ThinkPickUpAsync();
                }
                else if (GetIntention() == CtrlIntention.AiIntentionInteract)
                {
                    //ThinkInteract();
                }
            }
            finally
            {
                _thinking = false;
            }
        }

        private async Task ThinkCastAsync()
        {
            Character target = CastTarget;
            Skill skill = Skill;
            if (CheckTargetLost(target))
            {
                if (skill.IsOffensive && (GetAttackTarget() != null))
                {
                    // Notify the target
                    CastTarget = null;
                }
                return;
            }
            if (skill.HitTime > 50)
            {
                await ClientStopMovingAsync(null);
            }
            if ((target != null) && await MaybeMoveToPawnAsync(target, _actor.Stat.GetMagicalAttackRange(skill)))
            {
                return;
            }
            await _actor.AI.DoCastAsync(skill);
        }

        protected override Task OnIntentionActiveAsync()
        {
            return Task.FromResult(1); //TODO
        }
        
        protected override async Task ClientNotifyDeadAsync()
        {
            _clientMovingToPawnOffset = 0;
            _clientMoving = false;
            await base.ClientNotifyDeadAsync();
        }
        
        private async Task ThinkPickUpAsync()
        {
            if (_actor.Status.IsAllSkillsDisabled())
            {
                return;
            }
		
            WorldObject target = Target;
            if (CheckTargetLost(target))
            {
                return;
            }
		
            if (await MaybeMoveToPawnAsync(target, 36))
            {
                return;
            }
		
            SetIntention(CtrlIntention.AiIntentionIdle);
            await ((PlayerInstance) _actor).DoPickupItemAsync((ItemInstance) target);
        }

        private async Task ThinkAttackAsync()
        {
            Character target = AttackTarget;
            if (target == null)
            {
                return;
            }
		
            if (CheckTargetLostOrDead(target))
            {
                // Notify the target
                AttackTarget = null;
                return;
            }
            
            if (await MaybeMoveToPawnAsync(target, _actor.Stat.GetPhysicalAttackRange()))
            {
                return;
            }
            await _actor.AI.DoAttackAsync(target).ContinueWith(HandleException);
        }
    }
}