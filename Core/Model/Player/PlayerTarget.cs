﻿using System.Threading.Tasks;
using Core.Model.Actor;
using Core.Model.World;

namespace Core.Model.Player
{
    public class PlayerTarget : CharacterTarget
    {
        private readonly PlayerInstance _playerInstance;
        public PlayerTarget(PlayerInstance playerInstance) : base(playerInstance)
        {
            _playerInstance = playerInstance;
        }

        public override void SetTarget(WorldObject newTarget)
        {
            // Get the current target
            WorldObject oldTarget = GetTarget();
            if (oldTarget != null)
            {
                if (oldTarget.Equals(newTarget))
                {
                    return; // no target change
                }
			
                // Remove the PlayerInstance from the _statusListener of the old target if it was a Creature
                if (oldTarget is Character oldCharacter)
                {
                    oldCharacter.Status.RemoveStatusListener(_playerInstance);
                }
            }
            
            // Add the PlayerInstance to the _statusListener of the new target if it's a Creature
            
            if (newTarget is Character character)
            {
                character.Status.AddStatusListener(_playerInstance);
                //TargetSelected my = new TargetSelected(character.ObjectId, newTarget.ObjectId, character.GetX(), character.GetY(), character.GetZ());
			
                // Send packet just to me and to party, not to any other that does not use the information
                //await character.SendPacketAsync(my);
            }
            // Target the new WorldObject (add the target to the PlayerInstance _target, _knownObject and PlayerInstance to _KnownObject of the WorldObject)
            base.SetTarget(newTarget);
        }

        public async Task CancelTargetAsync(int unselect)
        {
            if (unselect == 0)
            {
                if (_playerInstance.MagicCast().IsCastingNow() && _playerInstance.MagicCast().CanAbortCast())
                {
                    await _playerInstance.MagicCast().AbortCastAsync();
                }
                else if (_playerInstance.Target.GetTarget() != null)
                {
                    _playerInstance.Target.RemoveTargetAsync();
                }
            }
            else if (_playerInstance.Target.GetTarget() != null)
            {
                _playerInstance.Target.RemoveTargetAsync();
            }
        }
    }
}