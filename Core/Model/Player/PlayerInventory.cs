﻿using System;
using System.Threading.Tasks;
using Core.Model.Actor;
using Core.Model.Items;
using Core.Model.World;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;
using L2Logger;

namespace Core.Model.Player
{
    public class PlayerInventory : Inventory
    {
        private const int AdenaId = 57;
        private const int AncientAdenaId = 5575;
        private readonly PlayerInstance _playerInstance;
        private readonly ItemModel _itemModel;
        public ItemInstance AdenaInstance { get; set; }
        public ItemInstance AncientAdenaInstance { get; set; }
        
        public PlayerInventory(PlayerInstance playerInstance) : base(playerInstance)
        {
            _itemModel = new ItemModel();
            _playerInstance = playerInstance;
            AdenaInstance = GetItemByItemId(AdenaId);
            AncientAdenaInstance = GetItemByItemId(AncientAdenaId);
        }

        protected override Character GetOwner()
        {
            return _playerInstance;
        }

        protected override ItemLocation GetBaseLocation()
        {
            return ItemLocation.Inventory;
        }
        

        /// <summary>
        /// Adds item in inventory and checks _adena and _ancientAdena
        /// </summary>
        /// <param name="process"></param>
        /// <param name="itemInstance"></param>
        /// <param name="playerInstance"></param>
        /// <param name="worldObject"></param>
        /// <returns></returns>
        public override ItemInstance AddItem(ItemAction process, ItemInstance itemInstance, PlayerInstance playerInstance, WorldObject worldObject)
        {
            itemInstance = base.AddItem(process, itemInstance, playerInstance, worldObject);
            if ((itemInstance != null) && (itemInstance.ItemId == AdenaId) && !itemInstance.Equals(AdenaInstance))
            {
                AdenaInstance = itemInstance;
            }
            if ((itemInstance != null) && (itemInstance.ItemId == AncientAdenaId) && !itemInstance.Equals(AncientAdenaInstance))
            {
                AncientAdenaInstance = itemInstance;
            }
            return itemInstance;
        }
        
        /// <summary>
        /// Adds item in inventory and checks _adena and _ancientAdena
        /// </summary>
        /// <param name="process"></param>
        /// <param name="itemId"></param>
        /// <param name="count"></param>
        /// <param name="actor"></param>
        /// <param name="reference"></param>
        /// <returns></returns>
        public override ItemInstance AddItem(ItemAction process, int itemId, int count, PlayerInstance actor, WorldObject reference)
        {
            ItemInstance item = base.AddItem(process, itemId, count, actor, reference);
            if ((item != null) && (item.ItemId == AdenaId) && !item.Equals(AdenaInstance))
            {
                AdenaInstance = item;
            }
            if ((item != null) && (item.ItemId == AncientAdenaId) && !item.Equals(AncientAdenaInstance))
            {
                AncientAdenaInstance = item;
            }
            return item;
        }

        public int GetCurrentLoad()
        {
            return 100;
        }

        protected override ItemLocation GetEquipLocation()
        {
            return ItemLocation.PaperDoll;
        }

        /// <summary>
        /// Get back items in inventory from database
        /// </summary>
        /// <returns></returns>
        public void RestoreInventory()
        {
            try
            {
                var items = _itemModel.GetItemsByOwnerIdAndLocId(_playerInstance.ObjectId, GetBaseLocation().ToString(),
                    GetEquipLocation().ToString());
                foreach (var invItem in items)
                {
                    ItemInstance itemInstance = new ItemInstance(invItem.ObjectId, invItem.ItemId);
                    itemInstance.ExistsInDb = true;
                    itemInstance.StoredInDb = true;
                    itemInstance.SetOwnerId(invItem.OwnerId);
                    itemInstance.Count = invItem.Count;
                    itemInstance.EnchantLevel = invItem.EnchantLevel;
                    itemInstance.CustomType1 = invItem.CustomType1;
                    itemInstance.CustomType2 = invItem.CustomType2;
                    itemInstance.ItemLocation = (ItemLocation) Enum.Parse(typeof(ItemLocation), invItem.Loc);
                    itemInstance.LocData = invItem.LocData;
                    itemInstance.PriceSell = invItem.PriceSell;
                    itemInstance.PriceBuy = invItem.PriceBuy;
                    itemInstance.Mana = (int) invItem.ManaLeft;

                    if (GetOwner() is PlayerInstance playerInstance)
                    {
                        int itemId = itemInstance.ItemId;
                        if (((itemId >= 6611) && (itemId <= 6621)) || (itemId == 6842))
                        {
                            itemInstance.SetLocation(ItemLocation.Inventory);
                        }
                    }
                    Initializer.WorldInit().StoreObject(itemInstance);
                    // If stackable item is found in inventory just add to current quantity
                    if (itemInstance.IsStackable())
                    {
                        AddItem(ItemAction.Restore, itemInstance, null, GetOwner());
                    }
                    else
                    {
                        AddItem(itemInstance);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Error("Method: LoadInventory " + ex.Message);
            }
        }

        public override int GetAdena()
        {
            return AdenaInstance?.Count ?? 0;
        }
        
        public void AddAdena(ItemAction process, int count, PlayerInstance actor, WorldObject reference)
        {
            if (count > 0)
            {
                AddItem(process, AdenaId, count, actor, reference);
            }
        }

        public void AddAdena(ItemAction process, int count, WorldObject reference, bool sendMessage)
        {
            if (count <= 0)
                return;
            AddAdena(process, count, _playerInstance, reference);
            if (sendMessage)
            {
                SystemMessage sm = new SystemMessage(SystemMessageId.EarnedS1Adena);
                sm.AddNumber(count);
                _playerInstance.SendPacketAsync(sm);
            }
            InventoryUpdate iu = new InventoryUpdate();
            iu.AddItem(AdenaInstance);
            _playerInstance.SendPacketAsync(iu);
        }

        public override ItemInstance DestroyItem(ItemAction process, int objectId, int count, PlayerInstance actor, WorldObject reference)
        {
            var item = base.DestroyItem(process, objectId, count, actor, reference);
            if ((AdenaInstance != null) && (AdenaInstance.Count <= 0))
            {
                AdenaInstance = null;
            }
            if ((AncientAdenaInstance != null) && (AncientAdenaInstance.Count <= 0))
            {
                AncientAdenaInstance = null;
            }
            return item;
        }
    }
}