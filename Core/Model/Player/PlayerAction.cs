﻿using Core.Model.Actor;
using Core.Model.Zones;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Player
{
    public class PlayerAction
    {
        private readonly PlayerInstance _playerInstance;

        public bool IsSitting { get; set; }
        public bool IsTeleporting { get; set; }
        
        private long _protectEndTime = 0;
        private long _teleportProtectEndTime = 0;
        private readonly CharacterZone _characterZone;

        public PlayerAction(PlayerInstance playerInstance)
        {
            _playerInstance = playerInstance;
            _characterZone = playerInstance.Zone();
        }

        public bool IsSpawnProtected()
        {
            return (_protectEndTime != 0) && (_protectEndTime > Initializer.TimeController().GetGameTicks());
        }
        
        public bool IsTeleportProtected()
        {
            return (_teleportProtectEndTime != 0) && (_teleportProtectEndTime > Initializer.TimeController().GetGameTicks());
        }
        
        public void OnActionRequest()
        {
            if (IsSpawnProtected())
            {
                //SetProtection(false);
                if (!_characterZone.IsInsideZone(ZoneId.Peace))
                {
                    _playerInstance.SendPacketAsync(
                        new SystemMessage("You are no longer protected from aggressive monsters."));
                }
            }
            if (IsTeleportProtected())
            {
                //setTeleportProtection(false);
                if (!_characterZone.IsInsideZone(ZoneId.Peace))
                {
                    _playerInstance.SendPacketAsync(new SystemMessage("Teleport spawn protection ended."));
                }
            }
        }
        
        /// <summary>
        /// TODO
        /// </summary>
        public void StandUp()
        {
            IsSitting = false;
        }
        
        public void SitDown()
        {
            IsSitting = true;
        }
    }
}