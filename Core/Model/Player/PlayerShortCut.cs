﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Model.Items;
using Core.Network.GameServicePackets.ServerPackets;
using DataBase.Entities;
using DataBase.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Model.Player
{
    public class PlayerShortCut
    {
        private readonly PlayerInstance _playerInstance;
        private readonly IList<ShortCut> _shortCuts;
        private readonly IShortCutRepository _shortCutRepository;
        
        public PlayerShortCut(PlayerInstance playerInstance)
        {
            _playerInstance = playerInstance;
            _shortCuts = new List<ShortCut>();
            _shortCutRepository = Initializer.ServiceProvider.GetRequiredService<IUnitOfWork>().ShortCut;
        }
        
        public IList<ShortCut> GetAllShortCuts()
        {
            return _shortCuts;
        }
        
        public void RegisterShortCut(ShortCut shortcut)
        {
            var shortCutEntity = ToEntity(shortcut);
            ShortCut currentShortCut = _shortCuts.FirstOrDefault(sc => (sc.Slot == shortcut.Slot) && (sc.Page == shortcut.Page));
            if (currentShortCut != null)
            {
                _shortCuts.Remove(shortcut);
                _shortCutRepository.DeleteShortCutAsync(shortCutEntity);
            }
            _shortCuts.Add(shortcut);
            _shortCutRepository.AddAsync(shortCutEntity);
        }

        private ShortCutEntity ToEntity(ShortCut shortcut)
        {
            ShortCutEntity shortCutEntity = new ShortCutEntity
            {
                CharacterObjectId = _playerInstance.ObjectId,
                Level = shortcut.Level,
                Page = shortcut.Page,
                Slot = shortcut.Slot,
                Type = shortcut.Type,
                ClassIndex = (int) _playerInstance.Template.Stat.ClassId.Id,
                ShortcutId = shortcut.Id
            };
            return shortCutEntity;
        }

        /// <summary>
        /// DeleteShortCut
        /// </summary>
        /// <param name="slot"></param>
        /// <param name="page"></param>
        public async Task DeleteShortCutAsync(int slot, int page)
        {
            ShortCut currentShortCut = _shortCuts.FirstOrDefault(sc => (sc.Slot == slot) && (sc.Page == page));
            if (currentShortCut == null)
            {
                return;
            }
            _shortCuts.Remove(currentShortCut);
            await _shortCutRepository.DeleteShortCutAsync(ToEntity(currentShortCut));

            if (currentShortCut.Type == ShortCut.TypeItem)
            {
                ItemInstance item = _playerInstance.PlayerInventory().GetItemByObjectId(currentShortCut.Id);
                //TODO add soulshot
            }
            await _playerInstance.SendPacketAsync(new ShortCutInit(_playerInstance));
        }

        public void RestoreShortCuts()
        {
            var shortCuts = _shortCutRepository.GetShortCutsByOwnerIdAsync(_playerInstance.ObjectId,
                (int) _playerInstance.Template.Stat.ClassId.Id);
            foreach (var shortCutEntity in shortCuts.Result)
            {
                ShortCut sc = new ShortCut(shortCutEntity.Slot, shortCutEntity.Page, shortCutEntity.Type,
                    shortCutEntity.ShortcutId, shortCutEntity.Level);
                _shortCuts.Add( sc);
            }
        }
    }
}