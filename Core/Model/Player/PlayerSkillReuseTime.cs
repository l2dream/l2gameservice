﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Helper;
using Core.Model.Skills;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Player
{
    public class PlayerSkillReuseTime
    {
        private readonly ConcurrentDictionary<int, Timestamp> _reuseTimestamps;
        
        private readonly PlayerInstance _playerInstance;
        public PlayerSkillReuseTime(PlayerInstance playerInstance)
        {
            _reuseTimestamps = new ConcurrentDictionary<int, Timestamp>();
            _playerInstance = playerInstance;
        }

        /// <summary>
        /// Index according to skill id the current timestamp of use.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="r"></param>
        public void AddTimestamp(Skill s, int r)
        {
            _reuseTimestamps.TryAdd(s.Id, new Timestamp(s, r));
        }
        
        /// <summary>
        /// Index according to skill this TimeStamp instance for restoration purposes only.
        /// </summary>
        /// <param name="t"></param>
        private void AddTimestamp(Timestamp t)
        {
            _reuseTimestamps.TryAdd(t.Skill.Id, t);
        }
        
        /// <summary>
        /// Index according to skill id the current timestamp of use.
        /// </summary>
        /// <param name="skill"></param>
        public void RemoveTimestamp(Skill skill)
        {
            _reuseTimestamps.TryRemove(skill.Id, out _);
        }
        
        public IEnumerable<Timestamp> GetReuseTimeStamps()
        {
            return _reuseTimestamps.Values;
        }

        public async Task ResetSkillTimeAsync(bool sendSkillList)
        {
            if (sendSkillList)
            {
                await _playerInstance.PlayerSkill().SendSkillListAsync();
            }

            await _playerInstance.SendPacketAsync(new SkillCoolTime(_playerInstance));
        }
    }
}