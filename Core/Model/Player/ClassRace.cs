﻿using System.ComponentModel;

namespace Core.Model.Player
{
    public enum ClassRace
    {
        [Description("Human")]
        Human,
        [Description("Elf")]
        Elf,
        [Description("Dark Elf")]
        DarkElf,
        [Description("Orc")]
        Orc,
        [Description("Dwarf")]
        Dwarf
    }
}
