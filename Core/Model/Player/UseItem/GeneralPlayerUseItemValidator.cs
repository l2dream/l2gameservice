﻿using Core.Model.Items;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Player.UseItem
{
    public class GeneralPlayerUseItemValidator : IPlayerUseItemValidator
    {
        private readonly PlayerInstance _playerInstance;
        private readonly PlayerMessage _playerMessage;
        public GeneralPlayerUseItemValidator(PlayerInstance playerInstance)
        {
            _playerInstance = playerInstance;
            _playerMessage = playerInstance.PlayerMessage();
        }
        public bool IsValid(ItemInstance itemInstance)
        {
            if (IsDead(itemInstance.ItemId)) return false;
            if (IsAdena(itemInstance.ItemId)) return false;
            return true;
        }

        public bool IsValidEquipped()
        {
            if (_playerInstance.IsStunned || _playerInstance.IsSleeping || _playerInstance.IsParalyzed || _playerInstance.IsAlikeDead)
            {
                return false;
            }
            if (_playerInstance.MagicCast().IsCastingNow())
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Adena can't be used
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>
        private bool IsAdena(int itemId)
        {
            // Items that cannot be used
            return itemId == 57;
        }

        private bool IsDead(int itemId)
        {
            return _playerInstance.Status.IsDead();
        }
    }
}