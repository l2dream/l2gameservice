﻿using Core.Model.Items;

namespace Core.Model.Player.UseItem
{
    public interface IPlayerUseItemValidator
    {
        bool IsValid(ItemInstance itemInstance);
        bool IsValidEquipped();
    }
}