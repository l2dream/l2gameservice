﻿using System.Collections.Generic;
using System.Linq;

namespace Core.Model.Player
{
    public sealed class ClassId
    {
        public ClassIds Id { get; set; }

        public ClassRace ClassRace { get; }
        public bool IsMageClass { get; }

        public ClassId Parent { get; }

        private ClassId(ClassIds classId, bool isMageClass, ClassRace raceId, ClassId parent)
        {
            Id = classId;
            ClassRace = raceId;
            Parent = parent;
            IsMageClass = isMageClass;
        }

        public static IEnumerable<ClassId> Values
        {
            get
            {
                yield return HumanFighter;
                yield return Warrior;
                yield return Gladiator;
                yield return Warlord;
                yield return Knight;
                yield return Paladin;
                yield return DarkAvenger;
                yield return Rogue;
                yield return TreasureHunter;
                yield return Hawkeye;

                yield return HumanMystic;
                yield return HumanWizard;
                yield return Sorcerer;
                yield return Necromancer;
                yield return Warlock;
                yield return Cleric;
                yield return Bishop;
                yield return Prophet;

                yield return ElvenFighter;
                yield return ElvenKnight;
                yield return TempleKnight;
                yield return SwordSinger;
                yield return ElvenScout;
                yield return PlainsWalker;
                yield return SilverRanger;

                yield return ElvenMystic;
                yield return ElvenWizard;
                yield return Spellsinger;
                yield return ElementalSummoner;
                yield return ElvenOracle;
                yield return ElvenElder;

                yield return DarkFighter;
                yield return PalusKnight;
                yield return ShillienKnight;
                yield return Bladedancer;
                yield return Assassin;
                yield return AbyssWalker;
                yield return PhantomRanger;

                yield return DarkMystic;
                yield return DarkWizard;
                yield return Spellhowler;
                yield return PhantomSummoner;
                yield return ShillienOracle;
                yield return ShillienElder;

                yield return OrcFighter;
                yield return OrcRaider;
                yield return Destroyer;
                yield return Monk;
                yield return Tyrant;

                yield return OrcMystic;
                yield return OrcShaman;
                yield return Overlord;
                yield return Warcryer;

                yield return DwarvenFighter;
                yield return Scavenger;
                yield return BountyHunter;
                yield return Artisan;
                yield return Warsmith;

                yield return Duelist;
                yield return Dreadnought;
                yield return PhoenixKnight;
                yield return HellKnight;
                yield return Saggitarius;
                yield return Adventurer;
                yield return Archmage;
                yield return Soultaker;
                yield return ArcanaLord;
                yield return Cardinal;
                yield return Hierophant;

                yield return EvasTemplar;
                yield return SwordMuse;
                yield return WindRider;
                yield return MoonlightSentinel;
                yield return MysticMuse;
                yield return ElementalMaster;
                yield return EvasSaint;

                yield return ShillienTemplar;
                yield return SpectralDancer;
                yield return GhostHunter;
                yield return GhostSentinel;
                yield return StormScreamer;
                yield return SpectralMaster;
                yield return ShillienSaint;

                yield return Titan;
                yield return GrandKhavatari;
                yield return Dominator;
                yield return Doomcryer;

                yield return FortuneSeeker;
                yield return Maestro;
            }
        }

        private static readonly ClassId HumanFighter = new ClassId(ClassIds.HumanFighter, false, ClassRace.Human, null);
        private static readonly ClassId Warrior = new ClassId(ClassIds.Warrior, false, ClassRace.Human, HumanFighter);
        private static readonly ClassId Gladiator = new ClassId(ClassIds.Gladiator, false, ClassRace.Human, Warrior);
        private static readonly ClassId Warlord = new ClassId(ClassIds.Warlord, false, ClassRace.Human, Warrior);
        private static readonly ClassId Knight = new ClassId(ClassIds.Knight, false, ClassRace.Human, HumanFighter);
        private static readonly ClassId Paladin = new ClassId(ClassIds.Paladin, false, ClassRace.Human, Knight);
        private static readonly ClassId DarkAvenger = new ClassId(ClassIds.DarkAvenger, false, ClassRace.Human, Knight);
        private static readonly ClassId Rogue = new ClassId(ClassIds.Rogue, false, ClassRace.Human, HumanFighter);
        private static readonly ClassId TreasureHunter = new ClassId(ClassIds.TreasureHunter, false, ClassRace.Human, Rogue);
        private static readonly ClassId Hawkeye = new ClassId(ClassIds.Hawkeye, false, ClassRace.Human, Rogue);

        private static readonly ClassId HumanMystic = new ClassId(ClassIds.HumanMystic, true, ClassRace.Human, null);
        private static readonly ClassId HumanWizard = new ClassId(ClassIds.HumanWizard, true, ClassRace.Human, HumanMystic);
        private static readonly ClassId Sorcerer = new ClassId(ClassIds.Sorcerer, true, ClassRace.Human, HumanWizard);
        private static readonly ClassId Necromancer = new ClassId(ClassIds.Necromancer, true, ClassRace.Human, HumanWizard);
        private static readonly ClassId Warlock = new ClassId(ClassIds.Warlock, true, ClassRace.Human, HumanWizard);
        private static readonly ClassId Cleric = new ClassId(ClassIds.Cleric, true, ClassRace.Human, HumanMystic);
        private static readonly ClassId Bishop = new ClassId(ClassIds.Bishop, true, ClassRace.Human, Cleric);
        private static readonly ClassId Prophet = new ClassId(ClassIds.Prophet, true, ClassRace.Human, Cleric);

        private static readonly ClassId ElvenFighter = new ClassId(ClassIds.ElvenFighter, false, ClassRace.Elf, null);
        private static readonly ClassId ElvenKnight = new ClassId(ClassIds.ElvenKnight, false, ClassRace.Elf, ElvenFighter);
        private static readonly ClassId TempleKnight = new ClassId(ClassIds.TempleKnight, false, ClassRace.Elf, ElvenKnight);
        private static readonly ClassId SwordSinger = new ClassId(ClassIds.SwordSinger, false, ClassRace.Elf, ElvenKnight);
        private static readonly ClassId ElvenScout = new ClassId(ClassIds.ElvenScout, false, ClassRace.Elf, ElvenFighter);
        private static readonly ClassId PlainsWalker = new ClassId(ClassIds.PlainsWalker, false, ClassRace.Elf, ElvenScout);
        private static readonly ClassId SilverRanger = new ClassId(ClassIds.SilverRanger, false, ClassRace.Elf, ElvenScout);

        private static readonly ClassId ElvenMystic = new ClassId(ClassIds.ElvenMystic, true, ClassRace.Elf, null);
        private static readonly ClassId ElvenWizard = new ClassId(ClassIds.ElvenWizard, true, ClassRace.Elf, ElvenMystic);
        private static readonly ClassId Spellsinger = new ClassId(ClassIds.Spellsinger, true, ClassRace.Elf, ElvenWizard);
        private static readonly ClassId ElementalSummoner = new ClassId(ClassIds.ElementalSummoner, true, ClassRace.Elf, ElvenWizard);
        private static readonly ClassId ElvenOracle = new ClassId(ClassIds.ElvenOracle, true, ClassRace.Elf, ElvenMystic);
        private static readonly ClassId ElvenElder = new ClassId(ClassIds.ElvenElder, true, ClassRace.Elf, ElvenOracle);

        private static readonly ClassId DarkFighter = new ClassId(ClassIds.DarkFighter, false, ClassRace.DarkElf, null);
        private static readonly ClassId PalusKnight = new ClassId(ClassIds.PalusKnight, false, ClassRace.DarkElf, DarkFighter);
        private static readonly ClassId ShillienKnight = new ClassId(ClassIds.ShillienKnight, false, ClassRace.DarkElf, PalusKnight);
        private static readonly ClassId Bladedancer = new ClassId(ClassIds.Bladedancer, false, ClassRace.DarkElf, PalusKnight);
        private static readonly ClassId Assassin = new ClassId(ClassIds.Assassin, false, ClassRace.DarkElf, DarkFighter);
        private static readonly ClassId AbyssWalker = new ClassId(ClassIds.AbyssWalker, false, ClassRace.DarkElf, Assassin);
        private static readonly ClassId PhantomRanger = new ClassId(ClassIds.PhantomRanger, false, ClassRace.DarkElf, Assassin);

        private static readonly ClassId DarkMystic = new ClassId(ClassIds.DarkMystic, true, ClassRace.DarkElf, null);
        private static readonly ClassId DarkWizard = new ClassId(ClassIds.DarkWizard, true, ClassRace.DarkElf, DarkMystic);
        private static readonly ClassId Spellhowler = new ClassId(ClassIds.Spellhowler, true, ClassRace.DarkElf, DarkWizard);
        private static readonly ClassId PhantomSummoner = new ClassId(ClassIds.PhantomSummoner, true, ClassRace.DarkElf, DarkWizard);
        private static readonly ClassId ShillienOracle = new ClassId(ClassIds.ShillienOracle, true, ClassRace.DarkElf, DarkMystic);
        private static readonly ClassId ShillienElder = new ClassId(ClassIds.ShillienElder, true, ClassRace.DarkElf, ShillienOracle);

        private static readonly ClassId OrcFighter = new ClassId(ClassIds.OrcFighter, false, ClassRace.Orc, null);
        private static readonly ClassId OrcRaider = new ClassId(ClassIds.OrcRaider, false, ClassRace.Orc, OrcFighter);
        private static readonly ClassId Destroyer = new ClassId(ClassIds.Destroyer, false, ClassRace.Orc, OrcRaider);
        private static readonly ClassId Monk = new ClassId(ClassIds.Monk, false, ClassRace.Orc, OrcFighter);
        private static readonly ClassId Tyrant = new ClassId(ClassIds.Tyrant, false, ClassRace.Orc, Monk);

        private static readonly ClassId OrcMystic = new ClassId(ClassIds.OrcMystic, true, ClassRace.Orc, null);
        private static readonly ClassId OrcShaman = new ClassId(ClassIds.OrcShaman, true, ClassRace.Orc, OrcMystic);
        private static readonly ClassId Overlord = new ClassId(ClassIds.Overlord, true, ClassRace.Orc, OrcShaman);
        private static readonly ClassId Warcryer = new ClassId(ClassIds.Warcryer, true, ClassRace.Orc, OrcShaman);

        private static readonly ClassId DwarvenFighter = new ClassId(ClassIds.DwarvenFighter, false, ClassRace.Dwarf, null);
        private static readonly ClassId Scavenger = new ClassId(ClassIds.Scavenger, false, ClassRace.Dwarf, DwarvenFighter);
        private static readonly ClassId BountyHunter = new ClassId(ClassIds.BountyHunter, false, ClassRace.Dwarf, Scavenger);
        private static readonly ClassId Artisan = new ClassId(ClassIds.Artisan, false, ClassRace.Dwarf, DwarvenFighter);
        private static readonly ClassId Warsmith = new ClassId(ClassIds.Warsmith, false, ClassRace.Dwarf, Artisan);

        private static readonly ClassId Duelist = new ClassId(ClassIds.Duelist, false, ClassRace.Human, Gladiator);
        private static readonly ClassId Dreadnought = new ClassId(ClassIds.Dreadnought, false, ClassRace.Human, Warlord);
        private static readonly ClassId PhoenixKnight = new ClassId(ClassIds.PhoenixKnight, false, ClassRace.Human, Paladin);
        private static readonly ClassId HellKnight = new ClassId(ClassIds.HellKnight, false, ClassRace.Human, DarkAvenger);
        private static readonly ClassId Saggitarius = new ClassId(ClassIds.Saggitarius, false, ClassRace.Human, Hawkeye);
        private static readonly ClassId Adventurer = new ClassId(ClassIds.Adventurer, false, ClassRace.Human, TreasureHunter);
        private static readonly ClassId Archmage = new ClassId(ClassIds.Archmage, true, ClassRace.Human, Sorcerer);
        private static readonly ClassId Soultaker = new ClassId(ClassIds.Soultaker, true, ClassRace.Human, Necromancer);
        private static readonly ClassId ArcanaLord = new ClassId(ClassIds.ArcanaLord, true, ClassRace.Human, Warlock);
        private static readonly ClassId Cardinal = new ClassId(ClassIds.Cardinal, true, ClassRace.Human, Bishop);
        private static readonly ClassId Hierophant = new ClassId(ClassIds.Hierophant, true, ClassRace.Human, Prophet);

        private static readonly ClassId EvasTemplar = new ClassId(ClassIds.EvasTemplar, false, ClassRace.Elf, TempleKnight);
        private static readonly ClassId SwordMuse = new ClassId(ClassIds.SwordMuse, false, ClassRace.Elf, SwordSinger);
        private static readonly ClassId WindRider = new ClassId(ClassIds.WindRider, false, ClassRace.Elf, PlainsWalker);
        private static readonly ClassId MoonlightSentinel = new ClassId(ClassIds.MoonlightSentinel, false, ClassRace.Elf, SilverRanger);
        private static readonly ClassId MysticMuse = new ClassId(ClassIds.MysticMuse, true, ClassRace.Elf, Spellsinger);
        private static readonly ClassId ElementalMaster = new ClassId(ClassIds.ElementalMaster, true, ClassRace.Elf, ElementalSummoner);
        private static readonly ClassId EvasSaint = new ClassId(ClassIds.EvasSaint, true, ClassRace.Elf, ElvenElder);

        private static readonly ClassId ShillienTemplar = new ClassId(ClassIds.ShillienTemplar, false, ClassRace.DarkElf, ShillienKnight);
        private static readonly ClassId SpectralDancer = new ClassId(ClassIds.SpectralDancer, false, ClassRace.DarkElf, Bladedancer);
        private static readonly ClassId GhostHunter = new ClassId(ClassIds.GhostHunter, false, ClassRace.DarkElf, AbyssWalker);
        private static readonly ClassId GhostSentinel = new ClassId(ClassIds.GhostSentinel, false, ClassRace.DarkElf, PhantomRanger);
        private static readonly ClassId StormScreamer = new ClassId(ClassIds.StormScreamer, true, ClassRace.DarkElf, Spellhowler);
        private static readonly ClassId SpectralMaster = new ClassId(ClassIds.SpectralMaster, true, ClassRace.DarkElf, PhantomSummoner);
        private static readonly ClassId ShillienSaint = new ClassId(ClassIds.ShillienSaint, true, ClassRace.DarkElf, ShillienElder);

        private static readonly ClassId Titan = new ClassId(ClassIds.Titan, true, ClassRace.Orc, Destroyer);
        private static readonly ClassId GrandKhavatari = new ClassId(ClassIds.GrandKhavatari, true, ClassRace.Orc, Tyrant);
        private static readonly ClassId Dominator = new ClassId(ClassIds.Dominator, true, ClassRace.Orc, Overlord);
        private static readonly ClassId Doomcryer = new ClassId(ClassIds.Doomcryer, true, ClassRace.Orc, Warcryer);

        private static readonly ClassId FortuneSeeker = new ClassId(ClassIds.FortuneSeeker, false, ClassRace.Dwarf, BountyHunter);
        private static readonly ClassId Maestro = new ClassId(ClassIds.Maestro, false, ClassRace.Dwarf, Warsmith);

        public static ClassId GetClass(int classId)
        {
            return Values.Where(cl => (int)cl.Id == classId).FirstOrDefault();
        }

        public int Level()
        {
            if (Parent == null)
                return 0;

            return 1 + Parent.Level();
        }

    }
}
