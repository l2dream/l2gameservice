﻿using System.Threading.Tasks;
using Core.Model.Skills;

namespace Core.Model.Player.SkillMagic
{
    public interface IPlayerSkillMagicValidator
    {
        Task<bool> IsValid(PlayerInstance playerInstance, Skill skill);
    }
}