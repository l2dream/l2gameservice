﻿using System.Threading.Tasks;
using Core.Model.Skills;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Player.SkillMagic
{
    public class GeneralPlayerSkillMagicValidator : IPlayerSkillMagicValidator
    {
        public async Task<bool> IsValid(PlayerInstance playerInstance, Skill skill)
        {
            if (await IsDead(playerInstance)) return false;
            // Check if the skill is active
            if (IsActiveSkill(playerInstance, skill)) return false;
            // prevent casting signets to peace zone
            if (IsCastingInPeaceZone(playerInstance, skill)) return false;
            if (IsSkillDisabled(playerInstance, skill)) return false;
            if (IsCastingNow(playerInstance)) return false;
            return true;
        }

        /// <summary>
        /// Check if skill is in reuse time
        /// </summary>
        /// <param name="playerInstance"></param>
        /// <param name="skill"></param>
        /// <returns></returns>
        private bool IsSkillDisabled(PlayerInstance playerInstance, Skill skill)
        {
            if (playerInstance.MagicCast().IsSkillDisabled(skill))
            {
                SystemMessage sm = new SystemMessage(SystemMessageId.S1PreparedForReuse);
                sm.AddSkillName(skill.Id, skill.Level);
                //playerInstance.SendPacket(sm);
                return true;
            }
            return false;
        }

        private bool IsCastingNow(PlayerInstance playerInstance)
        {
            if (playerInstance.MagicCast().IsCastingNow())
            {
                return true;
            }
            return false;
        }

        private bool IsCastingInPeaceZone(PlayerInstance playerInstance, Skill skill)
        {
            if (((skill.GetSkillType() == SkillType.Signet) || (skill.GetSkillType() == SkillType.SignetCastTime)) &&
                playerInstance.Zone().IsInsidePeaceZone(playerInstance))
            {
                SystemMessage sm = new SystemMessage(SystemMessageId.S1CannotBeUsed);
                sm.AddSkillName(skill.Id, 1);
                //playerInstance.SendPacket(sm);
                return true;
            }
            return false;
        }

        private bool IsActiveSkill(PlayerInstance playerInstance, Skill skill)
        {
            if (skill.IsPassive)
            {
                // just ignore the passive skill request. why does the client send it anyway ??
                // Send a Server->Client packet ActionFailed to the PlayerInstance
                return true;
            }
            return false;
        }

        private async Task<bool> IsDead(PlayerInstance playerInstance)
        {
            if (playerInstance.Status.IsDead())
            {
                await playerInstance.MagicCast().AbortCastAsync();
                await playerInstance.SendActionFailedPacketAsync();
                return true;
            }
            return false;
        }
    }
}