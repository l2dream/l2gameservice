﻿using System.Threading.Tasks;
using Core.AI;
using Core.Model.Items;
using Core.Model.Items.Handlers;
using Core.Model.Items.Type;
using Core.Model.World;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;
using L2Logger;

namespace Core.Model.Player
{
    public sealed class PlayerPickup
    {
        private readonly PlayerInstance _playerInstance;
        private readonly ItemInstance _itemInstance;
        private readonly PlayerInventory _playerInventory;
        private const int AdenaId = 57;
        public PlayerPickup(PlayerInstance playerInstance, ItemInstance itemInstance)
        {
            _playerInstance = playerInstance;
            _itemInstance = itemInstance;
            _playerInventory = _playerInstance.PlayerInventory();
        }

        /// <summary>
        /// Manage Pickup Task
        /// </summary>
        public async Task DoPickupItemAsync()
        {
            if (_playerInstance.IsAlikeDead || _playerInstance.Status.IsFakeDeath())
            {
                return;
            }
            // Set the AI Intention to AI_INTENTION_IDLE
            _playerInstance.AI.SetIntention(CtrlIntention.AiIntentionIdle);
            // Send a Server->Client packet StopMove to this PlayerInstance
            await _playerInstance.SendPacketAsync(new StopMove(_playerInstance));

            // Check if the target to pick up is visible
            if (!_itemInstance.IsVisible())
            {
                // Send a Server->Client packet ActionFailed to this PlayerInstance
                await _playerInstance.SendActionFailedPacketAsync();
                return;
            }
            
            // Remove the ItemInstance from the world and send server->client GetItem packets
            await _itemInstance.PickupMeAsync(_playerInstance).ContinueWith(HandleException);
            await DoPickUp();
        }

        private void HandleException(Task obj)
        {
            if (obj.IsFaulted)
            {
                LoggerManager.Error(GetType().Name + " " + obj.Exception);
            }
        }

        private async Task DoPickUp()
        {
            if (_itemInstance.Item is EtcItem etcItem)
            {
                await PickUpEtcItem(etcItem);
                return;
            }
            SystemMessage msg = new SystemMessage(SystemMessageId.AttentionS1PickedUpS2);
            msg.AddString(_itemInstance.Item.Name);
            msg.AddItemName(_itemInstance.ItemId);
            await _playerInstance.SendBroadcastPacketAsync(msg, 1400);
            await AddItem(ItemAction.Pickup, _itemInstance, null, true);
        }

        private async Task PickUpEtcItem(EtcItem etcItem)
        {
            // Auto use herbs - pick up
            if (etcItem.EtcItemType.Id == EtcItemTypeId.Herb)
            {
                //TODO 
                IItemHandler handler = Initializer.ItemHandlerService().GetItemHandler(_itemInstance.ItemId);
                await handler.UseItemAsync(_playerInstance, _itemInstance);
            }
            else if (etcItem.EtcItemType.Id == EtcItemTypeId.Money)
            {
                if (_itemInstance.ItemId == AdenaId)
                {
                    await AddAdena(ItemAction.Pickup, _itemInstance.Count, null, true);
                }
            }
            else
            {
                await AddItem(ItemAction.Pickup, _itemInstance, null, true);
            }
        }

        private async Task AddItem(ItemAction process, ItemInstance item, WorldObject reference, bool sendMessage)
        {
            if (item.Count > 0)
            {
                // Sends message to client if requested
                if (sendMessage)
                {
                    if (item.Count > 1)
                    {
                        SystemMessage sm = new SystemMessage(SystemMessageId.YouPickedUpS2S1);
                        sm.AddItemName(item.ItemId);
                        sm.AddNumber(item.Count);
                        await _playerInstance.SendPacketAsync(sm);
                    }
                }
                // Add the item to inventory
                ItemInstance newItem = _playerInventory.AddItem(process, item, _playerInstance, reference);
                InventoryUpdate inventoryUpdate = new InventoryUpdate();
                inventoryUpdate.AddItem(newItem);
                await _playerInstance.SendPacketAsync(inventoryUpdate);
                
                // Update current load as well
                StatusUpdate su = new StatusUpdate(_playerInstance.ObjectId);
                su.AddAttribute(StatusUpdate.CurLoad, _playerInstance.Stat.CurrentLoad());
                await _playerInstance.SendPacketAsync(su);
            }
        }

        /// <summary>
        /// Add adena to Inventory of the PlayerInstance and send a Server->Client InventoryUpdate packet to the PlayerInstance.
        /// </summary>
        /// <param name="process"></param>
        /// <param name="count"></param>
        /// <param name="reference"></param>
        /// <param name="sendMessage"></param>
        private async Task AddAdena(ItemAction process, int count, WorldObject reference, bool sendMessage)
        {
            if (count > 0)
            {
                if (_playerInventory.GetAdena() == int.MaxValue)
                {
                    return;
                }
                else if (_playerInventory.GetAdena() >= (int.MaxValue - count))
                {
                    count = int.MaxValue - _playerInventory.GetAdena();
                    _playerInventory.AddAdena(process, count, _playerInstance, reference);
                }
                else if (_playerInventory.GetAdena() < (int.MaxValue - count))
                {
                    _playerInventory.AddAdena(process, count, _playerInstance, reference);
                }
                if (sendMessage)
                {
                    SystemMessage sm = new SystemMessage(SystemMessageId.EarnedS1Adena);
                    sm.AddNumber(count);
                    await _playerInstance.SendPacketAsync(sm);
                }
            }
        }
    }
}