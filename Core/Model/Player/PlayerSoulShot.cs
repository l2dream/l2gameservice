﻿using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace Core.Model.Player
{
    public class PlayerSoulShot
    {
        private readonly PlayerInstance _playerInstance;
        private readonly ConcurrentDictionary<int, int> _activeSoulShots;
        private readonly PlayerInventory _playerInventory;

        public PlayerSoulShot(PlayerInstance playerInstance)
        {
            _activeSoulShots = new ConcurrentDictionary<int, int>();
            _playerInstance = playerInstance;
            _playerInventory = _playerInstance.PlayerInventory();
        }
        
        public void AddAutoSoulShot(int itemId)
        {
            _activeSoulShots.TryAdd(itemId, itemId);
        }
        
        public void RemoveAutoSoulShot(int itemId)
        {
            _activeSoulShots.TryRemove(itemId, out _);
        }
        
        public ConcurrentDictionary<int, int> GetAutoSoulShot()
        {
            return _activeSoulShots;
        }

        public async Task RechargeAutoSoulShotAsync()
        {
            foreach (var itemId in GetAutoSoulShot().Values)
            {
                var item = _playerInventory.GetItemByItemId(itemId);
                if (item != null)
                {
                    var handler = Initializer.ItemHandlerService().GetItemHandler(itemId);
                    await handler.UseItemAsync(_playerInstance, item);
                }
                else
                {
                    RemoveAutoSoulShot(itemId);
                }
            }
        }
    }
}