﻿using System.Threading.Tasks;

namespace Core.Model.Player.ActionUse
{
    public interface IActionUseHandler
    {
        Task DoActionAsync(PlayerInstance playerInstance);
    }
}