﻿using System.Threading.Tasks;
using Core.AI;
using Core.Model.Items;
using Core.Model.Items.Handlers;
using Core.Model.Player.UseItem;
using Core.Model.World;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;
using L2Logger;

namespace Core.Model.Player
{
    public class PlayerUseItem
    {
        private readonly PlayerInstance _playerInstance;
        private IPlayerUseItemValidator Validator { get; }

        public PlayerUseItem(PlayerInstance playerInstance)
        {
            _playerInstance = playerInstance;
            Validator = new GeneralPlayerUseItemValidator(playerInstance);
        }

        public async Task UseItemAsync(ItemInstance itemInstance)
        {
            if (!Validator.IsValid(itemInstance))
            {
                return;
            }
            if (itemInstance.IsEquippable())
            {
                await UseEquippedItemAsync(itemInstance);
                return;
            }
            IItemHandler handler = Initializer.ItemHandlerService().GetItemHandler(itemInstance.ItemId);
            await handler.UseItemAsync(_playerInstance, itemInstance);
            LoggerManager.Info(handler.GetType().Name + " " + "TODO Use Item handler");
        }

        private async Task UseEquippedItemAsync(ItemInstance itemInstance)
        {
            if (!Validator.IsValidEquipped())
            {
                return;
            }
            await IntentionAttackAsync(itemInstance);
            await EquipUnEquipItemAsync(itemInstance);
            SendStatusUpdateAsync();
        }

        private void SendStatusUpdateAsync()
        {
            _playerInstance.Attack().AbortAttackAsync();
            _playerInstance.SendPacketAsync(new EtcStatusUpdate(_playerInstance));
            // If an "invisible" item has changed (Jewels, helmet), we dont need to send broadcast packet to all other users.
            _playerInstance.SendPacketAsync(new UserInfo(_playerInstance));
            _playerInstance.SendPacketAsync(new ItemList(_playerInstance, true));
        }

        /// <summary>
        /// Equip or unEquip
        /// </summary>
        /// <param name="itemInstance"></param>
        private async Task EquipUnEquipItemAsync(ItemInstance itemInstance)
        {
            
            if (itemInstance.IsEquipped())
            {
                await SendMessageUnEquipAsync(itemInstance);
                int bodyPart = GetBodyPart(itemInstance);
                _playerInstance.PlayerInventory().UnEquipItemInBodySlotAndRecord(bodyPart);
                return;
            }
            await SendMessageEquipAsync(itemInstance);
            _playerInstance.PlayerInventory().EquipItemAndRecord(itemInstance);
        }

        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemInstance"></param>
        /// <returns></returns>
        private static int GetBodyPart(ItemInstance itemInstance)
        {
            int bodyPart = itemInstance.LocData switch
            {
                1 => (int) ItemSlotId.SlotLEar,
                2 => (int) ItemSlotId.SlotREar,
                4 => (int) ItemSlotId.SlotLFinger,
                5 => (int) ItemSlotId.SlotRFinger,
                _ => (int) itemInstance.Item.BodyPart
            };
            return bodyPart;
        }

        private async Task SendMessageEquipAsync(ItemInstance itemInstance)
        {
            SystemMessage sm;
            if (itemInstance.EnchantLevel > 0)
            {
                sm = new SystemMessage(SystemMessageId.S1S2Equipped);
                sm.AddNumber(itemInstance.EnchantLevel);
                sm.AddItemName(itemInstance.ItemId);
            }
            else
            {
                sm = new SystemMessage(SystemMessageId.S1Equipped);
                sm.AddItemName(itemInstance.ItemId);
            }
            await _playerInstance.SendPacketAsync(sm);
        }
        
        private async Task SendMessageUnEquipAsync(ItemInstance itemInstance)
        {
            SystemMessage sm;
            if (itemInstance.EnchantLevel > 0)
            {
                sm = new SystemMessage(SystemMessageId.EquipmentS1S2Removed);
                sm.AddNumber(itemInstance.EnchantLevel);
                sm.AddItemName(itemInstance.ItemId);
            }
            else
            {
                sm = new SystemMessage(SystemMessageId.S1Disarmed);
                sm.AddItemName(itemInstance.ItemId);
            }
            await _playerInstance.SendPacketAsync(sm);
        }

        private async Task IntentionAttackAsync(ItemInstance itemInstance)
        {
            int bodyPart = (int) itemInstance.Item.BodyPart;
            if (_playerInstance.IsMoving && _playerInstance.Attack().IsAttackingNow() && ((bodyPart == (int) ItemSlotId.SlotLRHand) ||
                (bodyPart == (int) ItemSlotId.SlotLHand) ||
                (bodyPart == (int) ItemSlotId.SlotRHand)))
            {
                WorldObject target = _playerInstance.Target.GetTarget();
                _playerInstance.Target.RemoveTargetAsync();
                await _playerInstance.Movement().StopMoveAsync(null);
                _playerInstance.Target.SetTarget(target);
                _playerInstance.AI.SetIntention(CtrlIntention.AiIntentionAttack);
            }
        }
    }
}