﻿using System;

namespace Core.Model.Zones.Form
{
    public class ZoneCylinder : ZoneForm
    {
        private int _x;
        private int _y;
        private int _z1;
        private int _z2;
        private int _rad;
        private int _radS;

        public ZoneCylinder(int x, int y, int z1, int z2, int rad)
        {
            _x = x;
            _y = y;
            _z1 = z1;
            _z2 = z2;
            _rad = rad;
            _radS = rad * rad;
        }
        public override bool IsInsideZone(int x, int y, int z)
        {
            return ((Math.Pow(_x - x, 2) + Math.Pow(_y - y, 2)) <= _radS) && (z >= _z1) && (z <= _z2);
        }

        public override bool IntersectsRectangle(int ax1, int ax2, int ay1, int ay2)
        {
            // Circles point inside the rectangle?
            if ((_x > ax1) && (_x < ax2) && (_y > ay1) && (_y < ay2))
            {
                return true;
            }
		
            // Any point of the rectangle intersecting the Circle?
            if ((Math.Pow(ax1 - _x, 2) + Math.Pow(ay1 - _y, 2)) < _radS)
            {
                return true;
            }
		
            if ((Math.Pow(ax1 - _x, 2) + Math.Pow(ay2 - _y, 2)) < _radS)
            {
                return true;
            }
		
            if ((Math.Pow(ax2 - _x, 2) + Math.Pow(ay1 - _y, 2)) < _radS)
            {
                return true;
            }
		
            if ((Math.Pow(ax2 - _x, 2) + Math.Pow(ay2 - _y, 2)) < _radS)
            {
                return true;
            }
		
            // Collision on any side of the rectangle?
            if ((_x > ax1) && (_x < ax2))
            {
                if (Math.Abs(_y - ay2) < _rad)
                {
                    return true;
                }
			
                if (Math.Abs(_y - ay1) < _rad)
                {
                    return true;
                }
            }
		
            if ((_y > ay1) && (_y < ay2))
            {
                if (Math.Abs(_x - ax2) < _rad)
                {
                    return true;
                }
			
                if (Math.Abs(_x - ax1) < _rad)
                {
                    return true;
                }
            }
		
            return false;
        }

        public override double GetDistanceToZone(int x, int y)
        {
            if (IsInsideZone(x, y, _z1))
            {
                return 0; // If you are inside the zone distance to zone is 0.
            }
            return Math.Sqrt((Math.Pow(_x - x, 2) + Math.Pow(_y - y, 2))) - _rad;
        }

        public override int GetLowZ()
        {
            return _z1;
        }

        public override int GetHighZ()
        {
            return _z2;
        }

        public override void VisualizeZone(int id, int z)
        {
            throw new System.NotImplementedException();
        }
    }
}