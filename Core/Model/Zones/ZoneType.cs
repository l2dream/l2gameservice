﻿using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using Core.Model.Actor;
using Core.Model.Player;

namespace Core.Model.Zones
{
    public abstract class ZoneType
    {
        public int Id { get; }
        private bool _checkAffected;
        private int _minLevel;
        private int _maxLevel;
        private int _classType;
        private int[] _race;
        private int[] _class;

        private readonly ConcurrentDictionary<int, Character> _characterList;
        
        public ZoneForm Zone { get; set; }

        public ZoneType(int id)
        {
            Id = id;
            _checkAffected = false;
            _minLevel = 0;
            _maxLevel = 250;
            _classType = 0;
            _characterList = new ConcurrentDictionary<int, Character>();
        }
        
        public virtual void SetParameter(string name, string value)
        {
            _checkAffected = true;
            
            switch (name)
            {
                // Minimum level
                case "affectedLvlMin":
                    _minLevel = int.Parse(value);
                    break;
                // Maximum level
                case "affectedLvlMax":
                    _maxLevel = int.Parse(value);
                    break;
                // Affected Races
                case "affectedRace":
                    // Create a new array holding the affected race
                    if (_race == null)
                    {
                        _race = new int[1];
                        _race[0] = int.Parse(value);
                    }
                    else
                    {
                        int[] temp = new int[_race.Length + 1];
                        int i = 0;
                        for (; i < _race.Length; i++)
                        {
                            temp[i] = _race[i];
                        }
                        temp[i] = int.Parse(value);
                        _race = temp;
                    }
                    break;
                // Affected classes
                case "affectedClassId":
                    // Create a new array holding the affected classIds
                    if (_class == null)
                    {
                        _class = new int[1];
                        _class[0] = int.Parse(value);
                    }
                    else
                    {
                        int[] temp = new int[_class.Length + 1];
                        int i = 0;
                        for (; i < _class.Length; i++)
                        {
                            temp[i] = _class[i];
                        }
                        temp[i] = int.Parse(value);
                        _class = temp;
                    }
                    break;
                // Affected class type
                case "affectedClassType":
                    _classType = value.Equals("Fighter") ? 1 : 2;
                    break;
            }
        }
        
        public bool IsInsideZone(int x, int y, int z)
        {
            return Zone.IsInsideZone(x, y, z);
        }
        
        public double GetDistanceToZone(int x, int y)
        {
            return Zone.GetDistanceToZone(x, y);
        }
        
        public void VisualizeZone(int z)
        {
            Zone.VisualizeZone(Id, z);
        }
        
        
        private bool IsAffected(Character character)
        {
            // Check lvl
            if ((character.Stat.Level < _minLevel) || (character.Stat.Level > _maxLevel))
            {
                return false;
            }
		
            if (character is PlayerInstance playerInstance)
            {
                // Check class type
                if (_classType != 0)
                {
                    if (playerInstance.IsMageClass)
                    {
                        if (_classType == 1)
                        {
                            return false;
                        }
                    }
                    else if (_classType == 2)
                    {
                        return false;
                    }
                }
			
                // Check race
                if (_race != null)
                {
                    return _race.Any(element => playerInstance.Template.Stat.Race == (ClassRace) element);
                }
			
                // Check class
                if (_class != null)
                {
                    return _class.Any(classId => playerInstance.Template.Stat.ClassId.Id == (ClassIds) classId);
                }
            }
            return true;
        }
        
        public void RevalidateInZone(Character character)
        {
            // If the character can't be affected by this zone return
            if (_checkAffected && !IsAffected(character))
            {
                return;
            }
		
            // If the object is inside the zone...
            if (Zone.IsInsideZone(character.GetX(), character.GetY(), character.GetZ()))
            {
                if (!_characterList.ContainsKey(character.ObjectId))
                {
                    // Was the character not yet inside this zone?
                    _characterList.TryAdd(character.ObjectId, character);
                    OnEnter(character);
                }
            }
            // Was the character inside this zone?
            else if (_characterList.ContainsKey(character.ObjectId))
            {
                _characterList.TryRemove(character.ObjectId, out _);
                OnExit(character);
            }
        }
        
        protected abstract void OnEnter(Character character);
        protected abstract void OnExit(Character character);
        protected internal abstract void OnDieInside(Character character);
        protected internal abstract void OnReviveInside(Character character);

        public void RemoveCharacter(Character character)
        {
            if (_characterList.TryRemove(character.ObjectId, out _))
            {
                OnExit(character);
            }
        }
    }
}