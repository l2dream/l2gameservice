﻿using System.Collections.Generic;
using System.Linq;
using Core.Model.Actor;

namespace Core.Model.Zones
{
    /// <summary>
    /// This class manages all zones for a given world region
    /// </summary>
    public class ZoneManager
    {
        private readonly List<ZoneType> _zones;

        public ZoneManager()
        {
            _zones = new List<ZoneType>();
        }
        
        public void RegisterNewZone(ZoneType zone)
        {
            _zones.Add(zone);
        }
        
        public void RevalidateZones(Character character)
        {
            _zones.ForEach(e =>
            {
                e?.RevalidateInZone(character);
            });
        }
        
        public void RemoveCharacter(Character character)
        {
            _zones.ForEach(e =>
            {
                e?.RemoveCharacter(character);
            });
        }
        
        public void OnDeath(Character character)
        {
            _zones.ForEach(e =>
            {
                e?.OnDieInside(character);
            });
        }
        
        public void OnRevive(Character character)
        {
            _zones.ForEach(e =>
            {
                e?.OnReviveInside(character);
            });
        }
        
        public void UnregisterZone(ZoneType zone)
        {
            _zones.Remove(zone);
        }
        
        public List<ZoneType> GetZones()
        {
            return _zones;
        }
    }
}