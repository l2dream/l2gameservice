﻿using Core.Model.Actor;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class ScriptZone : ZoneType
    {
        public ScriptZone(int id) : base(id)
        {
        }

        protected override void OnEnter(Character character)
        {
            LoggerManager.Info("OnEnter ScriptZone");
        }

        protected override void OnExit(Character character)
        {
            LoggerManager.Info("OnExit ScriptZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside ScriptZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside ScriptZone");
        }
    }
}