﻿using Core.Model.Actor;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class BossZone : ZoneType
    {
        public BossZone(int id, int bossId) : base(id)
        {
        }


        protected override void OnEnter(Character character)
        {
            LoggerManager.Info("OnEnter BossZone");
        }

        protected override void OnExit(Character character)
        {
            LoggerManager.Info("OnExit BossZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside BossZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside BossZone");
        }
    }
}