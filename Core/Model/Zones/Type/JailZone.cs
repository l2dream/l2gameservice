﻿using Core.Model.Actor;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class JailZone : ZoneType
    {
        public JailZone(int id) : base(id)
        {
        }

        protected override void OnEnter(Character character)
        {
            LoggerManager.Info("OnEnter JailZone");
        }

        protected override void OnExit(Character character)
        {
            LoggerManager.Info("OnExit JailZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside JailZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside JailZone");
        }
    }
}