﻿using Core.Model.Actor;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class NoSummonFriendZone : ZoneType
    {
        public NoSummonFriendZone(int id) : base(id)
        {
        }

        protected override void OnEnter(Character character)
        {
            LoggerManager.Info("OnEnter NoSummonFriendZone");
        }

        protected override void OnExit(Character character)
        {
            LoggerManager.Info("OnExit NoSummonFriendZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside NoSummonFriendZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside NoSummonFriendZone");
        }
    }
}