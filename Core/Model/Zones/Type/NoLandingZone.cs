﻿using Core.Model.Actor;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class NoLandingZone : ZoneType
    {
        public NoLandingZone(int id) : base(id)
        {
        }

        protected override void OnEnter(Character character)
        {
            LoggerManager.Info("OnEnter NoLandingZone");
        }

        protected override void OnExit(Character character)
        {
            LoggerManager.Info("OnExit NoLandingZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside NoLandingZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside NoLandingZone");
        }
    }
}