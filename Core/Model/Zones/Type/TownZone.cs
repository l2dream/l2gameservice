﻿using System;
using Core.Model.Actor;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class TownZone : ZoneRespawn
    {
        public int TaxById { get; private set; }
        public string TownName { get; set; }
        public bool IsPeaceZone { get; private set; }
        public int TownId { get; private set; }
        public int RedirectTownId { get; set; }
        
        public TownZone(int id) : base(id)
        {
            TaxById = 0;
            IsPeaceZone = true;
        }
        
        public override void SetParameter(string name, string value)
        {
            if (name.Equals("name"))
            {
                TownName = value;
            } 
            else if (name.Equals("townId"))
            {
                TownId = Convert.ToInt32(value);
            } 
            else if (name.Equals("redirectTownId"))
            {
                RedirectTownId = Convert.ToInt32(value);
            }
            else if (name.Equals("taxById"))
            {
                TaxById = Convert.ToInt32(value);
            } 
            else if (name.Equals("noPeace"))
            {
                IsPeaceZone = bool.Parse(value);
            }
            else
            {
                base.SetParameter(name, value);
            }
        }

        protected override void OnEnter(Character character)
        {
            if (IsPeaceZone)
                character.Zone().SetInsideZone(ZoneId.Peace, true);
            character.Zone().SetInsideZone(ZoneId.Town, true);
            //LoggerManager.Info("OnEnter TownZone");
        }

        protected override void OnExit(Character character)
        {
            if (IsPeaceZone)
                character.Zone().SetInsideZone(ZoneId.Peace, false);

            character.Zone().SetInsideZone(ZoneId.Town, false);
            //LoggerManager.Info("OnExit TownZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside TownZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside TownZone");
        }
    }
}