﻿using Core.Model.Actor;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class FortZone : ZoneRespawn
    {
        public FortZone(int id) : base(id)
        {
        }

        protected override void OnEnter(Character character)
        {
            LoggerManager.Info("OnEnter FortZone");
        }

        protected override void OnExit(Character character)
        {
            LoggerManager.Info("OnExit FortZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside FortZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside FortZone");
        }
    }
}