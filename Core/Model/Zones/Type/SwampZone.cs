﻿using Core.Model.Actor;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class SwampZone : ZoneType
    {
        public SwampZone(int id) : base(id)
        {
        }

        protected override void OnEnter(Character character)
        {
            LoggerManager.Info("OnEnter SwampZone");
        }

        protected override void OnExit(Character character)
        {
            LoggerManager.Info("OnExit SwampZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside SwampZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside SwampZone");
        }
    }
}