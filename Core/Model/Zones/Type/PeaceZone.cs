﻿using Core.Model.Actor;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class PeaceZone : ZoneType
    {
        public PeaceZone(int id) : base(id)
        {
        }

        protected override void OnEnter(Character character)
        {
            character.Zone().SetInsideZone(ZoneId.Peace, true);
            //LoggerManager.Info("OnEnter PeaceZone");
        }

        protected override void OnExit(Character character)
        {
            character.Zone().SetInsideZone(ZoneId.Peace, false);
            //LoggerManager.Info("OnExit PeaceZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside PeaceZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside PeaceZone");
        }
    }
}