﻿using Core.Model.Actor;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class WaterZone : ZoneType
    {
        public WaterZone(int id) : base(id)
        {
        }

        protected override void OnEnter(Character character)
        {
            character.Zone().SetInsideZone(ZoneId.Water, true);
            //LoggerManager.Info("OnEnter WaterZone");
        }

        protected override void OnExit(Character character)
        {
            character.Zone().SetInsideZone(ZoneId.Water, false);
            //LoggerManager.Info("OnExit WaterZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside WaterZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside WaterZone");
        }
    }
}