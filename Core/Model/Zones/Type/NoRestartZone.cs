﻿using Core.Model.Actor;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class NoRestartZone : ZoneType
    {
        public NoRestartZone(int id) : base(id)
        {
        }

        protected override void OnEnter(Character character)
        {
            LoggerManager.Info("OnEnter NoRestartZone");
        }

        protected override void OnExit(Character character)
        {
            LoggerManager.Info("OnExit NoRestartZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside NoRestartZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside NoRestartZone");
        }
    }
}