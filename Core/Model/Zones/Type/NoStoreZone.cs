﻿using Core.Model.Actor;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class NoStoreZone : ZoneType
    {
        public NoStoreZone(int id) : base(id)
        {
        }

        protected override void OnEnter(Character character)
        {
            LoggerManager.Info("OnEnter NoStoreZone");
        }

        protected override void OnExit(Character character)
        {
            LoggerManager.Info("OnExit NoStoreZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside NoStoreZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside NoStoreZone");
        }
    }
}