﻿using Core.Model.Actor;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class CastleTeleportZone : ZoneType
    {
        public CastleTeleportZone(int id) : base(id)
        {
        }

        protected override void OnEnter(Character character)
        {
            LoggerManager.Info("OnEnter CastleTeleportZone");
        }

        protected override void OnExit(Character character)
        {
            LoggerManager.Info("OnExit CastleTeleportZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside CastleTeleportZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside CastleTeleportZone");
        }
    }
}