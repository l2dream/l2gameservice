﻿using Core.Model.Actor;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class EffectZone : ZoneType
    {
        public EffectZone(int id) : base(id)
        {
        }

        protected override void OnEnter(Character character)
        {
            LoggerManager.Info("OnEnter EffectZone");
        }

        protected override void OnExit(Character character)
        {
            LoggerManager.Info("OnExit EffectZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside EffectZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside EffectZone");
        }
    }
}