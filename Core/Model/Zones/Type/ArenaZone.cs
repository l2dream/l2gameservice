﻿using Core.Model.Actor;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class ArenaZone : ZoneRespawn
    {
        public ArenaZone(int id) : base(id)
        {
        }
        
        protected override void OnEnter(Character character)
        {
            character.Zone().SetInsideZone(ZoneId.Pvp, true);
            character.Zone().SetInsideZone(ZoneId.NoSummonFriend, true);
        }

        protected override void OnExit(Character character)
        {
            LoggerManager.Info("OnExit ArenaZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside ArenaZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside ArenaZone");
        }
    }
}