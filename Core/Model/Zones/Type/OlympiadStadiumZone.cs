﻿﻿using Core.Model.Actor;
 using L2Logger;

 namespace Core.Model.Zones.Type
{
    public class OlympiadStadiumZone : ZoneType
    {
        public OlympiadStadiumZone(int id) : base(id)
        {
        }

        protected override void OnEnter(Character character)
        {
            LoggerManager.Info("OnEnter OlympiadStadiumZone");
        }

        protected override void OnExit(Character character)
        {
            LoggerManager.Info("OnExit OlympiadStadiumZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside OlympiadStadiumZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside OlympiadStadiumZone");
        }
    }
}