﻿using Core.Model.Actor;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class DamageZone : ZoneType
    {
        public DamageZone(int id) : base(id)
        {
        }


        protected override void OnEnter(Character character)
        {
            LoggerManager.Info("OnEnter DamageZone");
        }

        protected override void OnExit(Character character)
        {
            LoggerManager.Info("OnExit DamageZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside DamageZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside DamageZone");
        }
    }
}