﻿using Core.Model.Actor;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class FishingZone : ZoneType
    {
        public FishingZone(int id) : base(id)
        {
        }

        public int GetWaterZ()
        {
            return Zone.GetHighZ();
        }

        protected override void OnEnter(Character character)
        {
            //LoggerManager.Info("OnEnter FishingZone");
        }

        protected override void OnExit(Character character)
        {
            //LoggerManager.Info("OnExit FishingZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside FishingZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside FishingZone");
        }
    }
}