﻿using Core.Model.Actor;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class ClanHallZone : ZoneRespawn
    {
        public ClanHallZone(int id) : base(id)
        {
        }

        protected override void OnEnter(Character character)
        {
            LoggerManager.Info("OnEnter ClanHallZone");
        }

        protected override void OnExit(Character character)
        {
            LoggerManager.Info("OnExit ClanHallZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside ClanHallZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside ClanHallZone");
        }
    }
}