﻿using System;
using Core.Model.Actor;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class CastleZone : ZoneRespawn
    {
        public CastleZone(int id) : base(id)
        {
        }
        
        public override void SetParameter(string name, string value)
        {
            switch (name)
            {
                case "castleId":
                {
                    int castleId = Convert.ToInt32(value);
                    // Register self to the correct castle
                    //_castle = CastleManager.getInstance().getCastleById(castleId);
                    //_castle.setZone(this);
                    break;
                }
                default:
                {
                    base.SetParameter(name, value);
                    break;
                }
            }
        }

        protected override void OnEnter(Character character)
        {
            LoggerManager.Info("OnEnter CastleZone");
        }

        protected override void OnExit(Character character)
        {
            LoggerManager.Info("OnExit CastleZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside CastleZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside CastleZone");
        }
    }
}