﻿using Core.Model.Actor;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class PoisonZone : ZoneType
    {
        public PoisonZone(int id) : base(id)
        {
        }

        protected override void OnEnter(Character character)
        {
            LoggerManager.Info("OnEnter PoisonZone");
        }

        protected override void OnExit(Character character)
        {
            LoggerManager.Info("OnExit PoisonZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside PoisonZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside PoisonZone");
        }
    }
}