﻿using System.Threading.Tasks;
using Core.Model.Actor;
using Core.Model.Player;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class MotherTreeZone : ZoneType
    {
        public MotherTreeZone(int id) : base(id)
        {
        }

        protected override void OnEnter(Character character)
        {
            //LogerManager.Info("OnEnter MotherTreeZone");
            if (character is PlayerInstance playerInstance)
            {
                playerInstance.Zone().SetInsideZone(ZoneId.MotherTree, true);
                playerInstance.SendPacketAsync(new SystemMessage(SystemMessageId.EnterShadowMotherTree));
            }
        }

        protected override void OnExit(Character character)
        {
            //LogerManager.Info("OnExit MotherTreeZone");
            if (character is PlayerInstance playerInstance && character.Zone().IsInsideZone(ZoneId.MotherTree))
            {
                playerInstance.Zone().SetInsideZone(ZoneId.MotherTree, false);
                playerInstance.SendPacketAsync(new SystemMessage(SystemMessageId.ExitShadowMotherTree));
            }
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside MotherTreeZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside MotherTreeZone");
        }
    }
}