﻿using Core.Model.Actor;
using L2Logger;

namespace Core.Model.Zones.Type
{
    public class BigheadZone : ZoneType
    {
        public BigheadZone(int id) : base(id)
        {
        }

        protected override void OnEnter(Character character)
        {
            LoggerManager.Info("OnEnter BigheadZone");
        }

        protected override void OnExit(Character character)
        {
            LoggerManager.Info("OnExit BigheadZone");
        }

        protected internal override void OnDieInside(Character character)
        {
            LoggerManager.Info("OnDieInside BigheadZone");
        }

        protected internal override void OnReviveInside(Character character)
        {
            LoggerManager.Info("OnReviveInside BigheadZone");
        }
    }
}