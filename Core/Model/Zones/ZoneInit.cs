﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using Core.Model.World;
using Core.Model.World.RegionData;
using Core.Model.Zones.Form;
using Core.Model.Zones.Manager;
using Core.Model.Zones.Type;
using L2Logger;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Model.Zones
{
    public sealed class ZoneInit
    {
        private readonly IDictionary<System.Type, Dictionary<int, ZoneType>> _classZones;
        private int _lastDynamicId = 300000;
        private Dictionary<int, ZoneType> _map;
        private readonly WorldInit _worldInit;

        public ZoneInit(IServiceProvider serviceProvider)
        {
	        LoggerManager.Info(GetType().Name + ": Loading zones...");
	        _classZones = new Dictionary<System.Type, Dictionary<int, ZoneType>>();
	        _worldInit = serviceProvider.GetService<WorldInit>();
	        Initialize();
        }

        private void Initialize()
        {
            XmlDocument doc = new XmlDocument();
            string[] xmlFilesArray = Directory.GetFiles(@"StaticData/Xml/Zones");
            // Get the world regions
            WorldRegionData[,] worldRegions = _worldInit.AllWorldRegions;

            try
            {
                foreach (string items in xmlFilesArray)
                {
                    doc.Load(items);
                    XmlNodeList nodes = doc.DocumentElement?.SelectNodes("/list/zone");
                    if (nodes == null) continue;

                    foreach (XmlNode node in nodes)
                    {
                        XmlElement ownerElement = node.Attributes?[0].OwnerElement;
                        if (ownerElement != null && node.Attributes != null && ownerElement.Name == "zone")
                        {
                            // Generate dynamically zone's ID.
                            int zoneId = _lastDynamicId++;
                            
                            XmlNamedNodeMap attrs = node.Attributes;
                            string zoneType = attrs.GetNamedItem("type").Value;
                            string zoneShape = attrs.GetNamedItem("shape").Value;
                            int minZ = Int32.Parse(attrs.GetNamedItem("minZ").Value);
                            int maxZ = Int32.Parse(attrs.GetNamedItem("maxZ").Value);
                            
                            // Create the zone
                            ZoneType temp = null;
                            
                            switch (zoneType)
                            {
	                            case "FishingZone":
	                            {
		                            temp = new FishingZone(zoneId);
		                            break;
	                            }
								case "ClanHallZone":
								{
									temp = new ClanHallZone(zoneId);
									break;
								}
								case "PeaceZone":
								{
									temp = new PeaceZone(zoneId);
									break;
								}
								case "TownZone":
								{
									temp = new TownZone(zoneId);
									break;
								}
								case "OlympiadStadiumZone":
								{
									temp = new OlympiadStadiumZone(zoneId);
									break;
								}
								case "CastleZone":
								{
									temp = new CastleZone(zoneId);
									break;
								}
								case "FortZone":
								{
									temp = new FortZone(zoneId);
									break;
								}
								case "DamageZone":
								{
									temp = new DamageZone(zoneId);
									break;
								}
								case "ArenaZone":
								{
									temp = new ArenaZone(zoneId);
									break;
								}
								case "MotherTreeZone":
								{
									temp = new MotherTreeZone(zoneId);
									break;
								}
								case "BigheadZone":
								{
									temp = new BigheadZone(zoneId);
									break;
								}
								case "NoLandingZone":
								{
									temp = new NoLandingZone(zoneId);
									break;
								}
								case "NoRestartZone":
								{
									temp = new NoRestartZone(zoneId);
									break;
								}
								case "NoStoreZone":
								{
									temp = new NoStoreZone(zoneId);
									break;
								}
								case "NoSummonFriendZone":
								{
									temp = new NoSummonFriendZone(zoneId);
									break;
								}
								case "JailZone":
								{
									temp = new JailZone(zoneId);
									break;
								}
								case "DerbyTrackZone":
								{
									temp = new DerbyTrackZone(zoneId);
									break;
								}
								case "WaterZone":
								{
									temp = new WaterZone(zoneId);
									break;
								}
								case "HqZone":
								{
									temp = new HqZone(zoneId);
									break;
								}
								case "BossZone":
								{
									int bossId = Int32.Parse(attrs.GetNamedItem("bossId").Value);
									temp = new BossZone(zoneId, bossId);
									break;
								}
								case "EffectZone":
								{
									temp = new EffectZone(zoneId);
									break;
								}
								case "PoisonZone":
								{
									temp = new PoisonZone(zoneId);
									break;
								}
								case "ScriptZone":
								{
									temp = new ScriptZone(zoneId);
									break;
								}
								case "CastleTeleportZone":
								{
									temp = new CastleTeleportZone(zoneId);
									break;
								}
								case "SwampZone":
								{
									temp = new SwampZone(zoneId);
									break;
								}
								default:
									LoggerManager.Warn($"ZoneData: No such zone type: {zoneType}");
									continue;
                            }
                            
                            try
                            {

	                            List<int[]> coords = new List<int[]>();
	                            foreach (XmlNode innerData in node.ChildNodes)
	                            {
		                            if (innerData != null && innerData.Name == "node")
		                            {
			                            var attrsInner = innerData.Attributes;
			                            int[] point = new int[2];
			                            if (attrsInner?["X"] == null || attrsInner["Y"] == null) continue;
			                            point[0] = Int32.Parse(attrsInner["X"].Value);
			                            point[1] = Int32.Parse(attrsInner["Y"].Value);
			                            coords.Add(point);
		                            }
	                            }

	                            if (coords.Count == 0)
	                            {
		                            LoggerManager.Warn(GetType().Name + ": missing data for zone: " + zoneId);
		                            continue;
	                            }

	                            // Create this zone. Parsing for cuboids is a bit different than for other polygons cuboids need exactly 2 points to be defined.
	                            // Other polygons need at least 3 (one per vertex)
	                            if (zoneShape.Contains("Cuboid"))
	                            {
		                            if (coords.Count == 2)
		                            {
			                            temp.Zone = new ZoneCuboid(coords[0][0], coords[1][0], coords[0][1],
				                            coords[1][1], minZ, maxZ);
		                            }
		                            else
		                            {
			                            LoggerManager.Warn(GetType().Name + ": Missing cuboid vertex in data for zone: " + zoneId);
		                            }
	                            }
	                            else if (zoneShape.Contains("NPoly"))
	                            {
		                            // nPoly needs to have at least 3 vertices
		                            if (coords.Count > 2)
		                            {
			                            int[] aX = new int[coords.Count];
			                            int[] aY = new int[coords.Count];
			                            for (int i = 0; i < coords.Count; i++)
			                            {
				                            aX[i] = coords[i][0];
				                            aY[i] = coords[i][1];
			                            }

			                            temp.Zone = new ZoneNPoly(aX, aY, minZ, maxZ);
		                            }
	                            }
	                            else if (zoneShape.Contains("Cylinder"))
	                            {
		                            // A Cylinder zone requires a center point at x,y and a radius
		                            int zoneRad = Int32.Parse(attrs.GetNamedItem("rad").Value);
		                            if ((coords.Count == 1) && (zoneRad > 0))
		                            {
			                            temp.Zone = new ZoneCylinder(coords[0][0], coords[0][1], minZ, maxZ, zoneRad);
		                            }
		                            else
		                            {
			                            LoggerManager.Warn(GetType().Name + ": Bad data for zone: " + zoneId);
		                            }
	                            }
	                            else
	                            {
		                            LoggerManager.Warn(GetType().Name + ": Unknown shape: " + zoneShape);
	                            }
                            }
                            catch (Exception e)
                            {
	                            LoggerManager.Error(GetType().Name + ": Failed to load zone " + zoneId + " coordinates: " + e.Message);
                            }
                            
                            // Check for additional parameters
                            foreach (XmlNode innerData in node.ChildNodes)
                            {
	                            if (innerData != null && innerData.Name == "stat")
	                            {
		                            var attrsInner = innerData.Attributes;
		                            string name = attrsInner["name"].Value;
		                            string val = attrsInner["val"].Value;
		                            temp.SetParameter(name, val);
	                            } 
	                            else if (innerData != null && innerData.Name == "spawn" && (temp is ZoneRespawn zoneRespawn))
	                            {
		                            var attrsInner = innerData.Attributes;
		                            int spawnX = Int32.Parse(attrsInner["X"].Value);
		                            int spawnY = Int32.Parse(attrsInner["Y"].Value);
		                            int spawnZ = Int32.Parse(attrsInner["Z"].Value);

		                            if (attrsInner?["isChaotic"] != null && bool.Parse(attrsInner["isChaotic"].Value))
		                            {
			                            zoneRespawn.AddChaoticSpawn(spawnX, spawnY, spawnZ);
		                            }
		                            else
		                            {
			                            zoneRespawn.AddSpawn(spawnX, spawnY, spawnZ);
		                            }
	                            }
                            }
                            AddZone(zoneId, temp);
                            
                            // Register the zone into any world region it intersects with...
                            int rows = worldRegions.GetUpperBound(0) + 1;
                            int columns = worldRegions.Length / rows;
                            for (int x = 0; x < rows; x++)
                            {
	                            for (int y = 0; y < columns; y++)
	                            {
		                            if (temp.Zone.IntersectsRectangle(
			                            (x - _worldInit.OFFSET_X) << _worldInit.SHIFT_BY,
			                            ((x + 1) - _worldInit.OFFSET_X) << _worldInit.SHIFT_BY,
			                            (y - _worldInit.OFFSET_Y) << _worldInit.SHIFT_BY,
			                            ((y + 1) - _worldInit.OFFSET_Y) << _worldInit.SHIFT_BY))
		                            {
			                            worldRegions[x, y].AddZone(temp);
		                            }
	                            }
                            }
                            switch (temp)
                            {
	                            // Special managers for arenas, towns...
	                            case ArenaZone arenaZone:
		                            ArenaManager.Instance.AddArena(arenaZone);
		                            break;
	                            case OlympiadStadiumZone stadiumZone:
		                            OlympiadStadiumManager.Instance.AddStadium(stadiumZone);
		                            break;
	                            case BossZone bossZone:
		                            GrandBossManager.Instance.AddZone(bossZone);
		                            break;
                            }
                        }
                    }
                }
            } catch (Exception exception)
            {
                LoggerManager.Error($"Error parsing Zones templates. {exception.Message}");
            }
            int size = _classZones.Values.Sum(d => d.Count);
            LoggerManager.Info(GetType().Name + ": Loaded " + _classZones.Count + $" zones classes and total {size} zones ");
        }
        
        public void AddZone<T>(int zoneId, T zone) where T : ZoneType
        {
	        var dd = new Dictionary<int, T>();
	        if (_classZones.ContainsKey(zone.GetType()))
	        {
		        _map.Add(zoneId, zone);
	        }
	        else
	        {
		        _map = new Dictionary<int, ZoneType> {{zoneId, zone}};
		        _classZones.Add(zone.GetType(), _map);
	        }
        }

        public Dictionary<int, ZoneType>.ValueCollection GetAllZones<T>(System.Type zoneType) where T : ZoneType
        {
	        return _classZones[zoneType].Values;
        }
    }
}