﻿using System.Collections.Generic;
using System.Linq;
using Helpers;

namespace Core.Model.Zones
{
    public abstract class ZoneRespawn : ZoneType
    {
        private List<Location> _spawnLocs;
        private List<Location> _chaoticSpawnLocs;
        
        protected ZoneRespawn(int id) : base(id)
        {
        }
        
        public void AddSpawn(int x, int y, int z)
        {
            if (_spawnLocs == null)
                _spawnLocs = new List<Location>();

            _spawnLocs.Add(new Location(x, y, z));
        }
        
        public void AddChaoticSpawn(int x, int y, int z)
        {
            if (_chaoticSpawnLocs == null)
                _chaoticSpawnLocs = new List<Location>();

            _chaoticSpawnLocs.Add(new Location(x, y, z));
        }

        public List<Location> GetSpawns()
        {
            return _spawnLocs;
        }

        public Location GetSpawnLoc()
        {
            return Rnd.Get(_spawnLocs).FirstOrDefault();
        }

        public Location GetChaoticSpawnLoc()
        {
            return _chaoticSpawnLocs != null ? Rnd.Get(_chaoticSpawnLocs).FirstOrDefault() : GetSpawnLoc();
        }
    }
}