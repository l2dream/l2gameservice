﻿using System.Collections.Generic;
using System.Linq;
using Core.Model.Zones.Type;

namespace Core.Model.Zones.Manager
{
    public class GrandBossManager
    {
        private static volatile GrandBossManager _instance;
        private static readonly object SyncRoot = new object();
        private List<BossZone> _zones;
        
        public static GrandBossManager Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;

                lock (SyncRoot)
                {
                    if (_instance == null)
                        _instance = new GrandBossManager();
                }
                return _instance;
            }
        }
        
        public void AddZone(BossZone zone)
        {
            _zones ??= new List<BossZone>();
            _zones.Add(zone);
        }

        public BossZone GetZone(int x, int y, int z)
        {
            return _zones?.FirstOrDefault(zone => zone.IsInsideZone(x, y, z));
        }
    }
}