﻿using System.Collections.Generic;
using System.Linq;
using Core.Model.Zones.Type;

namespace Core.Model.Zones.Manager
{
    public class OlympiadStadiumManager
    {
        private static volatile OlympiadStadiumManager _instance;
        private static readonly object SyncRoot = new object();
        
        private List<OlympiadStadiumZone> _olympiadStadia;
        
        public static OlympiadStadiumManager Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;

                lock (SyncRoot)
                {
                    if (_instance == null)
                        _instance = new OlympiadStadiumManager();
                }

                return _instance;
            }
        }

        public void AddStadium(OlympiadStadiumZone zone)
        {
            _olympiadStadia ??= new List<OlympiadStadiumZone>();
            _olympiadStadia.Add(zone);
        }

        public OlympiadStadiumZone GetStadium(int x, int y, int z)
        {
            return _olympiadStadia?.FirstOrDefault(zone => zone.IsInsideZone(x, y, z));
        }
    }
}