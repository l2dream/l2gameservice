﻿using System.Collections.Generic;
using System.Linq;
using Core.Model.Zones.Type;

namespace Core.Model.Zones.Manager
{
    public class ArenaManager
    {
        private static volatile ArenaManager _instance;
        private static readonly object SyncRoot = new object();
        private List<ArenaZone> _arenaZones;
        
        public static ArenaManager Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;

                lock (SyncRoot)
                {
                    if (_instance == null)
                        _instance = new ArenaManager();
                }

                return _instance;
            }
        }

        public void AddArena(ArenaZone arena)
        {
            _arenaZones ??= new List<ArenaZone>();
            _arenaZones.Add(arena);
        }

        public ArenaZone GetArena(int x, int y, int z)
        {
            return _arenaZones?.FirstOrDefault(zone => zone.IsInsideZone(x, y, z));
        }
    }
}