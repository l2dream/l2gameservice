﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Model.CalculateStats;
using Core.Model.Npc;
using Core.Model.Npc.Type;
using DataBase.Interfaces;
using L2Logger;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Model.Spawn
{
    public sealed class SpawnInit
    {
        private readonly ConcurrentDictionary<int, SpawnData> _spawnData;
        private int _highestId;
        
        private readonly ISpawnListRepository _spawnListRepository;
        private readonly NpcTableInit _npcTableInit;
        private readonly Dictionary<string, Type> _npcTypes;
        private readonly IdFactory _idFactory;
        private readonly Calculator[] _calculators;

        public SpawnInit(IServiceProvider serviceProvider)
        {
            _spawnData = new ConcurrentDictionary<int, SpawnData>();
            _spawnListRepository = serviceProvider.GetService<IUnitOfWork>()?.SpawnList;
            _npcTableInit = serviceProvider.GetService<NpcTableInit>();
            _idFactory = serviceProvider.GetService<IdFactory>();
            _npcTypes = new Dictionary<string, Type>();
            _calculators = Formulas.GetStandardNpcCalculators();
            InitNpcTypes();
            Task.Run(Initialize);
        }

        private async Task Initialize()
        {
            LoggerManager.Info(GetType().Name + ": Loading Spawns...");
            try
            {
                var spawnList = await _spawnListRepository.GetAllAsync();

                foreach (var spawn in spawnList)
                {
                    NpcTemplate template = _npcTableInit.GetTemplate(spawn.TemplateId);
                    if (template.Stat.Type.Equals("L2SiegeGuard"))
                    {
                        // Don't spawn
                        continue;
                    }

                    if (template.Stat.Type.Equals("L2RaidBoss"))
                    {
                        // Don't spawn
                        continue;
                    }

                    if (template.Stat.Type.Equals("L2GrandBoss"))
                    {
                        // Don't spawn
                        continue;
                    }

                    if (template.Stat.Type.Equals("L2ClassMaster"))
                    {
                        // Don't spawn
                        continue;
                    }

                    SpawnData spawnData = new SpawnData(template, _idFactory, _calculators)
                    {
                        Id = spawn.SpawnId,
                        Amount = 1, //in table always 1
                        X = spawn.LocX,
                        Y = spawn.LocY,
                        Z = spawn.LocZ,
                        Heading = spawn.Heading,
                        RespawnDelay = spawn.RespawnDelay
                    };

                    switch (spawn.PeriodOfDay)
                    {
                        case 0: // default
                        {
                            spawnData.Init();
                            break;
                        }
                        case 1: // Day
                        {
                            //DayNightSpawnManager.getInstance().addDayCreature(spawnDat);
                            break;
                        }
                        case 2: // Night
                        {
                            //DayNightSpawnManager.getInstance().addNightCreature(spawnDat);
                            break;
                        }
                    }
                    _spawnData.TryAdd(spawnData.Id, spawnData);
                    
                    if (spawnData.Id > _highestId)
                    {
                        _highestId = spawnData.Id;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Info($"Spawn Error {ex.Message}");
            }
            LoggerManager.Info($"Loaded {_spawnData.Count} spawns");
        }

        public Type NpcTypeList(string type)
        {
            return _npcTypes[type];
        }

        public void AddNewSpawn(SpawnData spawn)
        {
            _highestId++;
            spawn.Id = _highestId;
            _spawnData.TryAdd(_highestId, spawn);
        }
        
        private void InitNpcTypes()
        {
            _npcTypes.Add("L2Guard", typeof(L2Guard));
            _npcTypes.Add("L2Pet", typeof(L2Pet));
            _npcTypes.Add("L2Monster", typeof(L2Monster));
            _npcTypes.Add("L2Npc", typeof(L2Npc));
            _npcTypes.Add("L2FriendlyMob", typeof(L2FriendlyMob));
            _npcTypes.Add("L2Chest", typeof(L2Chest));
            _npcTypes.Add("L2RaidBoss", typeof(L2RaidBoss));
            _npcTypes.Add("L2FeedableBeast", typeof(L2FeedableBeast));
            _npcTypes.Add("L2TownPet", typeof(L2TownPet));
            _npcTypes.Add("L2CastleChamberlain", typeof(L2CastleChamberlain));
            _npcTypes.Add("L2SiegeNpc", typeof(L2SiegeNpc));
            _npcTypes.Add("L2CastleDoormen", typeof(L2CastleDoormen));
            _npcTypes.Add("L2CastleTeleporter", typeof(L2CastleTeleporter));
            _npcTypes.Add("L2Teleporter", typeof(L2Teleporter));
            _npcTypes.Add("L2VillageMasterPriest", typeof(L2VillageMasterPriest));
            _npcTypes.Add("L2VillageMasterFighter", typeof(L2VillageMasterFighter));
            _npcTypes.Add("L2Trainer", typeof(L2Trainer));
            _npcTypes.Add("L2Merchant", typeof(L2Merchant));
            _npcTypes.Add("L2Warehouse", typeof(L2Warehouse));
            _npcTypes.Add("L2ManorManager", typeof(L2ManorManager));
            _npcTypes.Add("L2Fisherman", typeof(L2Fisherman));
            _npcTypes.Add("L2OlympiadManager", typeof(L2OlympiadManager));
            _npcTypes.Add("L2Adventurer", typeof(L2Adventurer));
            _npcTypes.Add("L2NpcWalker", typeof(L2NpcWalker));
            _npcTypes.Add("L2VillageMasterDElf", typeof(L2VillageMasterDElf));
            _npcTypes.Add("L2VillageMasterDwarf", typeof(L2VillageMasterDwarf));
            _npcTypes.Add("L2VillageMasterOrc", typeof(L2VillageMasterOrc));
            _npcTypes.Add("L2FestivalGuide", typeof(L2FestivalGuide));
            _npcTypes.Add("L2ClanHallManager", typeof(L2ClanHallManager));
            _npcTypes.Add("L2ClanHallDoormen", typeof(L2ClanHallDoormen));
            _npcTypes.Add("L2SymbolMaker", typeof(L2SymbolMaker));
            _npcTypes.Add("L2DawnPriest", typeof(L2DawnPriest));
            _npcTypes.Add("L2DuskPriest", typeof(L2DuskPriest));
            _npcTypes.Add("L2SignsPriest", typeof(L2SignsPriest));
            _npcTypes.Add("L2Auctioneer", typeof(L2Auctioneer));
            _npcTypes.Add("L2MercManager", typeof(L2MercManager));
            _npcTypes.Add("L2VillageMasterMystic", typeof(L2VillageMasterMystic));
            _npcTypes.Add("L2Doormen", typeof(L2Doormen));
            _npcTypes.Add("L2GoldenRam", typeof(L2GoldenRam));
            _npcTypes.Add("L2MutedNpc", typeof(L2MutedNpc));
            _npcTypes.Add("L2SepulcherNpc", typeof(L2SepulcherNpc));
            _npcTypes.Add("L2CastleBlacksmith", typeof(L2CastleBlacksmith));
            _npcTypes.Add("L2CastleWarehouse", typeof(L2CastleWarehouse));
            _npcTypes.Add("L2WyvernManager", typeof(L2WyvernManager));
            _npcTypes.Add("L2RaceManager", typeof(L2RaceManager));
            _npcTypes.Add("L2Observation", typeof(L2Observation));
            _npcTypes.Add("L2DungeonGatekeeper", typeof(L2DungeonGatekeeper));
            _npcTypes.Add("L2CastleMagician", typeof(L2CastleMagician));
        }
    }
}