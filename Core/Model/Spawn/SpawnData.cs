﻿using System;
using System.Threading.Tasks;
using Core.Model.CalculateStats;
using Core.Model.Npc;
using Helpers;
using L2Logger;

namespace Core.Model.Spawn
{
    public sealed class SpawnData
    {
        public int Id { get; set; }

        public int Amount { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }
        public int Heading { get; set; }
        public int RespawnDelay { get; set; }
        public int RespawnMinDelay { get; set; }
        public int RespawnMaxDelay { get; set; }

        private int _currentCount;

        private readonly IdFactory _idFactory;
        private readonly Calculator[] _calculators;

        public NpcTemplate Template { get; set; }
        
        public SpawnData(NpcTemplate template, IdFactory idFactory, Calculator[] calculators)
        {
            Template = template ?? throw new ArgumentNullException(nameof(template));
            _idFactory = idFactory;
            _calculators = calculators;
        }

        public int Init()
        {
            Task.Run(DoSpawn);
            return _currentCount;
        }

        public async Task<NpcInstance> DoSpawn()
        {
            NpcInstance npcInstance = null;
            try
            {
                if (Template.Stat.Type.Equals("L2Pet") || Template.Stat.Type.Equals("L2Minion"))
                {
                    _currentCount++;
                    return npcInstance;
                }
                // Get NpcInstance Init parameters and its generate an Identifier
                Object[] parameters =
                {
                    _idFactory.NextId(),
                    Template,
                };
                
                npcInstance = (NpcInstance) Activator.CreateInstance(Initializer.SpawnInit().NpcTypeList(Template.Stat.Type), parameters);
                npcInstance.Calculators = _calculators;
                await npcInstance.Status.SetCurrentHpAsync(npcInstance.Stat.GetMaxHp(), false);
                npcInstance.Status.SetCurrentMp(npcInstance.Stat.GetMaxMp()); // send the StatusUpdate only once
                
                // Set the heading of the NpcInstance (random heading if not defined)
                if (Heading == -1)
                {
                    npcInstance.Stat.Heading = Rnd.Next(61794);
                }
                else
                {
                    npcInstance.Stat.Heading = Heading;
                }

                npcInstance.Spawn = this;
                npcInstance.SpawnMe(X, Y, Z);
                
                // Increase the current number of NpcInstance managed by this Spawn
                _currentCount++;
                
            }
            catch (Exception ex)
            {
                LoggerManager.Error(GetType().Name + " " + ex.Message);
            }

            return npcInstance;
        }
    }
}