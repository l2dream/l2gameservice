﻿using System.Collections.Generic;
using System.IO;
using L2Logger;
using Newtonsoft.Json;

namespace Core.Model.Teleport
{
    public class TeleportLocationInit
    {
        private readonly string _basePath;
        private readonly Dictionary<int, TeleportLocation> _teleportLocation;
        
        public TeleportLocationInit()
        {
            _basePath = Initializer.Config().ServerConfig.StaticData + "/Json";
            _teleportLocation = new Dictionary<int, TeleportLocation>();
            Init();
        }

        private void Init()
        {
            try
            {
                LoggerManager.Info("Json Teleports start...");
                using (StreamReader sr = new StreamReader(_basePath + "/" + "Teleports.json"))
                {
                    var teleportEntity = JsonConvert.DeserializeObject<TeleportEntity>(sr.ReadToEnd());
                    teleportEntity?.Items.ForEach(e =>
                    {
                        _teleportLocation.Add(e.Id, e);
                    });
                }
            }
            catch (JsonException e)
            {
                LoggerManager.Error(e.Message);
            }
        }

        public TeleportLocation GetTeleportLocation(int teleportId)
        {
            return _teleportLocation[teleportId];
        }
    }
}