﻿using System.Collections;
using System.Collections.Generic;

namespace Core.Model.Teleport
{
    internal sealed class TeleportEntity
    {
        public string Table { get; set; }
        public List<TeleportLocation> Items { get; set; }
    }

    public sealed class TeleportLocation {
        public int Id { get; set; }
        public string Description { get; set; }
        public int LocX { get; set; }
        public int LocY { get; set; }
        public int LocZ { get; set; }
        public int Price { get; set; }
        public bool Noble { get; set; }
    }
}