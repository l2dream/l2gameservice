﻿using System;
using System.Threading.Tasks;
using Core.AI;
using Core.Model.Actor;
using L2Logger;

namespace Core.Model.AI
{
    public class NotifyAiTask
    {
        private readonly Character _outerInstance;

        private readonly CtrlEvent _evt;

        /// <summary>
        /// Instantiates a new notify ai task. </summary>
        /// <param name="outerInstance"></param>
        /// <param name="evt"> the evt </param>
        public NotifyAiTask(Character outerInstance, CtrlEvent evt)
        {
            _outerInstance = outerInstance;
            _evt = evt;
            Task.Run(RunImpl);
        }

        private void RunImpl()
        {
            try
            {
                _outerInstance.AI.NotifyEvent(_evt, null);
            }
            catch (Exception exception)
            {
                LoggerManager.Warn(exception.Message);
            }
        }
    }
}