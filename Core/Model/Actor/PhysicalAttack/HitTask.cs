﻿using System;
using System.Threading.Tasks;
using Core.AI;
using Core.Model.Actor.Data;
using Core.Model.CalculateStats;
using Core.Model.Items.Type;
using Core.Model.Npc;
using Core.Model.Player;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;
using L2Logger;

namespace Core.Model.Actor.PhysicalAttack
{
    public class HitTask : IRunnable
    {
	    private readonly Character _character;
        private readonly Character _hitTarget;
        private readonly int _damage;
        private readonly bool _crit;
        private readonly bool _miss;
        private readonly bool _shld;
        private readonly bool _soulshot;

        /// <summary>
        /// Instantiates a new hit task. </summary>
        /// <param name="character"></param>
        /// <param name="target"> the target </param>
        /// <param name="damage"> the damage </param>
        /// <param name="crit"> the crit </param>
        /// <param name="miss"> the miss </param>
        /// <param name="soulshot"> the soulshot </param>
        /// <param name="shld"> the shld </param>
        public HitTask(Character character,  Character target, int damage, bool crit, bool miss, bool soulshot, bool shld)
        {
	        _character = character;
            _hitTarget = target;
            _damage = damage;
            _crit = crit;
            _shld = shld;
            _miss = miss;
            _soulshot = soulshot;
        }

        public async Task Run()
        {
            try
            {
                await OnHitTimer(_hitTarget, _damage, _crit, _miss, _soulshot, _shld);
            }
            catch (Exception e)
            {
                LoggerManager.Info("fixme:hit task unhandled exception " + e);
            }
        }

        private async Task OnHitTimer(Character target, int damage, bool crit, bool miss, bool soulshot, bool shld)
		{
			// If the attacker/target is dead or use fake death, notify the AI with EVT_CANCEL
			// and send a Server->Client packet ActionFailed (if attacker is a PlayerInstance)
			if ((target == null) || _character.Status.IsAlikeDead())
			{
				_character.AI.NotifyEvent(CtrlEvent.EvtCancel);
				return;
			}
			
			if (miss)
			{
				if (target is PlayerInstance playerInstance)
				{
					SystemMessage sm = new SystemMessage(SystemMessageId.AvoidedS1Attack);
					sm.AddString(_character.CharacterName);
					await playerInstance.SendPacketAsync(sm);
				}
			}
			
			// If attack isn't aborted, send a message system (critical hit, missed...) to attacker/target if they are PlayerInstance
			if (!_character.Attack().IsAttackAborted())
			{
				if (_character is PlayerInstance playerInstance)
				{
					playerInstance.PlayerMessage().SendDamageMessageAsync(target, damage, false, crit, miss);
				}
				
				if (!miss && (damage > 0))
				{
					Weapon weapon = _character.GetActiveWeaponItem();
					bool isBow = (weapon != null) && weapon.WeaponType.Id == WeaponTypeId.Bow;
					if (!isBow) // Do not reflect or absorb if weapon is of type bow
					{
						// Absorb HP from the damage inflicted
						double absorbPercent = _character.Stat.GetCalc().CalcStat(CharacterStatId.AbsorbDamagePercent, 0, null, null);
						if (absorbPercent > 0)
						{
							int maxCanAbsorb = (int) (_character.Stat.GetMaxHp() - _character.Status.GetCurrentHp());
							int absorbDamage = (int) ((absorbPercent / 100) * damage);
							if (absorbDamage > maxCanAbsorb)
							{
								absorbDamage = maxCanAbsorb; // Can't absord more than max hp
							}
							
							if (absorbDamage > 0)
							{
								//setCurrentHp(getStatus().getCurrentHp() + absorbDamage);
							}
						}
						
						// Reduce HP of the target and calculate reflection damage to reduce HP of attacker if necessary
						double reflectPercent = target.Stat.GetCalc().CalcStat(CharacterStatId.ReflectDamagePercent, 0, null, null);
						if (reflectPercent > 0)
						{
							int reflectedDamage = (int) ((reflectPercent / 100) * damage);
							damage -= reflectedDamage;
							if (reflectedDamage > target.Stat.GetMaxHp())
							{
								reflectedDamage = (int) target.Stat.GetMaxHp();
							}
							
							//getStatus().reduceHp(reflectedDamage, target, true);
						}
					}
					
					await target.ReduceCurrentHpAsync(damage, _character);
					if (target is PlayerInstance player)
					{
						if (_character is NpcInstance npcInstance)
						{
							player.PlayerMessage().SendMessageToPlayerByNpc(npcInstance.NpcId, damage);
						}
					}
					
					// Notify AI with EVT_ATTACKED
					target.AI.NotifyEvent(CtrlEvent.EvtAttacked, _character);
					await _character.AI.ClientStartAutoAttackAsync();
				}
				
				// Launch weapon Special ability effect if available
				Weapon activeWeapon = _character.GetActiveWeaponItem();
				if (activeWeapon != null)
				{
					//activeWeapon.GetSkillEffects(this, target, crit);
				}
				return;
			}
			_character.AI.NotifyEvent(CtrlEvent.EvtCancel);
		}
    }
}