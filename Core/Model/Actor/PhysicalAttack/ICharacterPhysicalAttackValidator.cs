﻿using Core.Model.Items.Type;

namespace Core.Model.Actor.PhysicalAttack
{
    public interface ICharacterPhysicalAttackValidator
    {
        bool IsValid(Character target);
        bool IsValidBowAttack(Weapon weapon);
    }
}