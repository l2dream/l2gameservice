﻿namespace Core.Model.Actor.PhysicalAttack
{
    public readonly struct AttackHit
    {
        public int Damage { get; }
        public bool IsShield { get; }
        public bool IsCritical { get; }
        public bool IsMissed { get; }
        public AttackHit(int damage, bool isShield, bool isCritical, bool isMissed )
        {
            Damage = damage;
            IsShield = isShield;
            IsCritical = isCritical;
            IsMissed = isMissed;
        }
    }
}