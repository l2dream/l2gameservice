﻿using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Actor.PhysicalAttack
{
    public interface ICharacterPhysicalAttack
    {
        void DoAttack(Character target);
        AttackHit DoAttackHitSimple(Attack attack, Character target, int sAtk);
    }
}