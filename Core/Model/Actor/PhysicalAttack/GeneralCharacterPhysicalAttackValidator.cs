﻿using Core.AI;
using Core.Model.Items.Type;
using Core.Model.Npc;
using Core.Model.Player;
using Core.TaskManager;

namespace Core.Model.Actor.PhysicalAttack
{
    public class GeneralCharacterPhysicalAttackValidator : ICharacterPhysicalAttackValidator
    {
        private readonly Character _character;
        public GeneralCharacterPhysicalAttackValidator(Character character)
        {
            _character = character;
        }
        public bool IsValid(Character target)
        {
            if (IsAlikeDead()) return false;
            if (IsCharacterDead(target)) return false;
            if (IsKnownListEmpty(target)) return false;
            if (IsAttackingDisabled()) return false;
            if (IsAttackingSelf(target)) return false;
            return true;
        }

        public bool IsValidBowAttack(Weapon weapon)
        {
            if (IsNotEnoughArrows()) return false;
            if (IsNotEnoughMana()) return false;
            if (IsDisabledBowAttack()) return false;
            return true;
        }

        private bool IsNotEnoughMana()
        {
            return false;
        }

        private bool IsNotEnoughArrows()
        {
            // Cancel the action because the PlayerInstance have no arrow
            //_character.AI.SetIntention(CtrlIntention.AiIntentionIdle);
            //_character.SendActionFailedPacket();
            //_character.SendPacket(new SystemMessage(SystemMessageId.NotEnoughArrows));
            return false;
        }

        private bool IsDisabledBowAttack()
        {
            if (_character is PlayerInstance playerInstance)
            {
                // Verify if the bow can be use
                if (playerInstance.Attack().GetDisabledBowAttackEndTime() <= Initializer.TimeController().GetGameTicks())
                {
                    return false;
                }
                ThreadPoolManager.Instance.Schedule(
                    () => { playerInstance.AI.NotifyAiTask(playerInstance, CtrlEvent.EvtReadyToAct); },
                    1000);
                return true;
            }

            if (_character is NpcInstance npcInstance)
            {
                if (npcInstance.Attack().GetDisabledBowAttackEndTime() > Initializer.TimeController().GetGameTicks())
                {
                    return true;
                }
            }
            return true;
        }

        private bool IsAlikeDead()
        {
            if (_character.Status.IsAlikeDead())
            {
                // If PlayerInstance is dead or the target is dead, the action is stopped
                _character.AI.SetIntention(CtrlIntention.AiIntentionActive);
                return true;
            }
            return false;
        }

        /// <summary>
        /// If PlayerInstance is dead or the target is dead, the action is stopped
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        private bool IsCharacterDead(Character target)
        {
            switch (_character)
            {
                case NpcInstance _ when target.Status.IsDead():
                    _character.AI.SetIntention(CtrlIntention.AiIntentionActive);
                    return true;
                case PlayerInstance _ when target.Status.IsDead():
                    _character.AI.SetIntention(CtrlIntention.AiIntentionActive);
                    return true;
                case PlayerInstance playerInstance when playerInstance.Status.IsDead():
                    playerInstance.AI.SetIntention(CtrlIntention.AiIntentionActive);
                    return true;
                default:
                    return false;
            }
        }

        private bool IsKnownListEmpty(Character target)
        {
            if (_character.GetKnownList().HasKnowsObject(target)) return false;
            _character.AI.SetIntention(CtrlIntention.AiIntentionActive);
            return true;
        }

        private bool IsAttackingDisabled()
        {
            return _character.Status.IsAttackingDisabled();
        }

        private bool IsAttackingSelf(Character target)
        {
            if (_character.ObjectId == target.ObjectId)
            {
                return true;
            }
            return false;
        }
    }
}