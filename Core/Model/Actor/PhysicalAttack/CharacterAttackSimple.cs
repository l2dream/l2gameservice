﻿using Core.Model.CalculateStats;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Actor.PhysicalAttack
{
    public class CharacterAttackSimple
    {
        private readonly Character _character;
        public CharacterAttackSimple(Character character)
        {
            _character = character;
        }

        public AttackHit GetAttackHitSimple(Attack attack, Character target)
        {
            int damage = 0;
            bool isShield = false;
            bool isCritical = false;
		
            // Calculate if hit is missed or not
            bool missAttack = Formulas.CalcHitMiss(_character, target);
		
            // Check if hit isn't missed
            if (!missAttack)
            {
                // Calculate if shield defense is efficient
                isShield = Formulas.CalcShieldUse(_character, target);
			
                // Calculate if hit is critical
                isCritical = Formulas.CalcCrit(_character.Stat.GetCriticalHit(target));
			
                // Calculate physical damages
                damage = (int) Formulas.CalcPhysDam(_character, target, null, isShield, isCritical, false, attack.Soulshot);
            }
            return new AttackHit(damage, isShield, isCritical, missAttack);
        }
    }
}