﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.AI;
using Core.Helper;
using Core.Model.Actor.MagicCast;
using Core.Model.CalculateStats;
using Core.Model.Items;
using Core.Model.Items.Type;
using Core.Model.Player;
using Core.Model.Skills;
using Core.Model.World;
using Core.Network.GameServicePackets.ServerPackets;
using Core.TaskManager;
using L2Logger;

namespace Core.Model.Actor
{
    public class CharacterMagicCast : CharacterAttack
    {
        private readonly Character _character;
        private int _castEndTime;
        private int _castInterruptTime;
        private readonly IList<Skill> _disabledSkills;
        private Task _skillCast;
        private CancellationTokenSource _cts;
        public CharacterMagicCast(Character character) : base(character)
        {
            _character = character;
            _disabledSkills = new List<Skill>();
        }
        
        public async Task DoCastAsync (Skill skill)
        {
            WorldObject[] targets = skill.GetTargetList(_character, false);
            var target = GetTarget(skill, targets);

            SetLastCast(skill);

            // Get the Identifier of the skill
            int magicId = skill.Id;
            int reuseDelay = ReuseTime(skill);
            bool soulSpiritShot = (HasBlessedSpiritShot() || HasSpiritShot());
            bool forceBuff = ForceBuff(skill, target);
            // Init the reuse time of the skill
            DisableSkill(skill, reuseDelay);
            await RechargeAutoSoulShotAsync(skill);
		
            // Get the casting time of the skill (base)
            HitCoolTime hitCoolTime = new HitCoolTime(_character, skill);
            int coolTime = hitCoolTime.GetCoolTime(forceBuff, soulSpiritShot);
            int hitTime = hitCoolTime.GetHitTime(soulSpiritShot);
            
            SetCastTime(coolTime, hitTime);

            SetHeading(target);
            StopMove(skill);
            await SendToKnownListAsync(skill, target, hitTime, reuseDelay);
            
            // Send a system message USE_S1 to the Player
            await SendMessageToPlayerAsync(skill, magicId);
            
            // Skill reuse check
            if ((reuseDelay > 30000) && _character.IsPlayer)
            {
                //getActingPlayer().addTimestamp(skill, reuseDelay);
            }
            
            // launch the magic in hitTime milliseconds
            await LaunchSkillAsync(skill, hitTime, targets, coolTime);
        }

        /// <summary>
        /// Like L2OFF after a skill the player must stop the movement, unless it is toggle or potion.
        /// </summary>
        /// <param name="skill"></param>
        private void StopMove(Skill skill)
        {
            if (!skill.IsToggle() && !skill.IsPotion && (_character is PlayerInstance playerInstance))
            {
                playerInstance.Movement().StopMoveAsync(null);
            }
        }

        /// <summary>
        /// Init the reuse time of the skill
        /// </summary>
        /// <param name="skill"></param>
        /// <returns></returns>
        private int ReuseTime(Skill skill)
        {
            int reuseDelay = skill.ReuseDelay;
            if ((_character is PlayerInstance) && Formulas.CalcSkillMastery(_character))
            {
                return 0;
            }
            if (!skill.StaticReuse && !skill.IsPotion)
            {
                if (skill.IsMagic)
                {
                    reuseDelay *= _character.Stat.GetMReuseRate(skill);
                }
                else
                {
                    reuseDelay *= _character.Stat.GetPReuseRate(skill);
                }
                reuseDelay = (reuseDelay * 333 / _character.Stat.GetMAtkSpd());
            }
            return reuseDelay;
        }

        private void SetCastTime(int coolTime, int hitTime)
        {
            _castEndTime = 10 + Initializer.TimeController().GameTicks +
                           ((coolTime + hitTime) / Initializer.TimeController().MillisInTick);
            _castInterruptTime = -2 + Initializer.TimeController().GameTicks +
                                 (hitTime / Initializer.TimeController().MillisInTick);
        }

        /// <summary>
        /// Send a Server->Client packet MagicSkillUse with target, displayId, level, skillTime, reuseDelay
        /// to the Creature AND to all PlayerInstance in the _KnownPlayers of the Creature
        /// </summary>
        /// <param name="skill"></param>
        /// <param name="target"></param>
        /// <param name="hitTime"></param>
        /// <param name="reuseDelay"></param>
        private async Task SendToKnownListAsync(Skill skill, Character target, int hitTime, int reuseDelay)
        {
            if (!skill.IsToggle())
            {
                await _character.SendBroadcastPacketAsync(
                    new MagicSkillUse(_character, target, skill.Id, skill.Level, hitTime, reuseDelay));
            }
        }

        /// <summary>
        /// To turn local player in target direction
        /// </summary>
        /// <param name="target"></param>
        private void SetHeading(Character target)
        {
            _character.Heading =
                CalculateRange.CalculateHeadingFrom(_character.GetX(), _character.GetY(), target.GetX(), target.GetY());
        }

        private async Task LaunchSkillAsync(Skill skill, int hitTime, WorldObject[] targets, int coolTime)
        {
            if (hitTime > 210)
            {
                await _character.SendPacketAsync(new SetupGauge(SetupGauge.Blue, hitTime));
                _cts = new CancellationTokenSource();
                _skillCast = ThreadPoolManager.Instance.ScheduleAtFixed(async () =>
                    {
                        var magicUse = new MagicUseTask(_character, targets, skill, coolTime, 1);
                        await magicUse.Run();
                    }
                    , hitTime - 200, _cts.Token);
            }
            else
            {
                var magicUse = new MagicUseTask(_character, targets, skill, coolTime, 1);
                await magicUse.OnMagicLaunchedTimer(targets, skill, coolTime, true);
            }
        }

        private async Task RechargeAutoSoulShotAsync(Skill skill)
        {
            if (skill.UseSoulShot() || skill.UseSpiritShot())
            {
                if (_character is PlayerInstance playerInstance)
                {
                    await playerInstance.PlayerSoulShot().RechargeAutoSoulShotAsync();
                }
            }
        }

        public bool HasBlessedSpiritShot()
        {
            Weapon weaponInst = _character.GetActiveWeaponItem();
            if (weaponInst == null) return false;
            return weaponInst.ChargedSpiritShot == Charged.ChargedBlessedSpiritShot;
        }

        public bool HasSpiritShot()
        {
            Weapon weaponInst = _character.GetActiveWeaponItem();
            if (weaponInst == null) return false;
            return weaponInst.ChargedSpiritShot == Charged.ChargedSpiritShot;
        }
        
        public async Task RemoveBlessedSpiritShotAsync()
        {
            Weapon weaponInst = _character.GetActiveWeaponItem();
            if (weaponInst == null) return;
            if (weaponInst.ChargedSpiritShot == Charged.ChargedBlessedSpiritShot)
            {
                weaponInst.ChargedSpiritShot = Charged.ChargedNone;
            }
            await ReloadShotsAsync();
        }

        public async Task RemoveSpiritShotAsync()
        {
            Weapon weaponInst = _character.GetActiveWeaponItem();
            if (weaponInst == null) return;
            if (weaponInst.ChargedSpiritShot == Charged.ChargedSpiritShot)
            {
                weaponInst.ChargedSpiritShot = Charged.ChargedNone;
            }
            await ReloadShotsAsync();
        }

        public async Task ReloadShotsAsync()
        {
            if (_character is PlayerInstance playerInstance)
            {
                await playerInstance.PlayerSoulShot().RechargeAutoSoulShotAsync();
            }
        }

        private async Task SendMessageToPlayerAsync(Skill skill, int magicId)
        {
            if ((_character is PlayerInstance playerInstance) && (magicId != 1312))
            {
                await playerInstance.PlayerMessage().SendMessageToPlayerAsync(skill, magicId);
            }
        }

        private bool ForceBuff(Skill skill, Character target)
        {
            return (skill.GetSkillType() == SkillType.ForceBuff) && (target is PlayerInstance);
        }

        private void SetLastCast(Skill skill)
        {
            if (skill.IsPotion)
            {
                LastPotionCast = skill;
            }
            else
            {
                LastSkillCast = skill;
            }
        }

        /// <summary>
        /// Get all possible targets of the skill in a table in function of the skill target type
        /// </summary>
        /// <param name="skill"></param>
        /// <param name="targets"></param>
        /// <returns></returns>
        private Character GetTarget(Skill skill, WorldObject[] targets)
        {
            Character target = (Character) targets.FirstOrDefault();
            if (target == null || targets.Length == 0)
            {
                _character.AI.NotifyEvent(CtrlEvent.EvtCancel);
            }
            
            if ((skill.TargetType == SkillTargetType.TargetAura) ||
                (skill.TargetType == SkillTargetType.TargetGround) || skill.IsPotion)
            {
                return _character;
            }
            if (((skill.GetSkillType() == SkillType.Buff) || (skill.GetSkillType() == SkillType.Heal) ||
                      (skill.GetSkillType() == SkillType.CombatPointHeal) ||
                      (skill.GetSkillType() == SkillType.CombatPointPercentHeal) ||
                      (skill.GetSkillType() == SkillType.ManaHeal) || (skill.GetSkillType() == SkillType.Reflect) ||
                      (skill.GetSkillType() == SkillType.Seed) ||
                      (skill.TargetType == SkillTargetType.TargetSelf) ||
                      (skill.TargetType == SkillTargetType.TargetPet) ||
                      (skill.TargetType == SkillTargetType.TargetParty) ||
                      (skill.TargetType == SkillTargetType.TargetClan) ||
                      (skill.TargetType == SkillTargetType.TargetAlly)) && !skill.IsPotion)
            {
                return (Character) targets.FirstOrDefault();
            }
            return (Character) _character.Target.GetTarget();
        }

        public async Task AbortCastAsync(bool force = false)
        {
            if (IsCastingNow() || force)
            {
                _castEndTime = 0;
                _castInterruptTime = 0;
                if (_skillCast != null)
                {
                    _cts.Cancel();
                    _skillCast = null;
                }
                if (_character is PlayerInstance playerInstance)
                {
                    playerInstance.AI.NotifyEvent(CtrlEvent.EvtFinishCasting); // setting back previous intention
                    await playerInstance.SendBroadcastPacketAsync(new MagicSkillCancel(playerInstance.ObjectId)); // broadcast packet to stop animations client-side
                    await playerInstance.SendActionFailedPacketAsync(); // send an "action failed" packet to the caster    
                }
            }
        }
        
        public bool IsCastingNow()
        {
            return _castEndTime > Initializer.TimeController().GameTicks;
        }
        
        public bool CanAbortCast()
        {
            return _castInterruptTime > Initializer.TimeController().GameTicks;
        }
        
        public void SetSkillCastEndTime(int newSkillCastEndTime)
        {
            _castEndTime = newSkillCastEndTime;
            // for interrupt -12 ticks; first removing the extra second and then -200 ms
            _castInterruptTime = newSkillCastEndTime - 12;
        }

        private void DisableSkill(Skill skill, int reuseDelay)
        {
            if (reuseDelay <= 10) return;
            if (!_disabledSkills.Contains(skill))
            {
                _disabledSkills.Add(skill);
            }
            ThreadPoolManager.Instance.Schedule(() =>
            {
                EnableSkill(skill);
            }, reuseDelay);
        }

        private void EnableSkill(Skill skill)
        {
            _disabledSkills.Remove(skill);
        }

        public bool IsSkillDisabled(Skill skill)
        {
            return _disabledSkills.Contains(skill);
        }

        public void CallSkill(Skill skill, WorldObject[] targets)
        {
            try
            {
                foreach (WorldObject target in targets)
                {
                    if (target is Character)
                    {

                    }
                }
                var handler = Initializer.SkillHandlerService().GetSkillHandler(skill.GetSkillType());
                handler.UseSkillAsync(_character, skill, targets);
            }
            catch (Exception ex)
            {
                LoggerManager.Error(GetType().Name + ": " + ex.Message);
            }
        }
    }
}