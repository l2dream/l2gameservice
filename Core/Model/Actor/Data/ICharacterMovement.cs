﻿using System.Threading.Tasks;

namespace Core.Model.Actor.Data
{
    public interface ICharacterMovement
    {
        bool IsMoving { get; }
        bool IsRunning();
        Task SetRunningAsync(bool value);
        Task SetRunningAsync();
        Task SetWalkingAsync();
        void MoveToLocation(int x, int y, int z, int offset);
        bool UpdatePosition(int gameTicks);
        int GetXDestination();
        int GetYDestination();
        int GetZDestination();
        Task StopMoveAsync(Location pos, bool updateKnownObjects = true);
        Task<bool> MoveToNextRoutePoint();
        bool IsOnGeoDataPath();
    }
}