﻿using System.Threading.Tasks;
using Core.Model.World.RegionData;

namespace Core.Model.Actor.Data
{
    public interface ICharacterTeleport
    {
        Task OnTeleported();
        Task TeleToLocationAsync(TeleportWhereType teleportWhere);
        Task TeleToLocationAsync(int x, int y, int z);
        Task TeleToLocationAsync(int x, int y, int z, bool allowRandomOffset);
        Task TeleToLocationAsync(Location loc, bool allowRandomOffset);
    }
}