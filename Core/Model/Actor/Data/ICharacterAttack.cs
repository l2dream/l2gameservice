﻿using System.Threading.Tasks;
using Core.Model.Items.Type;

namespace Core.Model.Actor.Data
{
    public interface ICharacterAttack
    {
        Task DoAttackAsync(Character target);
        int GetDisabledBowAttackEndTime();
        int CalculateReuseTime(Character target, Weapon weapon);
        int CalculateTimeBetweenAttacks(Character target, Weapon weapon);
        void SetAttackingBodypart();
        bool IsAttackingNow();
        bool IsAttackAborted();
        Task AbortAttackAsync();
    }
}