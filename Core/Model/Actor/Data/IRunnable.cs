﻿using System.Threading.Tasks;

namespace Core.Model.Actor.Data
{
    public interface IRunnable
    {
        Task Run();
    }
}