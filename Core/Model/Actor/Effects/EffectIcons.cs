﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Core.Model.CalculateStats;
using Core.Model.Player;
using Core.Model.Skills.Effects;
using Core.Network.GameServicePackets.ServerPackets;
using Core.TaskManager;

namespace Core.Model.Actor.Effects
{
    internal sealed class EffectIcons
    {
        private readonly List<Effect> _effects;
        private Task _shortBuffTask;
        private CancellationTokenSource _cts;
        private readonly Character _character;
        
        public EffectIcons(Character character, List<Effect> effects)
        {
            _character = character;
            _effects = effects;
        }
        
        public void UpdateEffectIcons(bool partyOnly = false)
        {
            // Create the main packet if needed
            MagicEffectIcons mi = null;
            if (!partyOnly)
            {
                mi = new MagicEffectIcons();
            }

            lock (_effects)
            {
                foreach (var effect in _effects)
                {
                    if ((effect.GetEffectType() == EffectTypeEnum.Charge))
                    {
                        // handled by EtcStatusUpdate
                        continue;
                    }
                    if (!effect.InUse) continue;
                    if (mi != null)
                    {
                        effect.AddIcon(mi, effect);
                    }
                }
                // Send the packets if needed
                if (mi != null)
                {
                    _character.SendPacketAsync(mi);
                }
            }
        }
        
        public async Task RemoveStatsOwnerAsync(Effect owner)
        {
            List<CharacterStatId> modifiedStats = null;
            foreach (Calculator calc in _character.Calculators)
            {
                if (calc != null)
                {
                    // Delete all Func objects of the selected owner
                    if (modifiedStats != null)
                    {
                        modifiedStats.AddRange(calc.RemoveOwner(owner));
                    }
                    else
                    {
                        modifiedStats = calc.RemoveOwner(owner);
                    }
                }
            }

            if (owner is {PreventExitUpdate: false})
            {
                await _character.BroadcastModifiedStatsAsync(modifiedStats);
            }
        }
        
        public void ShortBuffStatusUpdate(int magicId, int level, int time)
        {
            CancelShortBuffTask();
            _cts = new CancellationTokenSource();
            lock (_character.Calculators)
            {
                _shortBuffTask = ThreadPoolManager.Instance.ScheduleAtFixed(
                    async () => await new ShortBuffTask((PlayerInstance) _character).Run(),
                    15000, _cts.Token);
                _character.SendPacketAsync(new ShortBuffStatusUpdate(magicId, level, time));
            }
        }
        
        private void CancelShortBuffTask()
        {
            if (_shortBuffTask is {Status: TaskStatus.WaitingForActivation})
            {
                _cts.Cancel();
            }
        }
    }
}