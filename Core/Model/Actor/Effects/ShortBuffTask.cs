﻿using System.Threading.Tasks;
using Core.Model.Actor.Data;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Actor.Effects
{
    public class ShortBuffTask : IRunnable
    {
        private readonly PlayerInstance _playerInstance;
        
        public ShortBuffTask(PlayerInstance playerInstance)
        {
            _playerInstance = playerInstance;
        }
        
        public async Task Run()
        {
            await _playerInstance.SendPacketAsync(new ShortBuffStatusUpdate(0, 0, 0));
        }
    }
}