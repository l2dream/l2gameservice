﻿using Core.Model.CalculateStats;
using Core.Model.Player;
using Core.Model.Skills;

namespace Core.Model.Actor.Stat
{
    public class CalculateStat
    {
        private readonly Character _character;

        public CalculateStat(Character character)
        {
            _character = character;
        }
        
        /// <summary>
        /// Calculate the new value of the state with modifiers that will be applied on the targeted Creature
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="init"></param>
        /// <param name="target"></param>
        /// <param name="skill"></param>
        /// <returns></returns>
        public int CalcStat(CharacterStatId stat, int init, Character target, Skill skill)
        {
            if (_character == null)
            {
                return init;
            }

            
            
            //Calculator calculator = Formulas.GetStandardNpcCalculators()[(int) stat];

            Calculator calculator = _character.Calculators[(int) stat];
            if (calculator == null || calculator.Size() == 0)
            {
                return init;
            }
            
            // Create and init an Env object to pass parameters to the Calculator
            Env env = new Env
            {
                Player = _character,
                Target = target,
                Skill = skill,
                Value = init,
                BaseValue = init
            };
            
            // Launch the calculation
            if (_character is PlayerInstance && stat == CharacterStatId.MaxCp)
            {
                calculator.Calc(env);
            }
            else
            {
                calculator.Calc(env);
            }
            
            
            // avoid some troubles with negative stats (some stats should never be negative)
            if (env.Value <= 0)
            {
                switch (stat)
                {
                    case CharacterStatId.MaxHp:
                    case CharacterStatId.MaxMp:
                    case CharacterStatId.MaxCp:
                    case CharacterStatId.MagicDefence:
                    case CharacterStatId.PowerDefence:
                    case CharacterStatId.PowerAttack:
                    case CharacterStatId.MagicAttack:
                    case CharacterStatId.PowerAttackSpeed:
                    case CharacterStatId.MagicAttackSpeed:
                    case CharacterStatId.ShieldDefence:
                    case CharacterStatId.StatCon:
                    case CharacterStatId.StatDex:
                    case CharacterStatId.StatInt:
                    case CharacterStatId.StatMen:
                    case CharacterStatId.StatStr:
                    case CharacterStatId.StatWit:
                    {
                        env.Value = 1;
                        break;
                    }
                }
            }
            return (int) env.Value;
        }
    }
}