﻿using Core.Model.Actor.Stat.Data;
using Core.Model.CalculateStats;

namespace Core.Model.Actor.Stat
{
    public abstract class CharacterBaseStat : ICharacterBaseStat
    {
        protected readonly Character Character;
        protected readonly CalculateStat CalculateStat;
        public int Level { get; set; } = 1;

        protected CharacterBaseStat(Character character)
        {
            CalculateStat = new CalculateStat(character);
            Character = character;
        }
        
        public int GetStr()
        {
            return (int) CalculateStat.CalcStat(CharacterStatId.StatStr, Character.Template.Stat.BaseStr, null, null);
        }

        public int GetWit()
        {
            return (int) CalculateStat.CalcStat(CharacterStatId.StatWit, Character.Template.Stat.BaseWit, null, null);
        }

        public int GetDex()
        {
            return (int) CalculateStat.CalcStat(CharacterStatId.StatDex, Character.Template.Stat.BaseDex, null, null);
        }

        public int GetCon()
        {
            return (int) CalculateStat.CalcStat(CharacterStatId.StatCon, Character.Template.Stat.BaseCon, null, null);
        }

        public int GetInt()
        {
            return (int) CalculateStat.CalcStat(CharacterStatId.StatInt, Character.Template.Stat.BaseInt, null, null);
        }

        public int GetMen()
        {
            return (int) CalculateStat.CalcStat(CharacterStatId.StatMen, Character.Template.Stat.BaseMen, null, null);
        }
        
        public int GetMaxCp()
        {
            return CalculateStat.CalcStat(CharacterStatId.MaxCp, Character.Template.Stat.BaseCpMax, null, null);
        }

        public virtual int GetMaxHp()
        {
            return CalculateStat.CalcStat(CharacterStatId.MaxHp, Character.Template.Stat.BaseHpMax, null, null);
        }

        public int GetMaxMp()
        {
            return CalculateStat.CalcStat(CharacterStatId.MaxMp, Character.Template.Stat.BaseMpMax, null, null);
        }
    }
}