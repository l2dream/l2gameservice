﻿using System.Threading.Tasks;

namespace Core.Model.Actor.Stat.Data
{
    public interface IPlayerStat : ICharacterStat
    {
        long Exp { get; set; }
        long ExpBeforeDeath { get; set; }
        long Sp { get; set; }
        int Karma { get; set; }
        int PvpKills { get; set; }
        int PkKills { get; set; }
        bool Noble { get; set; }
        bool Newbie { get; set; }
        int PowerGrade { get; set; }
        int WantsPeace { get; set; }

        Task AddExpAndSp(long addToExp, int addToSp);
    }
}