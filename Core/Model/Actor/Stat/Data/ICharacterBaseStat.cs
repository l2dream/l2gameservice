﻿namespace Core.Model.Actor.Stat.Data
{
    public interface ICharacterBaseStat
    {
        int Level { get; set; }
        int GetStr();
        int GetWit();
        int GetDex();
        int GetCon();
        int GetInt();
        int GetMen();
        int GetMaxHp();
        int GetMaxMp();
        int GetMaxCp();
    }
}