﻿using Core.Model.Skills;

namespace Core.Model.Actor.Stat.Data
{
    public interface ICharacterStat : ICharacterBaseStat
    {
        string Title { get; set; }
        int Heading { get; set; }
        long OnlineTime { get; set; }
        int GetRunSpeed();
        int GetWalkSpeed();
        int GetMoveSpeed();
        int GetPhysicalAttackRange();
        int GetReuseModifier(Character target);
        int GetAccuracy();
        int GetPAtk(Character target = null);
        int GetPAtkSpd();
        int GetPDef(Character target = null);
        int GetEvasionRate(Character target = null);
        int GetCriticalHit(Character target = null, Skill skill = null);
        int GetMagicCriticalHit(Character target = null, Skill skill = null);
        int GetMReuseRate(Skill skill);
        int GetPReuseRate(Skill skill);
        int GetMAtk(Character target = null, Skill skill = null);
        int GetMAtkSpd();
        int GetMDef(Character target = null, Skill skill = null);
        float GetAttackSpeedMultiplier();
        float GetMovementSpeedMultiplier();
        int CurrentLoad();
        int GetMaxLoad();
        CalculateStat GetCalc();
        int GetMagicalAttackRange(Skill skill);
        int GetShieldDef();
        int GetMpConsume(Skill skill);
    }
}