﻿using System.Threading.Tasks;
using Core.Model.Actor.Stat.Data;
using Core.Model.CalculateStats;
using Core.Model.Player;
using Core.Model.Skills;
using Core.Model.Zones;
using Helpers;

namespace Core.Model.Actor.Stat
{
    public class CharacterStat : CharacterBaseStat, ICharacterStat
    {
        private int _weaponExpertisePenalty = 1;
        private int _armourExpertisePenalty = 1;
        
        public string Title { get; set; }
        public int Heading { get; set; }
        public long OnlineTime { get; set; }
        public int Karma { get; set; }
        public int PvpKills { get; set; }
        public int PkKills { get; set; }
        public bool Noble { get; set; }
        public bool Newbie { get; set; }
        public int PowerGrade { get; set; }
        public int WantsPeace { get; set; }
        public long Exp { get; set; }
        public long ExpBeforeDeath { get; set; }
        public long Sp { get; set; }

        public CharacterStat(Character character) : base(character)
        {
            
        }


        public int GetAccuracy()
        {
            return (int) (CalculateStat.CalcStat(CharacterStatId.AccuracyCombat, 0, null, null) / _weaponExpertisePenalty);
        }
        
        public int CurrentLoad()
        {
            return 1;
        }
        
        public int GetMaxLoad()
        {
            int con = GetCon();
            if (con < 1)
            {
                return 31000;
            }
		
            if (con > 59)
            {
                return 176000;
            }

            return 200000;
        }

        public int GetPAtk(Character target = null)
        {
            int bonusAtk = 1;
            return CalculateStat.CalcStat(CharacterStatId.PowerAttack, Character.Template.Stat.BasePAtk * bonusAtk, target, null);
        }
        
        public int GetPAtkSpd()
        {
            int bonusAtk = 1;
            
            double val = CalculateStat.CalcStat(CharacterStatId.PowerAttackSpeed, Character.Template.Stat.BasePAtkSpd * bonusAtk, null, null);
            val /= _armourExpertisePenalty;
            
            return (int) val;
        }
        
        public int GetPDef(Character target = null)
        {
            // Get the base PDef of the Creature
            int defence = Character.Template.Stat.BasePDef;
            return CalculateStat.CalcStat(CharacterStatId.PowerDefence, defence, target, null);
        }
        
        public int GetEvasionRate(Character target = null)
        {
            return (int) (CalculateStat.CalcStat(CharacterStatId.EvasionRate, 0, target, null) / _armourExpertisePenalty);
        }
        
        public int GetCriticalHit(Character target = null, Skill skill = null)
        {
            int criticalHit = (int) ((CalculateStat.CalcStat(CharacterStatId.CriticalRate,
                Character.Template.Stat.BaseCritRate, target, skill) * 10.0) + 0.5);
            criticalHit /= 10;
            return criticalHit;
        }

        public int GetMagicCriticalHit(Character target, Skill skill)
        {
            double mRate = CalculateStat.CalcStat(CharacterStatId.MCriticalRate, (Character.Template.Stat.BaseCritRate), target, skill);
            return (int) mRate;
        }
        
        public int GetMAtk(Character target = null, Skill skill = null)
        {
            float bonusAtk = 1;
            int attack = (int) (Character.Template.Stat.BaseMAtk * bonusAtk);
            
            // Get the skill type to calculate its effect in function of base stats of the Creature target.
            // Calculate modifiers Magic Attack
            return (int) CalculateStat.CalcStat(CharacterStatId.MagicAttack, attack, target, skill);
        }
        
        public int GetMAtkSpd()
        {
            int bonusSpdAtk = 1;
            int baseMAtk = Character.Template.Stat.BaseMAtkSpd;
            int val = CalculateStat.CalcStat(CharacterStatId.MagicAttackSpeed, baseMAtk * bonusSpdAtk, null, null);
            val /= _armourExpertisePenalty;
            return (int) val;
        }
        
        public int GetMDef(Character target = null, Skill skill = null)
        {
            // Get the base MDef of the Creature
            int defence = Character.Template.Stat.BaseMDef;
            
            return CalculateStat.CalcStat(CharacterStatId.MagicDefence, defence, target, skill);
        }

        public int GetMReuseRate(Skill skill)
        {
            return CalculateStat.CalcStat(CharacterStatId.MagicReuseRate, Character.Template.Stat.BaseMReuseRate, null, skill);
        }
        
        public int GetPReuseRate(Skill skill)
        {
            return CalculateStat.CalcStat(CharacterStatId.MagicReuseRate, Character.Template.Stat.BaseMReuseRate, null, skill);
        }
        
        public float GetMovementSpeedMultiplier()
        {
            return (float) GetRunSpeed() / Character.Template.Stat.BaseRunSpd;
        }
        
        public virtual float GetAttackSpeedMultiplier()
        {
            return (float)((1.1 * GetPAtkSpd()) / Character.Template.Stat.BasePAtkSpd);
        }

        public int GetRunSpeed()
        {
            int baseRunSpeed = Character.Template.Stat.BaseRunSpd;
            int val = CalculateStat.CalcStat(CharacterStatId.RunSpeed, baseRunSpeed, null, null);
            if (Character.Zone().IsInsideZone(ZoneId.Water))
            {
                val /= 2;
            }
            val /= _armourExpertisePenalty;
            return val;
        }

        public int GetWalkSpeed()
        {
            if (Character.IsPlayer)
            {
                return (GetRunSpeed() * 70) / 100;
            }
            
            return CalculateStat.CalcStat(CharacterStatId.WalkSpeed, Character.Template.Stat.BaseWalkSpd, null, null);
        }
        
        public int GetMoveSpeed()
        {
            if (Character.Movement().IsRunning())
            {
                return GetRunSpeed();
            }
		
            return GetWalkSpeed();
        }
        
        public int GetPhysicalAttackRange()
        {
            if (Character == null)
            {
                return 1;
            }
            int baseAtkRange = Character.Template.Stat.BaseAtkRange;
            return CalculateStat.CalcStat(CharacterStatId.PowerAttackRange, baseAtkRange, null, null);
        }
        
        public int GetReuseModifier(Character target)
        {
            return CalculateStat.CalcStat(CharacterStatId.AtkReuse, 1, target, null);
        }

        public CalculateStat GetCalc()
        {
            return CalculateStat;
        }
        
        public int GetMagicalAttackRange(Skill skill)
        {
            if (skill != null)
            {
                return (int) CalculateStat.CalcStat(CharacterStatId.MagicAttackRange, skill.CastRange, null, skill);
            }
            return Character.Template.Stat.BaseAtkRange;
        }

        public int GetShieldDef()
        {
            return (int) CalculateStat.CalcStat(CharacterStatId.ShieldDefence, 0, null, null);
        }

        public int GetMpConsume(Skill skill)
        {
            if (skill == null)
            {
                return 1;
            }

            int mpConsume = skill.MpConsume;
            return (int) CalculateStat.CalcStat(CharacterStatId.MpConsume, mpConsume, null, skill);
        }
    }
}