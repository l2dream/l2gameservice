﻿using System;
using System.Threading.Tasks;
using Core.Model.Actor.Stat.Data;
using Core.Model.Player;
using Core.Model.World;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Actor.Stat
{
    public class PlayerStat : CharacterStat, IPlayerStat
    {
        private readonly PlayerInstance _playerInstance;
        private readonly ExperienceDataInit _experienceDataInit;
        private readonly int _maxLevel;
        private int _oldMaxHp; // stats watch

        public PlayerStat(PlayerInstance playerInstance) : base(playerInstance)
        {
            _playerInstance = playerInstance;
            _experienceDataInit = Initializer.ExperienceDataService();
            _maxLevel = _experienceDataInit.GetMaxLevel();
        }

        private async Task CalculateExp(long value)
        {
            if (((Exp + value) < 0) || ((value > 0) && (Exp == (GetExpForLevel(_maxLevel) - 1))))
            {
                return;
            }
		
            if ((Exp + value) >= GetExpForLevel(_maxLevel))
            {
                value = GetExpForLevel(_maxLevel) - 1 - Exp;
            }
		
            Exp += value;
            byte level = 1;
            for (level = 1; level <= _maxLevel; level++)
            {
                if (Exp >= GetExpForLevel(level))
                {
                    continue;
                }
                --level;
                break;
            }
		
            if (level != Level)
            {
                await AddLevel(level - Level);
            }
        }

        public async Task<bool> AddLevel(int value)
        {
            if ((Level + value) > (GetExpForLevel(_maxLevel) - 1))
            {
                if (Level < (GetExpForLevel(_maxLevel) - 1))
                {
                    value = (byte) (GetExpForLevel(_maxLevel) - 1 - Level);
                }
                else
                {
                    return false;
                }
            }
            
            bool levelIncreased = (Level + value) > Level;
            value += Level;
            Level = value;
            
            // Sync up exp with current level
            if ((Exp >= GetExpForLevel(Level + 1)) || (GetExpForLevel(Level) > Exp))
            {
                Exp = GetExpForLevel(Level);
            }
            if (!levelIncreased)
            {
                return false;
            }
            
            await _playerInstance.Status.SetCurrentCpAsync(GetMaxCp());
            await _playerInstance.Status.SetCurrentHpAsync(GetMaxHp());
            _playerInstance.Status.SetCurrentMp(GetMaxMp());
            
            await _playerInstance.SendBroadcastPacketAsync(new SocialAction(_playerInstance.ObjectId, 15));
            await _playerInstance.SendPacketAsync(new SystemMessage(SystemMessageId.YouIncreasedYourLevel));
            
            StatusUpdate su = new StatusUpdate(_playerInstance.ObjectId);
            su.AddAttribute(StatusUpdate.Level, Level);
            su.AddAttribute(StatusUpdate.MaxCp, GetMaxCp());
            su.AddAttribute(StatusUpdate.MaxHp, GetMaxHp());
            su.AddAttribute(StatusUpdate.MaxMp, GetMaxMp());
            await _playerInstance.SendPacketAsync(su);
		
            // Update the overloaded status of the PlayerInstance
            //_playerInstance.RefreshOverloaded();
            // Update the expertise status of the PlayerInstance
            //getActiveChar().refreshExpertisePenalty();
            // Send a Server->Client packet UserInfo to the PlayerInstance
            await _playerInstance.SendPacketAsync(new UserInfo(_playerInstance));
            // getActiveChar().setLocked(false);
            return levelIncreased;
        }

        public override int GetMaxHp()
        {
            // Get the Max HP (base+modifier) of the PlayerInstance
            int val = base.GetMaxHp();
            if (val != _oldMaxHp)
            {
                _oldMaxHp = val;
			
                // Launch a regen task if the new Max HP is higher than the old one
                if (_playerInstance.Status.GetCurrentHp() != val)
                {
                    Task.Run(() => _playerInstance.Status.SetCurrentHpAsync(_playerInstance.Status.GetCurrentHp())); // trigger start of regeneration)
                }
            }
            return val;
        }

        private void CalculateSp(int value)
        {
            int currentSp = (int) Sp;
            if (currentSp == int.MaxValue)
            {
                return;
            }
            if (currentSp > (int.MaxValue - value))
            {
                value = int.MaxValue - currentSp;
            }
            Sp = currentSp + value;
        }

        private async Task CalculateExpAndSp(long addToExp, int addToSp)
        {
            if (addToExp >= 0)
            {
                await CalculateExp(addToExp);
            }
		
            if (addToSp >= 0)
            {
                CalculateSp(addToSp);
            }
        }

        private long GetExpForLevel(int level)
        {
            return _experienceDataInit.GetExpForLevel(level);
        }

        private async Task AddExpAndSpAsync(long addToExp, int addToSp)
        {
            await CalculateExpAndSp(addToExp, addToSp);

            // Send a Server->Client System Message to the PlayerInstance
            SystemMessage sm = new SystemMessage(SystemMessageId.YouEarnedS1ExpAndS2Sp);
            sm.AddNumber((int) addToExp);
            sm.AddNumber(addToSp);
            await _playerInstance.SendPacketAsync(sm);
            await _playerInstance.SendPacketAsync(new UserInfo(_playerInstance));
        }
        public async Task AddExpAndSp(long addToExp, int addToSp)
        {
            await AddExpAndSpAsync(addToExp, addToSp);
        }
    }
}