﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.AI;
using Core.Helper;
using Core.Model.Actor.AttackableInfo;
using Core.Model.Actor.KnownList;
using Core.Model.CalculateStats;
using Core.Model.Npc;
using Core.Model.Npc.Type;
using Core.Model.Player;
using L2Logger;

namespace Core.Model.Actor
{
    /// <summary>
    /// This class manages all NPC that can be attacked
    /// </summary>
    public class Attackable : L2Npc
    {
        private bool ReturningToSpawnPoint { get; set; } = false;
        private bool ReturnToSpawnPoint { get; set; } = false;
        public Character MostHated { get; set; }
        

        private readonly ConcurrentDictionary<Character, AggroInfo> _aggroList;

        protected Attackable(int objectId, NpcTemplate template) : base(objectId, template)
        {
            _aggroList = new ConcurrentDictionary<Character, AggroInfo>();
            //GetKnownList(); // init knownlist
            SetKnownList(new AttackableKnownList(this));
            AI = new AttackableAi(this);
        }

        public override WorldObjectKnownList GetKnownList()
        {
            return (AttackableKnownList) base.GetKnownList();
        }

        public override async Task ReduceCurrentHpAsync(int damage, Character attacker)
        {
            // Add damage and hate to the attacker AggroInfo of the Attackable _aggroList
            if (attacker != null)
            {
                AddDamage(attacker, (int) damage);
            }
            
            // Reduce the current HP of the Attackable and launch the doDie Task if necessary
            await base.ReduceCurrentHpAsync(damage, attacker);
        }
        
        /// <summary>
        /// Add damage and hate to the attacker AggroInfo of the Attackable _aggroList
        /// </summary>
        /// <param name="attacker">The Creature that gave damages to this Attackable</param>
        /// <param name="damage">The number of damages given by the attacker Creature</param>
        public void AddDamage(Character attacker, int damage)
        {
            AddDamageHate(attacker, damage, damage);
        }

        public virtual void AddDamageHate(Character attacker, int damage, int aggro)
        {
            if (attacker == null)
            {
                return;
            }
            // Get the AggroInfo of the attacker Creature from the _aggroList of the Attackable
            if (!_aggroList.TryGetValue(attacker, out var ai))
            {
                
                ai = new AggroInfo(attacker) {Damage = 0, Hate = 0};
                _aggroList.TryAdd(attacker, ai);
            }
            // If aggro is negative, its comming from SEE_SPELL, buffs use constant 150
            if (aggro < 0)
            {
                ai.Hate -= (aggro * 150) / (Level + 7);
                aggro = -aggro;
            }// if damage == 0 -> this is case of adding only to aggro list, dont apply formula on it
            else if (damage == 0)
            {
                ai.Hate += aggro;
                // else its damage that must be added using constant 100
            }
            else
            {
                ai.Hate += (aggro * 100) / (Level + 7);
            }
            
            // Add new damage and aggro (=damage) to the AggroInfo object
            ai.Damage += damage;
            // Set the intention to the Attackable to AI_INTENTION_ACTIVE
            if ((AI != null) && (aggro > 0) && (AI.GetIntention() == CtrlIntention.AiIntentionIdle))
            {
                AI.SetIntention(CtrlIntention.AiIntentionActive);
            }

            if (damage > 0)
            {
                AI?.NotifyEvent(CtrlEvent.EvtAttacked, attacker);
            }
        }
        
        protected override void CalculateRewards(Character killer)
        {
            // Creates an empty list of rewards
            ConcurrentDictionary<Character, RewardInfo> rewards = new ConcurrentDictionary<Character, RewardInfo>();
            try
            {
                if (_aggroList.IsEmpty)
                {
                    return;
                }
                PlayerInstance maxDealer = null;
                int maxDamage = 0;
                int damage;
                Character attacker;
                Character damageDealer;
                
                // While Interacting over This Map Removing Object is Not Allowed
                lock (_aggroList)
                {
                    foreach (AggroInfo info in _aggroList.Values)
                    {
                        if (info == null)
                        {
                            continue;
                        }
                        // Get the Creature corresponding to this attacker
                        attacker = info.Attacker;
                        // Get damages done by this attacker
                        damage = info.Damage;
                        // Prevent unwanted behavior
                        if (damage > 1)
                        {
                            damageDealer = info.Attacker;
                            // Check if ddealer isn't too far from this (killed monster)
                            if (!CalculateRange.CheckIfInRange(1500, this, damageDealer, true))
                            {
                                continue;
                            }
                            // Calculate real damages (Summoners should get own damage plus summon's damage)
                            RewardInfo reward;

                            if (rewards.TryGetValue(damageDealer, out reward))
                            {
                                reward.AddDamage(damage);
                            }
                            else
                            {
                                reward = new RewardInfo(damageDealer, damage);
                            }

                            rewards.TryAdd(damageDealer, reward);
                            
                            if ((damageDealer is Playable) && (reward.Damage > maxDamage))
                            {
                                maxDealer = damageDealer as PlayerInstance;
                                maxDamage = reward.Damage;
                            }
                        }
                        
                    }
                }
                
                // Manage Base, Quests and Sweep drops of the Attackable
                DoItemDrop((maxDealer != null) && maxDealer.PlayerModel().IsOnline() ? maxDealer : killer);

                if (!rewards.IsEmpty)
                {
                    int exp;
                    int levelDiff;
                    int sp;
                    int penalty;
                    int[] tmp;
                    foreach (RewardInfo reward in rewards.Values)
                    {
                        if (reward == null)
                        {
                            continue;
                        }
                        // Penalty applied to the attacker's XP
                        penalty = 0;
					
                        // Attacker to be rewarded
                        attacker = reward.Attacker;
					
                        // Total amount of damage done
                        damage = reward.Damage;
                        
                        // If the attacker is a Pet, get the party of the owner
                        if (attacker is L2Pet)
                        {
                            //attackerParty = ((PetInstance) attacker).getParty();
                        }
                        else if (attacker is PlayerInstance)
                        {
                            //attackerParty = ((PlayerInstance) attacker).getParty();
                        }
                        else
                        {
                            return;
                        }
                        
                        // We must avoid "over damage", if any
                        if (damage > attacker.Stat.GetMaxHp())
                        {
                            damage = (int) attacker.Stat.GetMaxHp();
                        }
                        
                        // Calculate Exp and SP rewards
                        if (attacker.GetKnownList().HasKnowsObject(this))
                        {
                            // Calculate the difference of level between this attacker (PlayerInstance or SummonInstance owner) and the Attackable
                            // mob = 24, atk = 10, diff = -14 (full xp)
                            // mob = 24, atk = 28, diff = 4 (some xp)
                            // mob = 24, atk = 50, diff = 26 (no xp)
                            levelDiff = attacker.Stat.Level - Level;
                            tmp = CalculateExpAndSp(levelDiff, damage);
                            exp = tmp[0];
                            exp *= 1 - penalty;
                            sp = tmp[1];
                            
                            // Distribute the Exp and SP between the PlayerInstance and its Summon
                            if (!attacker.Status.IsDead())
                            {
                                var calcExp = attacker.Stat.GetCalc().CalcStat(CharacterStatId.ExpSpRate, exp, null, null);
                                var calcSp = attacker.Stat.GetCalc().CalcStat(CharacterStatId.ExpSpRate, sp, null, null);
                                attacker.PlayerInstance.Stat.AddExpAndSp((int) calcExp, (int) calcSp);
                                //attacker.addExpAndSp(Math.round(attacker.calcStat(Stat.EXPSP_RATE, exp, null, null)), (int) attacker.calcStat(Stat.EXPSP_RATE, sp, null, null));
                            }
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Error(ex.Message);
            }
        }
        
        public void DoItemDrop(Character lastAttacker)
        {
            var itemDrop = new AttackedItemDrop(this, lastAttacker, Template);
            itemDrop.DoItemDrop();
            //DoItemDrop(Template, lastAttacker);
        }
        
        public void StopHating(Character target)
        {
            if (target == null)
            {
                return;
            }
            AggroInfo ai = _aggroList[target];
            if (ai == null)
            {
                return;
            }
            ai.Hate = 0;
        }

        public Character GetMostHated()
        {
            if (_aggroList.IsEmpty || Status.IsAlikeDead())
            {
                return null;
            }
            Character mostHated = null;
            int maxHate = 0;
		
            // While iterating over This Map Removing Object is Not Allowed

            lock (_aggroList)
            {
                foreach (AggroInfo ai in _aggroList.Values)
                {
                    if (ai == null)
                    {
                        continue;
                    }
                    if (ai.Attacker.Status.IsAlikeDead() || !ai.Attacker.IsVisible())
                    {
                        ai.Hate = 0;
                    }
				
                    if (ai.Hate > maxHate)
                    {
                        mostHated = ai.Attacker;
                        maxHate = ai.Hate;
                    }
                }
            }
            if (mostHated != null)
            {
                MostHated = mostHated;
            }
		
            return mostHated;
        }

        private int[] CalculateExpAndSp(int levelDiff, long damage)
        {
            double exp;
            double sp;
            if (levelDiff < -5)
            {
                levelDiff = -5; // makes possible to use ALT_GAME_EXPONENT configuration
            }
            exp = (GetExpReward() * damage) / Stat.GetMaxHp();
            sp = (GetSpReward() * damage) / Stat.GetMaxHp();
            
            //todo add config Config.ALT_GAME_EXPONENT_XP == 0) && (Config.ALT_GAME_EXPONENT_SP
            if (levelDiff > 5) // formula revised May 07
            {
                double pow = Math.Pow((double) 5 / 6, levelDiff - 5);
                exp *= pow;
                sp *= pow;
            }
			
            if (exp <= 0)
            {
                exp = 0;
                sp = 0;
            }
            else if (sp <= 0)
            {
                sp = 0;
            }
            return new int[]
            {
                (int) exp,
                (int) sp
            };
        }
        
        public int GetExpReward()
        {
            double rateXp = Stat.GetCalc().CalcStat(CharacterStatId.MaxHp, 1, this, null);
            return (int) (Template.Stat.RewardExp * rateXp * 1f); //todo add config
        }
        
        public int GetSpReward()
        {
            double rateSp = Stat.GetCalc().CalcStat(CharacterStatId.MaxHp, 1, this, null);
            return (int) (Template.Stat.RewardSp * rateSp * 1f); //todo add config
        }
    }
}