﻿namespace Core.Model.Actor.AttackableInfo
{
    public class AggroInfo
    {
        /** The attaker Creature concerned by this AggroInfo of this Attackable */
        public Character Attacker { get; }
		
        /** Hate level of this Attackable against the attaker Creature (hate = damage) */
        public int Hate { get; set; }
		
        /** Number of damages that the attaker Creature gave to this Attackable */
        public int Damage { get; set; }

        public AggroInfo(Character pAttacker)
        {
            Attacker = pAttacker;
        }

        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }
            if (obj is AggroInfo aggroInfo)
            {
                return aggroInfo.Attacker == Attacker;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Attacker.ObjectId;
        }
    }
}