﻿namespace Core.Model.Actor.AttackableInfo
{
    public class RewardInfo
    {
        public Character Attacker { get; }
        public int Damage { get; set; } = 0;

        public RewardInfo(Character pAttacker, int damage)
        {
            Damage = damage;
            Attacker = pAttacker;
        }
            
        public void AddDamage(int damage)
        {
            Damage += damage;
        }
            
        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }
        
            if (obj is RewardInfo rewardInfo)
            {
                return rewardInfo.Attacker == Attacker;
            }
        
            return false;
        }

        public override int GetHashCode()
        {
            return Attacker.ObjectId;
        }
    }
}