﻿using System;
using System.Linq;
using Core.Model.CalculateStats;
using Core.Model.Items;
using Core.Model.Items.Type;
using Core.Model.Npc;
using Core.Model.Npc.Type;
using Core.Model.Player;
using Helpers;

namespace Core.Model.Actor.AttackableInfo
{
    /// <summary>
    /// Manage Base, Quests and Special Events drops of Attackable (called by calculateRewards)
    /// </summary>
    public class AttackedItemDrop
    {
        private readonly Character _lastAttacker;
        private readonly Character _character;
        private readonly NpcTemplate _template;
        private const int AdenaItemId = 57;
        private const bool DeepBlueDropRules = true; //TODO Add Config
        private const int ItemsRaid = 1; //TOdo add Config ITEMS_RAID
        private const int RateDropItems = 1; // TODO add Config Config.RATE_DROP_ITEMS
        private const int AdenaRaid = 1; // Todo Config.ADENA_RAID
        private const int RateDropAdena = 1; // Todo Config.RATE_DROP_ADENA
        private const int RateDropCommonHerbs = 20; // Todo add config Config.RATE_DROP_COMMON_HERBS
        private const int RateDropSpecialHerbs = 15; //todo Config.RATE_DROP_SPECIAL_HERBS
        private const int RateDropMpHpHerbs = 10; // todo Config.RATE_DROP_MP_HP_HERBS
        private const int RateDropGreaterHerbs = 10; // todo Config.RATE_DROP_GREATER_HERBS
        private const int RateDropSuperiorHerbs = 80; //todo Config.RATE_DROP_SUPERIOR_HERBS
        private const int RandDropLim = 70;
        
        
        public AttackedItemDrop(Character character, Character lastAttacker, NpcTemplate template)
        {
            _character = character;
            _lastAttacker = lastAttacker;
            _template = template;
        }

        public void DoItemDrop()
        {
            if (_lastAttacker is PlayerInstance playerInstance)
            {
                int levelModifier = CalculateLevelModifierForDrop(playerInstance); // level modifier in %'s (will be subtracted from drop chance)
                
                // now throw all categorized drops and handle spoil.
                foreach (DropCategory cat in _template.GetDropData())
                {
                    RewardItem item = null;
                    if (cat.IsSweep())
                    {
                        //Todo Spoil
                    }
                    else
                    {
                        item = CalculateCategorizedRewardItem(playerInstance, cat, levelModifier);
                    }

                    if (item != null)
                    {
                        // drop the item on the ground
                        DropItem(playerInstance, item);
                    }
                }
                DropHerbs(playerInstance);
            }
        }

        private void DropHerbs(PlayerInstance playerInstance)
        {
            if (_template.Stat.Type.Equals("L2Monster")) // only Monster with <= 1x HP can drop herbs
            {
                DropPAtkHerb(playerInstance);
                DropMAtkHerb(playerInstance);
                DropRecoveryHerb(playerInstance);
            }
        }

        /// <summary>
        /// Herb of Power, Herb of Atk. Spd, Herb of Critical Attack
        /// </summary>
        /// <param name="playerInstance"></param>
        private void DropPAtkHerb(PlayerInstance playerInstance)
        {
            for (int i = 0; i < 3; i++)
            {
                int random = Rnd.Next(100);
                if (random > RateDropCommonHerbs) continue;
                RewardItem item = i switch
                {
                    0 => new RewardItem(8606, 1), // Herb of Power
                    1 => new RewardItem(8608, 1), // Herb of Atk. Spd.
                    2 => new RewardItem(8610, 1), // Herb of Critical Attack
                    _ => null
                };
                DropItem(playerInstance, item);
                break;
            }
        }

        /// <summary>
        /// Herb of Magic, Herb of Casting Speed
        /// </summary>
        /// <param name="playerInstance"></param>
        private void DropMAtkHerb(PlayerInstance playerInstance)
        {
            for (int i = 0; i < 2; i++)
            {
                int random = Rnd.Next(100);
                if (random > RateDropCommonHerbs) continue;
                RewardItem item = i switch
                {
                    0 => new RewardItem(8607, 1), // Herb of Magic
                    1 => new RewardItem(8609, 1), // Herb of Casting Speed
                    _ => null
                };
                DropItem(playerInstance, item);
                break;
            }
        }

        private void DropRecoveryHerb(PlayerInstance playerInstance)
        {
            bool isHpDropped = false;
            bool isMpDropped = false;
            int random = Rnd.Next(100);
            if (random < RateDropSpecialHerbs)
            {
                RewardItem item = new RewardItem(8614, 1); // Herb of Recovery
                DropItem(playerInstance, item);
                isHpDropped = true;
            }

            if (!isHpDropped)
            {
                random = Rnd.Next(100);
                if (random < RateDropMpHpHerbs)
                {
                    RewardItem item = new RewardItem(8600, 1); // Herb of Life
                    DropItem(playerInstance, item);
                    isHpDropped = true;
                }
            }

            if (!isHpDropped)
            {
                random = Rnd.Next(100);
                if (random < RateDropGreaterHerbs)
                {
                    RewardItem item = new RewardItem(8601, 1); // Greater Herb of Life
                    DropItem(playerInstance, item);
                    isHpDropped = true;
                }
            }

            if (!isHpDropped)
            {
                random = Rnd.Next(100);
                if (random < RateDropSuperiorHerbs)
                {
                    RewardItem item = new RewardItem(8602, 1); // Greater Herb of Life
                    DropItem(playerInstance, item);
                }
            }

            random = Rnd.Next(100);
            if (random < RateDropMpHpHerbs)
            {
                RewardItem item = new RewardItem(8603, 1); // Herb of Mana
                DropItem(playerInstance, item);
                isMpDropped = true;
            }
            
            if (!isMpDropped)
            {
                random = Rnd.Next(100);
                if (random < RateDropGreaterHerbs)
                {
                    RewardItem item = new RewardItem(8604, 1); // Greater Herb of Mana
                    DropItem(playerInstance, item);
                    isMpDropped = true;
                }
            }
            
            if (!isMpDropped)
            {
                random = Rnd.Next(100);
                if (random < RateDropSuperiorHerbs)
                {
                    RewardItem item = new RewardItem(8605, 1); // Superior Herb of Mana
                    DropItem(playerInstance, item);
                }
            }
            
            random = Rnd.Next(100);
            if (random < RateDropCommonHerbs)
            {
                RewardItem item = new RewardItem(8611, 1); // Herb of Speed
                DropItem(playerInstance, item);
            }
        }

        private ItemInstance DropItem(PlayerInstance mainDamageDealer, RewardItem item)
        {
            // Randomize drop position
            int newX = (_character.GetX() + Rnd.Next((RandDropLim * 2) + 1)) - RandDropLim;
            int newY = (_character.GetY() + Rnd.Next((RandDropLim * 2) + 1)) - RandDropLim;
            int newZ = (_character.GetZ() + Rnd.Next((RandDropLim * 2) + 1)) - RandDropLim;
            //int newZ = Math.Max(_character.GetZ(), mainDamageDealer.GetZ()) + 20;
		
            // Init the dropped ItemInstance and add it in the world as a visible object at the position where mob was last
            var dropItem = Initializer.ItemService().CreateItem(ItemAction.Loot, item.ItemId, item.Count, mainDamageDealer, _character);
            
            dropItem.DropMe(_character, newX, newY, newZ);
            return dropItem;
        }

        private int CalculateLevelModifierForDrop(PlayerInstance lastAttacker)
        {
            if (DeepBlueDropRules)
            {
                int highestLevel = lastAttacker.Stat.Level;

                if (_character.GetAttackByList().Any())
                {
                    foreach (Character atkChar in _character.GetAttackByList())
                    {
                        if ((atkChar != null) && (atkChar.Stat.Level > highestLevel))
                        {
                            highestLevel = atkChar.Stat.Level;
                        }
                    }
                }
                // Check to prevent very high level player to nearly kill mob and let low level player do the last hit.
                // According to official data (Prima), deep blue mobs are 9 or more levels below players
                if ((highestLevel - 9) >= _template.Stat.Level)
                {
                    return (highestLevel - (_template.Stat.Level + 8)) * 9;
                }
            }
            return 0;
        }

        private RewardItem CalculateCategorizedRewardItem(PlayerInstance lastAttacker, DropCategory categoryDrops,
            int levelModifier)
        {
            int baseCategoryDropChance = categoryDrops.GetCategoryChance();
            int categoryDropChance = baseCategoryDropChance;
            int deepBlueDrop = 1;
            
            // Check if we should apply our maths so deep blue mobs will not drop that easy
            if (DeepBlueDropRules)
            {
                categoryDropChance = (categoryDropChance - ((categoryDropChance * levelModifier) / 100)) / deepBlueDrop;
            }
            // Applies Drop rates
            if (_character is L2RaidBoss)
            {
                categoryDropChance *= ItemsRaid;
            }
            else
            {
                categoryDropChance *= RateDropItems;
            }
            
            // Check if an Item from this category must be dropped
            
            if (Rnd.Next(DropData.MaxChance) < categoryDropChance)
            {
                DropData drop = categoryDrops.DropOne(_character.IsRaid);
                int dropChance = 0;

                switch (drop.ItemId)
                {
                    case 6662: // todo core ring
                        break;
                    case 6661: // todo orfen earring
                        break;
                    case 6659: // todo zaken earring
                        break;
                    case 6660: // todo aq ring
                        break;
                    default:
                        dropChance = drop.Chance;
                        break;
                }

                if (drop.ItemId == AdenaItemId)
                {
                    if (_character is L2RaidBoss)
                    {
                        dropChance *= AdenaRaid;
                    }
                    else
                    {
                        dropChance *= RateDropAdena;
                    }
                }
                
                // Get min and max Item quantity that can be dropped in one time
                int min = drop.MinDrop;
                int max = drop.MaxDrop;
                
                // Get the item quantity dropped
                int itemCount = 0;
                
                // Check if the Item must be dropped
                int random = Rnd.Next(DropData.MaxChance);
                
                while (random < dropChance)
                {
                    // Get the item quantity dropped
                    if (min < max)
                    {
                        itemCount += Rnd.Next(min, max);
                    }
                    else if (min == max)
                    {
                        itemCount += min;
                    }
                    else
                    {
                        itemCount++;
                    }
				
                    // Prepare for next iteration if dropChance > DropData.MAX_CHANCE
                    dropChance -= DropData.MaxChance;
                }
                if (itemCount > 0)
                {
                    return new RewardItem(drop.ItemId, itemCount);
                }
            }
            return null;
        }
    }
}