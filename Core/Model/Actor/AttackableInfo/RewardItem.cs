﻿namespace Core.Model.Actor.AttackableInfo
{
    public class RewardItem
    {
        public int ItemId { get; }
        public int Count { get; }
		
        public RewardItem(int itemId, int count)
        {
            ItemId = itemId;
            Count = count;
        }
    }
}