using System.Collections.Generic;
using System.Threading.Tasks;
using Core.AI;
using Core.Model.Actor.Data;
using Core.Model.Actor.KnownList;
using Core.Model.Actor.Stat;
using Core.Model.Actor.Stat.Data;
using Core.Model.CalculateStats;
using Core.Model.Items;
using Core.Model.Items.Type;
using Core.Model.Player;
using Core.Model.World;
using Core.Model.World.RegionData;
using Core.Network;
using CharacterStatus = Core.Model.Actor.Status.CharacterStatus;

namespace Core.Model.Actor
{
    /// <summary>
    /// Mother class of all character objects of the world (PC, NPC...)
    /// </summary>
    public abstract class Character : WorldObject
    {
        public string CharacterName { get; protected set; }
        public ICharacterStat Stat { get; }
        public CharacterStatus Status { get; }
        private readonly ICharacterMovement _movement;
        protected CharacterClientServer CharacterBroadcast;
        public PlayerInstance PlayerInstance { get; protected set; }
        public CharacterTemplate Template { get; }
        public CharacterEffects Effects { get; }
        public CharacterTarget Target { get; protected set; }
        private readonly ICharacterTeleport _characterTeleport;

        public CharacterAi AI { get; protected set;  }
        private readonly ICharacterAttack _characterAttack;
        private readonly CharacterMagicCast _characterMagicCast;
        private readonly CharacterAbnormalEffect _characterAbnormalEffect;
        private readonly CharacterZone _zone;
        private readonly CharacterFunc _func;
        private bool KilledAlready { get; set; }
        public bool IsMoving => _movement.IsMoving;

        public virtual double LevelMod => 1;
        
        public Calculator[] Calculators { get; set; }
        private IList<Character> _attackByList;

        protected Character(int objectId, CharacterTemplate template) : base(objectId)
        {
            _movement = new CharacterMovement(this);
            Template = template;
            Status = new CharacterStatus(this);
            AI = new CharacterAi(this);
            CharacterBroadcast = new CharacterClientServer(this);
            Target = new CharacterTarget(this);
            _characterAttack = new CharacterPhysicalAttack(this);
            _characterMagicCast = new CharacterMagicCast(this);
            Stat = new CharacterStat(this);
            Effects = new CharacterEffects(this);
            _zone = new CharacterZone(this);
            _func = new CharacterFunc(this);
            _characterTeleport = new CharacterTeleport(this);
            _characterAbnormalEffect = new CharacterAbnormalEffect(this);
        }
        
        public IList<Character> GetAttackByList()
        {
            if (_attackByList == null)
            {
                _attackByList = new List<Character>(); 
            }
            return _attackByList;
        }
        
        public override WorldObjectKnownList GetKnownList()
        {
            if (!(base.GetKnownList() is CharacterKnownList))
            {
                SetKnownList(new CharacterKnownList(this));
            }
            return (CharacterKnownList) base.GetKnownList();
        }

        public CharacterZone Zone()
        {
            return _zone;
        }
        public ICharacterTeleport Teleport()
        {
            return _characterTeleport;
        }

        public ICharacterAttack Attack()
        {
            return _characterAttack;
        }

        public CharacterMagicCast MagicCast()
        {
            return _characterMagicCast;
        }

        public ICharacterMovement Movement()
        {
            return _movement;
        }

        /// <summary>
        /// Return the active weapon instance (always equipped in the right hand)
        /// </summary>
        /// <returns></returns>
        public abstract ItemInstance GetActiveWeaponInstance();
        
        /// <summary>
        /// Return the active weapon item (always equipped in the right hand)
        /// </summary>
        /// <returns></returns>
        public abstract Weapon GetActiveWeaponItem();

        public virtual async Task DoDieAsync(Character killer)
        {
            if (KilledAlready)
            {
                return;
            }
            KilledAlready = true;
            // Stop movement
            await _movement.StopMoveAsync(null);
            
            // Stop HP/MP/CP Regeneration task
            Status.StopHpMpRegeneration();
            
            Character mostHated = null;
            if (this is Attackable attackable)
            {
                mostHated = attackable.MostHated;
            }

            if ((mostHated != null) && _zone.IsInsideRadius(mostHated, 200, false, false))
            {
                CalculateRewards(mostHated);
            }
            else
            {
                CalculateRewards(killer);
            }
            
            // Send the Server->Client packet StatusUpdate with current HP and MP to all other PlayerInstance to inform
            // Set target to null and cancel Attack or Cast
            Target.RemoveTargetAsync();
            killer.Target.RemoveTargetAsync();
            await CharacterBroadcast.BroadcastStatusUpdateAsync();

            // Notify Creature AI
            AI.NotifyEvent(CtrlEvent.EvtDead, null);
            if (GetWorldRegion() != null)
            {
                GetWorldRegion().OnDeath(this);
            }
            GetAttackByList().Clear();
        }
        
        protected virtual void CalculateRewards(Character killer)
        {
        }
        
        public virtual async Task ReduceCurrentHpAsync(int i, Character attacker)
        {
            await Status.ReduceCurrentHpAsync(i, attacker);
        }
        
        public double GetPlanDistanceSq(int x, int y)
        {
            double dx = x - GetX();
            double dy = y - GetY();
            return (dx * dx) + (dy * dy);
        }
        
        public virtual void OnDecay()
        {
            WorldRegionData reg = GetWorldRegion();
            if (reg != null)
            {
                reg.RemoveFromZones(this);
            }
            DecayMe();
        }

        public bool UpdatePosition(int gameTicks) => _movement.UpdatePosition(gameTicks);
        public int GetXDestination() => _movement.GetXDestination();
        public int GetYDestination() => _movement.GetYDestination();

        public int GetZDestination() => _movement.GetZDestination();

        public virtual async Task BroadcastStatusUpdateAsync()
        {
            await CharacterBroadcast.BroadcastStatusUpdateAsync();
        }

        public async Task SendBroadcastPacketAsync(ServerPacket serverPacket, int radiusInKnownList = default)
        {
            await CharacterBroadcast.SendBroadcastPacketAsync(serverPacket, radiusInKnownList);
        }

        public virtual void OnTeleported()
        {
            _characterTeleport.OnTeleported();
        }

        public void AddStatFuncs(FunctionObject[] funcs)
        {
            _func.AddStatFuncs(funcs);
        }

        public async Task RemoveStatsOwnerAsync(object owner)
        {
            await _func.RemoveStatsOwnerAsync(owner);
        }
        
        public async Task BroadcastModifiedStatsAsync(List<CharacterStatId> stats)
        {
            await CharacterBroadcast.BroadcastModifiedStatsAsync(stats);
        }
        
        public void AddStatFunc(FunctionObject f)
        {
            _func.AddStatFunc(f);
        }
        
        public abstract void UpdateAbnormalEffect();
        
        public void StartAbnormalEffect(int mask)
        {
            _characterAbnormalEffect.AbnormalEffects |= mask;
            UpdateAbnormalEffect();
        }
        public void StopAbnormalEffect(int mask)
        {
            _characterAbnormalEffect.AbnormalEffects &= ~mask;
            UpdateAbnormalEffect();
        }
    }
}