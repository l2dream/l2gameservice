﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Model.Actor.Effects;
using Core.Model.CalculateStats;
using Core.Model.Skills.Effects;

namespace Core.Model.Actor
{
    public class CharacterEffects
    {
        private readonly Character _character;
        private readonly List<Effect> _effects;
        private readonly EffectIcons _effectIcons;
        public CharacterEffects(Character character)
        {
            _character = character;
            _effects = new List<Effect>();
            _effectIcons = new EffectIcons(_character, _effects);
        }

        private IEnumerable<Effect> GetAllEffects()
        {
            lock (_effects)
            {
                return _effects.ToArray();
            }
        }

        /// <summary>
        /// TODO need remake this method
        /// Return the first Effect in progress on the Creature corresponding to the Effect Type (ex : BUFF, STUN, ROOT...)
        /// </summary>
        /// <param name="effectTypeEnum"></param>
        /// <returns></returns>
        public Effect GetFirstEffect(EffectTypeEnum effectTypeEnum)
        {
            IEnumerable<Effect> effects = GetAllEffects();
            Effect effNotInUse = null;
            foreach (Effect effect in effects)
            {
                if (effect == null)
                {
                    lock (_effects)
                    {
                        _effects.Remove(null);
                    }
                    continue;
                }
			
                if (effect.GetEffectType() == effectTypeEnum)
                {
                    if (effect.InUse)
                    {
                        return effect;
                    }
				
                    if (effNotInUse == null)
                    {
                        effNotInUse = effect;
                    }
                }
            }
            return effNotInUse;
        }

        /// <summary>
        /// TODO need remake this method
        /// </summary>
        /// <param name="newEffect"></param>
        public void AddEffect(Effect newEffect)
        {
            var effects = GetAllEffects();
            // Make sure there's no same effect previously
            foreach (Effect effect in effects)
            {
                if (effect.GetEffectType() == newEffect.GetEffectType() && 
                    effect.StackType.Equals(newEffect.StackType))
                {
                    effect.Exit(false);
                }
            }
            lock (_effects)
            {
                _effects.Add(newEffect);
                _character.AddStatFuncs(newEffect.GetStatFuncs());
                newEffect.InUse = true;
            }
            _effectIcons.UpdateEffectIcons();
        }

        public async Task RemoveEffectAsync(Effect effect)
        {
            await _effectIcons.RemoveStatsOwnerAsync(effect);
            // Remove the active skill L2effect from _effects of the Creature
            lock (_effects)
            {
                _effects.Remove(effect);
            }
            // Update active skills in progress (In Use and Not In Use because stacked) icones on client
            _effectIcons.UpdateEffectIcons();
        }

        public void ShortBuffStatusUpdate(int magicId, int level, int time)
        {
            _effectIcons.ShortBuffStatusUpdate(magicId, level, time);
        }
    }
}