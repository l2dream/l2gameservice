﻿using System.Threading.Tasks;
using Core.Model.Player;

namespace Core.Model.Actor
{
    /// <summary>
    /// This class represents all Playable characters in the world
    /// </summary>
    public abstract class Playable : Character
    {
        public override bool IsPlayable { get; } = true;
        
        protected Playable(int objectId, CharacterTemplate template) : base(objectId, template)
        {
        }

        public override async Task DoDieAsync(Character killer)
        {
            await base.DoDieAsync(killer);
            if (killer != null)
            {
                if (killer is PlayerInstance playerInstance)
                {
                    //playerInstance.OnKillUpdatePvPKarma(this);
                }
            }
        }
    }
}