﻿namespace Core.Model.Actor
{
    public class CharacterAbnormalEffect
    {
        public const int AbnormalEffectBleeding = 0x000001;
        public const int AbnormalEffectPoison = 0x000002;
        public const int AbnormalEffectRedCircle = 0x000004;
        public const int AbnormalEffectIce = 0x000008;
        public const int AbnormalEffectWind = 0x0000010;
        public const int AbnormalEffectFear = 0x0000020;
        public const int AbnormalEffectStun = 0x000040;
        public const int AbnormalEffectSleep = 0x000080;
        public const int AbnormalEffectMuted = 0x000100;
        public const int AbnormalEffectRoot = 0x000200;
        public const int AbnormalEffectHold1 = 0x000400;
        public const int AbnormalEffectHold2 = 0x000800;
        public const int AbnormalEffectUnknown13 = 0x001000;
        public const int AbnormalEffectBigHead = 0x002000;
        public const int AbnormalEffectFlame = 0x004000;
        public const int AbnormalEffectUnknown16 = 0x008000;
        public const int AbnormalEffectGrow = 0x010000;
        public const int AbnormalEffectFloatingRoot = 0x020000;
        public const int AbnormalEffectDanceStunned = 0x040000;
        public const int AbnormalEffectFireRootStun = 0x080000;
        public const int AbnormalEffectStealth = 0x100000;
        public const int AbnormalEffectImprisioning1 = 0x200000;
        public const int AbnormalEffectImprisioning2 = 0x400000;
        public const int AbnormalEffectMagicCircle = 0x800000;
        public const int AbnormalEffectConfused = 0x0020;
        public const int AbnormalEffectAfraid = 0x0010;

        private Character _character;
        public int AbnormalEffects { get; set; }

        public CharacterAbnormalEffect(Character character)
        {
            _character = character;
        }
        
        public int GetAbnormalEffect()
        {
            int ae = AbnormalEffects;
            if (_character.Status.IsStunned)
            {
                ae |= AbnormalEffectStun;
            }
            if (_character.Status.IsRooted)
            {
                ae |= AbnormalEffectRoot;
            }
            if (_character.Status.IsSleeping)
            {
                ae |= AbnormalEffectSleep;
            }
            if (_character.Status.IsConfused)
            {
                ae |= AbnormalEffectConfused;
            }
            if (_character.Status.IsMuted)
            {
                ae |= AbnormalEffectMuted;
            }
            if (_character.Status.IsAfraid)
            {
                ae |= AbnormalEffectAfraid;
            }
            if (_character.Status.IsPhysicalMuted)
            {
                ae |= AbnormalEffectMuted;
            }
            return ae;
        }
    }
}