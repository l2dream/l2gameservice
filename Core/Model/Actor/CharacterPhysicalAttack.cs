﻿using System.Threading.Tasks;
using Core.AI;
using Core.Helper;
using Core.Model.Actor.Data;
using Core.Model.Actor.PhysicalAttack;
using Core.Model.Items;
using Core.Model.Items.Type;
using Core.Model.Player;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;
using Core.TaskManager;

namespace Core.Model.Actor
{
    public class CharacterPhysicalAttack : CharacterAttack, ICharacterAttack
    {
        private readonly CharacterAttackSimple _attackSimple;
        private readonly Character _character;
        private int _disableBowAttackEndTime;
        private ICharacterPhysicalAttackValidator Validator { get; }
        public CharacterPhysicalAttack(Character character) : base(character)
        {
            _attackSimple = new CharacterAttackSimple(character);
            _character = character;
            Validator = new GeneralCharacterPhysicalAttackValidator(character);
        }
        
        public async Task DoAttackAsync(Character target)
        {
            if (!Validator.IsValid(target))
            {
                await _character.SendActionFailedPacketAsync();
                return;
            }
            await RechargeAutoSoulShotAsync();
            AddKnownObject(target);
            StopMoving();
            SetAttackingBodypart();
            SetHeading(target);
            
            Weapon weapon = GetActiveWeapon();

            // Get the Attack Speed of the Creature (delay (in milliseconds) before next attack)
            // the hit is calculated to happen halfway to the animation - might need further tuning e.g. in bow case
            int timeAtk = CalculateTimeBetweenAttacks(target, weapon);
            int timeToHit = timeAtk / 2;
            int reuse = CalculateReuseTime(target, weapon);
            SetAttackEndTime(timeAtk);
            
            bool isSoulShotCharged = IsSoulShotCharged(weapon);
            int ssGrade = GetCrystalType(weapon);
            var attack = await CheckHit(target, isSoulShotCharged, ssGrade, weapon);

            var hitTask = new HitTask(_character, target, attack.LastHit().Damage, attack.LastHit().IsCritical,
                attack.LastHit().IsMiss, attack.Soulshot, attack.LastHit().Shield);

            if (IsBowAttack(weapon))
            {
                await DoBowAttack(weapon, hitTask, timeAtk, reuse);
                return;
            }
            if (IsDualWeaponAttack(weapon))
            {
                DoDualWeaponAttack(target, weapon, timeToHit, timeAtk, reuse);
                return;
            }
            DoRegularAttack(hitTask, timeToHit, timeAtk, reuse);
        }

        private void StopMoving()
        {
            if (_character.IsMoving)
            {
                _character.Movement().StopMoveAsync(_character.WorldObjectPosition().WorldPosition());
            }
        }

        /// <summary>
        /// Create a Server->Client packet Attack
        /// </summary>
        /// <param name="target"></param>
        /// <param name="isSoulShotCharged"></param>
        /// <param name="ssGrade"></param>
        /// <param name="weapon"></param>
        /// <param name="attackHit"></param>
        /// <returns></returns>
        private async Task<Attack> CheckHit(Character target, bool isSoulShotCharged, int ssGrade, Weapon weapon)
        {
            Attack attack = new Attack(_character, isSoulShotCharged, ssGrade);

            var attackHit = GetAttackHit(weapon, attack, target);
            if (attackHit.IsMissed)
            {
                await _character.SendPacketAsync(new SystemMessage(SystemMessageId.MissedTarget));
                await AbortAttackAsync();
            }

            // If the Server->Client packet Attack contains at least 1 hit, send the Server->Client packet Attack
            // to the Creature AND to all PlayerInstance in the _KnownPlayers of the Creature
            attack.AddHit(target.ObjectId, attackHit.Damage, attackHit.IsMissed, attackHit.IsCritical, attackHit.IsShield);
            if (attack.HasHits())
            {
                await _character.SendBroadcastPacketAsync(attack);
                DischargeSoulShot(weapon, isSoulShotCharged);
            }

            return attack;
        }

        private void DoDualWeaponAttack(Character target, Weapon weapon, int timeToHit, int timeAtk, int reuse)
        {
            throw new System.NotImplementedException();
        }

        public int GetDisabledBowAttackEndTime()
        {
            return _disableBowAttackEndTime;
        }

        private async Task DoBowAttack(Weapon weapon, IRunnable hitTask, int timeAtk, int reuse)
        {
            if (!Validator.IsValidBowAttack(weapon))
            {
                return;
            }
            await BowAttack(timeAtk + reuse);
            // Add this hit to the Server-Client packet Attack
            ThreadPoolManager.Instance.Schedule(hitTask.Run, timeAtk);
            ThreadPoolManager.Instance.Schedule(() => { _character.AI.NotifyAiTask(_character, CtrlEvent.EvtReadyToAct); },
                timeAtk + reuse);
        }

        private bool IsBowAttack(Weapon weapon)
        {
            return (weapon != null && weapon.WeaponType.Id == WeaponTypeId.Bow);
        }
        
        private bool IsDualWeaponAttack(Weapon weapon)
        {
            if (weapon is null)
                return false;
            return weapon.WeaponType.Id switch
            {
                WeaponTypeId.Dual => true,
                WeaponTypeId.DualFist => true,
                _ => false
            };
        }

        private void SetAttackEndTime(int timeAtk)
        {
            AttackEndTime = Initializer.TimeController().GetGameTicks();
            AttackEndTime += (timeAtk / Initializer.TimeController().MillisInTick);
            AttackEndTime -= 1;
        }

        /// <summary>
        /// Get the active weapon item corresponding to the active weapon instance (always equipped in the right hand)
        /// </summary>
        /// <returns></returns>
        private Weapon GetActiveWeapon()
        {
            return _character.GetActiveWeaponItem();
        }

        private int GetCrystalType(Weapon weapon)
        {
            int ssGrade = 0;
            if (weapon != null)
            {
                ssGrade = (int) weapon.CrystalType;
            }
            return ssGrade;
        }

        /// <summary>
        /// Add the PlayerInstance to _knownObjects and _knownPlayer of the target
        /// </summary>
        /// <param name="target"></param>
        private void AddKnownObject(Character target)
        {
            target.GetKnownList().AddKnownObject(_character);
        }

        private void DoRegularAttack(IRunnable hitTask, int timeToHit, int timeAtk, int reuse)
        {
            ThreadPoolManager.Instance.Schedule(hitTask.Run, timeToHit);
            ThreadPoolManager.Instance.Schedule(() => { _character.AI.NotifyAiTask(_character, CtrlEvent.EvtReadyToAct); },
                timeAtk + reuse);
        }

        private async Task BowAttack(int timeAtk)
        {
            // Check if the Creature is a PlayerInstance
            if (_character is PlayerInstance playerInstance)
            {
                // Send a system message
                await playerInstance.SendPacketAsync(new SystemMessage(SystemMessageId.GettingReadyToShootAnArrow));
                // Send a Server->Client packet SetupGauge
                await playerInstance.SendPacketAsync(new SetupGauge(SetupGauge.Red, timeAtk));
            }
            // Calculate and set the disable delay of the bow in function of the Attack Speed
            _disableBowAttackEndTime = (timeAtk / Initializer.TimeController().MillisInTick) + Initializer.TimeController().GetGameTicks();
        }

        /// <summary>
        /// discharge soul shot
        /// </summary>
        /// <param name="weapon"></param>
        /// <param name="isSoulShotCharged"></param>
        private void DischargeSoulShot(Weapon weapon, bool isSoulShotCharged)
        {
            if (isSoulShotCharged)
            {
                weapon.ChargedSoulShot = Charged.ChargedNone;
            }
        }

        /// <summary>
        /// Heading calculation on every attack
        /// </summary>
        /// <param name="target"></param>
        private void SetHeading(Character target)
        {
            _character.Heading =
                CalculateRange.CalculateHeadingFrom(_character.GetX(), _character.GetY(), target.GetX(), target.GetY());
        }


        /// <summary>
        /// Select the type of attack to start
        /// </summary>
        /// <param name="weaponItem"></param>
        /// <param name="attack"></param>
        /// <param name="target"></param>
        /// <param name="timeToHit"></param>
        /// <returns></returns>
        private AttackHit GetAttackHit(Weapon weaponItem, Attack attack, Character target)
        {
            // Select the type of attack to start
            if (weaponItem == null)
            {
                return GetAttackHitSimple(attack, target);
            }
            if (weaponItem.WeaponType.Id == WeaponTypeId.Pole)
            {
                //return GetAttackHitByPole(attack, target, timeToHit);
            }
            if (weaponItem.WeaponType.Id == WeaponTypeId.Dual)
            {
                //return GetAttackHitByDual(attack, target, timeToHit);
            }
            return GetAttackHitSimple(attack, target);
        }

        private bool IsSoulShotCharged(Weapon weaponInst)
        {
            return (weaponInst != null) && (weaponInst.ChargedSoulShot != Charged.ChargedNone);
        }
        
        /// <summary>
        /// Recharge any active auto SoulShot tasks for player (or player's summon if one exists).
        /// for summon not implemented yet
        /// </summary>
        private async Task RechargeAutoSoulShotAsync()
        {
            if (_character is PlayerInstance playerInstance)
            {
                await playerInstance.PlayerSoulShot().RechargeAutoSoulShotAsync();
            }
        }

        private AttackHit GetAttackHitSimple(Attack attack, Character target)
        {
            return _attackSimple.GetAttackHitSimple(attack, target);
        }
        
    }
}