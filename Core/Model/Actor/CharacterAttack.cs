﻿using System.Threading.Tasks;
using Core.Model.CalculateStats;
using Core.Model.Items.Type;
using Core.Model.Skills;

using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Actor
{
    public abstract class CharacterAttack
    {
        private readonly Character _character;
        protected int AttackEndTime;
        private int _attacking;
        
        public Skill LastPotionCast { get; set; }
        public Skill LastSkillCast { get; set; }
        

        protected CharacterAttack(Character character)
        {
            _character = character;
        }

        /// <summary>
        /// Get the Attack Reuse Delay of the Weapon
        /// </summary>
        /// <param name="target"></param>
        /// <param name="weapon"></param>
        /// <returns></returns>
        public int CalculateReuseTime(Character target, Weapon weapon)
        {
            if (weapon == null)
            {
                return 0;
            }
            int reuse = weapon.AtkReuse;
            // only bows should continue for now
            if (reuse == 0)
            {
                return 0;
            }
            reuse *= _character.Stat.GetReuseModifier(target);
		
            int atkSpd = _character.Stat.GetPAtkSpd();
		
            switch (weapon.WeaponType.Id)
            {
                case WeaponTypeId.Bow:
                {
                    return ((reuse * 345) / atkSpd);
                }
                default:
                {
                    return ((reuse * 312) / atkSpd);
                }
            }
        }
        
        public int CalculateTimeBetweenAttacks(Character target, Weapon weapon)
        {
            int atkSpd;
            if (weapon != null)
            {
                switch (weapon.WeaponType.Id)
                {
                    case WeaponTypeId.Bow:
                    {
                        atkSpd = _character.Stat.GetPAtkSpd();
                        return ((1500 * 345) / atkSpd);
                    }
                    case WeaponTypeId.Dagger:
                    {
                        atkSpd = _character.Stat.GetPAtkSpd();
                        break;
                    }
                    default:
                    {
                        atkSpd = _character.Stat.GetPAtkSpd();
                        break;
                    }
                }
            }
            else
            {
                atkSpd = _character.Stat.GetPAtkSpd();
            }
            return Formulas.CalcPAtkSpd(_character, target, atkSpd);
        }
        
        /// <summary>
        /// Set the Attacking Body part to CHEST
        /// </summary>
        public void SetAttackingBodypart()
        {
            _attacking = Inventory.PaperdollChest;
        }
        
        public bool IsAttackingNow()
        {
            return AttackEndTime > Initializer.TimeController().GetGameTicks();
        }
	
        /**
	 * Return True if the Creature has aborted its attack.
	 * @return true, if is attack aborted
	 */
        public bool IsAttackAborted()
        {
            return _attacking <= 0;
        }
	
        /**
	 * Abort the attack of the Creature and send Server->Client ActionFailed packet.
	 */
        public async Task AbortAttackAsync()
        {
            if (IsAttackingNow())
            {
                _attacking = 0;
                await _character.SendPacketAsync(new ActionFailed());
            }
        }
    }
}