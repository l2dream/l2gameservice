﻿using System;
using System.Numerics;
using System.Threading.Tasks;
using Core.AI;
using Core.Helper;
using Core.Model.Actor.Data;
using Core.Model.Npc.Type;
using Core.Model.Player;
using Core.Model.World;
using Core.Model.Zones;
using Core.Network.GameServicePackets.ServerPackets;
using Core.TaskManager;
using Helpers;

namespace Core.Model.Actor
{
    internal sealed class CharacterMovement : ICharacterMovement
    {
        private readonly Character _character;
        private bool _isRunning;
        private MoveData _move;
        private readonly GameTimeController _timeController;
        public bool IsMoving => _move != null;
        private bool _cursorKeyMovement = false;

        public CharacterMovement(Character character)
        {
            _character = character;
            _timeController = Initializer.TimeController();
        }
        
        /// <summary>
        /// Return True if the Character is running.
        /// </summary>
        /// <returns>true, if is running</returns>
        public bool IsRunning()
        {
            return _isRunning;
        }
        
        /// <summary>
        /// Sets the checks if is running.
        /// </summary>
        /// <param name="value">value the new checks if is running</param>
        public async Task SetRunningAsync(bool value)
        {
            _isRunning = value;
            await _character.SendBroadcastPacketAsync(new ChangeMoveType(_character));
        }
        
        /// <summary>
        /// Set the Character movement type to run and send Server->Client packet ChangeMoveType to all others PlayerInstance
        /// </summary>
        public async Task SetRunningAsync()
        {
            if (!_isRunning)
            {
                await SetRunningAsync(true);
            }
        }

        public async Task SetWalkingAsync()
        {
            if (_isRunning)
            {
                await SetRunningAsync(false);
            }
        }

        public void MoveToLocation(int x, int y, int z, int offset)
        {
            // Get the Move Speed of the Creature
            int speed = _character.Stat.GetMoveSpeed();
            if ((speed <= 0) || _character.Status.IsMovementDisabled())
            {
                return;
            }
            
            // Get current position of the Creature
            int curX = _character.GetX();
            int curY = _character.GetY();
            int curZ = _character.GetZ();

            bool isInWater = _character.Zone().IsInsideZone(ZoneId.Water);
            if (isInWater && (z > curZ))
            {
                z = Math.Max(z - 500, curZ);
            }
            
            double dx = (x - curX);
            double dy = (y - curY);
            double dz = (z - curZ);
            double distance = Utility.Hypot(dx,dy);
            bool verticalMovementOnly = _character.Status.IsFlying && (distance == 0) && (dz != 0);
            if (verticalMovementOnly)
            {
                distance = Math.Abs(dz);
            }
            
            // Make water move short and use no geodata checks for swimming chars distance in a click can easily be over 3000.
            if (isInWater && (distance > 700))
            {
                double divider = 700 / distance;
                x = curX + (int) (divider * dx);
                y = curY + (int) (divider * dy);
                z = curZ + (int) (divider * dz);
                dx = (x - curX);
                dy = (y - curY);
                dz = (z - curZ);
                distance = Utility.Hypot(dx, dy);
            }
            
            double cos;
            double sin;
            
            // Check if a movement offset is defined or no distance to go through
            if ((offset > 0) || (distance < 1))
            {
                // approximation for moving closer when z coordinates are different
                // TODO: handle Z axis movement better
                offset -= (int)Math.Abs(dz);
                if (offset < 5)
                {
                    offset = 5;
                }
			
                // If no distance to go through, the movement is canceled
                if ((distance < 1) || ((distance - offset) <= 0))
                {
                    // Notify the AI that the Creature is arrived at destination
                    _character.AI.NotifyEvent(CtrlEvent.EvtArrived, null);
                    return;
                }
			
                // Calculate movement angles needed
                sin = dy / distance;
                cos = dx / distance;
                distance -= (offset - 5); // due to rounding error, we have to move a bit closer to be in range
			
                // Calculate the new destination with offset included
                x = curX + (int) (distance * cos);
                y = curY + (int) (distance * sin);
            }
            else
            {
                // Calculate movement angles needed
                sin = dy / distance;
                cos = dx / distance;
            }
            
            // Create and Init a MoveData object
            // GEODATA MOVEMENT CHECKS AND PATHFINDING
            // Initialize not on geodata path
            MoveData m = new MoveData {OnGeodataPathIndex = -1, DisregardingGeodata = false};

            if (!_character.Status.IsFlying && !isInWater && !(_character is L2NpcWalker) && !_cursorKeyMovement)
            {
                double originalDistance = distance;
                int originalX = x;
                int originalY = y;
                int originalZ = z;
                int gtx = (originalX - WorldData.MAP_MIN_X) >> 4;
                int gty = (originalY - WorldData.MAP_MIN_Y) >> 4;
                
                // Pathfinding checks.
                if (((originalDistance - distance) > 30) && !_character.Status.IsAfraid)
                {
                    m.OnGeodataPathIndex = 0; // on first segment
                    m.GeoPathGtx = gtx;
                    m.GeoPathGty = gty;
                    m.GeoPathAccurateTx = originalX;
                    m.GeoPathAccurateTy = originalY;
                    x = m.GeoPath[m.OnGeodataPathIndex].GetX();
                    y = m.GeoPath[m.OnGeodataPathIndex].GetY();
                    z = m.GeoPath[m.OnGeodataPathIndex].GetZ();
                    dx = x - curX;
                    dy = y - curY;
                    dz = z - curZ;
                    distance = verticalMovementOnly ? Math.Pow(dz, 2) : Utility.Hypot(dx, dy);
                    sin = dy / distance;
                    cos = dx / distance;
                }
            }
            
            // Apply Z distance for flying or swimming for correct timing calculations
            if ((_character.Status.IsFlying || isInWater) && !verticalMovementOnly)
            {
                distance = Utility.Hypot(distance, dz);
            }

            int ticksToMove = 1 + (int) ((_timeController.TicksPerSecond * distance) / speed);
            m.XDestination = x;
            m.YDestination = y;
            m.ZDestination = z; // this is what was requested from client
            // Calculate and set the heading of the Creature
            m.Heading = 0; // initial value for coordinate sync
            
            // Does not break heading on vertical movements
            if (!verticalMovementOnly)
            {
                _character.Heading = (CalculateRange.CalculateHeadingFrom(cos, sin));
            }
            
            m.MoveStartTime = _timeController.GetGameTicks();
            
            // Set the Creature _move object to MoveData object
            _move = m;
            
            _timeController.RegisterMovingObject(_character);

            // Create a task to notify the AI that Creature arrives at a check point of the movement
            if ((ticksToMove * _timeController.MillisInTick) > 3000)
            {
                ThreadPoolManager.Instance.Schedule(() => 
                {
                    _character.AI.NotifyAiTask(_character, CtrlEvent.EvtArrivedRevalidate);
                }, 2000);
            }
        }
        
        public bool UpdatePosition(int gameTicks)
        {
            MoveData m = _move;
            if (m == null)
            {
                return true;
            }
            
            if (!_character.IsVisible())
            {
                _move = null;
                return true;
            }
            
            // Check if the position has alreday be calculated
            if (m.MoveTimestamp == 0)
            {
                m.MoveTimestamp = m.MoveStartTime;
                m.XAccurate = _character.GetX();
                m.YAccurate = _character.GetY();
            }
            // Check if the position has already be calculated
            if (m.MoveTimestamp == gameTicks)
            {
                return false;
            }
            
            int xPrev = _character.GetX();
            int yPrev = _character.GetY();
            int zPrev = _character.GetZ(); // the z coordinate may be modified by coordinate synchronizations
            double dx;
            double dy;
            double dz;
            double distFraction;
            
            dx = m.XDestination - m.XAccurate;
            dy = m.YDestination - m.YAccurate;
            dz = m.ZDestination - zPrev;

            int speed = _character.Stat.GetMoveSpeed();

            if (_character.IsPlayer)
            {
                double distance = Utility.Hypot(dx, dy);
                if (_cursorKeyMovement // In case of cursor movement, avoid moving through obstacles.
                    || (distance > 3000)
                ) // Stop movement when player has clicked far away and intersected with an obstacle.
                {
                    double angle = Utility.ConvertHeadingToDegree(_character.Heading);
                    double radian = angle.ToRadians();
                    double course = Utility.ToRadians(180);
                    double frontDistance = 10 * (_character.Stat.GetMoveSpeed() / 100);
                    int x1 = (int) (Math.Cos(Math.PI + radian + course) * frontDistance);
                    int y1 = (int) (Math.Sin(Math.PI + radian + course) * frontDistance);
                    int x = xPrev + x1;
                    int y = yPrev + y1;
                    
                    
                }

                // Prevent player moving on ledges.
                if ((dz > 180) && (distance < 300))
                {
                    _move.OnGeodataPathIndex = -1;
                    //StopMove(getActingPlayer().getLastServerPosition());
                    return false;
                }
            }
            
            int distPassed = (speed * (gameTicks - m.MoveTimestamp)) / _timeController.TicksPerSecond;
            if ((((dx * dx) + (dy * dy)) < 10000) && ((dz * dz) > 2500)) // close enough, allows error between client and server geodata if it cannot be avoided
            {
                distFraction = distPassed / Math.Sqrt((dx * dx) + (dy * dy));
            }
            else
            {
                distFraction = distPassed / Math.Sqrt((dx * dx) + (dy * dy) + (dz * dz));
            }
            
            if (distFraction > 1)
            {
                // Set the position of the Creature to the destination
                _character.SetXYZ(m.XDestination, m.YDestination, m.ZDestination);
            }
            else
            {
                m.XAccurate += dx * distFraction;
                m.YAccurate += dy * distFraction;
                
                // Set the position of the Creature to estimated after parcial move
                _character.SetXYZ((int) m.XAccurate, (int) m.YAccurate, zPrev + (int) ((dz * distFraction) + 0.5));
            }
            // Set the timer of last position update to now
            m.MoveTimestamp = gameTicks;
            _character.Zone().RevalidateZone();
		
            return distFraction > 1;
        }
        
        /// <summary>
        /// Gets the xdestination.
        /// </summary>
        /// <returns></returns>
        public int GetXDestination()
        {
            MoveData m = _move;
            if (m != null)
            {
                return m.XDestination;
            }
            return _character.GetX();
        }
        
        /// <summary>
        /// Return the Y destination of the Creature or the Y position if not in movement.
        /// </summary>
        /// <returns></returns>
        public int GetYDestination()
        {
            MoveData m = _move;
            if (m != null)
            {
                return m.YDestination;
            }
            return _character.GetY();
        }
        
        /// <summary>
        /// Return the Z destination of the Creature or the Z position if not in movement.
        /// </summary>
        /// <returns></returns>
        public int GetZDestination()
        {
            MoveData m = _move;
            if (m != null)
            {
                return m.ZDestination;
            }
            return _character.GetZ();
        }

        public async Task StopMoveAsync(Location pos, bool updateKnownObjects = true)
        {
            // Delete movement data of the Creature
            _move = null;
            // Set AI_INTENTION_IDLE
            if ((_character is PlayerInstance playerInstance) && (_character.AI != null))
            {
                playerInstance.AI.SetIntention(CtrlIntention.AiIntentionIdle);
            }
            
            // Set the current position (x,y,z), its current WorldRegion if necessary and its heading
            // All data are contained in a CharPosition object
            if (pos != null)
            {
                _character.WorldObjectPosition().SetXYZ(pos.GetX(), pos.GetY(), pos.GetZ());
                _character.Heading = pos.GetHeading();
			
                if (_character is PlayerInstance player)
                {
                    _character.Zone().RevalidateZone();
                }
            }
            await _character.SendBroadcastPacketAsync(new StopMove(_character));
            if (updateKnownObjects)
            {
                _character.GetKnownList().UpdateKnownObjects();
            }
        }

        public async Task<bool> MoveToNextRoutePoint()
        {
            if (!IsOnGeoDataPath())
            {
                // Cancel the move action
                _move = null;
                return false;
            }
            
            // Get the Move Speed of the Creature
            float speed = _character.Stat.GetMoveSpeed();
            if ((speed <= 0) || _character.Status.IsMovementDisabled())
            {
                // Cancel the move action
                _move = null;
                return false;
            }
            MoveData md = _move;
            if (md == null)
            {
                return false;
            }
            
            // Create and Init a MoveData object
            MoveData m = new MoveData();
		
            // Update MoveData object
            m.OnGeodataPathIndex = md.OnGeodataPathIndex + 1; // next segment
            m.GeoPath = md.GeoPath;
            m.GeoPathGtx = md.GeoPathGtx;
            m.GeoPathGty = md.GeoPathGty;
            m.GeoPathAccurateTx = md.GeoPathAccurateTx;
            m.GeoPathAccurateTy = md.GeoPathAccurateTy;
            
            if (md.OnGeodataPathIndex == (md.GeoPath.Count - 2))
            {
                m.XDestination = md.GeoPathAccurateTx;
                m.YDestination = md.GeoPathAccurateTy;
                m.ZDestination = md.GeoPath[m.OnGeodataPathIndex].GetZ();
            }
            else
            {
                m.XDestination = md.GeoPath[m.OnGeodataPathIndex].GetX();
                m.YDestination = md.GeoPath[m.OnGeodataPathIndex].GetY();
                m.ZDestination = md.GeoPath[m.OnGeodataPathIndex].GetZ();
            }
            double distance = Utility.Hypot(m.XDestination - _character.GetX(), m.YDestination - _character.GetY());
            // Calculate and set the heading of the Creature
            if (distance != 0)
            {
                _character.Heading = CalculateRange.CalculateHeadingFrom(_character.GetX(), _character.GetY(), m.XDestination, m.YDestination);
            }
            
            // Calculate the number of ticks between the current position and the destination
            // One tick added for rounding reasons
            int ticksToMove = 1 + (int) ((_timeController.TicksPerSecond * distance) / speed);
            m.Heading = 0; // initial value for coordinate sync
            m.MoveStartTime = _timeController.GetGameTicks();
            // Set the Creature _move object to MoveData object
            _move = m;
            _timeController.RegisterMovingObject(_character);
            
            // Create a task to notify the AI that Creature arrives at a check point of the movement
            if ((ticksToMove * _timeController.TicksPerSecond) > 3000)
            {
                ThreadPoolManager.Instance.Schedule(() =>
                {
                    _character.AI.NotifyAiTask(_character, CtrlEvent.EvtArrivedRevalidate);
                }, 2000);
            }
            
            // the CtrlEvent.EVT_ARRIVED will be sent when the character will actually arrive to destination by GameTimeController
		
            // Send a Server->Client packet CharMoveToLocation to the actor and all PlayerInstance in its _knownPlayers
            await _character.SendBroadcastPacketAsync(new CharMoveToLocation(_character));
            return true;
        }

        /// <summary>
        /// TODO Geo Engine
        /// </summary>
        /// <returns></returns>
        public bool IsOnGeoDataPath()
        {
            MoveData m = _move;
            if (m == null)
            {
                return false;
            }
            /*
             * if (m.onGeodataPathIndex == -1)
		        {
			        return false;
		        }
		        if (m.onGeodataPathIndex >= (m.geoPath.size() - 1))
		        {
			        return false;
		        }
             */
            return false;
        }
    }
}