﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Model.CalculateStats;
using Core.Model.Npc;
using Core.Model.Player;
using Core.Network;
using Core.Network.GameServicePackets.ServerPackets;
using Core.Network.GameServicePackets.ServerPackets.CharacterPackets;
using L2Logger;

namespace Core.Model.Actor
{
    /// <summary>
    /// This class is responsible for sending packets to client->server
    /// </summary>
    public class CharacterClientServer
    {
        private readonly Character _character;
        
        public CharacterClientServer(Character character)
        {
            _character = character;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task SendBroadcastPacketAsync(ServerPacket serverPacket, int radiusInKnownList)
        {
            if (!(serverPacket is CharInfo))
            {
                await _character.SendPacketAsync(serverPacket);
            }

            foreach (PlayerInstance player in _character.GetKnownList().GetKnownPlayers().Values)
            {
                if (radiusInKnownList != default)
                {
                    if (!player.Zone().IsInsideRadius(player, radiusInKnownList, false, false))
                    {
                        continue;
                    }
                }
                
                await player.SendPacketAsync(serverPacket);
            }
        }

        public virtual async Task BroadcastStatusUpdateAsync()
        {
            if (_character.Status.GetStatusListener().Count < 1)
            {
                return;
            }
            // Create the Server->Client packet StatusUpdate with current HP and MP
            StatusUpdate su = null;
            if (_character is PlayerInstance playerInstance)
            {
                su = new StatusUpdate(playerInstance);
            } else {
                su = new StatusUpdate(_character.ObjectId);
                su.AddAttribute(StatusUpdate.CurHp, (int) _character.Status.GetCurrentHp());
                su.AddAttribute(StatusUpdate.CurMp, (int) _character.Status.GetCurrentMp());
            }
            
            // Go through the StatusListener
            // Send the Server->Client packet StatusUpdate with current HP and MP
            foreach (var character in _character.Status.GetStatusListener().Where(temp => temp != null))
            {
                await character.SendPacketAsync(su);
            }
        }
        public virtual void SendBroadcastUserInfo()
        {
            
        }
        
        public virtual void UpdateAndBroadcastStatus(int broadcastType)
        {

        }

        public async Task BroadcastModifiedStatsAsync(List<CharacterStatId> stats)
        {
            if ((stats == null) || !stats.Any())
			{
				return;
			}
			
			bool broadcastFull = false;
			bool otherStats = false;
			StatusUpdate su = null;
			foreach (CharacterStatId statId in stats)
			{
				if (statId == CharacterStatId.PowerAttackSpeed)
				{
					if (su == null)
					{
						su = new StatusUpdate(_character.ObjectId);
					}
					
					su.AddAttribute(StatusUpdate.AtkSpd, _character.Stat.GetPAtkSpd());
				}
				else if (statId == CharacterStatId.MagicAttackSpeed)
				{
					if (su == null)
					{
						su = new StatusUpdate(_character.ObjectId);
					}
					
					su.AddAttribute(StatusUpdate.CastSpd, _character.Stat.GetMAtkSpd());
				}
				else if (statId == CharacterStatId.MaxCp)
				{
					if (_character is PlayerInstance)
					{
						if (su == null)
						{
							su = new StatusUpdate(_character.ObjectId);
						}
						
						su.AddAttribute(StatusUpdate.MaxCp, _character.Stat.GetMaxCp());
					}
				}
				else if (statId == CharacterStatId.RunSpeed)
				{
					broadcastFull = true;
				}
				else
				{
					otherStats = true;
				}
			}
			
			if (_character is PlayerInstance playerInstance)
			{
				if (broadcastFull)
				{
					//playerInstance.UpdateAndBroadcastStatus(2);
				}
				else if (otherStats)
				{
					//((PlayerInstance) this).UpdateAndBroadcastStatus(1);
					//playerInstance.UpdateAndBroadcastStatus(1);
					if (su != null)
					{
						foreach (PlayerInstance player in _character.GetKnownList().GetKnownPlayers().Values)
						{
							try
							{
								await player.SendPacketAsync(su);
							}
							catch (Exception ex)
							{
								LoggerManager.Warn(ex.Message);
							}
						}
					}
				}
				else if (su != null)
				{
					await _character.SendBroadcastPacketAsync(su);
				}
			}
			else if (_character is NpcInstance)
			{
				if (broadcastFull && (_character.GetKnownList() != null) && (_character.GetKnownList().GetKnownPlayers() != null))
				{
					foreach (PlayerInstance player in _character.GetKnownList().GetKnownPlayers().Values)
					{
						if (player != null)
						{
							//player.SendPacket(new NpcInfo((NpcInstance) this, player));
						}
					}
				}
				else if (su != null)
				{
					await _character.SendBroadcastPacketAsync(su);
				}
			}
			else if (su != null)
			{
				await _character.SendBroadcastPacketAsync(su);
			}
        }
    }
}