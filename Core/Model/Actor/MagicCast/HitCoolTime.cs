﻿using Core.Model.CalculateStats;
using Core.Model.Skills;

namespace Core.Model.Actor.MagicCast
{
    public struct HitCoolTime
    {
        private readonly Character _character;
        private int _hitTime;
        private int _coolTime;
        private readonly Skill _skill;
        public HitCoolTime(Character character, Skill skill)
        {
            _character = character;
            _hitTime = skill.HitTime;;
            _coolTime = skill.CoolTime;
            _skill = skill;
        }

        public int GetCoolTime(bool forceBuff = false, bool ss = false)
        {
            bool effectWhileCasting = _skill.HasEffectWhileCasting();
            if (!effectWhileCasting && !forceBuff && !_skill.StaticHitTime)
            {
                _hitTime = Formulas.CalcMAtkSpd(_character, _skill, _hitTime);
                if (_coolTime > 0)
                {
                    _coolTime = Formulas.CalcMAtkSpd(_character, _skill, _coolTime);
                }
            }
            if (ss && !_skill.StaticHitTime && !_skill.IsPotion && _skill.IsMagic)
            {
                // Only takes 70% of the time to cast a BSpS/SpS cast
                _coolTime = (int) (0.70 * _coolTime);
            }
            return _coolTime;
        }

        public int GetHitTime(bool ss = false)
        {
            if (ss && !_skill.StaticHitTime && !_skill.IsPotion && _skill.IsMagic)
            {
                // Only takes 70% of the time to cast a BSpS/SpS cast
                _hitTime = (int) (0.70 * _hitTime);
            }
            return _hitTime;
        }
    }
}