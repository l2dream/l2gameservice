﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Helper;
using Core.Model.Actor.Data;
using Core.Model.Player;
using Core.Model.Skills;
using Core.Model.World;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;
using Core.TaskManager;
using L2Logger;

namespace Core.Model.Actor.MagicCast
{
    internal sealed class MagicUseTask : IRunnable
    {
        readonly WorldObject[] _targets;
        private readonly Character _character;
        readonly Skill _skill;
        readonly int _coolTime;
        readonly int _phase;

        public MagicUseTask(Character character, WorldObject[] targets, Skill skill, int coolTime, int phase)
        {
            _character = character;
            _targets = targets;
            _skill = skill;
            _coolTime = coolTime;
            _phase = phase;
        }
        
        /// <summary>
        /// Manage the magic skill launching task (MP, HP, Item consummation...) and display the magic skill animation on client
        /// </summary>
        /// <param name="targets"></param>
        /// <param name="skill"></param>
        /// <param name="coolTime"></param>
        /// <param name="instant"></param>
        public async Task OnMagicLaunchedTimer(WorldObject[] targets, Skill skill, int coolTime, bool instant)
        {
            // Escaping from under skill's radius and peace zone check. First version, not perfect in AoE skills.
            int escapeRange = 0;
            if (skill.EffectRange > escapeRange)
            {
                escapeRange = skill.EffectRange;
            }
            else if ((skill.CastRange < 0) && (skill.SkillRadius > 80))
            {
                escapeRange = skill.SkillRadius;
            }
            
            WorldObject[] finalTargets = null;
            int skipped = 0;
            if (escapeRange > 0)
            {
                List<Character> targetList = new List<Character>();
                foreach (var target in targets)
                {
                    if (target is Character character)
                    {
                        if (!CalculateRange.CheckIfInRange(escapeRange, _character, target, true))
                        {
                            continue;
                        }
                        // Check if the target is behind a wall
                        if ((skill.SkillRadius > 0) && skill.IsOffensive)
                        {
                            skipped++;
                            continue;
                        }

                        if (skill.IsOffensive)
                        {
                            /*
                            if (_character is PlayerInstance playerInstance)
                            {
                                if (playerInstance.IsInsidePeaceZone(playerInstance))
                                {
                                    continue;
                                }
                            }
                            else if (isInsidePeaceZone(this, targets[i]))
                            {
                                continue;
                            }
                            */
                        }
                        targetList.Add(character);
                    }
                }

                if (!targetList.Any() && (skill.TargetType != SkillTargetType.TargetAura))
                {
                    if (_character is PlayerInstance playerInstance)
                    {
                        for (int i = 0; i < skipped; i++)
                        {
                            await playerInstance.SendPacketAsync(new SystemMessage(SystemMessageId.CantSeeTarget));
                        }
                    }
                    //AbortCast();
                    return;
                }
                finalTargets = targetList.ToArray();
            } 
            else
            {
                finalTargets = targets;
            }
            
            // Get the display identifier of the skill
            int magicId = skill.DisplayId;
		
            // Get the level of the skill
            //int level = _character.getSkillLevel(skill.Id); TODO Skill level
            int level = 1;
            
            // Send a Server->Client packet MagicSkillLaunched to the Creature AND to all PlayerInstance in the _KnownPlayers of the Creature
            if (!skill.IsPotion)
            {
                await _character.SendBroadcastPacketAsync(new MagicSkillLaunched(_character, magicId, level, finalTargets));
            }

            if (instant)
            {
                await OnMagicHitTimer(finalTargets, skill, coolTime, true);
            }
            else if (skill.IsPotion)
            {
                //_potionCast = ThreadPool.schedule(new MagicUseTask(finalTargets, skill, coolTime, 2), 200);
            }
            else
            {
                ThreadPoolManager.Instance.Schedule(async () =>
                {
                    await new MagicUseTask(_character, finalTargets, skill, coolTime, 2).Run();
                }, 200);
            } 
        }

        private async Task OnMagicHitTimer(WorldObject[] targets, Skill skill, int coolTime, bool instant)
        {
            WorldObject[] targets2 = targets;
            if ((targets2 != null) && (targets2.Length != 0))
            {
                foreach (WorldObject target2 in targets2)
                {
                    if (target2 is Playable)
                    {
                        if ((skill.GetSkillType() == SkillType.Buff) || (skill.GetSkillType() == SkillType.Seed))
                        {
                            SystemMessage smsg = new SystemMessage(SystemMessageId.YouFeelS1Effect);
                            smsg.AddString(skill.Name);
                            await target2.SendPacketAsync(smsg);   
                        }
                    }
                }
            }
            
            StatusUpdate su = new StatusUpdate(_character.ObjectId);

            // Consume MP of the Creature and Send the Server->Client packet StatusUpdate with current HP and MP to all other PlayerInstance to inform
            double mpConsume = _character.Stat.GetMpConsume(skill);
            if (mpConsume > 0)
            {
                //TODo
            }
            // Launch the magic skill in order to calculate its effects
            _character.MagicCast().CallSkill(skill, targets);
        }
        
        public async Task Run()
        {
            try
            {
                switch (_phase)
                {
                    case 1:
                    {
                        await OnMagicLaunchedTimer(_targets, _skill, _coolTime, false);
                        break;
                    }
                    case 2:
                    {
                        await OnMagicHitTimer(_targets, _skill, _coolTime, false);
                        break;
                    }
                    case 3:
                    {
                        //OnMagicFinalizer(_targets, _skill);
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                LoggerManager.Error(GetType() + ": " + e.Message);
            }
        }
    }
}