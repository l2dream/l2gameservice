﻿using System.Collections.Generic;
using Core.Model.Items;
using Core.Model.Player;

namespace Core.Model.Actor
{
    public abstract class CharacterTemplateStat
    {
        public virtual int BaseCpMax { get; set; }
        public virtual int BaseHpMax { get; set; }
        public virtual int BaseMpMax { get; set; }
        public int BaseStr { get; set; }
        public int BaseCon { get; set; }
        public int BaseDex { get; set; }
        public int BaseInt { get; set; }
        public int BaseWit { get; set; }
        public int BaseMen { get; set; }
        public int BasePAtk { get; set; }
        public int BasePDef { get; set; }
        public int BaseMAtk { get; set; }
        public int BaseMDef { get; set; }
        public int BasePAtkSpd { get; set; }
        public int BaseMAtkSpd { get; set; }
        public int BaseCritRate { get; set; }
        public int BaseMagicCritRate { get; set; }
        public int BaseRunSpd { get; set; }
        public int BaseMReuseRate { get; set; }
        public float CollisionRadius { get; set; }
        public float CollisionHeight { get; set; }
        public List<ItemHolder> Items { get; set; }
        public double BaseHpReg { get; set; } = 1.5f;
        public double BaseMpReg { get; set; } = 0.9f;
        public int BaseWalkSpd { get; set; } = 0;
        public int BaseShldDef { get; set; } = 0;
        public int BaseShldRate { get; set; } = 0;
        public int BaseAtkRange { get; set; } = 40;
        public int BaseLevel { get; set; }
        public ClassId ClassId { get; set; }
        public ClassRace Race { get; set; }
        public string ClassName { get; set; }
        public int SpawnX { get; set; }
        public int SpawnY { get; set; }
        public int SpawnZ { get; set; }
        public int ClassBaseLevel { get; set; }
        public float LevelHpAdd { get; set; }
        public float LevelHpMod { get; set; }
        public float LevelCpAdd { get; set; }
        public float LevelCpMod { get; set; }
        public float LevelMpAdd { get; set; }
        public float LevelMpMod { get; set; }
    }
}
