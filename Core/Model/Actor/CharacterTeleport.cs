﻿using System.Threading.Tasks;
using Core.AI;
using Core.Model.Actor.Data;
using Core.Model.Player;
using Core.Model.World;
using Core.Model.World.RegionData;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Actor
{
    internal sealed class CharacterTeleport : ICharacterTeleport
    {
        private readonly Character _character;
        public CharacterTeleport(Character character)
        {
            _character = character;
        }

        /// <summary>
        /// Tele to location.
        /// </summary>
        /// <param name="teleportWhere"></param>
        public async Task TeleToLocationAsync(TeleportWhereType teleportWhere)
        {
            await TeleToLocationAsync(Initializer.MapRegionService().GetTeleToLocation(_character, teleportWhere), true);
        }

        /// <summary>
        /// Tele to location.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public async Task TeleToLocationAsync(int x, int y, int z)
        {
            await TeleToLocationAsync(x, y, z, false);
        }

        /// <summary>
        /// Tele to location
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="allowRandomOffset"></param>
        public async Task TeleToLocationAsync(int x, int y, int z, bool allowRandomOffset)
        {
            // Stop movement
            await _character.Movement().StopMoveAsync(null, false);
            await _character.Attack().AbortAttackAsync();
            _character.Status.IsTeleporting = true;
            _character.Target.RemoveTargetAsync();
            // Remove from world regions zones
            _character.OnDecay();
            _character.AI.SetIntention(CtrlIntention.AiIntentionActive);
            z += 5;
            
            // Send a Server->Client packet TeleportToLocationt to the Creature AND to all PlayerInstance in the _KnownPlayers of the Creature
            await _character.SendBroadcastPacketAsync(new TeleportToLocation(_character, x, y, z));
            
            // Set the x,y,z position of the WorldObject and if necessary modify its _worldRegion
            _character.WorldObjectPosition().SetXYZ(x, y, z);
            if (!(_character is PlayerInstance))
            {
                await OnTeleported();
            }
		
            _character.Zone().RevalidateZone();
        }

        public async Task OnTeleported()
        {
            if (!_character.Status.IsTeleporting)
            {
                return;
            }
		
            ObjectPosition pos = _character.WorldObjectPosition();
            if (pos != null)
            {
                _character.SpawnMe(pos.GetX(), pos.GetY(), pos.GetZ());
            }
            _character.Status.IsTeleporting = false;
            
            if (_character.Status.IsPendingRevive)
            {
                await DoReviveAsync();
            }
        }

        //TODO need create an other class
        public async Task DoReviveAsync()
        {
            if (!_character.Status.IsTeleporting)
            {
                _character.Status.IsPendingRevive = false;
                
                await _character.Status.SetCurrentCpAsync(_character.Stat.GetMaxCp() * 0.1); // TODO Config
                await _character.Status.SetCurrentHpAsync(_character.Stat.GetMaxHp() * 0.7); //TODO Config
            }
            // Start broadcast status
            await _character.SendBroadcastPacketAsync(new Revive(_character));
            if (_character.GetWorldRegion() != null)
            {
                _character.GetWorldRegion().OnRevive(_character);
            }
            else
            {
                _character.Status.IsPendingRevive = true;
            }
        }

        public async Task TeleToLocationAsync(Location loc, bool allowRandomOffset)
        {
            int x = loc.GetX();
            int y = loc.GetY();
            int z = loc.GetZ();
            await TeleToLocationAsync(x, y, z, allowRandomOffset);
        }
    }
}