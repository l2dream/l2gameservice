﻿using System.Threading.Tasks;
using Core.Model.Player;
using Core.Model.World;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Actor
{
    public class CharacterTarget
    {
        private readonly Character _character;
        private WorldObject _target;
        public CharacterTarget(Character character)
        {
            _character = character;
        }
        
        public int GetTargetId()
        {
            if (_target != null)
            {
                return _target.ObjectId;
            }
            return -1;
        }
        
        public WorldObject GetTarget()
        {
            return _target;
        }
        
        public virtual void SetTarget(WorldObject worldObject)
        {
            if (!worldObject.IsVisible())
            {
                worldObject = null;
            }
            if ((worldObject != null) && (worldObject != _target))
            {
                _character.GetKnownList().AddKnownObject(worldObject);
                worldObject.GetKnownList().AddKnownObject(_character);
            }
            _target = worldObject;
        }

        public void RemoveTargetAsync()
        {
            _character.SendPacketAsync(new TargetUnselected(_character));
            _target = null;
        }
    }
}