﻿using System;
using System.Threading.Tasks;
using Core.Model.Actor.Data;
using Core.Model.Actor.Stat.Data;
using Core.Model.CalculateStats;
using L2Logger;

namespace Core.Model.Actor.Status
{
    public class RegenTask : IRunnable
    {
        private readonly Character _character;
        private readonly double _currentCp = 0; // Current CP of the Creature
        private readonly double _currentHp = 0; // Current HP of the Creature
        private readonly double _currentMp = 0; // Current MP of the Creature
        private readonly CharacterStatus _status;
        public RegenTask(Character character)
        {
            _character = character;
            _status = _character.Status;
            _currentCp = _status.GetCurrentCp();
            _currentHp = _status.GetCurrentHp();
            _currentMp = _status.GetCurrentMp();
        }

        public async Task Run()
        {
            try
            {
                ICharacterStat characterStat = _character.Stat;
				
                // Modify the current CP of the Creature and broadcast Server->Client packet StatusUpdate
                if (_currentCp < characterStat.GetMaxCp())
                {
                    await _status.SetCurrentCpAsync( _currentCp + Formulas.CalcCpRegen(_character), false);
                }
				
                // Modify the current HP of the Creature and broadcast Server->Client packet StatusUpdate
                if (_currentHp < characterStat.GetMaxHp())
                {
                    await _status.SetCurrentHpAsync(_currentHp + Formulas.CalcHpRegen(_character), false);
                }
				
                // Modify the current MP of the Creature and broadcast Server->Client packet StatusUpdate
                if (_currentMp < characterStat.GetMaxMp())
                {
                    _status.SetCurrentMp(_currentMp + Formulas.CalcMpRegen(_character), false);
                }
				
                if (!_character.Zone().IsInActiveRegion())
                {
                    // no broadcast necessary for characters that are in inactive regions.
                    // stop regeneration for characters who are filled up and in an inactive region.
                    if ((_currentCp == characterStat.GetMaxCp()) && (_currentHp == characterStat.GetMaxHp()) &&
                        (_currentMp == characterStat.GetMaxMp()))
                    {
                        _status.StopHpMpRegeneration();
                    }
                }
                else
                {
                    await _character.BroadcastStatusUpdateAsync(); // send the StatusUpdate packet
                }
            }
            catch (Exception e)
            {
                LoggerManager.Info("RegenTask failed for " + _character.CharacterName + " " + e);
            }
        }
    }
}