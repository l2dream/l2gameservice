﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Core.Model.CalculateStats;
using Core.Model.Player;
using Core.TaskManager;
using Helpers;

namespace Core.Model.Actor.Status
{
    public class CharacterStatus
    {
        private readonly Character _character;
        double _currentCp = 0; // Current CP of the Creature
        double _currentHp = 0; // Current HP of the Creature
        double _currentMp = 0; // Current MP of the Creature
        
        private byte _flagsRegenActive = 1;
        private byte REGEN_FLAG_CP = 4;
        private static byte REGEN_FLAG_HP = 1;
        private static byte REGEN_FLAG_MP = 2;
        
       
        private bool _isImmobileUntilAttacked = false; // Is in immobile until attacked.
        public bool IsStunned { get; set; } = false; // Cannot move/attack until stun timed out
        public bool IsSleeping { get; set; } = false; // Cannot move/attack until sleep timed out or monster is attacked
        public bool IsRooted { get; set; } = false; // Cannot move until root timed out
        public bool IsMuted { get; set; } = false;
        public bool IsPhysicalMuted { get; set; } = false;
        private bool _isOverloaded = false; // the char is carrying too much
        public bool IsParalyzed { get; set; } = false;
        private int _isImmobilized = 0;
        private bool _isFakeDeath = false; // Fake death
        private bool _isFallsDown = false; // Falls down

        public bool IsAttackDisabled { get; set; } = false;
        public bool IsConfused { get; set; } = false; // Attack anyone randomly
        public bool IsAfraid { get; set; } = false; // Flee in a random direction
        public bool Blocked { get; set; }

        public bool IsTeleporting { get; set; }
        public bool IsPendingRevive { get; set; }
        public bool InObserverMode { get; set; } = false;
        public bool IsFlying { get; set; } = false;
        
        private ISet<Character> _StatusListener;
        private Task _regTask;
        private CancellationTokenSource _cts;
        
        public int CurrCp { get; set; }
        public int CurrHp { get; set; }
        public int CurrMp { get; set; }

        public CharacterStatus(Character character)
        {
            _character = character;
        }
        
        public bool IsDead()
        {
            return (GetCurrentHp() < 0.5);
        }
        
        public async Task SetCurrentHpDirectAsync(double newHp)
        {
            await SetCurrentHpAsync(newHp, true, true);
        }
        
        public void SetCurrentMpDirect(double newMp)
        {
            SetCurrentMp(newMp, true, true);
        }
        
        public async Task SetCurrentCpDirectAsync(double newCp)
        {
            await SetCurrentCpAsync(newCp, true, true);
        }

        public bool IsAttackingDisabled()
        {
            return _isImmobileUntilAttacked || IsStunned || IsSleeping || _character.Attack().IsAttackingNow() ||
                   _isFakeDeath || IsParalyzed || IsAttackDisabled;
        }

        public async Task SetCurrentCpAsync(double newCp, bool broadcastPacket = true, bool direct = false)
        {
            lock (this)
            {
                // Get the Max CP of the Character
                int maxCp = _character.Stat.GetMaxCp();
                if ((newCp >= maxCp) && !direct)
                {
                    _currentCp = maxCp;
                    _flagsRegenActive = (byte)(_flagsRegenActive & ~REGEN_FLAG_CP);
                    
                    // Stop the HP/MP/CP Regeneration task
                    if (_flagsRegenActive == 0)
                    {
                        StopHpMpRegeneration();
                    }
                }
                else
                {
                    // Set the RegenActive flag to true
                    _currentCp = newCp;
                    _flagsRegenActive |= REGEN_FLAG_CP;

                    // Start the HP/MP/CP Regeneration task with Medium priority
                    StartHpMpRegeneration();
                }
            }
            // Send the Server->Client packet StatusUpdate with current HP and MP to all other PlayerInstance to inform
            if (broadcastPacket)
            {
                await _character.BroadcastStatusUpdateAsync();
            }
        }

        public async Task SetCurrentHpAsync(double newHp, bool broadcastPacket = true, bool direct = false)
        {
            lock (this)
            {
                // Get the Max HP of the Character
                int maxHp = _character.Stat.GetMaxHp();
                if ((newHp >= maxHp) && !direct)
                {
                    // Set the RegenActive flag to false
                    _currentHp = maxHp;
                    _flagsRegenActive = (byte) (_flagsRegenActive & ~REGEN_FLAG_HP);
                    
                    // Stop the HP/MP/CP Regeneration task
                    if (_flagsRegenActive == 0)
                    {
                        StopHpMpRegeneration();
                    }
                }
                else
                {
                    // Set the RegenActive flag to true
                    _currentHp = newHp;
                    _flagsRegenActive |= REGEN_FLAG_HP;
                    
                    // Start the HP/MP/CP Regeneration task with Medium priority
                    StartHpMpRegeneration();
                }
            }
            
            // Send the Server->Client packet StatusUpdate with current HP and MP to all other PlayerInstance to inform
            if (broadcastPacket)
            {
                if (_character is PlayerInstance playerInstance)
                {
                    await playerInstance.BroadcastStatusUpdateAsync();
                }
                else
                {
                    await _character.BroadcastStatusUpdateAsync();
                }
            }
        }

        public void SetCurrentMp(double newMp, bool broadcastPacket = true, bool direct = false)
        {
            lock (this)
            {
                int maxMp = _character.Stat.GetMaxMp();

                if ((newMp >= maxMp) && !direct)
                {
                    // Set the RegenActive flag to false
                    _currentMp = maxMp;
                    _flagsRegenActive = (byte)(_flagsRegenActive & ~REGEN_FLAG_MP);
                }
                else
                {
                    // Set the RegenActive flag to true
                    _currentMp = newMp;
                    _flagsRegenActive |= REGEN_FLAG_MP;
                }
            }
        }
        
        public double GetCurrentHp()
        {
            return _currentHp;
        }
        
        public double GetCurrentMp()
        {
            return _currentMp;
        }
        
        public double GetCurrentCp()
        {
            return _currentCp;
        }
        
        /// <summary>
        /// Return True if the Character can't move (stun, root, sleep, overload, paralyzed).
        /// </summary>
        /// <returns>true, if is movement disabled</returns>
        public bool IsMovementDisabled()
        {
            return _isImmobileUntilAttacked || IsStunned || IsRooted || IsSleeping || _isOverloaded || IsParalyzed ||
                   IsImmobilized || _isFakeDeath || _isFallsDown;
        }
        
        /// <summary>
        /// Return True if the Creature can't use its skills (ex : stun, sleep...).
        /// </summary>
        /// <returns>true, if is all skills disabled</returns>
        public bool IsAllSkillsDisabled()
        {
            return _isImmobileUntilAttacked || IsStunned || IsSleeping || IsParalyzed;
        }
        
        public bool IsOutOfControl()
        {
            return IsConfused || IsAfraid || Blocked;
        }
        
        
        /// <summary>
        /// Checks if is immobilized
        /// true, if is immobilized
        /// Sets the checks if is immobilized.
        /// value the new checks if is immobilized
        /// </summary>
        public bool IsImmobilized
        {
            get => _isImmobilized > 0;
            set
            {
                //AI.SetIntention(CtrlIntention.AiIntentionIdle);
                if (value)
                {
                    _isImmobilized++;
                }
                else
                {
                    _isImmobilized--;
                }
            }
        }
        
        public bool IsAlikeDead()
        {
            return (GetCurrentHp() <= 0.5);
        }
        
        public bool IsFakeDeath()
        {
            return _isFakeDeath;
        }
        
        public ISet<Character> GetStatusListener()
        {
            if (_StatusListener == null)
            {
                _StatusListener = new HashSet<Character>();
            }
            return _StatusListener;
        }
        
        public void AddStatusListener(Character character)
        {
            if (character == _character)
            {
                return;
            }
		
            lock (GetStatusListener())
            {
                GetStatusListener().Add(character);
            }
        }
        
        public void RemoveStatusListener(Character character)
        {
            lock (GetStatusListener())
            {
                GetStatusListener().Remove(character);
            }
        }
        
        public void StartHpMpRegeneration()
        {
            // Get the Regeneration periode
            int period = Formulas.GetRegeneratePeriod(_character);

            // Create the HP/MP/CP Regeneration task
            //_regTask = ThreadPool.scheduleAtFixedRate(new RegenTask(), period, period);
            if (_regTask == null && !_character.Status.IsDead())
            {
                _cts = new CancellationTokenSource();
                _regTask = ThreadPoolManager.Instance.ScheduleAtFixedRate(async () =>
                {
                    var regen = new RegenTask(_character);
                    await regen.Run();
                }, period, period, _cts.Token);
            }
        }

        private async Task RegenTask(int period)
        {
            using var timer = new TaskTimer(period).Start(period);
            foreach (var task in timer)
            {
                await task;
                var regen = new RegenTask(_character);
                await regen.Run();
            }
        }
        
        public void StopHpMpRegeneration()
        {
            if (_regTask == null) return;
            // Stop the HP/MP/CP Regeneration task
            if (!_regTask.IsCanceled)
            {
                _cts.Cancel();
                _regTask = null;
            }
            // Set the RegenActive flag to false
            _flagsRegenActive = 0;
        }


        public async Task ReduceCurrentHpAsync(double damage, Character character, bool awake = true)
        {
            
            double value = 0;
            if (damage > 0) // Reduce Hp if any
            {
                value = _currentHp - damage; // Get diff of Hp vs value
            }
            await SetCurrentHpAsync(value); // Set Hp

            if (IsDead())
            {
                await _character.Attack().AbortAttackAsync();
                //_character.CharacterAttack.AbortCast();
                // Start the doDie process
                await _character.DoDieAsync(character);
			
                // now reset currentHp to zero
                await SetCurrentHpAsync(0);
            }
        }
    }
}