﻿using System;
using System.Threading.Tasks;
using Core.AI;
using Core.Model.Items.Type;
using Core.Model.Npc;
using Core.TaskManager;
using Helpers;
using L2Logger;

namespace Core.Model.Actor
{
    public class AttackableAi : CharacterAi
    {
        private const int RandomWalkRate = 30; // confirmed
        private const int MaxAttackTimeout = 300; // int ticks, i.e. 30 seconds
        private int _attackTimeout;
        
        /** The delay after wich the attacked is stopped */
        public int AttackTimeout { get; set; }
	
        /** The Attackable aggro counter */
        public int GlobalAggro { get; set; }
        
        /** The flag used to indicate that a thinking action is in progress */
        private bool _thinking; // to prevent recursive thinking
        
        public AttackableAi(Character character) : base(character)
        {
            AttackTimeout = int.MaxValue;
            GlobalAggro = -10; // 10 seconds timeout of ATTACK after respawn
        }
        
        public void StartAiTask()
        {
            AttackableThinkTaskManager.Instance.Add(GetActiveChar());
        }
        
        public void StopAiTask()
        {
            AttackableThinkTaskManager.Instance.Remove(GetActiveChar());
        }

        private async Task ThinkActive()
        {
            Attackable npc = GetActiveChar();
            if ((npc.Spawn != null) && (Rnd.Next(30) == 0))
            {
                int x1 = 0;
                int y1 = 0;
                int z1 = 0;
                
                // If NPC with random coord in territory
                if ((npc.Spawn.X == 0) && (npc.Spawn.Y == 0))
                {
                    
                }
                else
                {
                    // If NPC with fixed coord
                    x1 = (npc.Spawn.X + Rnd.Next(300 * 2)) - 300;
                    y1 = (npc.Spawn.Y + Rnd.Next(300 * 2)) - 300;
                    z1 = npc.GetZ();
                }

                await MoveToAsync(x1, y1, z1).ContinueWith(HandleException);
            }
        }

        public override void ChangeIntention(CtrlIntention intention, Object arg0, Object arg1)
        {
            if ((intention == CtrlIntention.AiIntentionIdle) || (intention == CtrlIntention.AiIntentionActive))
            {
                
            }
            // Set the Intention of this AttackableAI to intention
            base.ChangeIntention(intention, arg0, arg1);
            StartAiTask();
        }


        public override async Task OnEvtThinkAsync()
        {
            // Check if the actor can't use skills and if a thinking action isn't already in progress
            if (_thinking || _actor.Status.IsAllSkillsDisabled())
            {
                return;
            }
		
            // Start thinking action
            _thinking = true;
		
            try
            {
                // Manage AI thinks of a Attackable
                if (GetIntention() == CtrlIntention.AiIntentionActive)
                {
                    await ThinkActive();
                }
                else if (GetIntention() == CtrlIntention.AiIntentionAttack)
                {
                    await ThinkAttack();
                }
            }
            finally
            {
                // Stop thinking action
                _thinking = false;
            }
        }

        private async Task ThinkAttack()
        {
            if ((_actor == null))
            {
                return;
            }
            if (_actor.Status.IsAttackingDisabled())
            {
                return;
            }

            if (_attackTimeout < Initializer.TimeController().GetGameTicks())
            {
                // Check if the actor is running
                if (_actor.Movement().IsRunning())
                {
                    // Set the actor movement type to walk and send Server->Client packet ChangeMoveType to all others PlayerInstance
                    await _actor.Movement().SetWalkingAsync();
				
                    // Calculate a new attack timeout
                    _attackTimeout = MaxAttackTimeout + Initializer.TimeController().GetGameTicks();
                }
            }
            Character originalAttackTarget = GetAttackTarget();
            
            
            // Get all information needed to chose between physical or magical attack
            double dist2 = 0;
            int range = 0;
            
            _actor.Target.SetTarget(originalAttackTarget);
            range = (int) (_actor.Stat.GetPhysicalAttackRange() + _actor.Template.Stat.CollisionRadius + originalAttackTarget.Template.Stat.CollisionRadius);
            Weapon weapon = _actor.GetActiveWeaponItem();
            int collision = (int) _actor.Template.Stat.CollisionRadius;
            int combinedCollision = (int) (collision + originalAttackTarget.Template.Stat.CollisionRadius);
            
            
            // Force mobs to attack anybody if confused
            Character hated;
            if (_actor.Status.IsConfused)
            {
                hated = originalAttackTarget;
            }
            else
            {
                hated = (GetActiveChar()).GetMostHated();
            }
            
            if (hated == null)
            {
                SetIntention(CtrlIntention.AiIntentionActive);
                return;
            }
            // We should calculate new distance cuz mob can have changed the target
            dist2 = _actor.GetPlanDistanceSq(hated.GetX(), hated.GetY());
            if (hated.IsMoving)
            {
                range += 50;
            }
            
            // Check if the actor isn't far from target
            if (dist2 > (range * range))
            {
                // Move the actor to Pawn server side AND client side by sending Server->Client packet MoveToPawn (broadcast)
                if (hated.IsMoving)
                {
                    range -= 100;
                }
                if (range < 5)
                {
                    range = 5;
                }
                await MoveToPawnAsync(originalAttackTarget, range);
                return;
            }

            // Else, if this is close enough to attack
            _attackTimeout = MaxAttackTimeout + Initializer.TimeController().GetGameTicks();
            
            // Finally, physical attacks
            await ClientStopMovingAsync(null);
            await _actor.AI.DoAttackAsync(hated);
        }

        protected override async Task OnEvtAttackedAsync(Character attacker)
        {
            // Calculate the attack timeout
            _attackTimeout = MaxAttackTimeout + Initializer.TimeController().GameTicks;
            (GetActiveChar()).AddDamageHate(attacker, 0, 1);
            // Set the Creature movement type to run and send Server->Client packet ChangeMoveType to all others PlayerInstance
            if (!_actor.Movement().IsRunning())
            {
                await _actor.Movement().SetRunningAsync();
            }
            if (((!(_actor is NpcInstance) || (_actor is Attackable)) || (_actor is Playable)))
            {
                // Set the Intention to AI_INTENTION_ATTACK
                if (GetIntention() != CtrlIntention.AiIntentionAttack)
                {
                    SetIntention(CtrlIntention.AiIntentionAttack, attacker);
                }
            }
            await base.OnEvtAttackedAsync(attacker);
        }
        
        protected override async Task OnIntentionActiveAsync()
        {
            // Cancel attack timeout
            AttackTimeout = int.MaxValue;
            await base.OnIntentionActiveAsync();
        }
        
        public Attackable GetActiveChar()
        {
            return (Attackable) _actor;
        }
        
        protected override async Task OnEvtDeadAsync()
        {
            StopAiTask();
            await base.OnEvtDeadAsync();
        }
    }
}