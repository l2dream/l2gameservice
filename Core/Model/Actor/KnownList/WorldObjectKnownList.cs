﻿using System.Collections.Concurrent;
using System.Linq;
using Core.Helper;
using Core.Model.Player;
using Core.Model.World;

namespace Core.Model.Actor.KnownList
{
    public enum Type
    {
        ShowObject,
        RemoveObject
    }
    public class WorldObjectKnownList
    {
        public WorldObject ActiveObject { get; set; }
        private readonly ConcurrentDictionary<int, WorldObject> _knownObjects;
        private ConcurrentDictionary<int, PlayerInstance> _knownPlayers;
        private readonly WorldInit _worldInit;

        protected delegate void NpcSpawnHandler(WorldObject worldObject, Type type, Character dropper = null);
        protected event NpcSpawnHandler Notify;
        
        public WorldObjectKnownList(WorldObject activeObject)
        {
            ActiveObject = activeObject;
            _knownObjects = new ConcurrentDictionary<int, WorldObject>();
            _worldInit = Initializer.WorldInit();
        }
        
        public virtual bool AddKnownObject(WorldObject worldObject)
        {
            return AddKnownObject(worldObject, null);
        }

        public virtual bool AddKnownObject(WorldObject worldObject, Character dropper)
        {
            if ((worldObject == null) || (worldObject == ActiveObject))
            {
                return false;
            }
		
            // Check if already know object
            if (HasKnowsObject(worldObject))
            {
                if (!worldObject.IsVisible())
                {
                    RemoveKnownObject(worldObject);
                }
                return false;
            }
		
            // Check if object is not inside distance to watch object
            if (!CalculateRange.CheckIfInRange(GetDistanceToWatchObject(worldObject), ActiveObject, worldObject, true))
            {
                return false;
            }
            
            Notify?.Invoke(worldObject, Type.ShowObject, dropper);
            return GetKnownObjects().TryAdd(worldObject.ObjectId, worldObject);
        }
        
        public bool HasKnowsObject(WorldObject worldObject)
        {
            if (worldObject == null)
            {
                return false;
            }
            return (ActiveObject == worldObject) || GetKnownObjects().ContainsKey(worldObject.ObjectId);
        }
        
        public void RemoveAllKnownObjects()
        {
            GetKnownObjects().Clear();
        }
        
        public virtual bool RemoveKnownObject(WorldObject worldObject)
        {
            if (worldObject == null)
            {
                return false;
            }
            Notify?.Invoke(worldObject, Type.RemoveObject);
            return GetKnownObjects().TryRemove(worldObject.ObjectId, out _);
        }
        
        public void UpdateKnownObjects()
        {
            lock (this)
            {
                // Only bother updating knownobjects for Creature; don't for WorldObject
                if (ActiveObject is Character)
                {
                    FindCloseObjects();
                    ForgetObjects();
                }
            }
        }
        
        public ConcurrentDictionary<int, PlayerInstance> GetKnownPlayers()
        {
            if (_knownPlayers == null)
            {
                _knownPlayers = new ConcurrentDictionary<int, PlayerInstance>();
            }
            return _knownPlayers;
        }
        
        private void FindCloseObjects()
        {
            if (ActiveObject == null)
            {
                return;
            }

            if (ActiveObject.IsPlayable)
            {
                // Go through all visible WorldObject near the Creature
                foreach (var worldObject in _worldInit.GetVisibleObjects(ActiveObject)
                    .Where(worldObject => worldObject != null))
                {
                    // Try to add object to active object's known objects
                    // PlayableInstance sees everything
                    AddKnownObject(worldObject);

                    // Try to add active object to object's known objects
                    // Only if object is a Creature and active object is a PlayableInstance
                    if (worldObject is Character)
                    {
                        worldObject.GetKnownList().AddKnownObject(ActiveObject);
                    }

                    if (worldObject.IsNpc)
                    {
                        //await worldObject.SendPacketAsync(new NpcInfo((NpcBase) worldObject));
                    }
                }
            }
            else
            {
                // Go through all visible WorldObject near the Creature
                foreach (WorldObject playable in _worldInit.GetVisiblePlayers(ActiveObject))
                {
                    if (playable == null)
                    {
                        return;
                    }

                    // Try to add object to active object's known objects
                    // Creature only needs to see visible PlayerInstance and PlayableInstance, when moving. Other l2characters are currently only known from initial spawn area.
                    // Possibly look into getDistanceToForgetObject values before modifying this approach...
                    AddKnownObject(playable);
                }
            }
        }

        private void ForgetObjects()
        {
            GetKnownObjects().Values.ToList().ForEach(worldObject =>
            {
                if (worldObject == null) return;
                if (!worldObject.IsVisible() || !CalculateRange.CheckIfInRange(GetDistanceToForgetObject(worldObject),
                    ActiveObject, worldObject, true))
                {
                    RemoveKnownObject(worldObject);
                }
            });
        }
        
        protected virtual int GetDistanceToForgetObject(WorldObject worldObject)
        {
            return 0;
        }

        protected virtual int GetDistanceToWatchObject(WorldObject worldObject)
        {
            return 0;
        }
        
        public ConcurrentDictionary<int, WorldObject> GetKnownObjects()
        {
            return _knownObjects;
        }
    }
}