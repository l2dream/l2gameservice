﻿using Core.AI;
using Core.Model.Npc.Type;
using Core.Model.Player;
using Core.Model.World;

namespace Core.Model.Actor.KnownList
{
    public class FriendlyMobKnownList : AttackableKnownList
    {
        public override Character ActiveCharacter => (L2FriendlyMob) ActiveObject;
        
        public FriendlyMobKnownList(WorldObject activeObject) : base(activeObject)
        {
        }
        
        public override bool AddKnownObject(WorldObject worldObject)
        {
            return AddKnownObject(worldObject, null);
        }

        public override bool AddKnownObject(WorldObject worldObject, Character dropper)
        {
            if (!base.AddKnownObject(worldObject, dropper))
            {
                return false;
            }
            if ((worldObject is PlayerInstance) && (ActiveCharacter.AI.GetIntention() == CtrlIntention.AiIntentionIdle))
            {
                ActiveCharacter.AI.SetIntention(CtrlIntention.AiIntentionActive, null);
            }
            return true;
        }
        
    }
}