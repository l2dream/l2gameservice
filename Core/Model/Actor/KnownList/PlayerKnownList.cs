﻿using System.Threading.Tasks;
using Core.Model.Items;
using Core.Model.Npc;
using Core.Model.Player;
using Core.Model.World;
using Core.Network.GameServicePackets.ServerPackets;
using Core.Network.GameServicePackets.ServerPackets.CharacterPackets;

namespace Core.Model.Actor.KnownList
{
    public class PlayerKnownList : CharacterKnownList
    {
        public PlayerInstance ActivePlayer { get; }
        public PlayerKnownList(PlayerInstance player) : base(player)
        {
            ActivePlayer = player;
            Notify += WorkWithVisibleObject;
        }
        
        public override bool AddKnownObject(WorldObject worldObject)
        {
            return AddKnownObject(worldObject, null);
        }
        
        public override bool AddKnownObject(WorldObject worldObject, Character character)
        {
            if (!base.AddKnownObject(worldObject, character))
            {
                return false;
            }
            return true;
        }

        private async void WorkWithVisibleObject(WorldObject worldObject, Type type, Character dropper)
        {
            if (type == Type.ShowObject)
            {
                if (worldObject.IsNpc)
                {
                    await ActivePlayer.SendPacketAsync(new NpcInfo((NpcInstance) worldObject));
                } 
                else if (worldObject.IsPlayer)
                {
                    await ActiveCharacter.SendPacketAsync(new CharInfo((PlayerInstance) worldObject));
                } 
                else if (worldObject.IsItem)
                {
                    if (dropper != null)
                    {
                        await ActiveCharacter.SendPacketAsync(new DropItem((ItemInstance) worldObject, dropper.ObjectId));
                    }
                    else
                    {
                        await ActiveCharacter.SendPacketAsync(new SpawnItem((ItemInstance) worldObject));
                    }
                }
            }
            else
            {
                await ActivePlayer.SendPacketAsync(new DeleteObject(worldObject.ObjectId));
            } 
        }
        
        /// <summary>
        /// TODO
        /// </summary>
        /// <param name="worldObject"></param>
        /// <returns></returns>
        public override bool RemoveKnownObject(WorldObject worldObject)
        {
            if (!base.RemoveKnownObject(worldObject))
            {
                return false;
            }
            PlayerInstance activeChar = ActivePlayer;
            if (worldObject is PlayerInstance playerInstance)
            {
                // Send Server-Client Packet DeleteObject to the PlayerInstance
                activeChar.SendPacketAsync(new DeleteObject(playerInstance.ObjectId));
            }
            else
            {
                activeChar.SendPacketAsync(new DeleteObject(worldObject.ObjectId));
            }
            
            return true;
        }
        
        protected override int GetDistanceToForgetObject(WorldObject worldObject)
        {
            // When knownlist grows, the distance to forget should be at least the same as the previous watch range, or it becomes possible that extra charinfo packets are being sent (watch-forget-watch-forget).
            int knownListSize = GetKnownObjects().Count;
            if (knownListSize <= 25)
            {
                return 4200;
            }
		
            if (knownListSize <= 35)
            {
                return 3600;
            }
		
            if (knownListSize <= 70)
            {
                return 2910;
            }
            return 2310;
        }
        
        protected override int GetDistanceToWatchObject(WorldObject worldObject)
        {
            int knownListSize = GetKnownObjects().Count;
            if (knownListSize <= 25)
            {
                return 3500; // empty field
            }
		
            if (knownListSize <= 35)
            {
                return 2900;
            }
		
            if (knownListSize <= 70)
            {
                return 2300;
            }
            return 1700; // Siege, TOI, city
        }
    }
}