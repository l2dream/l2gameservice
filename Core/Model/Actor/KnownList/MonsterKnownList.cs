﻿using Core.AI;
using Core.Model.Npc.Type;
using Core.Model.Player;
using Core.Model.World;

namespace Core.Model.Actor.KnownList
{
    public class MonsterKnownList : AttackableKnownList
    {
        public override Character ActiveCharacter => (L2Monster) ActiveObject;
        
        public MonsterKnownList(WorldObject activeObject) : base(activeObject)
        {
        }
        
        public override bool AddKnownObject(WorldObject worldObject)
        {
            if (worldObject is PlayerInstance player)
            {
                player.SubscribeMonster((L2Monster) ActiveCharacter);
            }
            return AddKnownObject(worldObject, null);
        }
        
        public override bool AddKnownObject(WorldObject worldObject, Character dropper)
        {
            if (!base.AddKnownObject(worldObject, dropper))
            {
                return false;
            }
            
            // Set the MonsterInstance Intention to AI_INTENTION_ACTIVE if the state was AI_INTENTION_IDLE
            if ((worldObject is PlayerInstance) && (ActiveCharacter.AI.GetIntention() == CtrlIntention.AiIntentionIdle))
            {
                ActiveCharacter.AI.SetIntention(CtrlIntention.AiIntentionActive, null);
            }
            
            return true;
        }

        public override bool RemoveKnownObject(WorldObject worldObject)
        {
            if (worldObject is PlayerInstance player)
            {
                player.UnSubscribeMonster((L2Monster) ActiveCharacter);
            }
            if (!base.RemoveKnownObject(worldObject))
            {
                return false;
            }
            if (!(worldObject is Character))
            {
                return true;
            }
            // Notify the MonsterInstance AI with EVT_FORGET_OBJECT
            ActiveCharacter.AI.NotifyEvent(CtrlEvent.EvtForgetObject, worldObject);
            return true;
        }
    }
}