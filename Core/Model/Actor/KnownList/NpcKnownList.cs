﻿using Core.Model.Npc;
using Core.Model.Npc.Type;
using Core.Model.World;

namespace Core.Model.Actor.KnownList
{
    public class NpcKnownList: CharacterKnownList
    {
        public override Character ActiveCharacter => (NpcInstance) ActiveObject;

        public NpcKnownList(WorldObject activeObject) : base(activeObject)
        {
        }
        
        public override bool AddKnownObject(WorldObject worldObject)
        {
            if (!base.AddKnownObject(worldObject))
            {
                return false;
            }
            return true;
        }

        protected override int GetDistanceToForgetObject(WorldObject worldObject)
        {
            return 2 * GetDistanceToWatchObject(worldObject);
        }
        
        protected override int GetDistanceToWatchObject(WorldObject worldObject)
        {
            if (worldObject is L2FestivalGuide)
            {
                return 10000;
            }

            if (worldObject is Playable)
            {
                return 1500;
            }
		
            return 500;
        }
        
    }
}