﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Core.Helper;
using Core.Model.Player;
using Core.Model.World;

namespace Core.Model.Actor.KnownList
{
    public class CharacterKnownList : WorldObjectKnownList
    {
        
        private ConcurrentDictionary<int, int> _knownRelations;
        public virtual Character ActiveCharacter => (Character) ActiveObject;
        public CharacterKnownList(WorldObject activeObject) : base(activeObject)
        {
        }
        
        public override bool AddKnownObject(WorldObject worldObject)
        {
            return AddKnownObject(worldObject, null);
        }
        
        public override bool AddKnownObject(WorldObject worldObject, Character character)
        {
            if (!base.AddKnownObject(worldObject, character))
            {
                return false;
            }
            if (worldObject is PlayerInstance playerInstance)
            {
                GetKnownPlayers().TryAdd(worldObject.ObjectId, playerInstance);
                GetKnownRelations().TryAdd(worldObject.ObjectId, -1);
            }
            return true;
        }


        public override bool RemoveKnownObject(WorldObject worldObject)
        {
            if (!base.RemoveKnownObject(worldObject))
            {
                return false;
            }
		
            if (worldObject is PlayerInstance)
            {
                GetKnownPlayers().TryRemove(worldObject.ObjectId, out _);
                GetKnownRelations().TryRemove(worldObject.ObjectId, out _);
            }
            // If object is targeted by the Creature, cancel Attack or Cast
            if (worldObject == ActiveCharacter.Target.GetTarget())
            {
                ActiveCharacter.Target.RemoveTargetAsync();
            }
            return true;
        }

        public ConcurrentDictionary<int, int> GetKnownRelations()
        {
            if (_knownRelations == null)
            {
                _knownRelations = new ConcurrentDictionary<int, int>();
            }
            return _knownRelations;
        }
        
        public IEnumerable<PlayerInstance> GetKnownPlayersInRadius(long radius)
        {
            return GetKnownPlayers().Values.Where(player => CalculateRange.CheckIfInRange((int) radius, ActiveCharacter, player, true)).ToList();
        }
        
        protected override int GetDistanceToForgetObject(WorldObject worldObject)
        {
            return 0;
        }
	
        protected override int GetDistanceToWatchObject(WorldObject worldObject)
        {
            return 0;
        }
    }
}