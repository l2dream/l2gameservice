﻿using System.Linq;
using Core.AI;
using Core.Model.World;

namespace Core.Model.Actor.KnownList
{
    public class AttackableKnownList : NpcKnownList
    {
        public override Character ActiveCharacter => (Attackable) ActiveObject;

        public AttackableKnownList(WorldObject activeObject) : base(activeObject)
        {
        }

        public override bool RemoveKnownObject(WorldObject worldObject)
        {
            if (!base.RemoveKnownObject(worldObject))
            {
                return false;
            }
		
            // Remove the WorldObject from the _aggrolist of the Attackable
            if (worldObject is Character)
            {
                //ActiveCharacter.GetAggroList().remove(object);
            }
		
            // Set the Attackable Intention to AI_INTENTION_IDLE
            var known = GetKnownPlayers().Values;
		
            if (!known.Any())
            {
                ActiveCharacter.AI.SetIntention(CtrlIntention.AiIntentionIdle);
            }
            return true;
        }
    }
}