﻿using System;
using Core.Model.Npc;
using Core.Model.Npc.Type;
using Core.Model.Player;
using Core.Model.World;
using Core.Model.World.RegionData;
using Core.Model.Zones;
using L2Logger;

namespace Core.Model.Actor
{
    public sealed class CharacterZone
    {
        private readonly Character _character;
        private readonly byte[] _zones;

        public CharacterZone(Character character)
        {
            _character = character;
            _zones = new byte[ZoneId.GetZoneCount()];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="zone"></param>
        /// <returns></returns>
        public bool IsInsideZone(ZoneId zone)
        {
            return _zones[(int)zone.Id] > 0;
        }
        
        public bool IsInsidePeaceZone(PlayerInstance attacker)
        {
            return IsInsidePeaceZone(attacker, _character);
        }

        public bool IsInsidePeaceZone(WorldObject attacker, WorldObject target)
        {
            if (target == null)
            {
            	return false;
            }
            // Attack Monster on Peace Zone like L2OFF.
            if ((target is L2Merchant) || (attacker is L2Merchant))
            {
                return false;
            }
            // Attack Guard on Peace Zone like L2OFF.
            if ((target is L2Guard) || (attacker is L2Guard))
            {
                return false;
            }
            // Attack NPC on Peace Zone like L2OFF.
            if ((target is NpcInstance) || (attacker is NpcInstance))
            {
                return false;
            }
            if ((attacker is Character) && IsInsideZone(ZoneId.Peace))
            {
                return true;
            }
            if ((target is Character) && IsInsideZone(ZoneId.Peace))
            {
                return true;
            }
            return false;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="zone"></param>
        /// <param name="state"></param>
        public void SetInsideZone(ZoneId zone, bool state)
        {
            if (state)
                _zones[(int)zone.Id]++;
            else
                _zones[(int)zone.Id]--;
        }
        
        /// <summary>
        /// 
        /// </summary>
        public void RevalidateZone()
        {
            if (_character.GetWorldRegion() == null)
            {
                return;
            }
            _character.GetWorldRegion().RevalidateZones(_character);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="worldObject"></param>
        /// <param name="radius"></param>
        /// <param name="checkZ"></param>
        /// <param name="strictCheck"></param>
        /// <returns></returns>
        public bool IsInsideRadius(WorldObject worldObject, int radius, bool checkZ, bool strictCheck)
        {
            if (worldObject != null)
            {
                return IsInsideRadius(worldObject.GetX(), worldObject.GetY(), worldObject.GetZ(), radius, checkZ, strictCheck);
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="radius"></param>
        /// <param name="checkZ"></param>
        /// <param name="strictCheck"></param>
        /// <returns></returns>
        private bool IsInsideRadius(int x, int y, int z, int radius, bool checkZ, bool strictCheck)
        {
            double dx = x - _character.GetX();
            double dy = y - _character.GetY();
            double dz = z - _character.GetZ();
            if (strictCheck)
            {
                if (checkZ)
                {
                    return ((dx * dx) + (dy * dy) + (dz * dz)) < (radius * radius);
                }
                return ((dx * dx) + (dy * dy)) < (radius * radius);
            }
            if (checkZ)
            {
                return ((dx * dx) + (dy * dy) + (dz * dz)) <= (radius * radius);
            }
            return ((dx * dx) + (dy * dy)) <= (radius * radius);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsInActiveRegion()
        {
            try
            {
                WorldRegionData region = Initializer.WorldInit().GetRegion(_character.GetX(), _character.GetY());
                return (region != null) && region.IsActive;
            }
            catch (Exception)
            {
                LoggerManager.Info("Object " + _character.CharacterName + " at bad coords: (x: " + _character.GetX() + ", y: " + _character.GetY() + ", z: " + _character.GetZ() + ").");
                return false;
            }
        }
        
        public bool IsInsideRadius2D(WorldObject worldObject, int radius)
        {
            if (worldObject == null)
            {
                return false;
            }
            return IsInsideRadius2D(worldObject.GetX(), worldObject.GetY(), worldObject.GetZ(), radius);
        }
        
        public bool IsInsideRadius2D(int x, int y, int z, int radius)
        {
            return _character.CalculateDistanceSq2D(x, y, z) < (radius * radius);
        }
    }
}