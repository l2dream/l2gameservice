﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Model.CalculateStats;
using Core.Model.Player;

namespace Core.Model.Actor
{
    internal sealed class CharacterFunc
    {
        private readonly Character _character;
        
        public CharacterFunc(Character character)
        {
            _character = character;
        }
        
        public void AddStatFuncs(FunctionObject[] funcs)
        {
            List<CharacterStatId> modifiedStats = new List<CharacterStatId>();
            foreach (FunctionObject f in funcs)
            {
                modifiedStats.Add(f.StatId);
                AddStatFunc(f);
            }
            _character.BroadcastModifiedStatsAsync(modifiedStats).ConfigureAwait(false);
        }
        
        public void AddStatFunc(FunctionObject f)
        {
            if (_character is PlayerInstance playerInstance)
            {
                if (f == null)
                {
                    return;
                }
                int stat = (int)f.StatId;
                if (playerInstance.Calculators[stat] == null)
                {
                    playerInstance.Calculators[stat] = new Calculator();
                }
                // Add the Func to the calculator corresponding to the state
                playerInstance.Calculators[stat].AddFunc(f);
            }
        }
        
        public async Task RemoveStatsOwnerAsync(object owner)
        {
            List<CharacterStatId> modifiedStats = null;
            int i = 0;
            // Go through the Calculator set
            lock (_character.Calculators)
            {
                foreach (Calculator calc in _character.Calculators)
                {
                    if (calc != null)
                    {
                        // Delete all Func objects of the selected owner
                        if (modifiedStats != null)
                        {
                            modifiedStats.AddRange(calc.RemoveOwner(owner));
                        }
                        else
                        {
                            modifiedStats = calc.RemoveOwner(owner);
                        }
					
                        if (calc.Size() == 0)
                        {
                            _character.Calculators[i] = null;
                        }
                    }
                    i++;
                }
            }
            if ((owner is Effect effect) && !effect.PreventExitUpdate)
            {
                await _character.BroadcastModifiedStatsAsync(modifiedStats);
            }
        }
    }
}