﻿namespace Core.Model.Actor
{
    public class CharacterTemplate
    {
        public virtual CharacterTemplateStat Stat { get; }
        
        public CharacterTemplate(CharacterTemplateStat stat)
        {
            Stat = stat;
        }
    }
}