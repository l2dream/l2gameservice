﻿using System.Threading.Tasks;
using Core.AI;
using Core.Model.AI;
using Core.Model.Items;
using Core.Model.Items.Type;
using Core.Model.Player;
using Core.Model.Skills;
using Core.Model.World;
using L2Logger;

namespace Core.Model.Actor
{
    public class CharacterAi : AbstractAi
    {
        private readonly Character _character;
        /** The skill we are curently casting by INTENTION_CAST */
        public Skill Skill { get; set; }
        
        public CharacterAi(Character character) : base(character)
        {
            _character = character;
        }

        public NotifyAiTask NotifyAiTask(Character character, CtrlEvent ctrlEvent)
        {
            return new NotifyAiTask(character, ctrlEvent);
        }

        protected override async Task OnIntentionIdleAsync()
        {
            //LoggerManager.Info("CharacterAi: OnIntentionIdle");
            // Set the AI Intention to AI_INTENTION_IDLE
            ChangeIntention(CtrlIntention.AiIntentionIdle, null, null);
            // Init cast and attack target
            CastTarget = null;
            AttackTarget = null;
            await ClientStopMovingAsync(null);
        }

        protected override async Task OnIntentionActiveAsync()
        {
            //LoggerManager.Info("CharacterAi: OnIntentionActive");
            // Check if the Intention is not already Active
            if (GetIntention() != CtrlIntention.AiIntentionActive)
            {
                // Set the AI Intention to AI_INTENTION_ACTIVE
                ChangeIntention(CtrlIntention.AiIntentionActive, null, null);
			
                // Init cast and attack target
                CastTarget = null;
                AttackTarget = null;
			
                // Stop the actor movement server side AND client side by sending Server->Client packet StopMove/StopRotation (broadcast)
                await ClientStopMovingAsync(null);
			
                // Stop the actor auto-attack client side by sending Server->Client packet AutoAttackStop (broadcast)
                await ClientStopAutoAttackAsync();
			
                // Launch the Think Event
                await OnEvtThinkAsync();
            }
        }

        protected override void OnIntentionRest()
        {
            //LoggerManager.Info("CharacterAi: OnIntentionRest");
        }

        protected override async Task OnIntentionAttackAsync(Character target)
        {
            //LoggerManager.Info("CharacterAi: OnIntentionAttack");
            if (target == null)
            {
                await ClientActionFailedAsync();
                return;
            }
            if (GetIntention() == CtrlIntention.AiIntentionRest)
            {
                // Cancel action client side by sending Server->Client packet ActionFailed to the PlayerInstance actor
                await ClientActionFailedAsync();
                return;
            }
            
            if (_actor.Status.IsAllSkillsDisabled() || _actor.Status.IsAfraid)
            {
                // Cancel action client side by sending Server->Client packet ActionFailed to the PlayerInstance actor
                await ClientActionFailedAsync();
                return;
            }
            
            // Check if the Intention is already AI_INTENTION_ATTACK
            if (GetIntention() == CtrlIntention.AiIntentionAttack)
            {
                // Check if the AI already targets the Creature
                if (AttackTarget != target)
                {
                    // Set the AI attack target (change target)
                    AttackTarget = target;
				
                    StopFollow();
				
                    // Launch the Think Event
                    NotifyEvent(CtrlEvent.EvtThink, null);
                }
                else
                {
                    await ClientActionFailedAsync(); // else client freezes until cancel target
                }
            }
            else
            {
                // Set the Intention of this AbstractAI to AI_INTENTION_ATTACK
                ChangeIntention(CtrlIntention.AiIntentionAttack, target, null);
			
                // Set the AI attack target
                AttackTarget = target;
			
                StopFollow();

                if (_actor is PlayerInstance instance)
                {
                    await instance.PlayerMessage().SendMessageAsync("DEBUG: Hello dr3dd");
                }
			
                // Launch the Think Event
                NotifyEvent(CtrlEvent.EvtThink, null);
            }
            
        }

        protected override void OnIntentionCast(Skill skill, WorldObject target)
        {
            // Set the AI cast target
            CastTarget = ((Character) target);
            
            // Set the AI skill used by INTENTION_CAST
            Skill = skill;
            // Change the Intention of this AbstractAI to AI_INTENTION_CAST
            ChangeIntention(CtrlIntention.AiIntentionCast, skill, target);
            
            // Launch the Think Event
            NotifyEvent(CtrlEvent.EvtThink, null);
        }

        protected override async Task OnIntentionMoveToAsync(Location destination)
        {
            if (GetIntention() == CtrlIntention.AiIntentionRest)
            {
                // Cancel action client side by sending Server->Client packet ActionFailed to the PlayerInstance actor
                await ClientActionFailedAsync();
                return;
            }

            if ((_actor is PlayerInstance) &&
                (_actor.Attack().IsAttackingNow() || _actor.MagicCast().IsCastingNow()) &&
                !_actor.IsMoving)
            {
                // Cancel action client side by sending Server->Client packet ActionFailed to the PlayerInstance actor
                await ClientActionFailedAsync();
                return;
            }
		
            // Set the Intention of this AbstractAI to AI_INTENTION_MOVE_TO
            ChangeIntention(CtrlIntention.AiIntentionMoveTo, destination, null);
            
            // Stop the actor auto-attack client side by sending Server->Client packet AutoAttackStop (broadcast)
            await ClientStopAutoAttackAsync();
            
            // Abort the attack of the Creature and send Server->Client ActionFailed packet
            if (_actor is PlayerInstance playerInstance)
            {
                ItemInstance rhand = playerInstance.PlayerInventory().GetPaperdollItem(Inventory.PaperdollRhand);
                if (rhand != null && (rhand.Item is Weapon weapon && weapon.WeaponType.Id == WeaponTypeId.Bow))
                {
                    if (!playerInstance.Attack().IsAttackingNow())
                    {
                        await playerInstance.Attack().AbortAttackAsync();
                    }
                }
                else
                {
                    await playerInstance.Attack().AbortAttackAsync();
                }
            }
            else // case Npc
            {
                await _actor.Attack().AbortAttackAsync();
            }
           
            // Move the actor to Location (x,y,z) server side AND client side by sending Server->Client packet CharMoveToLocation (broadcast)
            await MoveToAsync(destination.GetX(), destination.GetY(), destination.GetZ()).ContinueWith(HandleException);
        }

        protected void HandleException(Task obj)
        {
            if (obj.IsFaulted)
            {
                LoggerManager.Error(GetType().Name + ": " + obj.Exception);
            }
        }

        protected override void OnIntentionMoveToInABoat(Location destination, Location origin)
        {
            LoggerManager.Info("CharacterAi: OnIntentionMoveToInABoat");
        }

        protected override async Task OnIntentionFollowAsync(Character target)
        {
            if (GetIntention() == CtrlIntention.AiIntentionRest)
            {
                // Cancel action client side by sending Server->Client packet ActionFailed to the PlayerInstance actor
                await ClientActionFailedAsync();
                return;
            }
            // Dead actors can`t follow
            if (_actor == target)
            {
                await ClientActionFailedAsync();
                return;
            }
            await ClientStopAutoAttackAsync();
            
            // Set the Intention of this AbstractAI to AI_INTENTION_MOVE_TO
            ChangeIntention(CtrlIntention.AiIntentionFollow, target, null);
            await StartFollowAsync(target);
        }

        protected override async Task OnIntentionPickUpAsync(WorldObject item)
        {
            LoggerManager.Info("CharacterAi: OnIntentionPickUp");
            if (GetIntention() == CtrlIntention.AiIntentionRest)
            {
                // Cancel action client side by sending Server->Client packet ActionFailed to the PlayerInstance actor
                await ClientActionFailedAsync();
                return;
            }
            if (_actor.Status.IsAllSkillsDisabled())
            {
                // Cancel action client side by sending Server->Client packet ActionFailed to the PlayerInstance actor
                await ClientActionFailedAsync();
                return;
            }
            
            if ((item is ItemInstance itemInstance) && itemInstance.ItemLocation != ItemLocation.Void)
            {
                // Cancel action client side by sending Server->Client packet ActionFailed to the PlayerInstance actor
                await ClientActionFailedAsync();
                return;
            }
            await ClientStopAutoAttackAsync();
            // Set the Intention of this AbstractAI to AI_INTENTION_PICK_UP
            ChangeIntention(CtrlIntention.AiIntentionPickUp, item, null);
            
            // Set the AI pick up target
            Target = item;
            
            if ((item.GetX() == 0) && (item.GetY() == 0))
            {
                if (_actor is PlayerInstance)
                {
                    await ClientActionFailedAsync();
                    return;
                }
                item.SetXYZ(_actor.GetX(), _actor.GetY(), _actor.GetZ() + 5);
            }
            await MoveToPawnAsync(item, 20);
        }
        
        protected bool CheckTargetLost(WorldObject target)
        {
            // check if player is fakedeath
            if (target is PlayerInstance)
            {
                PlayerInstance target2 = (PlayerInstance) target; // convert object to chara
                if (target2.Status.IsFakeDeath())
                {
                    //target2.Status.StopFakeDeath(null);
                    return false;
                }
            }
            if (target is null)
            {
                // Set the Intention of this AbstractAI to AI_INTENTION_ACTIVE
                SetIntention(CtrlIntention.AiIntentionActive);
                return true;
            }
            return false;
        }

        protected override async Task OnIntentionInteractAsync(WorldObject worldObject)
        {
            //LoggerManager.Info("CharacterAi: OnIntentionInteract");
            if (GetIntention() == CtrlIntention.AiIntentionRest)
            {
                // Cancel action client side by sending Server->Client packet ActionFailed to the PlayerInstance actor
                await ClientActionFailedAsync();
                return;
            }
		
            if (_actor.Status.IsAllSkillsDisabled())
            {
                // Cancel action client side by sending Server->Client packet ActionFailed to the PlayerInstance actor
                await ClientActionFailedAsync();
                return;
            }
		
            // Stop the actor auto-attack client side by sending Server->Client packet AutoAttackStop (broadcast)
            await ClientStopAutoAttackAsync();
		
            if (GetIntention() != CtrlIntention.AiIntentionInteract)
            {
                // Set the Intention of this AbstractAI to AI_INTENTION_INTERACT
                ChangeIntention(CtrlIntention.AiIntentionInteract, worldObject, null);
			
                // Set the AI interact target
                Target = worldObject;
			
                // Move the actor to Pawn server side AND client side by sending Server->Client packet MoveToPawn (broadcast)
                await MoveToPawnAsync(worldObject, 60);
            }
        }

        public override Task OnEvtThinkAsync()
        {
            //LoggerManager.Info("CharacterAi: OnEvtThink");
            return Task.FromResult(1);
        }

        protected override async Task OnEvtAttackedAsync(Character attacker)
        {
            //LoggerManager.Info("CharacterAi: OnEvtAttacked");
            await ClientStartAutoAttackAsync();
        }

        protected override void OnEvtAggression(Character target, int aggro)
        {
            LoggerManager.Info("CharacterAi: OnEvtAggression");
        }

        protected override void OnEvtStunned(Character attacker)
        {
            LoggerManager.Info("CharacterAi: OnEvtStunned");
        }

        protected override void OnEvtSleeping(Character attacker)
        {
            LoggerManager.Info("CharacterAi: OnEvtSleeping");
        }

        protected override void OnEvtRooted(Character attacker)
        {
            LoggerManager.Info("CharacterAi: OnEvtRooted");
        }

        protected override void OnEvtConfused(Character attacker)
        {
            LoggerManager.Info("CharacterAi: OnEvtConfused");
        }

        protected override void OnEvtMuted(Character attacker)
        {
            LoggerManager.Info("CharacterAi: OnEvtMuted");
        }

        protected override void OnEvtReadyToAct()
        {
            //LoggerManager.Info("CharacterAi: OnEvtReadyToAct");
            // Launch actions corresponding to the Event Think
            OnEvtThinkAsync();
        }

        protected override void OnEvtUserCmd(object arg0, object arg1)
        {
            LoggerManager.Info("CharacterAi: OnEvtUserCmd");
        }

        protected override async Task OnEvtArrivedAsync()
        {
            //LoggerManager.Info("CharacterAi: OnEvtArrived");
            
            // Launch an explore task if necessary
            if (GetActor() is PlayerInstance)
            {
                ((PlayerInstance) GetActor()).Zone().RevalidateZone();
            }
            else
            {
                GetActor().Zone().RevalidateZone();
            }

            if (await GetActor().Movement().MoveToNextRoutePoint())
            {
                return;
            }
		
            await ClientStoppedMovingAsync();
		
            // If the Intention was AI_INTENTION_MOVE_TO, set the Intention to AI_INTENTION_ACTIVE
            if (GetIntention() == CtrlIntention.AiIntentionMoveTo)
            {
                SetIntention(CtrlIntention.AiIntentionActive);
            }
		
            // Launch actions corresponding to the Event Think
            await OnEvtThinkAsync();
        }

        protected override void OnEvtArrivedRevalidate()
        {
            //LoggerManager.Info("CharacterAi: OnEvtArrivedRevalidate");
            OnEvtThinkAsync();
        }

        protected override async Task OnEvtArrivedBlockedAsync(Location location)
        {
            // If the Intention was AI_INTENTION_MOVE_TO, set the Intention to AI_INTENTION_ACTIVE
            if ((GetIntention() == CtrlIntention.AiIntentionMoveTo) || (GetIntention() == CtrlIntention.AiIntentionCast))
            {
                SetIntention(CtrlIntention.AiIntentionActive);
            }
            // Stop the actor movement server side AND client side by sending Server->Client packet StopMove/StopRotation (broadcast)
            await ClientStopMovingAsync(location);
            // Launch actions corresponding to the Event Think
            await OnEvtThinkAsync();
            //LoggerManager.Info("CharacterAi: OnEvtArrivedBlocked");
        }

        protected override void OnEvtForgetObject(Character worldObject)
        {
            //LoggerManager.Info("CharacterAi: OnEvtForgetObject");
        }

        protected override void OnEvtCancel()
        {
            //LoggerManager.Info("CharacterAi: OnEvtCancel");
        }

        protected override async Task OnEvtDeadAsync()
        {
            //LoggerManager.Info("CharacterAi: OnEvtDead");
            // Stop an AI Follow Task
            StopFollow();
		
            // Kill the actor client side by sending Server->Client packet AutoAttackStop, StopMove/StopRotation, Die (broadcast)
            await ClientNotifyDeadAsync();
		
            if (!(_actor is PlayerInstance))
            {
                await _actor.Movement().SetWalkingAsync();
            }
        }

        protected override void OnEvtFakeDeath()
        {
            LoggerManager.Info("CharacterAi: OnEvtFakeDeath");
        }

        protected override void OnEvtFinishCasting()
        {
            LoggerManager.Info("CharacterAi: OnEvtFinishCasting");
        }
        
        protected bool CheckTargetLostOrDead(Character target)
        {
            if ((target == null) || target.Status.IsAlikeDead())
            {
                // check if player is fakedeath
                if ((target != null) && target.Status.IsFakeDeath())
                {
                    // target.stopFakeDeath(null);
                    return false;
                }
			
                // Set the Intention of this AbstractAI to AI_INTENTION_ACTIVE
                SetIntention(CtrlIntention.AiIntentionActive);
			
                return true;
            }
            return false;
        }

        protected async Task<bool> MaybeMoveToPawnAsync(WorldObject target, int offset)
        {
            // Get the distance between the current position of the Creature and the target (x,y)
            if (target == null)
            {
                // LOGGER.warning("maybeMoveToPawn: target == NULL!");
                return false;
            }
            // skill radius -1
            if (offset < 0)
            {
                return false;
            }
            
            int offsetWithCollision = (int) (offset + _actor.Template.Stat.CollisionRadius);
            if (target is Character character)
            {
                offsetWithCollision += (int) character.Template.Stat.CollisionRadius;
            }

            if (!_actor.Zone().IsInsideRadius(target, offsetWithCollision, false, false))
            {
                Character follow = FollowTarget;
                if (follow != null)
                {
                    // if the target is too far (maybe also teleported)
                    if (!_actor.Zone().IsInsideRadius(target, 2000, false, false))
                    {
                        StopFollow();
                        SetIntention(CtrlIntention.AiIntentionIdle);
                        return true;
                    }
                    // allow larger hit range when the target is moving (check is run only once per second)
                    if (!_actor.Zone().IsInsideRadius(target, offsetWithCollision + 100, false, false))
                    {
                        return true;
                    }
                    StopFollow();
                    return false;
                }
                
                if (_actor.Status.IsMovementDisabled())
                {
                    return true;
                }
                // If not running, set the Creature movement type to run and send Server->Client packet ChangeMoveType to all others PlayerInstance
                if (!_actor.Movement().IsRunning() && !(this is PlayerAi))
                {
                    await _actor.Movement().SetRunningAsync();
                }
                StopFollow();

                if ((target is Character characterTarget))
                {
                    if (characterTarget.IsMoving)
                    {
                        offset -= 100;
                    }
                    if (offset < 5)
                    {
                        offset = 5;
                    }
                    await StartFollowAsync(characterTarget, offset);
                } else {
                    // Move the actor to Pawn server side AND client side by sending Server->Client packet MoveToPawn (broadcast)
                    await MoveToPawnAsync(target, offset);
                }
                return true;
            }
            if (FollowTarget != null)
            {
                StopFollow();
            }
            return false;
        }

        public virtual async Task DoAttackAsync(Character target)
        {
            await _character.Attack().DoAttackAsync(target);
        }
        
        public virtual async Task DoCastAsync(Skill skill)
        {
            await _character.MagicCast().DoCastAsync(skill);
        }
    }
}