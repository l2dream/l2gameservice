﻿using Core.Model.Items;

namespace Core.Model.InventoryListener.Data
{
    public interface IPaperdollListener
    {
        void NotifyEquiped(int slot, ItemInstance inst);
		
        void NotifyUnequiped(int slot, ItemInstance inst);
    }
}