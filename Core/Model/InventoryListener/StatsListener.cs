﻿using System.Threading.Tasks;
using Core.Model.Actor;
using Core.Model.InventoryListener.Data;
using Core.Model.Items;

namespace Core.Model.InventoryListener
{
    public class StatsListener : IPaperdollListener
    {
        private readonly Character _character;
        public StatsListener(Character character)
        {
            _character = character;
        }
        public void NotifyUnequiped(int slot, ItemInstance itemInstance)
        {
            if (slot == Inventory.PaperdollLRHand)
            {
                return;
            }
            Task.Run((() => _character.RemoveStatsOwnerAsync(itemInstance)));
        }

        public void NotifyEquiped(int slot, ItemInstance itemInstance)
        {
            if (slot == Inventory.PaperdollLRHand)
            {
                return;
            }
            _character.AddStatFuncs(itemInstance.GetStatFuncs(_character));
        }
    }
}