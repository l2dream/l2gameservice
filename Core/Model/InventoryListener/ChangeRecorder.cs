﻿using System.Collections.Generic;
using Core.Model.InventoryListener.Data;
using Core.Model.Items;

namespace Core.Model.InventoryListener
{
    public class ChangeRecorder : IPaperdollListener
    {
        private Inventory _inventory;
        private List<ItemInstance> _changed;

        public ChangeRecorder(Inventory inventory)
        {
            _inventory = inventory;
            _changed = new List<ItemInstance>();
            _inventory.AddPaperdollListener(this);
        }
        
        public void NotifyEquiped(int slot, ItemInstance item)
        {
            if (!_changed.Contains(item))
            {
                _changed.Add(item);
            }
        }

        public void NotifyUnequiped(int slot, ItemInstance item)
        {
            if (!_changed.Contains(item))
            {
                _changed.Add(item);
            }
        }
        
        public List<ItemInstance> GetChangedItems()
        {
            return _changed;
        }
    }
}