﻿using Core.Model.Player;

namespace Core.Model
{
    public class CharSelectInfoPackage
    {

        public string Name { get; set; }
        public int ObjectId { get; set; } = 0;
        public int CharacterId { get; set; } = 0x00030b7a;
        public long Exp { get; set; } = 0;
        public long Sp { get; set; } = 0;
        public int ClanId { get; set; } = 0;
        public int Race { get; set; } = 0;
        public int ClassId { get; set; } = 0;
        public int BaseClassId { get; set; } = 0;
        public long DeleteTimer { get; set; } = 0;
        public long LastAccess { get; set; } = 0;
        public int Face { get; set; } = 0;
        public int HairStyle { get; set; } = 0;
        public int HairColor { get; set; } = 0;
        public int Sex { get; set; } = 0;
        public int Level { get; set; } = 0;
        public double MaxHp { get; set; } = 0;
        public double CurrentHp { get; set; } = 0;
        public double CurrentMp { get; set; } = 0;
        public double MaxMp { get; set; } = 0;
        public int[,] PaperDoll { get; set; }
        public int Karma { get; set; } = 0;
        public int AugmentationId { get; set; } = 0;
        public int AccessLevel { get; set; } = 0;
        
        public CharSelectInfoPackage(int objectId, string name)
        {
            ObjectId = objectId;
            Name = name;
        }

        public int GetPaperdollObjectId(int slot)
        {
            return PaperDoll[slot,0];
        }
        
        public int GetPaperdollItemId(int slot)
        {
            return PaperDoll[slot,1];
        }
        
        public int GetEnchantEffect()
        {
            if (PaperDoll[Inventory.PaperdollRhand,2] > 0)
            {
                return PaperDoll[Inventory.PaperdollRhand,2];
            }
            return PaperDoll[Inventory.PaperdollLRHand,2];
        }
    }
}