﻿using System.Collections.Generic;
using Core.Model.Items;

namespace Core.Model.Skills
{
    public class SkillDataHolder
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public StatSet[] Sets { get; set; }
        public int CurrentLevel { get; set; }
        public IDictionary<int, Skill> Skills = new Dictionary<int, Skill>();
        public IList<Skill> CurrentSkills = new List<Skill>();
    }
}