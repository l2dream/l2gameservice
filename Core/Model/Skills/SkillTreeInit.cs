﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Model.Player;
using Core.Model.Player.Template;
using DataBase.Entities;
using DataBase.Interfaces;
using L2Logger;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Model.Skills
{
    public sealed class SkillTreeInit
    {
        private readonly Dictionary<ClassId, Dictionary<int, SkillLearn>> _skillTree;
        private readonly PlayerTemplateInit _playerTemplateInit;
        private readonly ISkillTreeRepository _skillTreeRepository;

        public SkillTreeInit()
        {
            _skillTree = new Dictionary<ClassId, Dictionary<int, SkillLearn>>();
            _playerTemplateInit = Initializer.ServiceProvider.GetService<PlayerTemplateInit>();
            _skillTreeRepository = Initializer.ServiceProvider.GetService<IUnitOfWork>().SkillTree;
            Initialize();
        }

        private void Initialize()
        {
            ClassId parentClassId = null;
            int count = 0;
            ClassId classId = null; 
            try
            {
                ICollection<PlayerTemplate> playerTemplates = _playerTemplateInit.GetAllTemplates();
                foreach (var playerTemplate in playerTemplates)
                {
                    Dictionary<int, SkillLearn> map = new Dictionary<int, SkillLearn>();
                    classId = playerTemplate.Stat.ClassId;
                    parentClassId = classId.Parent;
                   
                    var skillTreeTableList = _skillTreeRepository.GetSkillTreeListByClassId((int) classId.Id);
                    
                    foreach (SkillTreeEntity sttc in skillTreeTableList.Result)
                    {
                        map.Add(SkillInit.GetSkillHashCode(sttc.SkillId, sttc.Level), new SkillLearn(
                                sttc.SkillId,
                                sttc.Level,
                                sttc.Name,
                                sttc.Sp,
                                sttc.MinLevel,
                                0,
                                0
                            )
                        );
                    }

                    count += map.Count;
                    _skillTree.Add(classId, map);
                }
            } catch (Exception exception)
            {
                if (classId != null)
                {
                    LoggerManager.Error(exception.Message + "Error while creating skill tree (Class ID " + classId.Id + "):  " + exception);
                }
            }
            LoggerManager.Info("SkillTreeTable: Loaded " + count + " skills.");
        }
        
        public int GetMinSkillLevel(int skillId, int skillLvl)
        {
            int skillHashCode = SkillInit.GetSkillHashCode(skillId, skillLvl);
		
            // Look on all classes for this skill (takes the first one found)
            return (from map in _skillTree.Values
                where map.ContainsKey(skillHashCode)
                select map[skillHashCode].MinLevel).FirstOrDefault();
        }

        public int GetMinLevelForNewSkill(PlayerInstance playerInstance, ClassId classId)
        {
            var skills = _skillTree[classId].Values;
            int minLevel = 0;
            foreach (var sl in skills.Where(sl => 
                (sl.MinLevel > playerInstance.Stat.Level) && 
                (sl.SpCost != 0) && 
                ((minLevel == 0) || (sl.MinLevel < minLevel))))
            {
                minLevel = sl.MinLevel;
            }
            return minLevel;
        }
        
        public List<SkillLearn> GetAvailableSkills(PlayerInstance playerInstance, ClassId classId)
        {
            List<SkillLearn> result = new List<SkillLearn>();
            var skills = _skillTree[classId].Values.Where(skill => skill.MinLevel <= playerInstance.Stat.Level);
            var playerSkills = playerInstance.PlayerSkill().PlayerSkills();
            foreach (SkillLearn skill in skills)
            {
                if (skill.MinLevel <= playerInstance.Stat.Level)
                {
                    if (playerSkills.ContainsKey(skill.Id))
                    {
                        if (playerSkills[skill.Id].Level == (skill.Level - 1))
                        {
                            result.Add(skill);
                        }
                    } 
                    else if (skill.Level == 1)
                    {
                        result.Add(skill);
                    }
                }
            }
            return result;
        }
    }
}