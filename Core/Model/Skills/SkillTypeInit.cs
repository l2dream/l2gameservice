﻿using System.Collections.Generic;

namespace Core.Model.Skills
{
    public class SkillTypeInit
    {
        public IDictionary<string, SkillOpType> SkillOpTypes { get; }
        public IDictionary<string, SkillTargetType> SkillTargetTypes { get; }

        public SkillTypeInit()
        {
            SkillOpTypes = new Dictionary<string, SkillOpType>
            {
                {"OP_PASSIVE", SkillOpType.OpPassive},
                {"OP_ACTIVE", SkillOpType.OpActive},
                {"OP_TOGGLE", SkillOpType.OpToggle},
                {"OP_CHANCE", SkillOpType.OpChance}
            };
            SkillTargetTypes = new Dictionary<string, SkillTargetType>
            {
                {"TARGET_NONE", SkillTargetType.TargetNone},
                {"TARGET_SELF", SkillTargetType.TargetSelf},
                {"TARGET_ONE", SkillTargetType.TargetOne},
                {"TARGET_PARTY", SkillTargetType.TargetParty},
                {"TARGET_ALLY", SkillTargetType.TargetAlly},
                {"TARGET_CLAN", SkillTargetType.TargetClan},
                {"TARGET_PET", SkillTargetType.TargetPet},
                {"TARGET_AREA", SkillTargetType.TargetArea},
                {"TARGET_AURA", SkillTargetType.TargetAura},
                {"TARGET_CORPSE", SkillTargetType.TargetCorpse},
                {"TARGET_UNDEAD", SkillTargetType.TargetUndead},
                {"TARGET_AREA_UNDEAD", SkillTargetType.TargetAreaUndead},
                {"TARGET_MULTIFACE", SkillTargetType.TargetMultiface},
                {"TARGET_CORPSE_ALLY", SkillTargetType.TargetCorpseAlly},
                {"TARGET_CORPSE_CLAN", SkillTargetType.TargetCorpseClan},
                {"TARGET_CORPSE_PLAYER", SkillTargetType.TargetCorpsePlayer},
                {"TARGET_CORPSE_PET", SkillTargetType.TargetCorpsePet},
                {"TARGET_ITEM", SkillTargetType.TargetItem},
                {"TARGET_AREA_CORPSE_MOB", SkillTargetType.TargetCorpseMob},
                {"TARGET_CORPSE_MOB", SkillTargetType.TargetCorpseMob},
                {"TARGET_UNLOCKABLE", SkillTargetType.TargetUnlockable},
                {"TARGET_HOLY", SkillTargetType.TargetHoly},
                {"TARGET_PARTY_MEMBER", SkillTargetType.TargetPartyMember},
                {"TARGET_PARTY_OTHER", SkillTargetType.TargetPartyOther},
                {"TARGET_ENEMY_SUMMON", SkillTargetType.TargetEnemySummon},
                {"TARGET_OWNER_PET", SkillTargetType.TargetPet},
                {"TARGET_GROUND", SkillTargetType.TargetGround},
                {"TARGET_SIEGE", SkillTargetType.TargetSiege},
                {"TARGET_TYRANNOSAURUS", SkillTargetType.TargetTyrannosaurus},
                {"TARGET_AREA_AIM_CORPSE", SkillTargetType.TargetAreaAimCorpse},
                {"TARGET_CLAN_MEMBER", SkillTargetType.TargetClanMember}
            };
        }
    }
}