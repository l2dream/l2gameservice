﻿using System.Threading.Tasks;
using Core.Model.CalculateStats;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Skills.Effects
{
    internal sealed class EffectHealOverTime : Effect
    {
        public EffectHealOverTime(Env env, EffectTemplate template) : base(env, template)
        {
        }

        public override async Task<bool> OnActionTime()
        {
            double hp = Effected.Status.GetCurrentHp();
            double maxHp = Effected.Stat.GetMaxHp();
            hp += Calc();
            if (hp > maxHp)
            {
                hp = maxHp;
            }
            await Effected.Status.SetCurrentHpAsync(hp);
            StatusUpdate suhp = new StatusUpdate(Effected.ObjectId);
            suhp.AddAttribute(StatusUpdate.CurHp, (int) hp);
            await Effected.SendPacketAsync(suhp);
            return await Task.FromResult(true);
        }

        public override EffectTypeEnum GetEffectType()
        {
            return EffectTypeEnum.HealOverTime;
        }
    }
}