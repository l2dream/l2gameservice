﻿using System.Threading.Tasks;
using Core.Helper;
using Core.Model.CalculateStats;
using Helpers;

namespace Core.Model.Skills.Effects
{
    internal class EffectForce : Effect
    {
        public int forces = 0;
        private int _range = -1;
        
        public EffectForce(Env env, EffectTemplate template) : base(env, template)
        {
            forces = Skill.Level;
            _range = Skill.CastRange;
        }

        public override Task<bool> OnActionTime()
        {
            bool result = CalculateRange.CheckIfInRange(_range, Effector, Effected, true);
            return Task.FromResult<bool>(result);
        }

        public override EffectTypeEnum GetEffectType()
        {
            return EffectTypeEnum.Buff;
        }
    }
}