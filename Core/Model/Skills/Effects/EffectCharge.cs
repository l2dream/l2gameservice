﻿using System.Threading.Tasks;
using Core.Model.CalculateStats;
using Core.Model.Player;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Skills.Effects
{
    public sealed class EffectCharge : Effect
    {
        public int NumCharges { get; set; }
        public EffectCharge(Env env, EffectTemplate template) : base(env, template)
        {
            NumCharges = 1;
            if (env.Target is PlayerInstance playerInstance)
            {
                playerInstance.SendPacketAsync(new EtcStatusUpdate(playerInstance));
                SystemMessage sm = new SystemMessage(SystemMessageId.ForceIncreasedToS1);
                sm.AddNumber(NumCharges);
                playerInstance.SendPacketAsync(sm);
            }
        }
        
        public override int GetLevel()
        {
            return NumCharges;
        }

        public override Task<bool> OnActionTime()
        {
            throw new System.NotImplementedException();
        }

        public override EffectTypeEnum GetEffectType()
        {
            return EffectTypeEnum.Charge;
        }
        
        public void AddNumCharges(int i)
        {
            NumCharges += i;
        }
    }
}