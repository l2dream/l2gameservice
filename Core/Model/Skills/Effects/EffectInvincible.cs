﻿using System.Threading.Tasks;
using Core.Model.CalculateStats;

namespace Core.Model.Skills.Effects
{
    internal sealed class EffectInvincible : Effect
    {
        public EffectInvincible(Env env, EffectTemplate template) : base(env, template)
        {
        }

        public override Task<bool> OnActionTime()
        {
            throw new System.NotImplementedException();
        }

        public override EffectTypeEnum GetEffectType()
        {
            return EffectTypeEnum.Invincible;
        }
    }
}