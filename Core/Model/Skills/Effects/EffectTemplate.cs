﻿using System;
using System.Reflection;
using Core.Model.CalculateStats;
using Core.Model.CalculateStats.Conditions;
using Core.Model.CalculateStats.Functions;
using L2Logger;

namespace Core.Model.Skills.Effects
{
    public class EffectTemplate
    {
        public Condition AttachCond { get; set; }
        public Condition ApplyCond { get; set; }
        public Lambda Lambda { get; set; }
        public int Counter { get; set; }
        public int Period { get; set; } // in seconds
        public int AbnormalEffect { get; set; }
        public FuncTemplate[] FuncTemplates { get; set; }
        public ConstructorInfo Constructor { get; set; }
        public Type FuncInstance { get; set; }
        public bool ShowIcon { get; set; }
	
        public string StackType { get; set; }
        public float StackOrder { get; set; }
        public double EffectPower { get; set; } // to thandle chance
        public SkillType EffectType { get; set; } // to handle resistences etc...

        public EffectTemplate(Condition pAttachCond, Condition pApplyCond, Type func, Lambda pLambda, int pCounter,
            int pPeriod, int pAbnormalEffect, string pStackType, float pStackOrder, int pShowIcon, SkillType eType,
            double ePower)
        {
            AttachCond = pAttachCond;
            ApplyCond = pApplyCond;
            Lambda = pLambda;
            Counter = pCounter;
            Period = pPeriod;
            AbnormalEffect = pAbnormalEffect;
            StackType = pStackType;
            StackOrder = pStackOrder;
            ShowIcon = pShowIcon == 0;
            EffectType = eType;
            EffectPower = ePower;
            FuncInstance = func;
            Constructor = FuncInstance.GetConstructor(new[] {typeof(Env), typeof(EffectTemplate)});
        }
        
        
        public void Attach(FuncTemplate f)
        {
            if (FuncTemplates is null)
            {
                FuncTemplates = new FuncTemplate[]
                {
                    f
                };
            }
            else
            {
                int len = FuncTemplates.Length;
                FuncTemplate[] tmp = new FuncTemplate[len + 1];
                Array.Copy(FuncTemplates, 0, tmp, 0, len);
                tmp[len] = f;
                FuncTemplates = tmp;
            }
        }

        public Effect GetEffect(Env env)
        {
            try
            {
                if ((AttachCond != null) && !AttachCond.Test(env))
                {
                    return null;
                }

                return (Effect) Constructor.Invoke(new object[] {env, this});
            }
            catch (TargetInvocationException ex)
            {
                LoggerManager.Error(GetType().Name + " " + "Error creating new instance of Class " + FuncInstance + " Exception was: " + ex.Message); 
                throw;
            }
            catch (Exception ex)
            {
                LoggerManager.Error(GetType().Name + " " + ex.Message); 
                throw;
            }
        }
    }
}