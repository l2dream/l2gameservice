﻿using System.Threading.Tasks;
using Core.Model.CalculateStats;

namespace Core.Model.Skills.Effects
{
    internal sealed class EffectBuff : Effect
    {
        public EffectBuff(Env env, EffectTemplate template) : base(env, template)
        {
        }

        public override Task<bool> OnActionTime()
        {
            return Task.FromResult(false);
        }

        public override EffectTypeEnum GetEffectType()
        {
            return EffectTypeEnum.Buff;
        }
    }
}