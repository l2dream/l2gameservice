﻿using System.Threading.Tasks;
using Core.Model.CalculateStats;

namespace Core.Model.Skills.Effects
{
    internal sealed class EffectSilentMove : Effect
    {
        public EffectSilentMove(Env env, EffectTemplate template) : base(env, template)
        {
        }

        public override Task<bool> OnActionTime()
        {
            throw new System.NotImplementedException();
        }

        public override EffectTypeEnum GetEffectType()
        {
            return EffectTypeEnum.SilentMove;
        }
    }
}