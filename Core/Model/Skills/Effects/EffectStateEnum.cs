﻿namespace Core.Model.Skills.Effects
{
    public enum EffectStateEnum
    {
        Created,
        Acting,
        Finishing
    }
}