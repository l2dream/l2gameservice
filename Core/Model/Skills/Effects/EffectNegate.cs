﻿using System.Threading.Tasks;
using Core.Model.CalculateStats;
using Core.Model.Player;

namespace Core.Model.Skills.Effects
{
    internal sealed class EffectNegate : Effect
    {
        public EffectNegate(Env env, EffectTemplate template) : base(env, template)
        {
        }

        public override Task<bool> OnActionTime()
        {
            throw new System.NotImplementedException();
        }

        public override EffectTypeEnum GetEffectType()
        {
            return EffectTypeEnum.Negate;
        }
    }
}