﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using Core.Model.Actor;
using Core.Model.CalculateStats;
using Core.Model.CalculateStats.Conditions;
using Core.Model.CalculateStats.Functions;
using Core.Model.Items;
using Core.Model.Items.Type;
using Core.Model.Skills.Effects;
using L2Logger;
using Condition = Core.Model.CalculateStats.Conditions.Condition;

namespace Core.Model.Skills
{
    public sealed class SkillInit : ItemSkillAbstract
    {
        private readonly List<SkillDataHolder> _skills;
        private readonly Dictionary<int, Skill> _allSkills;
        private readonly List<Skill> _skillsInFile;
        private readonly SkillTypeInit _skillTypeInit;

        public SkillInit()
        {
            _skills = new List<SkillDataHolder>();
            _allSkills = new Dictionary<int, Skill>();
            _skillTypeInit = new SkillTypeInit();
            _skillsInFile = new List<Skill>();
            InitEffects();
            Initialize();

            if (_skillsInFile.Count > 0)
            {
                foreach (Skill skill in _skillsInFile)
                {
                    _allSkills.Add(GetSkillHashCode(skill.Id, skill.Level), skill);
                }
                LoggerManager.Info($"Loaded #{_allSkills.Count} Skills .");
            }
        }

        private void InitEffects()
        {
            _initEffects.Add("EffectBattleForce", typeof(EffectBattleForce));
            _initEffects.Add("EffectBestowSkill", typeof(EffectBestowSkill));
            _initEffects.Add("EffectBetray", typeof(EffectBetray));
            _initEffects.Add("EffectBigHead", typeof(EffectBigHead));
            _initEffects.Add("EffectBlockBuff", typeof(EffectBlockBuff));
            _initEffects.Add("EffectBluff", typeof(EffectBluff));
            _initEffects.Add("EffectBuff", typeof(EffectBuff));
            _initEffects.Add("EffectCancel", typeof(EffectCancel));
            _initEffects.Add("EffectChameleonRest", typeof(EffectChameleonRest));
            _initEffects.Add("EffectCharge", typeof(EffectCharge));
            _initEffects.Add("EffectCharmOfLuck", typeof(EffectCharmOfLuck));
            _initEffects.Add("EffectClanGate", typeof(EffectClanGate));
            _initEffects.Add("EffectCombatPointHealOverTime", typeof(EffectCombatPointHealOverTime));
            _initEffects.Add("EffectConfuseMob", typeof(EffectConfuseMob));
            _initEffects.Add("EffectConfusion", typeof(EffectConfusion));
            _initEffects.Add("EffectDamOverTime", typeof(EffectDamOverTime));
            _initEffects.Add("EffectDebuff", typeof(EffectDebuff));
            _initEffects.Add("EffectDeflectBuff", typeof(EffectDeflectBuff));
            _initEffects.Add("EffectFakeDeath", typeof(EffectFakeDeath));
            _initEffects.Add("EffectFear", typeof(EffectFear));
            _initEffects.Add("EffectForce", typeof(EffectForce));
            _initEffects.Add("EffectFusion", typeof(EffectFusion));
            _initEffects.Add("EffectGrow", typeof(EffectGrow));
            _initEffects.Add("EffectHealOverTime", typeof(EffectHealOverTime));
            _initEffects.Add("EffectImmobileUntilAttacked", typeof(EffectImmobileUntilAttacked));
            _initEffects.Add("EffectImobileBuff", typeof(EffectImobileBuff));
            _initEffects.Add("EffectImobilePetBuff", typeof(EffectImobilePetBuff));
            _initEffects.Add("EffectInterrupt", typeof(EffectInterrupt));
            _initEffects.Add("EffectInvincible", typeof(EffectInvincible));
            _initEffects.Add("EffectManaDamOverTime", typeof(EffectManaDamOverTime));
            _initEffects.Add("EffectManaHealOverTime", typeof(EffectManaHealOverTime));
            _initEffects.Add("EffectMeditation", typeof(EffectMeditation));
            _initEffects.Add("EffectMpConsumePerLevel", typeof(EffectMpConsumePerLevel));
            _initEffects.Add("EffectMute", typeof(EffectMute));
            _initEffects.Add("EffectNegate", typeof(EffectNegate));
            _initEffects.Add("EffectNoblesseBless", typeof(EffectNoblesseBless));
            _initEffects.Add("EffectParalyze", typeof(EffectParalyze));
            _initEffects.Add("EffectPetrification", typeof(EffectPetrification));
            _initEffects.Add("EffectPhoenixBless", typeof(EffectPhoenixBless));
            _initEffects.Add("EffectProtectionBlessing", typeof(EffectProtectionBlessing));
            _initEffects.Add("EffectPsychicalMute", typeof(EffectPsychicalMute));
            _initEffects.Add("EffectRelax", typeof(EffectRelax));
            _initEffects.Add("EffectRemoveTarget", typeof(EffectRemoveTarget));
            _initEffects.Add("EffectRoot", typeof(EffectRoot));
            _initEffects.Add("EffectSeed", typeof(EffectSeed));
            _initEffects.Add("EffectSignet", typeof(EffectSignet));
            _initEffects.Add("EffectSignetAntiSummon", typeof(EffectSignetAntiSummon));
            _initEffects.Add("EffectSignetMDam", typeof(EffectSignetMDam));
            _initEffects.Add("EffectSignetNoise", typeof(EffectSignetNoise));
            _initEffects.Add("EffectSilenceMagicPhysical", typeof(EffectSilenceMagicPhysical));
            _initEffects.Add("EffectSilentMove", typeof(EffectSilentMove));
            _initEffects.Add("EffectSleep", typeof(EffectSleep));
            _initEffects.Add("EffectSpellForce", typeof(EffectSpellForce));
            _initEffects.Add("EffectStun", typeof(EffectStun));
            _initEffects.Add("EffectStunSelf", typeof(EffectStunSelf));
            _initEffects.Add("EffectTargetMe", typeof(EffectTargetMe));
        }

        private void Initialize()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                string[] xmlFilesArray = Directory.GetFiles(@"StaticData/Xml/Stats/Skills/");
                foreach (string items in xmlFilesArray)
                {
                    doc.Load(items);
                    XmlNodeList nodes = doc.DocumentElement?.SelectNodes("/list/skill");
                    if (nodes == null)
                    {
                        continue;
                    }

                    foreach (XmlNode node in nodes)
                    {
                        _currentSkill = new SkillDataHolder();
                        XmlElement ownerElement = node.Attributes?[0].OwnerElement;

                        if (ownerElement != null && node.Attributes != null && ownerElement.Name == "skill")
                        {
                            XmlNamedNodeMap attrs = node.Attributes;
                            int skillId = int.Parse(attrs.GetNamedItem("id").Value);

                            string skillName = attrs.GetNamedItem("name").Value;
                            string skillLevels = attrs.GetNamedItem("levels").Value;
                            string skillEnchantLevels1;
                            skillEnchantLevels1 = attrs.GetNamedItem("enchantLevels1") == null? null : attrs.GetNamedItem("enchantLevels1").Value;
                            string skillEnchantLevels2;
                            skillEnchantLevels2 = attrs.GetNamedItem("enchantLevels2") == null? null : attrs.GetNamedItem("enchantLevels2").Value;
                            int lastLevel = Int32.Parse(skillLevels);

                            _currentSkill.Id = skillId;
                            _currentSkill.Name = skillName;
                            _currentSkill.Sets = new StatSet[lastLevel];

                            for (int i = 0; i < lastLevel; i++)
                            {
                                _currentSkill.Sets[i] = new StatSet();
                                _currentSkill.Sets[i].Add("skill_id", _currentSkill.Id.ToString());
                                _currentSkill.Sets[i].Add("level", (i + 1).ToString());
                                _currentSkill.Sets[i].Add("name", _currentSkill.Name);
                            }
                            
                            //parseTable
                            foreach (XmlNode innerData in node.ChildNodes)
                            {
                                if (innerData != null && innerData.Name == "table")
                                {
                                    ParseTable(innerData);
                                }
                            }
                            //parseBeanSet
                            for (int level = 1; level <= lastLevel; level++)
                            {
                                int currIterator = level - 1;
                                foreach (XmlNode innerData in node.ChildNodes)
                                {
                                    //XmlElement ownerChildElement = innerData.Attributes?[0].OwnerElement;
                                    if (innerData != null && innerData.Name == "set")
                                    {
                                        var attrsInner = innerData.Attributes;
                                        if (attrsInner?["name"] == null || attrsInner["val"] == null) continue;
                                        string value = innerData.Attributes["val"].Value;
                                        string name = innerData.Attributes["name"].Value;

                                        if (_currentSkill.Sets[currIterator].HasKey(name)) continue;
                                        ParseBeanSet(innerData, _currentSkill.Sets[currIterator], level);
                                        //_currentSkill.Sets[currIterator].Add(name, value);
                                    }
                                }
                            }
                            MakeSkills();
                            for (int level = 0; level < lastLevel; level++)
                            {
                                _currentSkill.CurrentLevel = level;
                                foreach (XmlNode innerData in node.ChildNodes)
                                {
                                    if (innerData != null && innerData.Name == "for")
                                    {
                                        ParseTemplate(innerData, _currentSkill.CurrentSkills[level]);
                                        /*
                                        foreach (XmlNode node1 in innerData.ChildNodes)
                                        {
                                            if (node1 is XmlComment)
                                                continue;
                                            ParseTemplate(node1, _currentSkill.CurrentSkills[level]);
                                        }
                                        */
                                    }
                                }
                            }
                            //_skills.Add(_currentSkill);
                        }
                        _skillsInFile.AddRange(_currentSkill.CurrentSkills);
                        _tables.Clear();
                        //_currentSkill.Skills.AddRange(_currentSkill.CurrentSkills);
                    }
                }
            } catch (Exception exception)
            {
                LoggerManager.Error(exception.Message + "Error parsing Skills templates.");
            }
        }


        private void MakeSkills()
        {
            int count = 0;
            for (int i = 0; i < _currentSkill.Sets.Length; i++)
            {
                try
                {
                    SkillType skillType = SkillType.ValueOf(_currentSkill.Sets[i].GetString("skillType"));
                    _currentSkill.CurrentSkills.Add(skillType.MakeSkill(_currentSkill.Sets[i], _skillTypeInit));
                    count++;
                }
                catch (Exception ex)
                {
                    LoggerManager.Error(GetType().Name + " " + ex.Message);
                }
            }
        }
        
        public static int GetSkillHashCode(int skillId, int skillLevel)
        {
            return (skillId * 256) + skillLevel;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="skillId"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public Skill GetSkill(int skillId, int level)
        {
            try
            {
                int newSkillId = GetSkillHashCode(skillId, level);
                if (_allSkills.ContainsKey(newSkillId))
                {
                    return _allSkills[newSkillId];
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Error(ex.Message);
                throw;
            }
            return _allSkills.FirstOrDefault().Value;
        }
    }
}