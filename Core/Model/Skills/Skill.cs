﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Model.Actor;
using Core.Model.CalculateStats;
using Core.Model.CalculateStats.Conditions;
using Core.Model.CalculateStats.Functions;
using Core.Model.Items;
using Core.Model.Player;
using Core.Model.Skills.Effects;
using Core.Model.World;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Skills
{
    public abstract class Skill
    {
        
        public int Id { get; }
        public int DisplayId { get; }
        public string Name { get; }
        public bool IsChance => OperateType == SkillOpType.OpChance;
        public bool IsPassive => OperateType == SkillOpType.OpPassive;
        public bool IsMagic { get; }
        public bool StaticReuse { get; }
        public bool StaticHitTime { get; }
        public int Level { get; }
        public SkillOpType OperateType { get; }
        public int HitTime { get; }
        public int CoolTime { get; }
        public int ReuseDelay { get; }
        public int BuffDuration { get; }
        public bool IsPotion { get; }
        public int SkillRadius { get; }
        public int CastRange { get; }
        public int EffectRange { get; }
        public bool IsOffensive { get; }
        //Target type of the skill : SELF, PARTY, CLAN, PET... */
        public SkillTargetType TargetType { get; }
        
        private readonly FunctionObject[] _emptyFunctionSet = new FunctionObject[0];
        private Effect[] _emptyEffectSet = new Effect[0];
        
        private readonly SkillType _skillType;
        private readonly double _power;
        public double SSBoost { get; } // If true skill will have SoulShot boost (power*2)
        
        protected Condition _preCondition;
        protected Condition _itemPreCondition;
        
        public int MpConsume { get; }
        public int LevelDepend { get; }
        public int MagicLevel { get; }
        public int MinChance { get; }
        public int MaxChance { get; }
        public bool IsDance { get; }
        private bool _isDeBuff;
        public int NumCharges { get; } 
        
        public FuncTemplate[] FuncTemplates { get; set; }
        public EffectTemplate[] EffectTemplatesSelf { get; set; }
        protected EffectTemplate[] EffectTemplates { get; set; }

        protected Skill(StatSet set, SkillTypeInit skillTypeInit)
        {
            Id = set.GetInt("skill_id");
            DisplayId = set.GetInt("displayId", Id);
            Level = set.GetInt("level", 1);
            Name = set.GetString("name");
            var operateType = set.GetString("operateType");
            OperateType = skillTypeInit.SkillOpTypes[operateType];
            IsMagic = set.GetBoolean("isMagic");
            StaticHitTime = set.GetBoolean("staticHitTime");
            StaticReuse = set.GetBoolean("staticReuse");
            var targetType = set.GetString("target");
            TargetType = skillTypeInit.SkillTargetTypes[targetType];
            HitTime = set.GetInt("hitTime");
            CoolTime = set.GetInt("coolTime");
            IsPotion = set.GetBoolean("isPotion");
            CastRange = set.GetInt("castRange");
            EffectRange = set.GetInt("effectRange", -1);
            SkillRadius = set.GetInt("skillRadius", 80);
            ReuseDelay = set.GetInt("reuseDelay");
            BuffDuration = set.GetInt("buffDuration");
            IsOffensive = set.GetBoolean("offensive", IsSkillTypeOffensive());
            MpConsume = set.GetInt("mpConsume");
            IsDance = set.GetBoolean("isDance");
            SSBoost = set.GetDouble("SSBoost");
            LevelDepend = set.GetInt("lvlDepend");
            MagicLevel = set.GetInt("magicLvl", Initializer.SkillTreeService().GetMinSkillLevel(Id, Level));
            MinChance = set.GetInt("minChance", 1);
            MaxChance = set.GetInt("maxChance", 99);
            NumCharges = set.GetInt("num_charges");
            _power = set.GetDouble("power");
            _skillType = SkillType.ValueOf(set.GetString("skillType"));
            _isDeBuff = set.GetBoolean("isDebuff");
        }

        private bool IsSkillTypeOffensive()
        {
            if (_skillType == SkillType.PDam)
                return true;
            if (_skillType == SkillType.MDam)
                return true;
            if (_skillType == SkillType.CpDam)
                return true;
            if (_skillType == SkillType.Dot)
                return true;
            if (_skillType == SkillType.Bleed)
                return true;
            if (_skillType == SkillType.Poison)
                return true;
            if (_skillType == SkillType.AggDamage)
                return true;
            if (_skillType == SkillType.DeBuff)
                return true;
            if (_skillType == SkillType.AggDeBuff)
                return true;
            if (_skillType == SkillType.Stun)
                return true;
            if (_skillType == SkillType.Root)
                return true;
            if (_skillType == SkillType.Confusion)
                return true;
            if (_skillType == SkillType.Erase)
                return true;
            if (_skillType == SkillType.Blow)
                return true;
            if (_skillType == SkillType.Fear)
                return true;
            if (_skillType == SkillType.Drain)
                return true;
            if (_skillType == SkillType.Sleep)
                return true;
            if (_skillType == SkillType.ChargeDam)
                return true;
            if (_skillType == SkillType.ConfuseMobOnly)
                return true;
            if (_skillType == SkillType.DeathLink)
                return true;
            if (_skillType == SkillType.DetectWeakness)
                return true;
            if (_skillType == SkillType.ManaDam)
                return true;
            if (_skillType == SkillType.MDot)
                return true;
            if (_skillType == SkillType.Mute)
                return true;
            if (_skillType == SkillType.SoulShot)
                return true;
            if (_skillType == SkillType.SpiritShot)
                return true;
            if (_skillType == SkillType.Spoil)
                return true;
            if (_skillType == SkillType.Weakness)
                return true;
            if (_skillType == SkillType.ManaByLevel)
                return true;
            if (_skillType == SkillType.Sweep)
                return true;
            if (_skillType == SkillType.Paralyze)
                return true;
            if (_skillType == SkillType.DrainSoul)
                return true;
            if (_skillType == SkillType.AggReduce)
                return true;
            if (_skillType == SkillType.Cancel)
                return true;
            if (_skillType == SkillType.MageBane)
                return true;
            if (_skillType == SkillType.WarriorBane)
                return true;
            if (_skillType == SkillType.AggRemove)
                return true;
            if (_skillType == SkillType.AggReduceChar)
                return true;
            if (_skillType == SkillType.FatalCounter)
                return true;
            if (_skillType == SkillType.Betray)
                return true;
            if (_skillType == SkillType.DeluxeKeyUnlock)
                return true;
            if (_skillType == SkillType.Sow)
                return true;
            return false;
        }

        public abstract void UseSkill(Character caster, WorldObject[] targets);
        
        public WorldObject GetFirstOfTargetList(Character character)
        {
            WorldObject[] targets;
            targets = GetTargetList(character, true);
            if ((targets is null) || (targets.Length == 0))
            {
                return null;
            }
            return targets.FirstOrDefault();
        }

        public WorldObject[] GetTargetList(Character character, bool onlyFirst, Character target)
        {
            List<Character> targetList = new List<Character>();
            if (IsPotion)
            {
                return new Character[]
                {
                    character
                };
            }

            switch (TargetType)
            {
                case SkillTargetType.TargetNone:
                    break;
                case SkillTargetType.TargetSelf:
                case SkillTargetType.TargetGround:
                    return new Character[]
                    {
                        character
                    };
                case SkillTargetType.TargetOne:
                    bool canTargetSelf = _skillType == SkillType.Buff ||
                                         _skillType == SkillType.Heal ||
                                         _skillType == SkillType.Hot;

                    // If a target is found, return it in a table else send a system message TARGET_IS_INCORRECT
                    return new Character[]
                    {
                        target
                    };
                case SkillTargetType.TargetParty:
                    break;
                case SkillTargetType.TargetAlly:
                    break;
                case SkillTargetType.TargetClan:
                    break;
                case SkillTargetType.TargetPet:
                    break;
                case SkillTargetType.TargetArea:
                    break;
                case SkillTargetType.TargetAura:
                    break;
                case SkillTargetType.TargetCorpse:
                    break;
                case SkillTargetType.TargetUndead:
                    break;
                case SkillTargetType.TargetAreaUndead:
                    break;
                case SkillTargetType.TargetMultiface:
                    break;
                case SkillTargetType.TargetCorpseAlly:
                    break;
                case SkillTargetType.TargetCorpseClan:
                    break;
                case SkillTargetType.TargetCorpsePlayer:
                    break;
                case SkillTargetType.TargetCorpsePet:
                    break;
                case SkillTargetType.TargetItem:
                    break;
                case SkillTargetType.TargetAreaCorpseMob:
                    break;
                case SkillTargetType.TargetCorpseMob:
                    break;
                case SkillTargetType.TargetUnlockable:
                    break;
                case SkillTargetType.TargetHoly:
                    break;
                case SkillTargetType.TargetPartyMember:
                    break;
                case SkillTargetType.TargetPartyOther:
                    break;
                case SkillTargetType.TargetEnemySummon:
                    break;
                case SkillTargetType.TargetOwnerPet:
                    break;
                case SkillTargetType.TargetSiege:
                    break;
                case SkillTargetType.TargetTyrannosaurus:
                    break;
                case SkillTargetType.TargetAreaAimCorpse:
                    break;
                case SkillTargetType.TargetClanMember:
                    break;
                default:
                    return null;
            }
            return new Character[]
            {
                target
            }; 
        }

        public FunctionObject[] GetStatFunctions(Effect effect, Character character)
        {
            if (!(character is PlayerInstance) && !(character is Attackable))
            {
                return _emptyFunctionSet;
            }
		
            if (FuncTemplates == null)
            {
                return _emptyFunctionSet;
            }
            
            List<FunctionObject> funcs = new List<FunctionObject>();
            foreach (FuncTemplate t in FuncTemplates)
            {
                var env = new Env
                {
                    Player = character, 
                    Skill = this
                };
                FunctionObject f = t.GetFunc(env, this); // skill is owner
                if (f != null)
                {
                    funcs.Add(f);
                }
            }
            if (!funcs.Any())
            {
                return _emptyFunctionSet;
            }
            return funcs.ToArray();
        }

        public WorldObject[] GetTargetList(Character character, bool onlyFirst)
        {
            // Init to null the target of the skill
            Character target = null;
            // Get the L2Objcet targeted by the user of the skill at this moment
            WorldObject objTarget = character.Target.GetTarget();
            if (objTarget is Character)
            {
                target = (Character) objTarget;
                // If the WorldObject targeted is a Creature, it becomes the Creature target
            }
            return GetTargetList(character, onlyFirst, target);
        }
        
        public SkillType GetSkillType()
        {
            return _skillType;
        }

        public void Attach(FuncTemplate f)
        {
            if (FuncTemplates == null)
            {
                FuncTemplates = new FuncTemplate[]
                {
                    f
                };
            }
            else
            {
                int len = FuncTemplates.Length;
                FuncTemplate[] tmp = new FuncTemplate[len + 1];
                Array.Copy( FuncTemplates, 0, tmp, 0, len);
                tmp[len] = f;
                FuncTemplates = tmp;
            }
        }
        
        public void Attach(EffectTemplate effect)
        {
            if (EffectTemplates == null)
            {
                EffectTemplates = new EffectTemplate[]
                {
                    effect
                };
            }
            else
            {
                int len = EffectTemplates.Length;
                EffectTemplate[] tmp = new EffectTemplate[len + 1];
                Array.Copy(EffectTemplates, 0, tmp, 0, len);
                tmp[len] = effect;
                EffectTemplates = tmp;
            }
        }
        
        public void AttachSelf(EffectTemplate effect)
        {
            if (EffectTemplatesSelf == null)
            {
                EffectTemplatesSelf = new EffectTemplate[]
                {
                    effect
                };
            }
            else
            {
                int len = EffectTemplatesSelf.Length;
                EffectTemplate[] tmp = new EffectTemplate[len + 1];
                Array.Copy(EffectTemplatesSelf, 0, tmp, 0, len);
                tmp[len] = effect;
                EffectTemplatesSelf = tmp;
            }
        }
        
        public double GetPower()
        {
            return _power;
        }
        
        public double GetPower(Character character)
        {
            if ((_skillType == SkillType.FatalCounter) && (character != null))
            {
                return _power * 3.5 * (1 - (character.Status.GetCurrentHp() / character.Stat.GetMaxHp()));
            }
            return _power;
        }
        
        public bool HasEffectWhileCasting()
        {
            return _skillType == SkillType.SignetCastTime;
        }
        
        public bool IsToggle()
        {
            return OperateType == SkillOpType.OpToggle;
        }
        
        public bool IsDeBuff()
        {
            bool typeDeBuff = _skillType == SkillType.AggDeBuff || _skillType == SkillType.DeBuff ||
                              _skillType == SkillType.Stun || _skillType == SkillType.Bleed ||
                              _skillType == SkillType.Confusion || _skillType == SkillType.Fear ||
                              _skillType == SkillType.Paralyze || _skillType == SkillType.Sleep ||
                              _skillType == SkillType.Sleep || _skillType == SkillType.Root ||
                              _skillType == SkillType.Poison || _skillType == SkillType.Mute ||
                              _skillType == SkillType.Weakness;
            return _isDeBuff || typeDeBuff;
        }

        public Effect[] GetEffects(Character effector, Character effected, bool ss = false, bool sps = false, bool bss = false)
        {
            if (EffectTemplates == null)
            {
                return _emptyEffectSet;
            }

            List<Effect> effects = new List<Effect>();
            bool skillMastery = !IsToggle() && Formulas.CalcSkillMastery(effector);

            Env env = new Env();
            env.Player = effector;
            env.Target = effected;
            env.Skill = this;
            env.skillMastery = skillMastery;
            foreach (EffectTemplate et in EffectTemplates)
            {
                bool success = true;
                if (et.EffectPower > -1)
                {
                    success = Formulas.CalcEffectSuccess(effector, effected, et, this, ss, sps, bss);
                }
			
                if (success)
                {
                    Effect e = et.GetEffect(env);
                    if (e != null)
                    {
                        effects.Add(e);
                    }
                }
            }
            return effects.ToArray();
        }

        public IList<Effect> GetEffectsSelf(Character effector)
        {
            if (IsPassive)
            {
                return _emptyEffectSet;
            }
            if (EffectTemplatesSelf == null)
            {
                return _emptyEffectSet;
            }

            if (effector is PlayerInstance playerInstance)
            {
                List<Effect> effects = new List<Effect>();
                Env env = new Env {Player = playerInstance, Target = playerInstance, Skill = this};
                foreach (EffectTemplate et in EffectTemplatesSelf)
                {
                    Effect e = et.GetEffect(env);
                    if (e != null)
                    {
                        // Implements effect charge
                        if (e.GetEffectType() == EffectTypeEnum.Charge)
                        {
                            env.Skill = Initializer.SkillService().GetSkill(8, playerInstance.PlayerSkill().GetSkillLevel(8));
                            EffectCharge effect = (EffectCharge) env.Target.Effects.GetFirstEffect(EffectTypeEnum.Charge);
                            
                            if (effect != null)
                            {
                                int effectcharge = effect.GetLevel();
                                if (effectcharge < NumCharges)
                                {
                                    effectcharge++;
                                    effect.AddNumCharges(effectcharge);
                                    if (env.Target is PlayerInstance)
                                    {
                                        env.Target.SendPacketAsync(new EtcStatusUpdate((PlayerInstance) env.Target));
                                        SystemMessage sm = new SystemMessage(SystemMessageId.ForceIncreasedToS1);
                                        sm.AddNumber(effectcharge);
                                        env.Target.SendPacketAsync(sm);
                                    }
                                }
                            }
                            else
                            {
                                effects.Add(e);
                            }
                        }
                    }
                }
                if (!effects.Any())
                {
                    return _emptyEffectSet;
                }
                return effects;
            }
            return null;
        }
        
        public bool UseSoulShot()
        {
            return (_skillType == SkillType.PDam) || (_skillType == SkillType.Stun) || (_skillType == SkillType.ChargeDam) || (_skillType == SkillType.Blow);
        }
        
        public bool UseSpiritShot()
        {
            return IsMagic;
        }
        
        public override string ToString()
        {
            return "" + Name + "[id=" + Id + ",lvl=" + Level + "]";
        }
    }
}