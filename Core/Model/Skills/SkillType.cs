﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Model.Items;
using Core.Model.Skills.Handlers;
using L2Logger;

namespace Core.Model.Skills
{
    public sealed class SkillType
    {
        // Damage
        public static readonly SkillType PDam = new SkillType("PDAM", SkillTypeEnum.PDam);
        public static readonly SkillType MDam = new SkillType("MDAM", SkillTypeEnum.MDam);
        public static readonly SkillType CpDam = new SkillType("CPDAM", SkillTypeEnum.CpDam);
        public static readonly SkillType ManaDam = new SkillType("MANADAM", SkillTypeEnum.ManaDam);
        public static readonly SkillType Dot = new SkillType("DOT", SkillTypeEnum.Dot);
        public static readonly SkillType MDot = new SkillType("MDOT", SkillTypeEnum.MDot);
        public static readonly SkillType DrainSoul = new SkillType("DRAIN_SOUL", SkillTypeEnum.DrainSoul);
        public static readonly SkillType Drain = new SkillType("DRAIN", SkillTypeEnum.Drain, typeof(SkillDrain));
        public static readonly SkillType DeathLink = new SkillType("DEATHLINK", SkillTypeEnum.DeathLink);
        public static readonly SkillType FatalCounter = new SkillType("FATALCOUNTER", SkillTypeEnum.FatalCounter);
        public static readonly SkillType Blow = new SkillType("BLOW", SkillTypeEnum.Blow);
        
        // Disablers
        public static readonly SkillType Bleed = new SkillType("BLEED", SkillTypeEnum.Bleed);
        public static readonly SkillType Poison = new SkillType("POISON", SkillTypeEnum.Poison);
        public static readonly SkillType Stun = new SkillType("STUN", SkillTypeEnum.Stun);
        public static readonly SkillType Root = new SkillType("ROOT", SkillTypeEnum.Root);
        public static readonly SkillType Confusion = new SkillType("CONFUSION", SkillTypeEnum.Confusion);
        public static readonly SkillType Fear = new SkillType("FEAR", SkillTypeEnum.Fear);
        public static readonly SkillType Sleep = new SkillType("SLEEP", SkillTypeEnum.Sleep);
        public static readonly SkillType ConfuseMobOnly = new SkillType("CONFUSE_MOB_ONLY", SkillTypeEnum.ConfuseMobOnly);
        public static readonly SkillType Mute = new SkillType("MUTE", SkillTypeEnum.Mute);
        public static readonly SkillType Paralyze = new SkillType("PARALYZE", SkillTypeEnum.Paralyze);
        public static readonly SkillType Weakness = new SkillType("WEAKNESS", SkillTypeEnum.Weakness);
        
        // hp, mp, cp
        public static readonly SkillType Heal = new SkillType("HEAL", SkillTypeEnum.Heal);
        public static readonly SkillType Hot = new SkillType("HOT", SkillTypeEnum.Hot);
        public static readonly SkillType BalanceLife = new SkillType("BALANCE_LIFE", SkillTypeEnum.BalanceLife);
        public static readonly SkillType HealPercent = new SkillType("HEAL_PERCENT", SkillTypeEnum.HealPercent);
        public static readonly SkillType HealStatic = new SkillType("HEAL_STATIC", SkillTypeEnum.HealStatic);
        public static readonly SkillType CombatPointHeal = new SkillType("COMBATPOINTHEAL", SkillTypeEnum.CombatPointHeal);
        public static readonly SkillType CombatPointPercentHeal = new SkillType("COMBATPOINTPERCENTHEAL", SkillTypeEnum.CombatPointPercentHeal);
        public static readonly SkillType CpHot = new SkillType("CPHOT", SkillTypeEnum.CpHot);
        public static readonly SkillType ManaHeal = new SkillType("MANAHEAL", SkillTypeEnum.ManaHeal);
        public static readonly SkillType ManaByLevel = new SkillType("MANA_BY_LEVEL", SkillTypeEnum.ManaByLevel);
        public static readonly SkillType ManaHealPercent = new SkillType("MANAHEAL_PERCENT", SkillTypeEnum.ManaHealPercent);
        public static readonly SkillType ManaRecharge = new SkillType("MANARECHARGE", SkillTypeEnum.ManaRecharge);
        public static readonly SkillType MpHot = new SkillType("MPHOT", SkillTypeEnum.MpHot);
        
        // Aggro
        public static readonly SkillType AggDamage = new SkillType("AGGDAMAGE", SkillTypeEnum.AggDamage);
        public static readonly SkillType AggReduce = new SkillType("AGGREDUCE", SkillTypeEnum.AggReduce);
        public static readonly SkillType AggRemove = new SkillType("AGGREMOVE", SkillTypeEnum.AggRemove);
        public static readonly SkillType AggReduceChar = new SkillType("AGGREDUCE_CHAR", SkillTypeEnum.AggReduceChar);
        public static readonly SkillType AggDeBuff = new SkillType("AGGDEBUFF", SkillTypeEnum.AggDeBuff);
        
        // Fishing
        public static readonly SkillType Fishing = new SkillType("FISHING", SkillTypeEnum.Fishing);
        public static readonly SkillType Pumping = new SkillType("PUMPING", SkillTypeEnum.Pumping);
        public static readonly SkillType Reeling = new SkillType("REELING", SkillTypeEnum.Reeling);
        
        // MISC
        public static readonly SkillType Unlock = new SkillType("UNLOCK", SkillTypeEnum.Unlock);
        public static readonly SkillType EnchantArmor = new SkillType("ENCHANT_ARMOR", SkillTypeEnum.EnchantArmor);
        public static readonly SkillType EnchantWeapon = new SkillType("ENCHANT_WEAPON", SkillTypeEnum.EnchantWeapon);
        public static readonly SkillType SoulShot = new SkillType("SOULSHOT", SkillTypeEnum.SoulShot);
        public static readonly SkillType SpiritShot = new SkillType("SPIRITSHOT", SkillTypeEnum.SpiritShot);
        public static readonly SkillType SiegeFlag = new SkillType("SIEGEFLAG", SkillTypeEnum.SiegeFlag);
        public static readonly SkillType TakeCastle = new SkillType("TAKECASTLE", SkillTypeEnum.TakeCastle);
        public static readonly SkillType DeluxeKeyUnlock = new SkillType("DELUXE_KEY_UNLOCK", SkillTypeEnum.DeluxeKeyUnlock);
        public static readonly SkillType Sow = new SkillType("SOW", SkillTypeEnum.Sow);
        public static readonly SkillType Harvest = new SkillType("HARVEST", SkillTypeEnum.Harvest);
        public static readonly SkillType GetPlayer = new SkillType("GET_PLAYER", SkillTypeEnum.GetPlayer);
        
        // Creation
        public static readonly SkillType CommonCraft = new SkillType("COMMON_CRAFT", SkillTypeEnum.CommonCraft);
        public static readonly SkillType DwarvenCraft = new SkillType("DWARVEN_CRAFT", SkillTypeEnum.DwarvenCraft);
        public static readonly SkillType CreateItem = new SkillType("CREATE_ITEM", SkillTypeEnum.CreateItem, typeof(SkillCreateItem));
        public static readonly SkillType SummonTreasureKey = new SkillType("SUMMON_TREASURE_KEY", SkillTypeEnum.SummonTreasureKey);
        
        // Summons
        public static readonly SkillType Summon = new SkillType("SUMMON", SkillTypeEnum.Summon, typeof(SkillSummon));
        public static readonly SkillType FeedPet = new SkillType("FEED_PET", SkillTypeEnum.FeedPet);
        public static readonly SkillType DeathLinkPet = new SkillType("DEATHLINK_PET", SkillTypeEnum.DeathLinkPet);
        public static readonly SkillType StrSiegeAssault = new SkillType("STRSIEGEASSAULT", SkillTypeEnum.StrSiegeAssault);
        public static readonly SkillType Erase = new SkillType("ERASE", SkillTypeEnum.Erase);
        public static readonly SkillType Betray = new SkillType("BETRAY", SkillTypeEnum.Betray);
        
        // Cancel
        public static readonly SkillType Cancel = new SkillType("CANCEL", SkillTypeEnum.Cancel);
        public static readonly SkillType MageBane = new SkillType("MAGE_BANE", SkillTypeEnum.MageBane);
        public static readonly SkillType WarriorBane = new SkillType("WARRIOR_BANE", SkillTypeEnum.WarriorBane);
        public static readonly SkillType Negate = new SkillType("NEGATE", SkillTypeEnum.Negate);
        
        //Other
        public static readonly SkillType Buff = new SkillType("BUFF", SkillTypeEnum.Buff);
        public static readonly SkillType DeBuff = new SkillType("DEBUFF", SkillTypeEnum.DeBuff);
        public static readonly SkillType Passive = new SkillType("PASSIVE", SkillTypeEnum.Passive);
        public static readonly SkillType Cont = new SkillType("CONT", SkillTypeEnum.Cont);
        public static readonly SkillType Signet = new SkillType("SIGNET", SkillTypeEnum.Signet, typeof(SkillSignet));
        public static readonly SkillType SignetCastTime = new SkillType("SIGNET_CASTTIME", SkillTypeEnum.SignetCastTime, typeof(SkillSignetCastTime));
        
        public static readonly SkillType Resurrect = new SkillType("RESURRECT", SkillTypeEnum.Resurrect);
        public static readonly SkillType Charge = new SkillType("CHARGE", SkillTypeEnum.Charge, typeof(SkillCharge));
        public static readonly SkillType ChargeEffect = new SkillType("CHARGE_EFFECT", SkillTypeEnum.ChargeEffect, typeof(SkillChargeEffect));
        public static readonly SkillType ChargeDam = new SkillType("CHARGEDAM", SkillTypeEnum.ChargeDam, typeof(SkillChargeDmg));
        public static readonly SkillType MHot = new SkillType("MHOT", SkillTypeEnum.MHot);
        public static readonly SkillType DetectWeakness = new SkillType("DETECT_WEAKNESS", SkillTypeEnum.DetectWeakness);
        public static readonly SkillType Luck = new SkillType("LUCK", SkillTypeEnum.Luck);
        public static readonly SkillType Recall = new SkillType("RECALL", SkillTypeEnum.Recall);
        public static readonly SkillType SummonFriend = new SkillType("SUMMON_FRIEND", SkillTypeEnum.SummonFriend);
        public static readonly SkillType Reflect = new SkillType("REFLECT", SkillTypeEnum.Reflect);
        public static readonly SkillType Spoil = new SkillType("SPOIL", SkillTypeEnum.Spoil);
        public static readonly SkillType Sweep = new SkillType("SWEEP", SkillTypeEnum.Sweep);
        public static readonly SkillType FakeDeath = new SkillType("FAKE_DEATH", SkillTypeEnum.FakeDeath);
        public static readonly SkillType UnBleed = new SkillType("UNBLEED", SkillTypeEnum.UnBleed);
        public static readonly SkillType UnPoison = new SkillType("UNPOISON", SkillTypeEnum.UnPoison);
        public static readonly SkillType UndeadDefense = new SkillType("UNDEAD_DEFENSE", SkillTypeEnum.UndeadDefense);
        public static readonly SkillType Seed = new SkillType("SEED", SkillTypeEnum.Seed, typeof(SkillSeed));
        public static readonly SkillType BeastFeed = new SkillType("BEAST_FEED", SkillTypeEnum.BeastFeed);
        public static readonly SkillType ForceBuff = new SkillType("FORCE_BUFF", SkillTypeEnum.ForceBuff);
        public static readonly SkillType ClanGate = new SkillType("CLAN_GATE", SkillTypeEnum.ClanGate);
        public static readonly SkillType GiveSp = new SkillType("GIVE_SP", SkillTypeEnum.GiveSp);
        public static readonly SkillType CoreDone = new SkillType("COREDONE", SkillTypeEnum.CoreDone);
        public static readonly SkillType ZakenPlayer = new SkillType("ZAKENPLAYER", SkillTypeEnum.ZakenPlayer);
        public static readonly SkillType ZakenSelf = new SkillType("ZAKENSELF", SkillTypeEnum.ZakenSelf);
        public static readonly SkillType NotDone = new SkillType("NOTDONE", SkillTypeEnum.NotDone);
        
        private readonly int _nextOrdinal = 0;
        private readonly int _ordinalValue;
        private readonly Type _type;
        private readonly string _nameValue;
        private readonly SkillTypeEnum _value;

        public static IEnumerable<SkillType> Values
        {
            get
            {
                yield return PDam;
                yield return PDam;
                yield return MDam;
                yield return CpDam;
                yield return ManaDam;
                yield return Dot;
                yield return MDot;
                yield return DrainSoul;
                yield return Drain;
                yield return DeathLink;
                yield return FatalCounter;
                yield return Blow;
                yield return Bleed;
                yield return Poison;
                yield return Stun;
                yield return Root;
                yield return Confusion;
                yield return Fear;
                yield return Sleep;
                yield return ConfuseMobOnly;
                yield return Mute;
                yield return Paralyze;
                yield return Weakness;
                yield return Heal;
                yield return Hot;
                yield return BalanceLife;
                yield return HealPercent;
                yield return HealStatic;
                yield return CombatPointHeal;
                yield return CombatPointPercentHeal;
                yield return CpHot;
                yield return ManaHeal;
                yield return ManaByLevel;
                yield return ManaHealPercent;
                yield return ManaRecharge;
                yield return MpHot;
                yield return AggDamage;
                yield return AggReduce;
                yield return AggRemove;
                yield return AggReduceChar;
                yield return AggDeBuff;
                yield return Fishing;
                yield return Pumping;
                yield return Reeling;
                yield return Unlock;
                yield return EnchantArmor;
                yield return EnchantWeapon;
                yield return SoulShot;
                yield return SpiritShot;
                yield return SiegeFlag;
                yield return TakeCastle;
                yield return DeluxeKeyUnlock;
                yield return Sow;
                yield return Harvest;
                yield return GetPlayer;
                yield return CommonCraft;
                yield return DwarvenCraft;
                yield return CreateItem;
                yield return SummonTreasureKey;
                yield return Summon;
                yield return FeedPet;
                yield return DeathLinkPet;
                yield return StrSiegeAssault;
                yield return Erase;
                yield return Betray;
                yield return Cancel;
                yield return MageBane;
                yield return WarriorBane;
                yield return Negate;
                yield return Buff;
                yield return DeBuff;
                yield return Passive;
                yield return Cont;
                yield return Signet;
                yield return SignetCastTime;
                yield return Resurrect;
                yield return Charge;
                yield return ChargeEffect;
                yield return ChargeDam;
                yield return MHot;
                yield return DetectWeakness;
                yield return Luck;
                yield return Recall;
                yield return SummonFriend;
                yield return Reflect;
                yield return Spoil;
                yield return Sweep;
                yield return FakeDeath;
                yield return UnBleed;
                yield return UnPoison;
                yield return UndeadDefense;
                yield return Seed;
                yield return BeastFeed;
                yield return ForceBuff;
                yield return ClanGate;
                yield return GiveSp;
                yield return CoreDone;
                yield return ZakenPlayer;
                yield return ZakenSelf;
                yield return NotDone;
            }
        }
        

        private SkillType(string name, SkillTypeEnum skillTypeEnum)
        {
            _type = typeof(SkillDefault);
            _nameValue = name;
            _ordinalValue = _nextOrdinal++;
            _value = skillTypeEnum;
        }

        private SkillType(string name, SkillTypeEnum skillTypeEnum, Type classType)
        {
            _type = classType;
            _nameValue = name;
            _ordinalValue = _nextOrdinal++;
            _value = skillTypeEnum;
        }

        private new Type GetType()
        {
            return _type;
        }
        
        public static SkillType ValueOf(string name)
        {
            try
            {
                return Values.FirstOrDefault(v => v._nameValue == name);
            }
            catch (Exception ex)
            {
                LoggerManager.Error("SkillType:" + ex.Message);
                throw;
            }
        }

        public Skill MakeSkill(StatSet set, SkillTypeInit skillTypeInit)
        {
            SkillType skillType = ValueOf(set.GetString("skillType"));
            Object[] parameters =
            {
                set,
                skillTypeInit
            };
            Skill skill = (Skill) Activator.CreateInstance(GetType(), parameters);
            return skill;
        }
    }
}