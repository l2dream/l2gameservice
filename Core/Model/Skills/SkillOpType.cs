﻿namespace Core.Model.Skills
{
    public enum SkillOpType
    {
        OpPassive,
        OpActive,
        OpToggle,
        OpChance
    }
}