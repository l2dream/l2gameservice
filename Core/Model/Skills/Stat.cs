﻿using System.Collections.Generic;
using System.Linq;
using Core.Model.CalculateStats;

namespace Core.Model.Skills
{
    public sealed class Stat
    {
        private static int _nextOrdinal = 0;

        private static int _ordinalValue;
        
        // HP & MP
        public static readonly Stat MaxHp = new Stat("maxHp", CharacterStatId.MaxHp);
        public static readonly Stat MaxMp = new Stat("maxMp", CharacterStatId.MaxMp);
        public static readonly Stat MaxCp = new Stat("maxCp", CharacterStatId.MaxCp);
        public static readonly Stat RegenerateHpRate = new Stat("regHp", CharacterStatId.RegenerateHpRate);
        public static readonly Stat RegenerateCpRate = new Stat("regCp", CharacterStatId.RegenerateCpRate);
        public static readonly Stat RegenerateMpRate = new Stat("regMp", CharacterStatId.RegenerateMpRate);
        public static readonly Stat RechargeMpRate = new Stat("gainMp", CharacterStatId.RechargeMpRate);
        public static readonly Stat HealEffectiveness = new Stat("gainHp", CharacterStatId.HealEffectiveness);
        
        // Atk & Def
        public static readonly Stat PowerDefence = new Stat("pDef", CharacterStatId.PowerDefence);
        public static readonly Stat MagicDefence = new Stat("mDef", CharacterStatId.MagicDefence);
        public static readonly Stat PowerAttack = new Stat("pAtk", CharacterStatId.PowerAttack);
        public static readonly Stat MagicAttack = new Stat("mAtk", CharacterStatId.MagicAttack);
        public static readonly Stat PowerAttackSpeed = new Stat("pAtkSpd", CharacterStatId.PowerAttackSpeed);
        public static readonly Stat MagicAttackSpeed = new Stat("mAtkSpd", CharacterStatId.MagicAttackSpeed);
        public static readonly Stat MagicReuseRate = new Stat("mReuse", CharacterStatId.MagicReuseRate);
        public static readonly Stat ShieldDefence = new Stat("sDef", CharacterStatId.ShieldDefence);
        public static readonly Stat CriticalDamage = new Stat("cAtk", CharacterStatId.CriticalDamage);
        public static readonly Stat CriticalDamageAdd = new Stat("cAtkAdd", CharacterStatId.CriticalDamageAdd);
        
        public static readonly Stat PvpPhysicalDmg = new Stat("pvpPhysDmg", CharacterStatId.PvpPhysicalDmg);
        public static readonly Stat PvpMagicalDmg = new Stat("pvpMagicalDmg", CharacterStatId.PvpMagicalDmg);
        public static readonly Stat PvpPhysSkillDmg = new Stat("pvpPhysSkillsDmg", CharacterStatId.PvpPhysSkillDmg);
        
        // Atk & Def rates
        public static readonly Stat EvasionRate = new Stat("rEvas", CharacterStatId.EvasionRate);
        public static readonly Stat PSkillEvasion = new Stat("pSkillEvas", CharacterStatId.PSkillEvasion);
        public static readonly Stat ShieldRate = new Stat("rShld", CharacterStatId.ShieldRate);
        public static readonly Stat ShieldAngle = new Stat("shldAngle", CharacterStatId.ShieldAngle);
        public static readonly Stat CriticalRate = new Stat("rCrit", CharacterStatId.CriticalRate);
        public static readonly Stat BlowRate = new Stat("blowRate", CharacterStatId.BlowRate);
        public static readonly Stat LethalRate = new Stat("lethalRate", CharacterStatId.LethalRate);
        public static readonly Stat MCriticalRate = new Stat("mCritRate", CharacterStatId.MCriticalRate);
        public static readonly Stat ExpSpRate = new Stat("rExp", CharacterStatId.ExpSpRate);
        public static readonly Stat AttackCancel = new Stat("cancel", CharacterStatId.AttackCancel);
        
        // Accuracy and range
        public static readonly Stat AccuracyCombat = new Stat("accCombat", CharacterStatId.AccuracyCombat);
        public static readonly Stat PowerAttackRange = new Stat("pAtkRange", CharacterStatId.PowerAttackRange);
        public static readonly Stat MagicAttackRange = new Stat("mAtkRange", CharacterStatId.MagicAttackRange);
        public static readonly Stat PowerAttackAngle = new Stat("pAtkAngle", CharacterStatId.PowerAttackAngle);
        public static readonly Stat AttackCountMax = new Stat("atkCountMax", CharacterStatId.AttackCountMax);
        public static readonly Stat RunSpeed = new Stat("runSpd", CharacterStatId.RunSpeed);
        public static readonly Stat WalkSpeed = new Stat("walkSpd", CharacterStatId.WalkSpeed);
        
        // Player-only stats
        public static readonly Stat StatStr = new Stat("STR", CharacterStatId.StatStr);
        public static readonly Stat StatCon = new Stat("CON", CharacterStatId.StatCon);
        public static readonly Stat StatDex = new Stat("DEX", CharacterStatId.StatDex);
        public static readonly Stat StatInt = new Stat("INT", CharacterStatId.StatInt);
        public static readonly Stat StatWit = new Stat("WIT", CharacterStatId.StatWit);
        public static readonly Stat StatMen = new Stat("MEN", CharacterStatId.StatMen);
        
        // Special stats, share one slot in Calculator
        public static readonly Stat Breath = new Stat("breath", CharacterStatId.Breath);
        public static readonly Stat Fall = new Stat("fall", CharacterStatId.Fall);
        public static readonly Stat Aggression = new Stat("aggression", CharacterStatId.Aggression);
        public static readonly Stat Bleed = new Stat("bleed", CharacterStatId.Bleed);
        public static readonly Stat Poison = new Stat("poison", CharacterStatId.Poison);
        public static readonly Stat Stun = new Stat("stun", CharacterStatId.Stun);
        public static readonly Stat Root = new Stat("root", CharacterStatId.Root);
        public static readonly Stat Movement = new Stat("movement", CharacterStatId.Movement);
        public static readonly Stat Confusion = new Stat("confusion", CharacterStatId.Confusion);
        public static readonly Stat Sleep = new Stat("sleep", CharacterStatId.Sleep);
        public static readonly Stat Fire = new Stat("fire", CharacterStatId.Fire);
        public static readonly Stat Wind = new Stat("wind", CharacterStatId.Wind);
        public static readonly Stat Water = new Stat("water", CharacterStatId.Water);
        public static readonly Stat Earth = new Stat("earth", CharacterStatId.Earth);
        public static readonly Stat Holy = new Stat("holy", CharacterStatId.Holy);
        public static readonly Stat Dark = new Stat("dark", CharacterStatId.Dark);
        
        public static readonly Stat AggressionVuln = new Stat("aggressionVuln", CharacterStatId.AggressionVuln);
        public static readonly Stat BleedVuln = new Stat("bleedVuln", CharacterStatId.BleedVuln);
        public static readonly Stat PoisonVuln = new Stat("poisonVuln", CharacterStatId.PoisonVuln);
        public static readonly Stat StunVuln = new Stat("stunVuln", CharacterStatId.StunVuln);
        public static readonly Stat ParalyzeVuln = new Stat("paralyzeVuln", CharacterStatId.ParalyzeVuln);
        public static readonly Stat RootVuln = new Stat("rootVuln", CharacterStatId.RootVuln);
        public static readonly Stat SleepVuln = new Stat("sleepVuln", CharacterStatId.SleepVuln);
        public static readonly Stat ConfusionVuln = new Stat("confusionVuln", CharacterStatId.ConfusionVuln);
        public static readonly Stat MovementVuln = new Stat("movementVuln", CharacterStatId.MovementVuln);
        public static readonly Stat FireVuln = new Stat("fireVuln", CharacterStatId.FireVuln);
        public static readonly Stat WindVuln = new Stat("windVuln", CharacterStatId.WindVuln);
        public static readonly Stat WaterVuln = new Stat("waterVuln", CharacterStatId.WaterVuln);
        public static readonly Stat EarthVuln = new Stat("earthVuln", CharacterStatId.EarthVuln);
        public static readonly Stat HolyVuln = new Stat("holyVuln", CharacterStatId.HolyVuln);
        public static readonly Stat DarkVuln = new Stat("darkVuln", CharacterStatId.DarkVuln);
        public static readonly Stat CancelVuln = new Stat("cancelVuln", CharacterStatId.CancelVuln);
        public static readonly Stat DerangementVuln = new Stat("derangementVuln", CharacterStatId.DerangementVuln);
        public static readonly Stat DebuffVuln = new Stat("debuffVuln", CharacterStatId.DebuffVuln);
        public static readonly Stat BuffVuln = new Stat("buffVuln", CharacterStatId.BuffVuln);
        public static readonly Stat FallVuln = new Stat("fallVuln", CharacterStatId.FallVuln);
        public static readonly Stat CastInterrupt = new Stat("concentration", CharacterStatId.CastInterrupt);
        public static readonly Stat CritVuln = new Stat("critVuln", CharacterStatId.CritVuln);
        
        public static readonly Stat DebuffImmunity = new Stat("debuffImmunity", CharacterStatId.DebuffImmunity);
        
        // Shields!!!
        public static readonly Stat NoneWpnVuln = new Stat("noneWpnVuln", CharacterStatId.NoneWpnVuln);
        public static readonly Stat SwordWpnVuln = new Stat("swordWpnVuln", CharacterStatId.SwordWpnVuln);
        public static readonly Stat BluntWpnVuln = new Stat("bluntWpnVuln", CharacterStatId.BluntWpnVuln);
        public static readonly Stat DaggerWpnVuln = new Stat("daggerWpnVuln", CharacterStatId.DaggerWpnVuln);
        public static readonly Stat BowWpnVuln = new Stat("bowWpnVuln", CharacterStatId.BowWpnVuln);
        public static readonly Stat PoleWpnVuln = new Stat("poleWpnVuln", CharacterStatId.PoleWpnVuln);
        public static readonly Stat EtcWpnVuln = new Stat("etcWpnVuln", CharacterStatId.EtcWpnVuln);
        public static readonly Stat FistWpnVuln = new Stat("fistWpnVuln", CharacterStatId.FistWpnVuln);
        public static readonly Stat DualWpnVuln = new Stat("dualWpnVuln", CharacterStatId.DualWpnVuln);
        public static readonly Stat DualfistWpnVuln = new Stat("dualFistWpnVuln", CharacterStatId.DualfistWpnVuln);
        public static readonly Stat PoleTargertCount = new Stat("poleTargetCount", CharacterStatId.PoleTargertCount);
        public static readonly Stat BigswordWpnVuln = new Stat("bigSwordWpnVuln", CharacterStatId.BigswordWpnVuln);
        public static readonly Stat BigbluntWpnVuln = new Stat("bigBluntWpnVuln", CharacterStatId.BigbluntWpnVuln);
        
        public static readonly Stat ReflectDamagePercent = new Stat("reflectDam", CharacterStatId.ReflectDamagePercent);
        public static readonly Stat ReflectSkillMagic = new Stat("reflectSkillMagic", CharacterStatId.ReflectSkillMagic);
        public static readonly Stat ReflectSkillPhysic = new Stat("reflectSkillPhysic", CharacterStatId.ReflectSkillPhysic);
        public static readonly Stat VengeanceSkillPhysicalDamage = new Stat("vengeanceSkillPhysic", CharacterStatId.VengeanceSkillPhysicalDamage);
        public static readonly Stat AbsorbDamagePercent = new Stat("absorbDam", CharacterStatId.AbsorbDamagePercent);
        public static readonly Stat TransferDamagePercent = new Stat("transDam", CharacterStatId.TransferDamagePercent);
        
        public static readonly Stat MaxLoad = new Stat("maxLoad", CharacterStatId.MaxLoad);
        public static readonly Stat WeightPenalty = new Stat("weightPenalty", CharacterStatId.WeightPenalty);
        
        //patk
        public static readonly Stat PAtkPlants = new Stat("pAtk-plants", CharacterStatId.PAtkPlants);
        public static readonly Stat PAtkInsects = new Stat("pAtk-insects", CharacterStatId.PAtkInsects);
        public static readonly Stat PAtkAnimals = new Stat("pAtk-animals", CharacterStatId.PAtkAnimals);
        public static readonly Stat PAtkMonsters = new Stat("pAtk-monsters", CharacterStatId.PAtkMonsters);
        public static readonly Stat PAtkDragons = new Stat("pAtk-dragons", CharacterStatId.PAtkDragons);
        public static readonly Stat PAtkUndead = new Stat("pAtk-undead", CharacterStatId.PAtkUndead);
        public static readonly Stat PAtkAngels = new Stat("pAtk-angels", CharacterStatId.PAtkAngels);
        public static readonly Stat PAtkGiants = new Stat("pAtk-giants", CharacterStatId.PAtkGiants);
        public static readonly Stat PAtkMagicCreatures = new Stat("pAtk-magicCreature", CharacterStatId.PAtkMagicCreatures);
        
        //pdef
        public static readonly Stat PDefGiants = new Stat("pDef-giants", CharacterStatId.PDefGiants);
        public static readonly Stat PDefMagicCreatures = new Stat("pDef-magicCreature", CharacterStatId.PDefMagicCreatures);
        public static readonly Stat PDefUndead = new Stat("pDef-undead", CharacterStatId.PDefUndead);
        public static readonly Stat PDefPlants = new Stat("pDef-plants", CharacterStatId.PDefPlants);
        public static readonly Stat PDefInsects = new Stat("pDef-insects", CharacterStatId.PDefInsects);
        public static readonly Stat PDefAnimals = new Stat("pDef-animals", CharacterStatId.PDefAnimals);
        public static readonly Stat PDefMonsters = new Stat("pDef-monsters", CharacterStatId.PDefMonsters);
        public static readonly Stat PDefDragons = new Stat("pDef-dragons", CharacterStatId.PDefDragons);
        public static readonly Stat PDefAngels = new Stat("pDef-angels", CharacterStatId.PDefAngels);
        
        public static readonly Stat AtkReuse = new Stat("atkReuse", CharacterStatId.AtkReuse);
        public static readonly Stat PReuse = new Stat("pReuse", CharacterStatId.PReuse);
        
        public static readonly Stat InvLim = new Stat("inventoryLimit", CharacterStatId.InvLim);
        public static readonly Stat WhLim = new Stat("whLimit", CharacterStatId.WhLim);
        public static readonly Stat FreightLim = new Stat("FreightLimit", CharacterStatId.FreightLim);
        public static readonly Stat PSellLim = new Stat("PrivateSellLimit", CharacterStatId.PSellLim);
        public static readonly Stat PBuyLim = new Stat("PrivateBuyLimit", CharacterStatId.PBuyLim);
        public static readonly Stat RecDLim = new Stat("DwarfRecipeLimit", CharacterStatId.RecDLim);
        public static readonly Stat RecCLim = new Stat("CommonRecipeLimit", CharacterStatId.RecCLim);
        
        // C4 Stats
        public static readonly Stat PhysicalMpConsumeRate = new Stat("PhysicalMpConsumeRate", CharacterStatId.PhysicalMpConsumeRate);
        public static readonly Stat MagicalMpConsumeRate = new Stat("MagicalMpConsumeRate", CharacterStatId.MagicalMpConsumeRate);
        public static readonly Stat DanceMpConsumeRate = new Stat("DanceMpConsumeRate", CharacterStatId.DanceMpConsumeRate);
        public static readonly Stat HpConsumeRate = new Stat("HpConsumeRate", CharacterStatId.HpConsumeRate);
        public static readonly Stat MpConsume = new Stat("MpConsume", CharacterStatId.MpConsume);
        public static readonly Stat SoulShotCount = new Stat("soulShotCount", CharacterStatId.SoulShotCount);
        
        // Skill mastery
        public static readonly Stat SkillMastery = new Stat("skillMastery", CharacterStatId.SkillMastery);

        private readonly string _value;

        public string Value => _value;
        public CharacterStatId CharacterStatId { get; }

        public Stat(string stat, CharacterStatId characterStatId)
        {
            _value = stat;
            _ordinalValue = _nextOrdinal++;
            CharacterStatId = characterStatId;
        }
        
        public static IEnumerable<Stat> Values
        {
            get
            {
                yield return MaxHp;
                yield return MaxMp;
                yield return MaxCp;
                yield return RegenerateHpRate;
                yield return RegenerateCpRate;
                yield return RegenerateMpRate;
                yield return RechargeMpRate;
                yield return HealEffectiveness;
                yield return PowerDefence;
                yield return MagicDefence;
                yield return PowerAttack;
                yield return MagicAttack;
                yield return PowerAttackSpeed;
                yield return MagicAttackSpeed;
                yield return MagicReuseRate;
                yield return ShieldDefence;
                yield return CriticalDamage;
                yield return CriticalDamageAdd;
                yield return PvpPhysicalDmg;
                yield return PvpMagicalDmg;
                yield return PvpPhysSkillDmg;
                yield return EvasionRate;
                yield return PSkillEvasion;
                yield return ShieldRate;
                yield return ShieldAngle;
                yield return CriticalRate;
                yield return BlowRate;
                yield return LethalRate;
                yield return MCriticalRate;
                yield return ExpSpRate;
                yield return AttackCancel;
                yield return AccuracyCombat;
                yield return PowerAttackRange;
                yield return MagicAttackRange;
                yield return PowerAttackAngle;
                yield return AttackCountMax;
                yield return RunSpeed;
                yield return WalkSpeed;
                yield return StatStr;
                yield return StatCon;
                yield return StatDex;
                yield return StatInt;
                yield return StatWit;
                yield return StatMen;
                yield return Breath;
                yield return Fall;
                yield return Aggression;
                yield return Bleed;
                yield return Poison;
                yield return Stun;
                yield return Root;
                yield return Movement;
                yield return Confusion;
                yield return Sleep;
                yield return Fire;
                yield return Wind;
                yield return Water;
                yield return Earth;
                yield return Holy;
                yield return Dark;
                yield return AggressionVuln;
                yield return BleedVuln;
                yield return PoisonVuln;
                yield return StunVuln;
                yield return ParalyzeVuln;
                yield return RootVuln;
                yield return SleepVuln;
                yield return ConfusionVuln;
                yield return MovementVuln;
                yield return FireVuln;
                yield return WindVuln;
                yield return WaterVuln;
                yield return EarthVuln;
                yield return HolyVuln;
                yield return DarkVuln;
                yield return CancelVuln;
                yield return DerangementVuln;
                yield return DebuffVuln;
                yield return BuffVuln;
                yield return FallVuln;
                yield return CastInterrupt;
                yield return CritVuln;
                yield return DebuffImmunity;
                yield return NoneWpnVuln;
                yield return SwordWpnVuln;
                yield return BluntWpnVuln;
                yield return DaggerWpnVuln;
                yield return BowWpnVuln;
                yield return PoleWpnVuln;
                yield return EtcWpnVuln;
                yield return FistWpnVuln;
                yield return DualWpnVuln;
                yield return DualfistWpnVuln;
                yield return PoleTargertCount;
                yield return BigswordWpnVuln;
                yield return BigbluntWpnVuln;
                yield return ReflectDamagePercent;
                yield return ReflectSkillMagic;
                yield return ReflectSkillPhysic;
                yield return VengeanceSkillPhysicalDamage;
                yield return AbsorbDamagePercent;
                yield return TransferDamagePercent;
                yield return MaxLoad;
                yield return WeightPenalty;
                yield return PAtkPlants;
                yield return PAtkInsects;
                yield return PAtkAnimals;
                yield return PAtkMonsters;
                yield return PAtkDragons;
                yield return PAtkUndead;
                yield return PAtkAngels;
                yield return PAtkGiants;
                yield return PAtkMagicCreatures;
                yield return PDefGiants;
                yield return PDefMagicCreatures;
                yield return PDefUndead;
                yield return PDefPlants;
                yield return PDefInsects;
                yield return PDefAnimals;
                yield return PDefMonsters;
                yield return PDefDragons;
                yield return PDefAngels;
                yield return AtkReuse;
                yield return PReuse;
                yield return InvLim;
                yield return WhLim;
                yield return FreightLim;
                yield return PSellLim;
                yield return PBuyLim;
                yield return RecDLim;
                yield return RecCLim;
                yield return PhysicalMpConsumeRate;
                yield return MagicalMpConsumeRate;
                yield return DanceMpConsumeRate;
                yield return HpConsumeRate;
                yield return MpConsume;
                yield return SoulShotCount;
                yield return SkillMastery;
            }
        }

        public static CharacterStatId GetStatByValue(string name)
        {
            return Values.FirstOrDefault(s => s.Value == name).CharacterStatId;
        }
    }
}
