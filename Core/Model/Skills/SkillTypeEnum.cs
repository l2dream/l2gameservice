﻿namespace Core.Model.Skills
{
    public enum SkillTypeEnum
    {
        // Damage
        PDam,
        MDam,
        CpDam,
        ManaDam,
        Dot,
        MDot,
        DrainSoul,
        Drain,
        DeathLink,
        FatalCounter,
        Blow,
        
        // Disablers
        Bleed,
        Poison,
        Stun,
        Root,
        Confusion,
        Fear,
        Sleep,
        ConfuseMobOnly,
        Mute,
        Paralyze,
        Weakness,

        // hp, mp, cp
        Heal,
        Hot,
        BalanceLife,
        HealPercent,
        HealStatic,
        CombatPointHeal,
        CombatPointPercentHeal,
        CpHot,
        ManaHeal,
        ManaByLevel,
        ManaHealPercent,
        ManaRecharge,
        MpHot,
	
        // Aggro
        AggDamage,
        AggReduce,
        AggRemove,
        AggReduceChar,
        AggDeBuff,
	
        // Fishing
        Fishing,
        Pumping,
        Reeling,
	
        // MISC
        Unlock,
        EnchantArmor,
        EnchantWeapon,
        SoulShot,
        SpiritShot,
        SiegeFlag,
        TakeCastle,
        DeluxeKeyUnlock,
        Sow,
        Harvest,
        GetPlayer,
	
        // Creation
        CommonCraft,
        DwarvenCraft,
        CreateItem,
        SummonTreasureKey,
	
        // Summons
        Summon,
        FeedPet,
        DeathLinkPet,
        StrSiegeAssault,
        Erase,
        Betray,
	
        // Cancel
        Cancel,
        MageBane,
        WarriorBane,
        Negate,
	
        Buff,
        DeBuff,
        Passive,
        Cont,
        Signet,
        SignetCastTime,
	
        Resurrect,
        Charge,
        ChargeEffect,
        ChargeDam,
        MHot,
        DetectWeakness,
        Luck,
        Recall,
        SummonFriend,
        Reflect,
        Spoil,
        Sweep,
        FakeDeath,
        UnBleed,
        UnPoison,
        UndeadDefense,
        Seed,
        BeastFeed,
        ForceBuff,
        ClanGate,
        GiveSp,
        CoreDone,
        ZakenPlayer,
        ZakenSelf,
        // unimplemented
        NotDone
    }
}