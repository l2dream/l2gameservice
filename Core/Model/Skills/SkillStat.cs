﻿namespace Core.Model.Skills
{
    public static class SkillStat
    {
        public const int SkillCubicMastery = 143;
        public const int SkillLucky = 194;
        public const int SkillCreateCommon = 1320;
        public const int SkillCreateDwarven = 172;
        public const int SkillCrystallize = 248;
        public const int SkillDivineInspiration = 1405;
	
        public const int SkillFakeInt = 9001;
        public const int SkillFakeWit = 9002;
        public const int SkillFakeMen = 9003;
        public const int SkillFakeCon = 9004;
        public const int SkillFakeDex = 9005;
        public const int SkillFakeStr = 9006;
        
        // elements
        public const int ElementWind = 1;
        public const int ElementFire = 2;
        public const int ElementWater = 3;
        public const int ElementEarth = 4;
        public const int ElementHoly = 5;
        public const int ElementDark = 6;
        
        // stat effected
		public const int StatPAtk = 301; // pAtk
		public const int StatPDef = 302; // pDef
		public const int StatMAtk = 303; // mAtk
		public const int StatMDef = 304; // mDef
		public const int StatMaxHp = 305; // maxHp
		public const int StatMaxMp = 306; // maxMp
		public const int StatCurHp = 307;
		public const int StatCurMp = 308;
		public const int StatHpRegen = 309; // regHp
		public const int StatMpRegen = 310; // regMp
		public const int StatCastingSpeed = 311; // sCast
		public const int StatAtkSpd = 312; // sAtk
		public const int StatCritdam = 313; // critDmg
		public const int StatCritrate = 314; // critRate
		public const int StatFireres = 315; // fireRes
		public const int StatWindres = 316; // windRes
		public const int StatWaterres = 317; // waterRes
		public const int StatEarthres = 318; // earthRes
		public const int StatHolyres = 336; // holyRes
		public const int StatDarkres = 337; // darkRes
		public const int StatRootres = 319; // rootRes
		public const int StatSleepres = 320; // sleepRes
		public const int StatConfusionres = 321; // confusRes
		public const int StatBreath = 322; // breath
		public const int StatAggression = 323; // aggr
		public const int StatBleed = 324; // bleed
		public const int StatPoison = 325; // poison
		public const int StatStun = 326; // stun
		public const int StatRoot = 327; // root
		public const int StatMovement = 328; // move
		public const int StatEvasion = 329; // evas
		public const int StatAccuracy = 330; // accu
		public const int StatCombatStrength = 331;
		public const int StatCombatWeakness = 332;
		public const int StatAttackRange = 333; // rAtk
		public const int StatNoagg = 334; // noagg
		public const int StatShieldDef = 335; // sDef
		public const int StatMpConsumeRate = 336; // Rate of mp consume per skill use
		public const int StatHpConsumeRate = 337; // Rate of hp consume per skill use
		public const int StatMcritrate = 338; // Magic Crit Rate
		
		// COMBAT DAMAGE MODIFIER SKILLS...DETECT WEAKNESS AND WEAKNESS/STRENGTH
		public const int CombatModAnimal = 200;
		public const int CombatModBeast = 201;
		public const int CombatModBug = 202;
		public const int CombatModDragon = 203;
		public const int CombatModMonster = 204;
		public const int CombatModPlant = 205;
		public const int CombatModHoly = 206;
		public const int CombatModUnholy = 207;
		public const int CombatModBow = 208;
		public const int CombatModBlunt = 209;
		public const int CombatModDagger = 210;
		public const int CombatModFist = 211;
		public const int CombatModDual = 212;
		public const int CombatModSword = 213;
		public const int CombatModPoison = 214;
		public const int CombatModBleed = 215;
		public const int CombatModFire = 216;
		public const int CombatModWater = 217;
		public const int CombatModEarth = 218;
		public const int CombatModWind = 219;
		public const int CombatModRoot = 220;
		public const int CombatModStun = 221;
		public const int CombatModConfusion = 222;
		public const int CombatModDark = 223;
		
		// conditional values
		public const int CondRunning = 0x0001;
		public const int CondWalking = 0x0002;
		public const int CondSit = 0x0004;
		public const int CondBehind = 0x0008;
		public const int CondCrit = 0x0010;
		public const int CondLowhp = 0x0020;
		public const int CondRobes = 0x0040;
		public const int CondCharges = 0x0080;
		public const int CondShield = 0x0100;
		public const int CondGradea = 0x010000;
		public const int CondGradeb = 0x020000;
		public const int CondGradec = 0x040000;
		public const int CondGraded = 0x080000;
		public const int CondGrades = 0x100000;
    }
}