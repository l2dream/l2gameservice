﻿using System.Threading.Tasks;
using Core.Model.Actor;
using Core.Model.World;

namespace Core.Model.Skills.Handlers
{
    public interface ISkillHandler
    {
        Task UseSkillAsync(Character character, Skill skill, WorldObject[] targets);
        SkillType[] GetSkillIds();
    }
}