﻿using Core.Model.Actor;
using Core.Model.Items;
using Core.Model.World;

namespace Core.Model.Skills.Handlers
{
    public class SkillChargeEffect : Skill
    {
        public SkillChargeEffect(StatSet set, SkillTypeInit skillTypeInit) : base(set, skillTypeInit)
        {
        }

        public override void UseSkill(Character caster, WorldObject[] targets)
        {
            throw new System.NotImplementedException();
        }
    }
}