﻿using System.Threading.Tasks;
using Core.Model.Actor;
using Core.Model.World;

namespace Core.Model.Skills.Handlers
{
    internal sealed class Continuous : ISkillHandler
    {
        private readonly SkillType[] _skillIds =
        {
            SkillType.Buff,
            SkillType.DeBuff,
            SkillType.Dot,
            SkillType.MDot,
            SkillType.Poison,
            SkillType.Bleed,
            SkillType.Hot,
            SkillType.CpHot,
            SkillType.MpHot,
            // Skill.SkillType.MANAHEAL,
            // Skill.SkillType.MANA_BY_LEVEL,
            SkillType.Fear,
            SkillType.Cont,
            SkillType.Weakness,
            SkillType.Reflect,
            SkillType.UndeadDefense,
            SkillType.AggDeBuff,
            SkillType.ForceBuff
        };
    
        public async Task UseSkillAsync(Character character, Skill skill, WorldObject[] targets)
        {
            bool bss = character.MagicCast().HasBlessedSpiritShot();
            bool sps = character.MagicCast().HasSpiritShot();
            bool ss = false;
            
            foreach (WorldObject target2 in targets)
            {
                Character target = (Character) target2;
                if (target == null)
                {
                    continue;
                }
                skill.GetEffects(character, target, ss, sps, bss);
            }
            if (bss)
            {
                await character.MagicCast().RemoveBlessedSpiritShotAsync();
            }
            else if (sps)
            {
                await character.MagicCast().RemoveSpiritShotAsync();
            }
            skill.GetEffectsSelf(character);
        }

        public SkillType[] GetSkillIds()
        {
            return _skillIds;
        }
    }
}