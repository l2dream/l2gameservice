﻿using System.Threading.Tasks;
using Core.Model.Actor;
using Core.Model.CalculateStats;
using Core.Model.Player;
using Core.Model.World;

namespace Core.Model.Skills.Handlers
{
    internal sealed class MagicDamage : ISkillHandler
    {
        private readonly SkillType[] _skillIds =
        {
            SkillType.MDam,
            SkillType.DeathLink
        };
        public async Task UseSkillAsync(Character character, Skill skill, WorldObject[] targets)
        {
            if (character.Status.IsAlikeDead())
            {
                return;
            }
            
            bool bss = character.MagicCast().HasBlessedSpiritShot();
            bool sps = character.MagicCast().HasSpiritShot();
            foreach (var o in targets)
            {
                var target = (Character) o;
                bool mCrit = Formulas.CalcMagicCrit(character.Stat.GetMagicCriticalHit(target, skill));
                int damage = (int) Formulas.CalcMagicDam(character, target, skill, sps, bss, mCrit);
                
                if (damage > 0)
                {
                    //character.sendDamageMessage(target, damage, mcrit, false, false);
                    await target.ReduceCurrentHpAsync(damage, character);

                    if (character is PlayerInstance playerInstance)
                    {
                        playerInstance.PlayerMessage().SendDamageMessageAsync(target, damage, mCrit);
                    }
                }
            }
            if (bss)
            {
                await character.MagicCast().RemoveBlessedSpiritShotAsync();
            }
            else if (sps)
            {
                await character.MagicCast().RemoveSpiritShotAsync();
            }
        }

        public SkillType[] GetSkillIds()
        {
            return _skillIds;
        }
    }
}