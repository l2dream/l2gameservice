﻿using System.Threading.Tasks;
using Core.Model.Actor;
using Core.Model.Player;
using Core.Model.World;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Skills.Handlers
{
    internal sealed class Heal : ISkillHandler
    {
        private readonly SkillType[] _skillIds =
        {
            SkillType.Heal,
            SkillType.HealPercent,
            SkillType.HealStatic
        };
    
        public async Task UseSkillAsync(Character character, Skill skill, WorldObject[] targets)
        {
            ISkillHandler handler = Initializer.SkillHandlerService().GetSkillHandler(SkillType.Buff);
            await handler.UseSkillAsync(character, skill, targets);

            foreach (WorldObject target2 in targets)
            {
                Character target = (Character) target2;
                double hp = skill.GetPower();
                if (skill.GetSkillType() == SkillType.HealPercent)
                {
                    hp = (target.Stat.GetMaxHp() * hp) / 100.0;
                }
                await target.Status.SetCurrentHpAsync(hp + target.Status.GetCurrentHp());
                StatusUpdate su = new StatusUpdate(target.ObjectId);
                su.AddAttribute(StatusUpdate.CurHp, (int) target.Status.GetCurrentHp());
                await target.SendPacketAsync(su);
                
                if (target is PlayerInstance)
                {
                    if (skill.Id == 4051)
                    {
                        await target.SendPacketAsync(new SystemMessage(SystemMessageId.RejuvenatingHp));
                    }
                    else if ((character is PlayerInstance) && (character != target))
                    {
                        SystemMessage sm = new SystemMessage(SystemMessageId.S2HpRestoredByS1);
                        sm.AddString(character.CharacterName);
                        sm.AddNumber((int) hp);
                        await target.SendPacketAsync(sm);
                    }
                    else
                    {
                        SystemMessage sm = new SystemMessage(SystemMessageId.S1HpRestored);
                        sm.AddNumber((int) hp);
                        await target.SendPacketAsync(sm);
                    }
                }
            }
        }

        public SkillType[] GetSkillIds()
        {
            return _skillIds;
        }
    }
}