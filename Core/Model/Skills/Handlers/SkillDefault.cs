﻿using Core.Model.Actor;
using Core.Model.Items;
using Core.Model.World;
using L2Logger;

namespace Core.Model.Skills.Handlers
{
    public class SkillDefault : Skill
    {
        public SkillDefault(StatSet set, SkillTypeInit skillTypeInit) : base(set, skillTypeInit)
        {
        }

        public override void UseSkill(Character caster, WorldObject[] targets)
        {
            caster.SendActionFailedPacketAsync();
            LoggerManager.Info("Skill not implemented.  Skill ID: " + Id + " " + GetSkillType());
        }
    }
}