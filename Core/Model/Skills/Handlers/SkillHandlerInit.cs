﻿using System;
using System.Collections.Generic;
using Core.Model.Items.Handlers;
using L2Logger;

namespace Core.Model.Skills.Handlers
{
    public class SkillHandlerInit
    {
        private readonly IDictionary<SkillType, ISkillHandler> _handlers;

        public SkillHandlerInit()
        {
            try
            {
                _handlers = new Dictionary<SkillType, ISkillHandler>();
                RegisterSkillHandler(new Heal());
                RegisterSkillHandler(new ManaHeal());
                RegisterSkillHandler(new Continuous());
                RegisterSkillHandler(new MagicDamage());
            }
            catch (Exception ex)
            {
                LoggerManager.Error(ex.Message);
            }
            
            LoggerManager.Info("SkillHandler: Loaded " + _handlers.Count + " handlers.");
        }
        
        private void RegisterSkillHandler(ISkillHandler handler)
        {
            SkillType[] types = handler.GetSkillIds();
            foreach (SkillType skillType in types)
            {
                _handlers.Add(skillType, handler);
            }
        }
        
        public ISkillHandler GetSkillHandler(SkillType skillType)
        {
            return _handlers[skillType];
        }
    }
}