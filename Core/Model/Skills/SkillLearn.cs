﻿namespace Core.Model.Skills
{
    public class SkillLearn
    {
        public int Id { get; }
        public int Level { get; }
        public string Name { get; }
        public int SpCost { get; }
        public int MinLevel { get; }
        public int CostId { get; }
        public int CostCount { get; }
        
        public SkillLearn(int id, int level, string name, int spCost, int minLevel, int costId, int costCount)
        {
            Id = id;
            Level = level;
            Name = name;
            SpCost = spCost;
            MinLevel = minLevel;
            CostId = costId;
            CostCount = costCount;
        }
    }
}