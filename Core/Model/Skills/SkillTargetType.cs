﻿namespace Core.Model.Skills
{
    public enum SkillTargetType
    {
        TargetNone,
        TargetSelf,
        TargetOne,
        TargetParty,
        TargetAlly,
        TargetClan,
        TargetPet,
        TargetArea,
        TargetAura,
        TargetCorpse,
        TargetUndead,
        TargetAreaUndead,
        TargetMultiface,
        TargetCorpseAlly,
        TargetCorpseClan,
        TargetCorpsePlayer,
        TargetCorpsePet,
        TargetItem,
        TargetAreaCorpseMob,
        TargetCorpseMob,
        TargetUnlockable,
        TargetHoly,
        TargetPartyMember,
        TargetPartyOther,
        TargetEnemySummon,
        TargetOwnerPet,
        TargetGround,
        TargetSiege,
        TargetTyrannosaurus,
        TargetAreaAimCorpse,
        TargetClanMember
    }
}