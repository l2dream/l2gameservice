﻿using System.Threading.Tasks;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Npc.Handlers
{
    public class L2NpcChat : IBypassHandler
    {
        private readonly NpcInstance _npcInstance;
        public L2NpcChat(NpcInstance npcInstance)
        {
            _npcInstance = npcInstance;
        }

        public async Task UseBypassAsync(string command, PlayerInstance player)
        {
            int val = int.Parse(command.Substring(5));
            await ShowTalkWindowAsync(player, val);
        }
        
        public async Task ShowTalkWindowAsync(PlayerInstance playerInstance, int value)
        {
            int npcId = _npcInstance.Template.Stat.NpcId;
            string fileName = _npcInstance.GetHtmPath(npcId, value);

            NpcHtmlMessage html = new NpcHtmlMessage(_npcInstance.ObjectId);
            html.SetFile(fileName);
            
            html.Replace("%npcname%", _npcInstance.CharacterName);
            html.Replace("%playername%", playerInstance.CharacterName);
            html.Replace("%objectId%", _npcInstance.ObjectId.ToString());
            await playerInstance.SendPacketAsync(html);
            await playerInstance.SendActionFailedPacketAsync();
        }
    }
}