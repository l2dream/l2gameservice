﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Core.Cache;
using Core.Model.Items;
using Core.Model.Npc.Contracts;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;
using L2Logger;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace Core.Model.Npc.Handlers
{
    public class L2MerchantBuy : IBypassHandler
    {
        private string _merchantCacheKey = "Merchant_";
        private readonly IDistributedCache _cache;

        public L2MerchantBuy()
        {
            _cache = Initializer.ServiceProvider.GetService<IDistributedCache>();
        }
        
        private async Task ShowBuyWindowAsync(PlayerInstance playerInstance, int value)
        {
            try
            {
                var path = Initializer.Config().ServerConfig.StaticData + "/Json/Merchant/" + value + ".json";
                string cacheKey = _merchantCacheKey + value;
                var data = await _cache.GetDataAsync(cacheKey);
                if (data is null)
                {
                    using (StreamReader sr = new StreamReader(path))
                    {
                        data = await sr.ReadToEndAsync();
                    }
                    await _cache.SetDataAsync(cacheKey, data);
                }
                await playerInstance.SendPacketAsync(new BuyList(GetMerchantList(data), value,
                        playerInstance.PlayerInventory().GetAdena()));
            }
            catch (Exception e)
            {
                LoggerManager.Error(GetType().Name + " " + e.Message);
            }
        }

        private List<ItemInstance> GetMerchantList(string data)
        {
            L2MerchantContract contract = JsonConvert.DeserializeObject<L2MerchantContract>(data);
            List<ItemInstance> list = new List<ItemInstance>();
            contract?.Items.ForEach(i =>
            {
                var item = Initializer.ItemService().CreateDummyItem(i.ItemId);
                item.PriceSell = i.Price;
                item.Count = i.Count;
                list.Add(item);
            });
            return list;
        }

        public async Task UseBypassAsync(string command, PlayerInstance player)
        {
            var value = int.Parse(command.Substring(4));
            await ShowBuyWindowAsync(player, value);
        }
    }
}