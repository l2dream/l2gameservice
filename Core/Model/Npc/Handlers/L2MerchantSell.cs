﻿using System.Threading.Tasks;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Npc.Handlers
{
    public class L2MerchantSell : IBypassHandler
    {
        public async Task UseBypassAsync(string command, PlayerInstance player)
        {
            await ShowSellWindowAsync(player);
        }
        
        private async Task ShowSellWindowAsync(PlayerInstance player)
        {
            await player.SendPacketAsync(new SellList(player));
        }
    }
}