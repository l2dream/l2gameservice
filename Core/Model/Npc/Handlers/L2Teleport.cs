﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Core.Model.Player;

namespace Core.Model.Npc.Handlers
{
    public class L2Teleport : IBypassHandler
    {
        private readonly NpcInstance _npcInstance;

        public L2Teleport(NpcInstance npcInstance)
        {
            _npcInstance = npcInstance;
        }
        public async Task UseBypassAsync(string command, PlayerInstance player)
        {
            if (command.StartsWith("goto"))
            {
                int npcId = _npcInstance.Template.Stat.NpcId;
                var id = Regex.Match(command.Substring(4), @"\d+").Value;
                int whereToGo = Convert.ToInt16(id);
                await DoTeleportAsync(player, whereToGo);
            }
        }
        
        private async Task DoTeleportAsync(PlayerInstance playerInstance, int value)
        {
            var teleportLocation = Initializer.TeleportLocationService().GetTeleportLocation(value);
            await playerInstance.Teleport().TeleToLocationAsync(teleportLocation.LocX, teleportLocation.LocY, teleportLocation.LocZ);
            await playerInstance.SendActionFailedPacketAsync();
        }
    }
}