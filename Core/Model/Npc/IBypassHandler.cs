﻿using System.Threading.Tasks;
using Core.Model.Player;

namespace Core.Model.Npc
{
    public interface IBypassHandler
    {
        Task UseBypassAsync(string command, PlayerInstance player);
    }
}