﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.AI;
using Core.Model.Actor;
using Core.Model.Npc.Type;
using Core.TaskManager;
using Helpers;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Model.Npc
{
    public class NpcWalkerAi : CharacterAi
    {
        private const int DefaultMoveDelay = 0;
        private long _nextMoveTime;
        private bool WalkingToNextPoint { get; set; }
        
        public int HomeX { get; set; }
        public int HomeY { get; set; }
        public int HomeZ { get; set; }

        private readonly List<NpcWalkerNode> _route;
        private int _currentPos;
        
        public NpcWalkerAi(Character character) : base(character)
        {
            var routeDataInit = Initializer.ServiceProvider.GetService<NpcWalkerRouteDataInit>();
            _route = routeDataInit?.GetRouteForNpc(GetNpcWalker().Template.Stat.NpcId);
            Task.Factory.StartNew(Run);
        }

        public override async Task OnEvtThinkAsync()
        {
            if (WalkingToNextPoint)
            {
                if (HasArrived())
                {
                    await SendMessageAsync();
                }
                return;
            }
            if (_nextMoveTime < DateTimeHelper.CurrentUnixTimeMillis())
            {
                await WalkToLocationAsync().ContinueWith(HandleException);
            }
        }

        protected override async Task OnEvtArrivedBlockedAsync(Location location)
        {
            if (_route.Count <= _currentPos)
            {
                return;
            }
            int destinationX = _route[_currentPos].MoveX;
            int destinationY = _route[_currentPos].MoveY;
            int destinationZ = _route[_currentPos].MoveZ;
            
            await GetNpcWalker().Teleport().TeleToLocationAsync(destinationX, destinationY, destinationZ, false);
            await base.OnEvtArrivedBlockedAsync(location);
        }

        private async Task WalkToLocationAsync()
        {
            if (_currentPos < (_route.Count - 1))
            {
                _currentPos++;
            }
            else
            {
                _currentPos = 0;
            }
		
            if (_route.Count <= _currentPos)
            {
                return;
            }
            bool moveType = _route[_currentPos].Running;
            if (moveType)
            {
                await GetNpcWalker().Movement().SetRunningAsync();
            }
            else
            {
                await GetNpcWalker().Movement().SetWalkingAsync();
            }
            
            // now we define destination
            int destinationX = _route[_currentPos].MoveX;
            int destinationY = _route[_currentPos].MoveY;
            int destinationZ = _route[_currentPos].MoveZ;
            
            // notify AI of MOVE_TO
            WalkingToNextPoint = true;
            SetIntention(CtrlIntention.AiIntentionMoveTo, new Location(destinationX, destinationY, destinationZ, 0));
        }

        /// <summary>
        /// 
        /// </summary>
        private bool HasArrived()
        {
            if (_route.Count <= _currentPos)
            {
                return false;
            }
            
            int destinationX = _route[_currentPos].MoveX;
            int destinationY = _route[_currentPos].MoveY;
            int destinationZ = _route[_currentPos].MoveZ;

            if ((GetNpcWalker().GetX() == destinationX) && 
                (GetNpcWalker().GetY() == destinationY) &&
                (GetNpcWalker().GetZ() == destinationZ))
            {
                // time in millis
                long delay = _route[_currentPos].Delay * 1000;
                // sleeps between each move
                if (delay < 0)
                {
                    delay = DefaultMoveDelay;
                }
                _nextMoveTime = DateTimeHelper.CurrentUnixTimeMillis() + delay;
                WalkingToNextPoint = false;
                return true;
            }
            return false;
        }

        private async Task SendMessageAsync()
        {
            string text = _route[_currentPos].ChatText;
            if ((text != null))
            {
                await GetNpcWalker().BroadCastChatAsync(text);
            }
        }

        private async Task Run()
        {
            await ThreadPoolManager.Instance.Schedule(async () => await OnEvtThinkAsync(), 0, 1000);
        }

        private L2NpcWalker GetNpcWalker()
        {
            return (L2NpcWalker) GetActor();
        }
    }
}