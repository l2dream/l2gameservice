﻿using System;
using System.Collections.Generic;
using System.Xml;
using Core.Model.Items;
using L2Logger;

namespace Core.Model.Npc
{
    public sealed class NpcWalkerRouteDataInit
    {
        private readonly Dictionary<int, List<NpcWalkerNode>> _routes;
        
        public NpcWalkerRouteDataInit()
        {
            _routes = new Dictionary<int, List<NpcWalkerNode>>();
            Initialize();
        }

        private void Initialize()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(@"StaticData/Xml/WalkerRoutes.xml");
                if (doc.DocumentElement == null)
                {
                    return;
                }
                XmlNodeList nodes = doc.DocumentElement.SelectNodes("/list/route");
                if (nodes != null)
                {
                    foreach (XmlNode node in nodes)
                    {
                        XmlElement ownerElement = node.Attributes?[0].OwnerElement;
                        if (ownerElement == null || node.Attributes == null || !ownerElement.Name.Equals("route"))
                        {
                            continue;
                        }
                        XmlNamedNodeMap attrs = node.Attributes;
                        int npcId = int.Parse(attrs.GetNamedItem("npcId").Value);
                        List<NpcWalkerNode> points = new List<NpcWalkerNode>();
                        foreach (XmlNode innerData in node.ChildNodes)
                        {
                            var attrsInner = innerData.Attributes;
                            if (attrsInner?["x"] == null || attrsInner["y"] == null) continue;
                            int x = int.Parse(attrsInner["x"].Value);
                            int y = int.Parse(attrsInner["y"].Value);
                            int z = int.Parse(attrsInner["z"].Value);
                            int delay = int.Parse(attrsInner["delay"].Value);
                            bool run = bool.Parse(attrsInner["run"].Value);
                            string chatText = null;
                            if (attrsInner["chat"] != null)
                            {
                                chatText = attrsInner["chat"].Value;
                            }

                            NpcWalkerNode route = new NpcWalkerNode
                            {
                                MoveX = x,
                                MoveY = y,
                                MoveZ = z,
                                Delay = delay,
                                Running = run,
                                ChatText = chatText
                            };
                            points.Add(route);
                        }
                        _routes.Add(npcId, points);
                    }
                }
                LoggerManager.Info(GetType().Name + $": Loaded " + _routes.Count + " walker routes.");
            }
            catch (Exception ex)
            {
                LoggerManager.Error(GetType().Name + ": Error while reading walker route data: " + ex.Message);
            }
        }
        
        public List<NpcWalkerNode> GetRouteForNpc(int id)
        {
            return _routes[id];
        }
    }
}