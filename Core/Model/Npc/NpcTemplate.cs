﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Model.Actor;
using Core.Model.Player;
using Core.Model.Skills;

namespace Core.Model.Npc
{
    public class NpcTemplate : CharacterTemplate
    {
        public new NpcTemplateStat Stat { get; }
        private readonly IDictionary<int, Skill> _skills;
        private readonly IList<ClassId> _teachInfo;
        private readonly IList<MinionData> _minions;

        /** The table containing all Item that can be dropped by NpcInstance using this NpcTemplate */
        private readonly IList<DropCategory> _categories;
        public NpcTemplate(NpcTemplateStat stat) : base(stat)
        {
            Stat = stat;
            _skills = new Dictionary<int, Skill>();
            _teachInfo = new List<ClassId>();
            _categories = new List<DropCategory>();
            _minions = new List<MinionData>();
        }

        public void AddDropData(DropData drop, int categoryId)
        {
            if (!drop.IsQuestDrop())
            {
                bool catExists = false;
                foreach (var cat in _categories.Where(cat => cat.GetCategoryType() == categoryId))
                {
                    cat.AddDropData(drop, Stat.Type.Equals("RaidBoss") || Stat.Type.Equals("GrandBoss"));
                    catExists = true;
                    break;
                }
                // if the category doesn't exit, create it and add the drop
                if (!catExists)
                {
                    DropCategory cat = new DropCategory(categoryId);
                    cat.AddDropData(drop, Stat.Type.Equals("RaidBoss") || Stat.Type.Equals("GrandBoss"));
                    _categories.Add(cat);
                }
            }
        }
        
        public void AddTeachInfo(ClassId classId)
        {
            _teachInfo.Add(classId);
        }
	
        public IList<ClassId> GetTeachInfo()
        {
            return _teachInfo;
        }
	
        public bool CanTeach(ClassId classId)
        {
            // If the player is on a third class, fetch the class teacher information for its parent class.
            return _teachInfo.Contains(classId.Id >= ClassIds.Duelist ? classId.Parent : classId);
        }

        public void AddMinionData(MinionData minion)
        {
            _minions.Add(minion);
        }
        
        public IList<MinionData> GetMinionData()
        {
            return _minions;
        }
        
        /// <summary>
        /// Return the list of all possible UNCATEGORIZED drops of this NpcTemplate.
        /// </summary>
        /// <returns></returns>
        public IList<DropCategory> GetDropData()
        {
            return _categories;
        }
        
        public void AddSkill(Skill skill)
        {
            _skills.TryAdd(skill.Id, skill);
        }
        
        public IDictionary<int, Skill> GetSkills()
        {
            return _skills;
        }
    }
}