﻿namespace Core.Model.Npc
{
    public class NpcWalkerNode
    {
        public int MoveX { get; set; }
        public int MoveY { get; set; }
        public int MoveZ { get; set; }
        public int Delay { get; set; }
        public bool Running { get; set; }
        public string ChatText { get; set; }
    }
}