﻿using Helpers;

namespace Core.Model.Npc
{
    public sealed class MinionData
    {
        public int MinionId { get; set; }
        public int AmountMin { get; set; }
        public int AmountMax { get; set; }
        private int _minionAmount;

        public int Amount
        {
            get => _minionAmount;
            set
            {
                if (AmountMax > AmountMin)
                {
                    _minionAmount = Rnd.Next(AmountMin, AmountMax);
                }
                _minionAmount = value;
            }
        }
    }
}