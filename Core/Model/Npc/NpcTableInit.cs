﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using Core.Model.Player;
using L2Logger;

namespace Core.Model.Npc
{
    public sealed class NpcTableInit
    {
        private readonly Dictionary<int, NpcTemplate> _npcs;
        private readonly Dictionary<string, object> _setData = new Dictionary<string, object>();

        public NpcTableInit()
        {
            _npcs = new Dictionary<int, NpcTemplate>();
            Initialize();
        }

        private void Initialize()
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                string[] xmlFilesArray = Directory.GetFiles(@"StaticData/Xml/Npcs");
                foreach (string i in xmlFilesArray)
                {
                    doc.Load(i);
                    XmlNodeList nodes = doc.DocumentElement?.SelectNodes("/list/npc");

                    if (nodes == null)
                    {
                        continue;
                    }
                    foreach (XmlNode node in nodes)
                    {
                        XmlElement ownerElement = node.Attributes?[0].OwnerElement;
                        if (ownerElement == null || node.Attributes == null || ownerElement.Name != "npc") continue;
                        XmlNamedNodeMap attrs = node.Attributes;
                            
                        NpcTemplateStat stat = new NpcTemplateStat();
                        int npcId = int.Parse(attrs.GetNamedItem("id").Value);
                        int templateId = attrs.GetNamedItem("idTemplate") == null ? npcId : int.Parse(attrs.GetNamedItem("idTemplate").Value);
                            
                        stat.NpcId = npcId;
                        stat.DisplayId = templateId;
                        stat.Name = attrs.GetNamedItem("name").Value;
                        stat.Title = attrs.GetNamedItem("title").Value;
                            
                        foreach(XmlNode innerData in node.ChildNodes)
                        {
                            if(innerData.Attributes["name"] != null && innerData.Attributes["val"] != null)
                            {
                                string value = innerData.Attributes["val"].Value;
                                string name = innerData.Attributes["name"].Value;
                                _setData.Add(name, value);
                            }
                            if (innerData.Name == "skills")
                            {
                                _setData.Add("SkillData", innerData);
                            }
                            if (innerData.Name == "drops")
                            {
                                _setData.Add("DropData", innerData);
                            }
                            
                            if (innerData.Name == "teachTo")
                            {
                                _setData.Add("TeachToData", innerData);
                            }
                            
                            if (innerData.Name == "minions")
                            {
                                _setData.Add("MinionsData", innerData);
                            }
                        }
                        stat.Level = Convert.ToInt32(_setData.GetValueOrDefault("level"));
                        stat.CollisionRadius = Convert.ToSingle(_setData.GetValueOrDefault("radius"), CultureInfo.InvariantCulture.NumberFormat);
                        stat.CollisionHeight = Convert.ToSingle(_setData.GetValueOrDefault("height"), CultureInfo.InvariantCulture.NumberFormat);
                        stat.Type = _setData.GetValueOrDefault("type").ToString();
                        stat.RewardExp = Convert.ToInt32(_setData.GetValueOrDefault("exp"));
                        stat.RewardSp = Convert.ToInt32(_setData.GetValueOrDefault("sp"));
                        stat.BasePAtkSpd = Convert.ToInt32(_setData.GetValueOrDefault("atkSpd"));
                        stat.BaseHpMax = (int) Convert.ToDouble(_setData.GetValueOrDefault("hp"), CultureInfo.InvariantCulture.NumberFormat);
                        stat.BaseMpMax = (int) Convert.ToDouble(_setData.GetValueOrDefault("mp"), CultureInfo.InvariantCulture.NumberFormat);
                        stat.BaseHpReg = Convert.ToDouble(_setData.GetValueOrDefault("hpRegen"), CultureInfo.InvariantCulture.NumberFormat);
                        stat.BaseMpReg = Convert.ToDouble(_setData.GetValueOrDefault("mpRegen"), CultureInfo.InvariantCulture.NumberFormat);
                        stat.BasePAtk = (int) Convert.ToSingle(_setData.GetValueOrDefault("pAtk"), CultureInfo.InvariantCulture.NumberFormat);
                        stat.BasePDef = (int) Convert.ToSingle(_setData.GetValueOrDefault("pDef"), CultureInfo.InvariantCulture.NumberFormat);
                        stat.BaseMAtk = (int) Convert.ToSingle(_setData.GetValueOrDefault("mAtk"), CultureInfo.InvariantCulture.NumberFormat);
                        stat.BaseMDef = (int) Convert.ToSingle(_setData.GetValueOrDefault("mDef"), CultureInfo.InvariantCulture.NumberFormat);
                        stat.BaseCritRate = Convert.ToInt32(_setData.GetValueOrDefault("crit"));
                        stat.RHand = Convert.ToInt32(_setData.GetValueOrDefault("rHand"));
                        stat.LHand = Convert.ToInt32(_setData.GetValueOrDefault("lHand"));
                        stat.BaseWalkSpd = Convert.ToInt32(_setData.GetValueOrDefault("walkSpd"));
                        stat.BaseRunSpd = Convert.ToInt32(_setData.GetValueOrDefault("runSpd"));
                        stat.BaseStr = Convert.ToInt32(_setData.GetValueOrDefault("str"));
                        stat.BaseCon = Convert.ToInt32(_setData.GetValueOrDefault("con"));
                        stat.BaseInt = Convert.ToInt32(_setData.GetValueOrDefault("int"));
                        stat.BaseDex = Convert.ToInt32(_setData.GetValueOrDefault("dex"));
                        stat.BaseWit = Convert.ToInt32(_setData.GetValueOrDefault("wit"));
                        stat.BaseMen = Convert.ToInt32(_setData.GetValueOrDefault("men"));
                        stat.BaseCpMax = 0;
                            
                        _npcs.Add(npcId, new NpcTemplate(stat));

                        //Skill Data
                        if (_setData.ContainsKey("SkillData"))
                        {
                            AddSkills((XmlNode) _setData["SkillData"], stat);
                        }
                            
                        //Drop Data
                        if (_setData.ContainsKey("DropData"))
                        {
                            AddDrops((XmlNode) _setData["DropData"], stat);
                        }
                        
                        //TeachTo
                        if (_setData.ContainsKey("TeachToData"))
                        {
                            AddTeachToData((XmlNode) _setData["TeachToData"], stat);
                        }
                        
                        //Minions Data
                        if (_setData.ContainsKey("MinionsData"))
                        {
                            AddMinionsData((XmlNode) _setData["MinionsData"], stat);
                        }
                        _setData.Clear();
                    }
                }
                
                LoggerManager.Info($"Loaded {_npcs.Count} NPC templates");
            }
            catch (Exception ex)
            {
                LoggerManager.Error(ex.Message);
            }
        }

        private void AddSkills(XmlNode xmlNode, NpcTemplateStat templateStat)
        {
            try
            {
                var skillService = Initializer.SkillService();
                foreach (XmlNode node in xmlNode.ChildNodes)
                {
                    var attributes = node.Attributes;
                    if (attributes == null) continue;
                    int id = Convert.ToInt32(attributes.GetNamedItem("id").Value);
                    int level = Convert.ToInt32(attributes.GetNamedItem("level").Value);

                    var npcData = _npcs[templateStat.NpcId];
                    var npcSkill = skillService.GetSkill(id, level);
                    if (npcSkill == null)
                        continue;
                    npcData.AddSkill(npcSkill);
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Error(GetType().Name + " AddSkills " + ex.Message);
            }
        }

        private void AddTeachToData(XmlNode xmlNode, NpcTemplateStat templateStat)
        {
            try
            {
                var attributes = xmlNode.Attributes;
                string[] classes = attributes?.GetNamedItem("classes").Value.Split(";");
                var npcData = _npcs[templateStat.NpcId];
                foreach (var id in classes)
                {
                    npcData.AddTeachInfo(ClassId.GetClass(Convert.ToInt16(id)));
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Error(GetType().Name + " AddTeachToData " + ex.Message);
            }
        }

        private void AddDrops(XmlNode xmlNode, NpcTemplateStat templateStat)
        {
            try
            {
                foreach (XmlNode node in xmlNode.ChildNodes)
                {
                    var attributes = node.Attributes;
                    if (attributes == null) continue;
                    int categoryId = Convert.ToInt32(attributes.GetNamedItem("id").Value);
                    foreach (XmlElement element in node.ChildNodes)
                    {
                        var innerAttributes = element.Attributes;
                        int itemId = Convert.ToInt32(innerAttributes.GetNamedItem("itemid").Value);
                        int min = Convert.ToInt32(innerAttributes.GetNamedItem("min").Value);
                        int max = Convert.ToInt32(innerAttributes.GetNamedItem("max").Value);
                        int chance = Convert.ToInt32(innerAttributes.GetNamedItem("chance").Value);

                        var dropData = new DropData
                        {
                            ItemId = itemId,
                            MinDrop = min,
                            MaxDrop = max,
                            Chance = chance
                        };
                        var npcData = _npcs[templateStat.NpcId];
                        npcData.AddDropData(dropData, categoryId);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Error(GetType().Name + " AddDrops " + ex.Message);
            }
        }

        private void AddMinionsData(XmlNode xmlNode, NpcTemplateStat templateStat)
        {
            try
            {
                foreach (XmlNode node in xmlNode.ChildNodes)
                {
                    var attributes = node.Attributes;
                    int minionId = Convert.ToInt32(attributes.GetNamedItem("id").Value);
                    int min = Convert.ToInt32(attributes.GetNamedItem("min").Value);
                    int max = Convert.ToInt32(attributes.GetNamedItem("max").Value);
                    MinionData minionData = new MinionData
                    {
                        MinionId = minionId,
                        AmountMin = min,
                        AmountMax = max
                    };
                    var npcData = _npcs[templateStat.NpcId];
                    npcData.AddMinionData(minionData);
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Error(GetType().Name + " AddMinionsData " + ex.Message);
            }
        }
        
        public NpcTemplate GetTemplate(int id)
        {
            return _npcs[id];
        }
    }
}