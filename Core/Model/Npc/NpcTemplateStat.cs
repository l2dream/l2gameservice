﻿using Core.Model.Actor;

namespace Core.Model.Npc
{
    public class NpcTemplateStat : CharacterTemplateStat
    {
        public int NpcId { get; set; }
        public int DisplayId { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public bool ServerSideName { get; set; }
        public string Title { get; set; }
        public bool ServerSideTitle { get; set; }
        public string Sex { get; set; }
        public int Level { get; set; }
        public int RewardExp { get; set; }
        public int RewardSp { get; set; }
        public int AggroRange { get; set; }
        public int RHand { get; set; }
        public int LHand { get; set; }
        public int Armor { get; set; }
        public int AbsorbLevel { get; set; }
        public AbsorbCrystalType AbsorbType { get; set; }
        public string FactionId { get; set; }
    }
}