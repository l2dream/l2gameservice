﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Model.Actor;
using Core.Model.Actor.KnownList;
using Core.Model.CalculateStats;
using Core.Model.Items;
using Core.Model.Items.Type;
using Core.Model.Player;
using Core.Model.Skills;
using Core.Model.Spawn;
using Core.Network.GameServicePackets.ServerPackets;
using Helpers;

namespace Core.Model.Npc
{
    public class NpcInstance : Character
    {
        public new readonly NpcTemplate Template;
        public const int InteractionDistance = 150;
        public int NpcHashId => Template.Stat.NpcId + 1000000;
        public int NpcId => Template.Stat.NpcId;
        public override bool IsNpc => true;
        public virtual bool Attackable => false;
        public int Level { get; }
        public SpawnData Spawn { get; set; }
        public double Radius => Template.Stat.CollisionRadius == 0 ? 12 : Template.Stat.CollisionRadius;

        public double Height => Template.Stat.CollisionHeight == 0 ? 22 : Template.Stat.CollisionHeight;
        private Calculator[] _calculators;
        private IDictionary<int, Skill> _skills;
        
        protected NpcInstance(int objectId, NpcTemplate template) : base(objectId, template)
        {
            Template = template;
            CharacterName = template.Stat.Name;
            Level = template.Stat.Level;
            SetKnownList(new NpcKnownList(this));
            InitSkills();
        }

        private void InitSkills()
        {
            // Copy the Standard Calculators of the NPCInstance in _calculators
            _calculators = Calculators;
            _skills = Template.GetSkills();
        }

        public override WorldObjectKnownList GetKnownList()
        {
            return (NpcKnownList) base.GetKnownList();
        }
        
        public override ItemInstance GetActiveWeaponInstance()
        {
            // regular NPCs dont have weapons
            return null;
        }

        public override Weapon GetActiveWeaponItem()
        {
            // Get the weapon identifier equipped in the right hand of the NpcInstance
            int weaponId = Template.Stat.RHand;
            if (weaponId < 1)
            {
                return null;
            }
		
            // Get the weapon item equipped in the right hand of the NpcInstance
            Item item = Initializer.ItemService().Items[weaponId];
		
            return (Weapon) item;
        }

        public override void UpdateAbnormalEffect()
        {
            foreach (PlayerInstance playerInstance in GetKnownList().GetKnownPlayers().Values
                .Where(playerInstance => playerInstance != null))
            {
                playerInstance.SendPacketAsync(new NpcInfo(this));
            }
        }

        private Task<bool> CanTarget(PlayerInstance player)
        {
            if (player.Status.IsOutOfControl())
            {
                return Task.FromResult(false);
            }
            if ((DateTimeHelper.CurrentUnixTimeMillis() - player.TimerToAttack) < 50)
            {
                return Task.FromResult(false);
            }
            return Task.FromResult(true);
        }

        protected Task<bool> IsTargetSelected(PlayerInstance player)
        {
            return Task.FromResult(this == player.Target.GetTarget());
        }

        private async Task SetCurrentTargetAsync(PlayerInstance player)
        {
            // Set the target of the PlayerInstance player
            player.Target.SetTarget(this);
            // Remove player spawn protection
            player.PlayerAction().OnActionRequest();
            player.TimerToAttack = DateTimeHelper.CurrentUnixTimeMillis();
            // Send a Server->Client packet ValidateLocation to correct the NpcInstance position and heading on the client
            await player.SendPacketAsync(new ValidateLocation(this));
        }
        
        public override async Task OnActionAsync(PlayerInstance player)
        {
            if (!await CanTarget(player))
            {
                await player.SendActionFailedPacketAsync();
                return;
            }
            await SetCurrentTargetAsync(player);
        }

        public virtual async Task OnBypassFeedbackAsync(PlayerInstance playerInstance, string command)
        {
            IBypassHandler handler = new BypassHandler(this).GetHandler(command); 
            await handler.UseBypassAsync(command, playerInstance);
        }

        //TODO need finish
        public virtual string GetHtmPath(int npcId, int value)
        {
            string pom = "" + npcId;   
            if (value > 0)
            {
                pom = npcId + "-" + value;
            }

            string tmp = "/default/" + pom + ".htm";
            return tmp;
        }
    }
}