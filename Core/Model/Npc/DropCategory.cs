﻿using System;
using System.Collections.Generic;
using Helpers;

namespace Core.Model.Npc
{
    public sealed class DropCategory
    {
        private readonly IList<DropData> _drops;
        private int _categoryChance; // a sum of chances for calculating if an item will be dropped from this category
        private int _categoryBalancedChance; // sum for balancing drop selection inside categories in high rate servers
        private readonly int _categoryType;
        private const int RateDropItems = 2; //TODO Config Config.RATE_DROP_ITEMS
        
        public DropCategory(int categoryType)
        {
            _categoryType = categoryType;
            _drops = new List<DropData>();
            _categoryChance = 0;
            _categoryBalancedChance = 0;
        }
        
        public void AddDropData(DropData drop, bool raid)
        {
            if (!drop.IsQuestDrop())
            {
                _drops.Add(drop);
                _categoryChance += drop.Chance;
                // for drop selection inside a category: max 100 % chance for getting an item, scaling all values to that.
                _categoryBalancedChance += Math.Min((drop.Chance * (raid ? 1 : RateDropItems)), DropData.MaxChance);
            }
        }
        
        public IList<DropData> GetAllDrops()
        {
            return _drops;
        }
        
        public void ClearAllDrops()
        {
            _drops.Clear();
        }
        
        public bool IsSweep()
        {
            return _categoryType == -1;
        }
        
        public int GetCategoryChance()
        {
            return _categoryType >= 0 ? _categoryChance : DropData.MaxChance;
        }
        
        public int GetCategoryBalancedChance()
        {
            return _categoryType >= 0 ? _categoryBalancedChance : DropData.MaxChance;
        }
        
        public int GetCategoryType()
        {
            return _categoryType;
        }
        
        public DropData DropSeedAllowedDropsOnly()
        {
            IList<DropData> drops = new List<DropData>();
            int subCatChance = 0;
            foreach (DropData drop in _drops)
            {
                if ((drop.ItemId == 57) || (drop.ItemId == 6360) || (drop.ItemId == 6361) || (drop.ItemId == 6362))
                {
                    drops.Add(drop);
                    subCatChance += drop.Chance;
                }
            }
		
            // among the results choose one.
            int randomIndex = Rnd.Next(subCatChance);
            int sum = 0;
            foreach (DropData drop in drops)
            {
                sum += drop.Chance;
                if (sum > randomIndex) // drop this item and exit the function
                {
                    drops.Clear();
                    return drop;
                }
            }
            // since it is still within category, only drop one of the acceptable drops from the results.
            return new DropData();
        }
        
        public DropData DropOne(bool raid)
        {
            int randomIndex = Rnd.Next(GetCategoryBalancedChance());
            int sum = 0;
            foreach (DropData drop in _drops)
            {
                sum += Math.Min((drop.Chance * (raid ? 1 : RateDropItems)), DropData.MaxChance);
                if (sum >= randomIndex)
                {
                    return drop;
                }
            }
            return new DropData();
        }
        
    }
}