﻿using Core.Model.Actor;
using Core.Model.Actor.KnownList;

namespace Core.Model.Npc.Type
{
    public class L2FriendlyMob : Attackable
    {
        public L2FriendlyMob(int objectId, NpcTemplate template) : base(objectId, template)
        {
            
        }
        
        public override WorldObjectKnownList GetKnownList()
        {
            if (!(base.GetKnownList() is FriendlyMobKnownList))
            {
                SetKnownList(new FriendlyMobKnownList(this));
            }
            return (FriendlyMobKnownList) base.GetKnownList();
        }
    }
}