﻿using System.Threading.Tasks;
using Core.Model.Player;

namespace Core.Model.Npc.Type
{
    public class L2Merchant : L2Npc
    {
        
        public L2Merchant(int objectId, NpcTemplate template) : base(objectId, template)
        {
            
        }

        public override async Task OnBypassFeedbackAsync(PlayerInstance playerInstance, string command)
        {
            IBypassHandler handler = new BypassHandler(this).GetHandler(command); 
            await handler.UseBypassAsync(command, playerInstance);
        }

        public override string GetHtmPath(int npcId, int value)
        {
            string pom = "" + npcId;   
            if (value > 0)
            {
                pom = npcId + "-" + value;
            }
            return "/merchant/" + pom + ".htm";;
        }
    }
}