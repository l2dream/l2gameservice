﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Model.Player;
using Core.Model.Skills;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Npc.Type
{
    public class L2Trainer : L2Npc
    {
        public L2Trainer(int objectId, NpcTemplate template) : base(objectId, template)
        {
        }

        public override async Task OnActionAsync(PlayerInstance player)
        {
            player.LastTrainerNpc = this;
            await base.OnActionAsync(player);
        }

        public override async Task OnBypassFeedbackAsync(PlayerInstance playerInstance, string command)
        {
            if (command.StartsWith("SkillList"))
            {
                await ShowSkillListAsync(playerInstance, playerInstance.Template.Stat.ClassId);
            }
            else
            {
                await base.OnBypassFeedbackAsync(playerInstance, command);
            }
        }

        public async Task ShowSkillListAsync(PlayerInstance playerInstance, ClassId classId)
        {
            int npcId = Template.Stat.NpcId;
            List<SkillLearn> skillList = Initializer.SkillTreeService().GetAvailableSkills(playerInstance, classId);
            if (skillList.Count == 0)
            {
                int minLevel = Initializer.SkillTreeService().GetMinLevelForNewSkill(playerInstance, classId);
                if (minLevel > 0)
                {
                    SystemMessage sm = new SystemMessage(SystemMessageId.DoNotHaveFurtherSkillsToLearnS1);
                    sm.AddNumber(minLevel);
                    await playerInstance.SendPacketAsync(sm);
                }
                else
                {
                    await playerInstance.SendPacketAsync(new SystemMessage(SystemMessageId.NoMoreSkillsToLearn));
                }
            }
            else
            {
                AcquireSkillList acquireSkillList = new AcquireSkillList(AcquireSkillList.SkillType.Usual);
                skillList.ForEach(skill =>
                {
                    Skill sk = Initializer.SkillService().GetSkill(skill.Id, skill.Level);
                    acquireSkillList.AddSkill(skill.Id, skill.Level, skill.Level, skill.SpCost, 0);
                });
                await playerInstance.SendPacketAsync(acquireSkillList);
            }
            
        }
        
        
        public override string GetHtmPath(int npcId, int value)
        {
            string pom = "" + npcId;   
            if (value > 0)
            {
                pom = npcId + "-" + value;
            }
            return "/trainer/" + pom + ".htm";
        }
    }
}