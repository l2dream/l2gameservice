﻿namespace Core.Model.Npc.Type
{
    public class L2DawnPriest : L2SignsPriest
    {
        public L2DawnPriest(int objectId, NpcTemplate template) : base(objectId, template)
        {
        }
        
        public override string GetHtmPath(int npcId, int value)
        {
            return "/seven_signs/dawn_priest_1a.htm";
        }
    }
}