﻿using Core.Model.Actor;
using Core.Model.World.RegionData;

namespace Core.Model.Npc.Type
{
    public class L2Guard : Attackable
    {
        public L2Guard(int objectId, NpcTemplate template) : base(objectId, template)
        {
            
        }
        
        public override string GetHtmPath(int npcId, int value)
        {
            string pom = "" + npcId;   
            if (value > 0)
            {
                pom = npcId + "-" + value;
            }
            return "/guard/" + pom + ".htm";;
        }

        protected override void OnSpawn()
        {
            base.OnSpawn();
            WorldRegionData region = Initializer.WorldInit().GetRegion(GetX(), GetY());
            if ((region != null) && !region.IsActive)
            {
                ((AttackableAi) AI).StopAiTask();
            }
        }
    }
}