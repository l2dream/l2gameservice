﻿using System;

namespace Core.Model.Npc.Type
{
    public class CustomEventArgs : EventArgs
    {
        public string MonsterAction { get; set; }
        public CustomEventArgs(string action)
        {
            MonsterAction = action;
        }
    }
}