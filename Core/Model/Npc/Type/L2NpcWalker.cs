﻿using System.Threading.Tasks;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets.CharacterPackets;

namespace Core.Model.Npc.Type
{
    public class L2NpcWalker : L2Npc
    {
        public L2NpcWalker(int objectId, NpcTemplate template) : base(objectId, template)
        {
            AI = new NpcWalkerAi(this);
        }

        public async Task BroadCastChatAsync(string text)
        {
            var knownPlayers = GetKnownList().GetKnownPlayers();
            if (knownPlayers is null && knownPlayers.IsEmpty)
            {
                return;
            }
            var cs = new CharacterSay(ObjectId, ChatType.General, CharacterName, text);
            foreach (PlayerInstance playerInstance in knownPlayers.Values)
            {
                await playerInstance.SendPacketAsync(cs);
            }
        }

        protected override void OnSpawn()
        {
            base.OnSpawn();
            ((NpcWalkerAi) AI).HomeX = GetX();
            ((NpcWalkerAi) AI).HomeY = GetY();
            ((NpcWalkerAi) AI).HomeZ = GetZ();
        }
    }
}