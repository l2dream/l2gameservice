﻿namespace Core.Model.Npc.Type
{
    public class L2DuskPriest : L2SignsPriest
    {
        public L2DuskPriest(int objectId, NpcTemplate template) : base(objectId, template)
        {
        }
        
        public override string GetHtmPath(int npcId, int value)
        {
            return "/seven_signs/dusk_priest_1a.htm";
        }
    }
}