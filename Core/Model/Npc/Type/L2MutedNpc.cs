﻿namespace Core.Model.Npc.Type
{
    public class L2MutedNpc : L2Npc
    {
        public L2MutedNpc(int objectId, NpcTemplate template) : base(objectId, template)
        {
        }
    }
}