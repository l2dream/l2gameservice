﻿using System.Threading.Tasks;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Npc.Type
{
    public class L2ClanHallDoormen : L2Npc
    {
        public L2ClanHallDoormen(int objectId, NpcTemplate template) : base(objectId, template)
        {
        }
        
        public override async Task OnBypassFeedbackAsync(PlayerInstance playerInstance, string command)
        {
            await base.OnBypassFeedbackAsync(playerInstance, command);
        }

        public override string GetHtmPath(int npcId, int value)
        {
            string filename = "/doorman/" + Template.Stat.NpcId + "-no.htm";
            return filename;
        }
    }
}