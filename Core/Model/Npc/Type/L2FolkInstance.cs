﻿using System.Threading.Tasks;
using Core.Model.Player;

namespace Core.Model.Npc.Type
{
    public class L2FolkInstance : L2Npc
    {
        protected L2FolkInstance(int objectId, NpcTemplate template) : base(objectId, template)
        {
        }

        public override async Task OnActionAsync(PlayerInstance player)
        {
            await base.OnActionAsync(player);
        }
    }
}