﻿namespace Core.Model.Npc.Type
{
    public class L2VillageMasterPriest : L2Npc
    {
        public L2VillageMasterPriest(int objectId, NpcTemplate template) : base(objectId, template)
        {
        }
    }
}