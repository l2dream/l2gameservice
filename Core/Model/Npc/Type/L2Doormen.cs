﻿using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Model.Npc.Type
{
    public class L2Doormen : L2Npc
    {
        public L2Doormen(int objectId, NpcTemplate template) : base(objectId, template)
        {
        }
    }
}