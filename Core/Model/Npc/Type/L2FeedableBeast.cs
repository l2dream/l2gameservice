﻿namespace Core.Model.Npc.Type
{
    public class L2FeedableBeast : L2Monster
    {
        public L2FeedableBeast(int objectId, NpcTemplate template) : base(objectId, template)
        {
        }
    }
}