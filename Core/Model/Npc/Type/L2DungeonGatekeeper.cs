﻿namespace Core.Model.Npc.Type
{
    public class L2DungeonGatekeeper : L2Npc
    {
        public L2DungeonGatekeeper(int objectId, NpcTemplate template) : base(objectId, template)
        {
        }
    }
}