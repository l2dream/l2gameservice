﻿namespace Core.Model.Npc.Type
{
    public class L2Teleporter : L2Npc
    {
        public L2Teleporter(int objectId, NpcTemplate template) : base(objectId, template)
        {
        }

        public override string GetHtmPath(int npcId, int value)
        {
            string pom = "" + npcId;   
            if (value > 0)
            {
                pom = npcId + "-" + value;
            }
            return "/teleporter/" + pom + ".htm";
        }
    }
}