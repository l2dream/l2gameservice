﻿using System;
using System.Threading.Tasks;
using Core.AI;
using Core.Model.Actor;
using Core.Model.Actor.KnownList;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;
using Core.TaskManager;
using L2Logger;

namespace Core.Model.Npc.Type
{
    public class L2Monster : Attackable
    {
        public override bool IsMonster => true;
        public override bool Attackable => true;
        
        public event EventHandler<CustomEventArgs> EventMonster;

        public L2Monster(int objectId, NpcTemplate template) : base(objectId, template)
        {
            
        }
        
        public override WorldObjectKnownList GetKnownList()
        {
            if (!(base.GetKnownList() is MonsterKnownList))
            {
                SetKnownList(new MonsterKnownList(this));
            }
            return (MonsterKnownList) base.GetKnownList();
        }
        
        public override async Task DoDieAsync(Character killer)
        {
            await base.DoDieAsync(killer);
                
            // Stop HP/MP/CP Regeneration task
            Status.StopHpMpRegeneration();
            
            // Notify Creature AI
            AI.NotifyEvent(CtrlEvent.EvtDead, null);
            
            DecayTaskManager.Instance.AddDecayTask(this);
        }

        public override void AddDamageHate(Character attacker, int damage, int aggro)
        {
            if (!(attacker is L2Monster))
            {
                base.AddDamageHate(attacker, damage, aggro);
            }
        }

        public override async Task OnActionAsync(PlayerInstance player)
        {
            if (await IsTargetSelected(player))
            {
                await SetIntentionAttackAsync(player);
                return;
            }
            await base.OnActionAsync(player);
            await ShowTargetInfoAsync(player);
        }

        private async Task SetIntentionAttackAsync(PlayerInstance player)
        {
            await player.SendPacketAsync(new ValidateLocation(this));
            // Check if the player is attackable (without a forced attack) and isn't dead
            if (Status.IsAlikeDead()) return;
            if (Math.Abs(player.GetZ() - GetZ()) < 400) // this max heigth difference might need some tweaking
            {
                // Set the PlayerInstance Intention to AI_INTENTION_ATTACK
                player.AI.SetIntention(CtrlIntention.AiIntentionAttack, this);
                return;
            }
            // Send a Server->Client ActionFailed to the PlayerInstance in order to avoid that the client wait another packet
            await player.SendPacketAsync(new ActionFailed());
            LoggerManager.Info("Do we really need this block???");
        }

        /// <summary>
        /// Send a Server->Client packet MyTargetSelected to the PlayerInstance player
        /// The player.getLevel() - getLevel() permit to display the correct color in the select window
        /// </summary>
        /// <param name="player"></param>
        private async Task ShowTargetInfoAsync(PlayerInstance player)
        {
            await player.SendPacketAsync(new MyTargetSelected(ObjectId, player.Stat.Level - Template.Stat.Level));
            // Send a Server->Client packet StatusUpdate of the NpcInstance to the PlayerInstance to update its HP bar
            StatusUpdate su = new StatusUpdate(ObjectId);
            su.AddAttribute(StatusUpdate.CurHp, (int) Status.GetCurrentHp());
            su.AddAttribute(StatusUpdate.MaxHp, Stat.GetMaxHp());
            await player.SendPacketAsync(su);
        }
        
        public void NotifyMoving()
        {
            // Write some code that does something useful here
            // then raise the event. You can also raise an event
            // before you execute a block of code.
            OnEventMonster(new CustomEventArgs($"{ObjectId} {CharacterName} is moving"));
        }

        protected virtual void OnEventMonster(CustomEventArgs e)
        {
            EventMonster?.Invoke(this, e);
        }

        public bool IsEventMonster()
        {
            return (EventMonster != null);
        }
    }
}