﻿namespace Core.Model.Npc.Type
{
    public class L2Fisherman : L2Npc
    {
        public L2Fisherman(int objectId, NpcTemplate template) : base(objectId, template)
        {
        }
        
        public override string GetHtmPath(int npcId, int value)
        {
            string pom = "" + npcId;   
            if (value > 0)
            {
                pom = npcId + "-" + value;
            }
            return "/fisherman/" + pom + ".htm";;
        }
    }
}