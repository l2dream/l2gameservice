﻿using System.Threading.Tasks;
using Core.Model.Npc.Handlers;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;
using Helpers;

namespace Core.Model.Npc.Type
{
    public class L2Npc : NpcInstance
    {
        private readonly L2NpcChat _npcChat;

        public L2Npc(int objectId, NpcTemplate template) : base(objectId, template)
        {
            _npcChat = new L2NpcChat(this);
        }
        
        public override async Task OnActionAsync(PlayerInstance player)
        {
            if (await IsTargetSelected(player))
            {
                await ShowTalkWindowAsync(player);
                return;
            }
            await base.OnActionAsync(player);
            await ShowTargetInfoAsync(player);
        }

        /// <summary>
        /// Send a Server->Client packet MyTargetSelected to the PlayerInstance player
        /// Send a Server->Client packet SocialAction to the all PlayerInstance on the _knownPlayer of the NpcInstance to display a social action of the NpcInstance on their client
        /// </summary>
        /// <param name="player"></param>
        private async Task ShowTalkWindowAsync(PlayerInstance player)
        {
            await player.SendPacketAsync(new ValidateLocation(this));
            await player.SendPacketAsync(new SocialAction(ObjectId, Rnd.Next(8)));
            await _npcChat.ShowTalkWindowAsync(player, 0);
            // to avoid player stuck
            await player.SendPacketAsync(new ActionFailed());
        }

        private async Task ShowTargetInfoAsync(PlayerInstance player)
        {
            await player.SendPacketAsync(new MyTargetSelected(ObjectId, 0));
            await player.SendPacketAsync(new ValidateLocation(this));
        }
    }
}