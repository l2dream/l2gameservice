﻿using System;

namespace Core.Model.Npc
{
    public sealed class DropData
    {
        public const int MaxChance = 1000000;
	
        public int ItemId { get; set; }
        public int MinDrop { get; set; }
        public int MaxDrop { get; set; }
        public int Chance { get; set; }
        public string QuestId { get; set; }
        public string[] StateId { get; set; }
        
        public bool IsQuestDrop()
        {
            return (QuestId != null) && (StateId != null);
        }

        public override string ToString()
        {
            string str = "ItemID: " + ItemId + " Min: " + MinDrop + " Max: " + MaxDrop + " Chance: " + (Chance / 10000.0) + "%";
            if (IsQuestDrop())
            {
                str += " QuestID: " + QuestId + " StateID's: " + StateId;
            }
            return str;
        }

        public override bool Equals(object obj)
        {
            if (obj is DropData dropData)
            {
                return dropData.ItemId == ItemId;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return ItemId;
        }
    }
}