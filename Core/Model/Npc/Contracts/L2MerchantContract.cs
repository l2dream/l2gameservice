﻿using System.Collections.Generic;

namespace Core.Model.Npc.Contracts
{
    public class L2MerchantContract
    {
        public int ShopId { get; set; }
        public List<Item> Items { get; set; }

        public class Item
        {
            public int ItemId { get; set; }
            public int Price { get; set; }
            public string Name { get; set; }
            public int Count { get; set; }
        }
    }
}