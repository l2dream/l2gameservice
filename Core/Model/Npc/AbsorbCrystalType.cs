﻿namespace Core.Model.Npc
{
    public enum AbsorbCrystalType
    {
        LastHit,
        FullParty,
        PartyOneRandom
    }
}