﻿using System;
using Core.Model.Npc.Handlers;

namespace Core.Model.Npc
{
    public class BypassHandler
    {
        private readonly NpcInstance _npcInstance;

        public BypassHandler(NpcInstance npcInstance)
        {
            _npcInstance = npcInstance;
        }

        public IBypassHandler GetHandler(string command)
        {
            if (command.StartsWith("Buy"))
            {
                return new L2MerchantBuy();
            }
            if (command.StartsWith("Sell"))
            {
                return new L2MerchantSell();
            }
            if (command.StartsWith("Chat"))
            {
                return new L2NpcChat(_npcInstance);
            }
            if (command.StartsWith("goto"))
            {
                return new L2Teleport(_npcInstance);
            }
            throw new NotImplementedException(command);
        }
    }
}