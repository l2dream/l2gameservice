﻿namespace Core.Model
{
    public class ShortCut
    {
        public const int TypeItem = 1;
        public const int TypeSkill = 2;
        public const int TypeAction = 3;
        public const int TypeMacro = 4;
        public const int TypeRecipe = 5;

        public int Slot { get; }
        public int Page { get; }
        public int Type { get; }
        public int Id { get; }
        public int Level { get; }

        public ShortCut(int slotId, int pageId, int shortcutType, int shortcutId, int shortcutLevel)
        {
            Slot = slotId;
            Page = pageId;
            Type = shortcutType;
            Id = shortcutId;
            Level = shortcutLevel;
        }
    }
}