﻿using System;
using System.Threading.Tasks;
using Core.Model.Actor.KnownList;
using Core.Model.Player;
using Core.Model.World.RegionData;
using Core.Network;
using L2Logger;

namespace Core.Model.World
{
    public abstract class WorldObject
    {
        private ObjectPosition _position;
        private WorldObjectKnownList _knownList;
        private bool _visible;
        public int ObjectId { get; } //ObjectId in the World
        
        public virtual bool IsNpc => false;
        public virtual bool IsPlayer => false;
        public virtual bool IsPlayable => false;
        public virtual bool IsSummon => false;
        public virtual bool IsPet => false;
        public virtual bool IsMonster => false;
        public virtual bool IsRaid => false;
        public virtual bool IsItem => false;
        public virtual int Heading { get; set; }

        protected WorldObject(int objectId)
        {
            ObjectId = objectId;
        }
        
        //public abstract bool IsAutoAttackable(Character attacker);
        public abstract Task OnActionAsync(PlayerInstance player);

        public int GetX()
        {
            return WorldObjectPosition().GetX();
        }
	
        public int GetY()
        {
            return WorldObjectPosition().GetY();
        }
	
        public int GetZ()
        {
            return WorldObjectPosition().GetZ();
        }
        
        public void SetXYZ(int x, int y, int z)
        {
            WorldObjectPosition().SetXYZ(x, y, z);
        }
        
        public void SetVisible(bool value)
        {
            _visible = value;
            if (!_visible)
            {
                WorldObjectPosition().SetWorldRegion(null);
            }
        }
        
        public bool IsVisible()
        {
            return WorldObjectPosition().GetWorldRegion() != null;
        }
        
        public WorldRegionData GetWorldRegion()
        {
            return WorldObjectPosition().GetWorldRegion();
        }

        public void SpawnMe(int x, int y, int z)
        {
            lock (this)
            {
                // Set the x,y,z position of the WorldObject spawn and update its _worldregion
                _visible = true;
                if (x > WorldData.MAP_MAX_X)
                {
                    x = WorldData.MAP_MAX_X - 5000;
                }
                if (x < WorldData.MAP_MIN_X)
                {
                    x = WorldData.MAP_MIN_X + 5000;
                }
                if (y > WorldData.MAP_MAX_Y)
                {
                    y = WorldData.MAP_MAX_Y - 5000;
                }
                if (y < WorldData.MAP_MIN_Y)
                {
                    y = WorldData.MAP_MIN_Y + 5000;
                }
                
                WorldObjectPosition().SetWorldPosition(x, y, z);
                var worldRegionData = Initializer.WorldInit().GetRegion(WorldObjectPosition().GetWorldPosition());
                WorldObjectPosition().SetWorldRegion(worldRegionData);
            }
            Initializer.WorldInit().StoreObject(this);
            WorldRegionData worldRegion = WorldObjectPosition().GetWorldRegion();
            
            if (worldRegion != null)
            {
                worldRegion.AddVisibleObject(this);
            }
            else
            {
                LoggerManager.Info("ATTENTION: no region found for location " + x + "," + y + "," + z + ". It's not possible to spawn object " + ObjectId + " here...");
                return;
            }
            Initializer.WorldInit().AddVisibleObject(this, worldRegion, null);
            
            //WorldInit.Instance.Add
            OnSpawn();
        }
        
        protected virtual void OnSpawn()
        {
        }
        
        public ObjectPosition WorldObjectPosition()
        {
            if (_position == null)
            {
                _position = new ObjectPosition(this);
            }
            return _position;
        }
        
        public virtual WorldObjectKnownList GetKnownList()
        {
            if (_knownList == null)
            {
                _knownList = new WorldObjectKnownList(this);
            }
            return _knownList;
        }
        
        public void SetKnownList(WorldObjectKnownList value)
        {
            _knownList = value;
        }

        public virtual Task SendPacketAsync(ServerPacket serverPacket)
        {
            return Task.FromResult(1);
        }
        public virtual Task SendActionFailedPacketAsync()
        {
            return Task.CompletedTask;
        }
        /// <summary>
        /// TODO Deprecated
        /// </summary>

        public void DecayMe()
        {
            // Remove the WorldObject from the world
            _visible = false;
            Initializer.WorldInit().RemoveVisibleObject(this, WorldObjectPosition().GetWorldRegion());
            Initializer.WorldInit().RemoveObject(this);
            WorldObjectPosition().SetWorldRegion(null);
        }
        
        public double CalculateDistanceSq2D(int x, int y, int z)
        {
            return Math.Pow(x - GetX(), 2) + Math.Pow(y - GetY(), 2);
        }
    }
}