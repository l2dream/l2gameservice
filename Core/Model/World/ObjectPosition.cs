﻿using System;
using Core.Model.World.RegionData;
using L2Logger;

namespace Core.Model.World
{
    public class ObjectPosition
    {
        private readonly WorldObject _activeObject;
        private Location _worldPosition;

        private WorldRegionData _worldRegion; // Object localization : Used for items/chars that are seen in the world
        public int Heading { get; set; } 
        public ObjectPosition(WorldObject activeObject)
        {
            _activeObject = activeObject;
        }
        
        public int GetX()
        {
            return GetWorldPosition().GetX();
        }
        
        public int GetY()
        {
            return GetWorldPosition().GetY();
        }
        
        public int GetZ()
        {
            return GetWorldPosition().GetZ();
        }

        public void SetXYZ(int x, int y, int z)
        {
            SetWorldPosition(x, y, z);

            try
            {
                if (Initializer.WorldInit().GetRegion(GetWorldPosition()) != GetWorldRegion())
                {
                    UpdateWorldRegion();
                }
            }
            catch (Exception)
            {
                LoggerManager.Info("Object Id at bad coords: (x: " + GetWorldPosition().GetX() + ", y: " + GetWorldPosition().GetY() + ", z: " + GetWorldPosition().GetZ() + ").");
            }
        }

        public void UpdateWorldRegion()
        {
            if (!_activeObject.IsVisible())
            {
                return;
            }
            WorldRegionData newRegion = Initializer.WorldInit().GetRegion(GetWorldPosition());
            if (newRegion != GetWorldRegion())
            {
                GetWorldRegion().RemoveVisibleObject(_activeObject);
                SetWorldRegion(newRegion);
                // Add the L2Oject spawn to _visibleObjects and if necessary to _allplayers of its WorldRegion
                GetWorldRegion().AddVisibleObject(_activeObject);
            }
        }
        
        public Location GetWorldPosition()
        {
            if (_worldPosition == null)
            {
                _worldPosition = new Location(0, 0, 0);
            }
            return _worldPosition;
        }
        
        public void SetXYZInvisible(int x, int y, int z)
        {
            if (x > WorldData.MAP_MAX_X)
            {
                x = WorldData.MAP_MAX_X - 5000;
            }
		
            if (x < WorldData.MAP_MIN_X)
            {
                x = WorldData.MAP_MIN_X + 5000;
            }
		
            if (y > WorldData.MAP_MAX_Y)
            {
                y = WorldData.MAP_MAX_Y - 5000;
            }
		
            if (y < WorldData.MAP_MIN_Y)
            {
                y = WorldData.MAP_MIN_Y + 5000;
            }
		
            SetWorldPosition(x, y, z);
            _activeObject.SetVisible(false);
        }
        
        public void SetWorldRegion(WorldRegionData value)
        {
            _worldRegion = value;
        }
        
        public void SetWorldPosition(int x, int y, int z)
        {
            WorldPosition().SetXYZ(x, y, z);
        }
        
        public WorldRegionData GetWorldRegion()
        {
            return _worldRegion;
        }
        
        public Location WorldPosition()
        {
            if (_worldPosition == null)
            {
                _worldPosition = new Location(0, 0, 0);
            }
            return _worldPosition;
        }
    }
}