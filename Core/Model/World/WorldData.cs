﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Core.Model.Actor;
using Core.Model.Actor.KnownList;
using Core.Model.Npc.Type;
using Core.Model.Player;
using Core.Model.World.RegionData;
using L2Logger;

namespace Core.Model.World
{
    public abstract class WorldData
    {
        public int SHIFT_BY = 12;

        // Geodata min/max tiles
        public int TILE_X_MIN = 16;
        public int TILE_X_MAX = 26;
        public int TILE_Y_MIN = 10;
        public int TILE_Y_MAX = 25;

        // Map dimensions
        public int TILE_SIZE = 32768;
        public static int MAP_MIN_X;
        public static int MAP_MAX_X;
        public static int MAP_MIN_Y;
        public static int MAP_MAX_Y;

        /** calculated offset used so top left region is 0,0. */
        protected int REGIONS_X;

        /** The Constant REGIONS_Y. */
        protected int REGIONS_Y;

        /** calculated offset used so top left region is 0,0. */
        public int OFFSET_X;

        /** The Constant OFFSET_Y. */
        public int OFFSET_Y;

        /** The _world regions. */
        protected WorldRegionData[,] _worldRegions;

        public WorldRegionData[,] AllWorldRegions => _worldRegions;
        
        /// <summary>
        /// containing all the players in game
        /// </summary>
        private static ConcurrentDictionary<string, PlayerInstance> _allPlayers;
        
        /// <summary>
        /// containing all visible objects
        /// </summary>
        private static ConcurrentDictionary<int, WorldObject> _allObjects;

        public WorldData()
        {
            _allObjects = new ConcurrentDictionary<int, WorldObject>();
            _allPlayers = new ConcurrentDictionary<string, PlayerInstance>();
        }
        
        public WorldRegionData GetRegion(Location location)
        {
            return _worldRegions[(location.GetX() >> SHIFT_BY) + OFFSET_X,(location.GetY() >> SHIFT_BY) + OFFSET_Y];
        }
        
        public PlayerInstance GetPlayer(int playerObjectId)
        {
            return _allPlayers.Values.FirstOrDefault(actual => actual.ObjectId == playerObjectId);
        }
        
        public WorldRegionData GetRegion(int x, int y)
        {
            return _worldRegions[(x >> SHIFT_BY) + OFFSET_X,(y >> SHIFT_BY) + OFFSET_Y];
        }
        
        /// <summary>
        /// Add WorldObject object in _allObjects.
        /// </summary>
        /// <param name="worldObject"></param>
        public void StoreObject(WorldObject worldObject)
        {
            _allObjects.TryAdd(worldObject.ObjectId, worldObject);
        }
        
        /// <summary>
        /// Remove WorldObject object from _allObjects of World
        /// </summary>
        /// <param name="worldObject"></param>
        public void RemoveObject(WorldObject worldObject)
        {
            _allObjects.Remove(worldObject.ObjectId, value: out _);
        }

        /// <summary>
        /// Removes the objects
        /// </summary>
        /// <param name="worldObjects"></param>
        public void RemoveObjects(List<WorldObject> worldObjects)
        {
            worldObjects.ForEach(worldObject => _allObjects.Remove(worldObject.ObjectId, value: out _));
        }

        /// <summary>
        /// Removes the objects
        /// </summary>
        /// <param name="worldObjects"></param>
        public void RemoveObjects(WorldObject[] worldObjects)
        {
            foreach (WorldObject worldObject in worldObjects)
            {
                _allObjects.Remove(worldObject.ObjectId, value: out _);
            }
        }

        /// <summary>
        /// Add a WorldObject in the world.
        /// </summary>
        /// <param name="worldObject"></param>
        /// <param name="newRegion"></param>
        /// <param name="dropper"></param>
        public void AddVisibleObject(WorldObject worldObject, WorldRegionData newRegion, Character dropper)
        {
            try
            {
                if (worldObject is PlayerInstance playerInstance)
                {
                    // just kick the player previous instance
                    PlayerInstance tmp;
                    if (_allPlayers.TryGetValue(playerInstance.CharacterName.ToLower(), out tmp) && tmp != playerInstance)
                    {
                        //tmp.Store(); // Store character and items
                        //tmp.Logout();
                        playerInstance.Controller.GameServiceHelper.CurrentPlayer =
                                null; // prevent deleteMe from being called a second time on disconnection
                    }

                    if (!newRegion.IsActive)
                    {
                        return;
                    }

                    // Go through the visible objects contained in the circular area
                    foreach (var wo in GetVisibleObjects(worldObject, 2000).Where(wo => wo != null))
                    {
                        // Add the object in WorldObjectHashSet(WorldObject) _knownObjects of the visible Creature according to conditions :
                        // - Creature is visible
                        // - object is not already known
                        // - object is in the watch distance
                        // If WorldObject is a PlayerInstance, add WorldObject in WorldObjectHashSet(PlayerInstance) _knownPlayer of the visible Creature
                        wo.GetKnownList().AddKnownObject(worldObject);
                        if (wo is L2Monster l2Monster)
                        {
                            if (worldObject is PlayerInstance player)
                            {
                                player.SubscribeMonster(l2Monster);
                            }
                        }

                        // Add the visible WorldObject in WorldObjectHashSet(WorldObject) _knownObjects of the object according to conditions
                        // If visible WorldObject is a PlayerInstance, add visible WorldObject in WorldObjectHashSet(PlayerInstance) _knownPlayer of the object
                        worldObject.GetKnownList().AddKnownObject(wo);
                    }

                    lock (_allPlayers)
                    {
                        _allPlayers.TryAdd(playerInstance.CharacterName.ToLower(), playerInstance);
                    }
                }

                // Go through the visible objects contained in the circular area
                foreach (WorldObject wo in GetVisibleObjects(worldObject, 2000))
                {
                    wo.GetKnownList().AddKnownObject(worldObject, dropper);
                    worldObject.GetKnownList().AddKnownObject(wo, dropper);
                }
            }
            catch (Exception exception)
            {
                LoggerManager.Info(exception.Message);
            }
        }
        
        /// <summary>
        /// Return all visible objects of the WorldRegions in the circular area (radius) centered on the object
        /// </summary>
        /// <param name="worldObject"></param>
        /// <param name="radius"></param>
        /// <returns></returns>
        public List<WorldObject> GetVisibleObjects(WorldObject worldObject, int radius)
        {
            if ((worldObject == null))
            {
                return new List<WorldObject>();
            }
            
            WorldRegionData region = worldObject.GetWorldRegion();
            
            int x = worldObject.GetX();
            int y = worldObject.GetY();
            int sqRadius = radius * radius;
            
            // Create a list in order to contain all visible WorldObject
            List<WorldObject> result = new List<WorldObject>();
            // Go through the list of region
            foreach (WorldRegionData worldRegion in region.GetSurroundingRegions())
            {
                foreach (var wo in worldRegion.GetVisibleObjects())
                {
                    if (wo == null)
                    {
                        continue;
                    }
                    if (wo.Equals(worldObject))
                    {
                        continue; // skip our own character
                    }
                    int x1 = wo.GetX();
                    int y1 = wo.GetY();
                    double dx = x1 - x;
                    double dy = y1 - y;
				
                    // If the visible object is inside the circular area add the object to the list result
                    if (((dx * dx) + (dy * dy)) < sqRadius)
                    {
                        result.Add(wo);
                    }
                }
            }

            return result;
        }
        
        public List<WorldObject> GetVisibleObjects(WorldObject worldObject)
        {
            if (worldObject == null)
            {
                return new List<WorldObject>();
            }
		
            WorldRegionData region = worldObject.GetWorldRegion();
            if (region == null)
            {
                return new List<WorldObject>();
            }
		
            // Create a list in order to contain all visible WorldObject
            List<WorldObject> result = new List<WorldObject>();
		
            // Go through the list of region
            foreach (WorldRegionData worldRegion in region.GetSurroundingRegions())
            {
                foreach (WorldObject wo in worldRegion.GetVisibleObjects())
                {
                    if (wo == null)
                    {
                        continue;
                    }
				
                    if (wo.Equals(worldObject))
                    {
                        continue; // skip our own character
                    }
				
                    if (!wo.IsVisible())
                    {
                        continue; // skip dying objects
                    }
				
                    result.Add(wo);
                }
            }
            return result;
        }
        
        /// <summary>
        /// Remove a WorldObject from the world
        /// </summary>
        /// <param name="worldObject"></param>
        /// <param name="oldRegion"></param>
        public void RemoveVisibleObject(WorldObject worldObject, WorldRegionData oldRegion)
        {
            if ((worldObject == null) || (oldRegion == null))
            {
                return;
            }
            oldRegion.RemoveVisibleObject(worldObject);

            foreach (WorldRegionData worldRegion in oldRegion.GetSurroundingRegions())
            {
                foreach (WorldObject wo in worldRegion.GetVisibleObjects())
                {
                    // Remove the WorldObject from the WorldObjectHashSet(WorldObject) _knownObjects of the surrounding WorldRegion Creatures
                    // If object is a PlayerInstance, remove the WorldObject from the WorldObjectHashSet(PlayerInstance) _knownPlayer of the surrounding WorldRegion Creatures
                    // If object is targeted by one of the surrounding WorldRegion Creatures, cancel ATTACK and cast
                    if ((wo != null) && (wo.GetKnownList() != null))
                    {
                        wo.GetKnownList().RemoveKnownObject(worldObject);
                    }
				
                    // Remove surrounding WorldRegion Creatures from the WorldObjectHashSet(WorldObject) _KnownObjects of object
                    // If surrounding WorldRegion Creatures is a PlayerInstance, remove it from the WorldObjectHashSet(PlayerInstance) _knownPlayer of object
                    //
                    if (worldObject.GetKnownList() != null)
                    {
                        worldObject.GetKnownList().RemoveKnownObject(wo);
                    }
                }
            }

            // If object is a Creature :
            // Remove all WorldObject from WorldObjectHashSet(WorldObject) containing all WorldObject detected by the Creature
            // Remove all PlayerInstance from WorldObjectHashSet(PlayerInstance) containing all player ingame detected by the Creature
            worldObject.GetKnownList().RemoveAllKnownObjects();
        }

        public List<PlayerInstance> GetVisiblePlayers(WorldObject worldObject)
        {
            WorldRegionData region = worldObject.GetWorldRegion();
            if (region == null)
            {
                return new List<PlayerInstance>();
            }
		
            // Create a list in order to contain all visible WorldObject
            List<PlayerInstance> result = new List<PlayerInstance>();
		
            // Go through the list of region
            foreach (WorldRegionData worldRegion in region.GetSurroundingRegions())
            {
                // Go through visible object of the selected region
                foreach (PlayerInstance playable in worldRegion.GetAllPlayers())
                {
                    if (playable == null)
                    {
                        continue;
                    }
				
                    if (playable.Equals(worldObject))
                    {
                        continue; // skip our own character
                    }
				
                    if (!playable.IsVisible())
                    {
                        continue; // skip dying objects
                    }
                    result.Add(playable);
                }
            }
            return result;
        }
        
        
        public void RemoveFromAllPlayers(PlayerInstance player)
        {
            if ((player != null))
            {
                _allPlayers.TryRemove(player.CharacterName.ToLower(), out _);
            }
        }
        
        public WorldObject FindObject(in int objectId)
        {
            return _allObjects[objectId];
        }
        
    }
}
