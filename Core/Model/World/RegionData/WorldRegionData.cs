﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Model.Actor;
using Core.Model.Player;
using Core.Model.Zones;

namespace Core.Model.World.RegionData
{
    public class WorldRegionData
    {
        
        private int _regionX;
        private int _regionY;

        private bool _active;

        private readonly ConcurrentDictionary<int, WorldObject> _visibleObjects;
        private readonly ConcurrentDictionary<int, PlayerInstance> _playerObjects;
        
        private readonly CancellationTokenSource _cancelTokenSource = new CancellationTokenSource();
        
        private Task _neighborsTask = null;

        private WorldRegionData[] _surroundingRegions;
        private ZoneManager _zoneManager;

        public bool IsActive => _active;
        public bool Active
        {
            set
            {
                if (_active.Equals(value))
                {
                    return;
                }
                _active = value;
            }
        }
        
        public WorldRegionData(int regionX, int regionY)
        {
            _regionX = regionX;
            _regionY = regionY;
            _visibleObjects = new ConcurrentDictionary<int, WorldObject>();
            _playerObjects = new ConcurrentDictionary<int, PlayerInstance>();
            _zoneManager = new ZoneManager();
        }

        public void SetSurroundingRegions(WorldRegionData[] regions)
        {
            _surroundingRegions = regions;
        }
        
        public WorldRegionData[] GetSurroundingRegions()
        {
            return _surroundingRegions;
        }
        
        public void AddVisibleObject(WorldObject worldObject)
        {
            if (worldObject == null)
            {
                return;
            }
            _visibleObjects.TryAdd(worldObject.ObjectId, worldObject);
            
            if (worldObject is PlayerInstance playerInstance)
            {
                _playerObjects.TryAdd(playerInstance.ObjectId, playerInstance);
                // if this is the first player to enter the region, activate self & neighbors
                if ((_playerObjects.Count == 1))
                {
                    StartActivation();
                }
            }
        }
        
        private void StartActivation()
        {
            // First set self to active and do self-tasks...
            Active = true;
            lock (this)
            {
                
                CancellationToken token = _cancelTokenSource.Token;
                
                if (_neighborsTask != null)
                {
                    _cancelTokenSource.Cancel();
                    _neighborsTask = null;
                }
                _neighborsTask = Task.Run(
                    () =>
                    {
                        if (token.IsCancellationRequested)
                        {
                            return;
                        }
                        //TODO move to config Config.GRID_NEIGHBOR_TURNON_TIME
                        Task.Delay(1000 * 30, token);
                        
                        foreach (WorldRegionData l2WorldRegion in _surroundingRegions)
                        {
                            l2WorldRegion.Active = true;
                        }
                    }, token);
            }
        }
        
        private void StartDeactivation()
        {
            lock (this)
            {
                CancellationToken token = _cancelTokenSource.Token;
                
                if (_neighborsTask != null)
                {
                    _cancelTokenSource.Cancel();
                    _neighborsTask = null;
                }
                
                // Start a timer to "suggest" a deactivate to self and neighbors.
                // Suggest means: first check if a neighbor has PlayerInstances in it. If not, deactivate.
                _neighborsTask = Task.Run(
                    () =>
                    {
                        if (token.IsCancellationRequested)
                        {
                            return;
                        }
                        //TODO move to config Config.GRID_NEIGHBOR_TURNON_TIME
                        Task.Delay(1000 * 30, token);
                        
                        foreach (var l2WorldRegion in _surroundingRegions.Where(l2WorldRegion => l2WorldRegion.AreEmptyNeighbors()))
                        {
                            l2WorldRegion.Active = false;
                        }
                    }, token);
            }
        }
        
        public bool AreEmptyNeighbors()
        {
            return _surroundingRegions.All(neighbor => neighbor._active);
        }
        
        public void RemoveVisibleObject(WorldObject worldObject)
        {
            if (worldObject == null)
            {
                return;
            }
		
            _visibleObjects.TryRemove(worldObject.ObjectId, out worldObject);
		
            if (worldObject is PlayerInstance playable)
            {
                _playerObjects.TryRemove(worldObject.ObjectId, out playable);
                
                if (_playerObjects.Count == 0)
                {
                    StartDeactivation();
                }
            }
        }
        
        public IEnumerable<WorldObject> GetVisibleObjects()
        {
            return _visibleObjects.Values;
        }
        
        public IEnumerable<PlayerInstance> GetAllPlayers()
        {
            return _playerObjects.Values;
        }
        
        public void AddZone(ZoneType zone)
        {
            _zoneManager.RegisterNewZone(zone);
        }
        
        public void RevalidateZones(Character character)
        {
            _zoneManager?.RevalidateZones(character);
        }
        
        public void RemoveFromZones(Character character)
        {
            _zoneManager?.RemoveCharacter(character);
        }
        
        public void OnDeath(Character character)
        {
            _zoneManager?.OnDeath(character);
        }
        
        public void OnRevive(Character character)
        {
            _zoneManager?.OnRevive(character);
        }
    }
}
