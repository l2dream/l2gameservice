﻿namespace Core.Model.World.RegionData
{
    public enum TeleportWhereType
    {
        Castle,
        ClanHall,
        SiegeFlag,
        Town
    }
}