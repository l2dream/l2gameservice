﻿using System.Linq;
using Core.Model.Actor;
using Core.Model.Player;
using Core.Model.Zones.Type;

namespace Core.Model.World.RegionData
{
    public class MapRegionData
    {
        protected const int RegionsX = 19;
        protected const int RegionsY = 21;

        protected readonly int[,] Regions = new int[RegionsX, RegionsY];

        public int GetMapRegion(int posX, int posY)
        {
            return Regions[GetMapRegionX(posX),GetMapRegionY(posY)];
        }

        public int GetMapRegionX(int posX)
        {
            // +4 to shift coords to center
            return (posX >> 15) + 4;
        }

        public int GetMapRegionY(int posY)
        {
            // +10 to shift coords to center
            return (posY >> 15) + 10;
        }
        
        public Location GetTeleToLocation(Character character, TeleportWhereType teleportWhere)
        {
            if (!(character is PlayerInstance))
            {
                return GetClosestTown(character.GetX(), character.GetY()).GetSpawnLoc();
            }

            PlayerInstance player = (PlayerInstance)character;
            var dd = GetClosestTown(player.Template.Stat.ClassId.ClassRace, player.GetX(), player.GetY());
            return dd.GetSpawnLoc();
        }

        private TownZone GetClosestTown(ClassRace race, int x, int y)
        {
            switch (GetMapRegion(x, y))
            {
                case 0: // TI
                    return GetTown(2);

                case 1: // Elven
                    return GetTown(race == ClassRace.DarkElf ? 1 : 3);

                case 2: // DE
                    return GetTown(race == ClassRace.Elf ? 3 : 1);

                case 3: // Orc
                    return GetTown(4);

                case 4: // Dwarven
                    return GetTown(6);

                case 5: // Gludio
                    return GetTown(7);

                case 6: // Gludin
                    return GetTown(5);

                case 7: // Dion
                    return GetTown(8);

                case 8: // Giran
                case 12: // Giran Harbor
                    return GetTown(9);

                case 9: // Oren
                    return GetTown(10);

                case 10: // Aden
                    return GetTown(12);

                case 11: // HV
                    return GetTown(11);

                case 13: // Heine
                    return GetTown(15);

                case 14: // Rune
                    return GetTown(14);

                case 15: // Goddard
                    return GetTown(13);

                case 16: // Schuttgart
                    return GetTown(17);

                case 17: // Floran
                    return GetTown(16);

                case 18: // Primeval Isle
                    return GetTown(19);
            }

            return GetTown(16); // Default to floran
        }

        private TownZone GetClosestTown(int x, int y)
        {
            switch (GetMapRegion(x, y))
            {
                case 0: // TI
                    return GetTown(2);

                case 1: // Elven
                    return GetTown(3);

                case 2: // DE
                    return GetTown(1);

                case 3: // Orc
                    return GetTown(4);

                case 4: // Dwarven
                    return GetTown(6);

                case 5: // Gludio
                    return GetTown(7);

                case 6: // Gludin
                    return GetTown(5);

                case 7: // Dion
                    return GetTown(8);

                case 8: // Giran
                case 12: // Giran Harbor
                    return GetTown(9);

                case 9: // Oren
                    return GetTown(10);

                case 10: // Aden
                    return GetTown(12);

                case 11: // HV
                    return GetTown(11);

                case 13: // Heine
                    return GetTown(15);

                case 14: // Rune
                    return GetTown(14);

                case 15: // Goddard
                    return GetTown(13);

                case 16: // Schuttgart
                    return GetTown(17);

                case 17: // Floran
                    return GetTown(16);

                case 18: // Primeval Isle
                    return GetTown(19);
            }

            return GetTown(16); // Default to floran
        }

        private TownZone GetTown(int townId)
        {
            var townZones = Initializer.ZoneService().GetAllZones<TownZone>(typeof(TownZone));
            TownZone zone = townZones.Cast<TownZone>().SingleOrDefault(t => t.TownId == townId);
            return zone;
        }
    }
}
