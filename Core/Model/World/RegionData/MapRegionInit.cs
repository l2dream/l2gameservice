﻿using System;
using System.Xml;
using L2Logger;

namespace Core.Model.World.RegionData
{
    public sealed class MapRegionInit : MapRegionData
    {
        public MapRegionInit()
        {
            Initialize();
        }
        private void Initialize()
        {
            int count = 0;
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(@"StaticData/Xml/MapRegions.xml");
                if (doc.DocumentElement == null)
                {
                    return;
                }

                XmlNodeList nodes = doc.DocumentElement.SelectNodes("/list/map");
                if (nodes != null)
                {
                    foreach (XmlNode node in nodes)
                    {
                        XmlElement ownerElement = node.Attributes?[0].OwnerElement;
                        if (ownerElement == null || node.Attributes == null || !ownerElement.Name.Equals("map"))
                        {
                            continue;
                        }

                        XmlNamedNodeMap attrs = node.Attributes;
                        int id = Convert.ToInt32(attrs.GetNamedItem("id").Value);
                        Regions[0, id] = Convert.ToInt32(attrs.GetNamedItem("region1").Value);
                        Regions[1, id] = Convert.ToInt32(attrs.GetNamedItem("region2").Value);
                        Regions[2, id] = Convert.ToInt32(attrs.GetNamedItem("region3").Value);
                        Regions[3, id] = Convert.ToInt32(attrs.GetNamedItem("region4").Value);
                        Regions[4, id] = Convert.ToInt32(attrs.GetNamedItem("region5").Value);
                        Regions[5, id] = Convert.ToInt32(attrs.GetNamedItem("region6").Value);
                        Regions[6, id] = Convert.ToInt32(attrs.GetNamedItem("region7").Value);
                        Regions[7, id] = Convert.ToInt32(attrs.GetNamedItem("region8").Value);
                        Regions[8, id] = Convert.ToInt32(attrs.GetNamedItem("region9").Value);
                        Regions[9, id] = Convert.ToInt32(attrs.GetNamedItem("region10").Value);

                        count++;
                    }
                }
            } catch (Exception ex)
            {
                LoggerManager.Error($"Loaded {this.GetType().Name} regions. {ex.Message}");
            }
        }
    }
}
