﻿using System;
using System.Collections.Generic;
using System.Xml;
using L2Logger;

namespace Core.Model.World
{
    public sealed class ExperienceDataInit
    {
        private byte _maxLevel;
        private byte _maxPetLevel;
        private readonly IDictionary<int, long> _expTable;
        
        public ExperienceDataInit()
        {
            _expTable = new Dictionary<int, long>();
            Initialize();
        }

        private void Initialize()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(@"StaticData/Xml/Stats/Experience.xml");
                if (doc.DocumentElement == null)
                {
                    return;
                }
                XmlNode firstNode = doc.DocumentElement.SelectSingleNode("/table");
                if (firstNode != null)
                {
                    XmlAttributeCollection firstNodeAttributes = firstNode.Attributes;
                    if (firstNodeAttributes != null)
                        _maxLevel = Convert.ToByte(firstNodeAttributes.GetNamedItem("maxLevel").Value);
                    if (firstNodeAttributes != null)
                        _maxPetLevel = Convert.ToByte(firstNodeAttributes.GetNamedItem("maxPetLevel").Value);
                }

                XmlNodeList nodes = doc.DocumentElement.SelectNodes("/table/experience");
                if (nodes != null)
                {
                    foreach (XmlNode node in nodes)
                    {
                        XmlElement ownerElement = node.Attributes?[0].OwnerElement;
                        if (ownerElement == null || node.Attributes == null || !ownerElement.Name.Equals("experience"))
                        {
                            continue;
                        }
                        XmlNamedNodeMap attrs = node.Attributes;
                        byte level = Convert.ToByte(attrs.GetNamedItem("level").Value);
                        long toNextLevel = Convert.ToInt64(attrs.GetNamedItem("tolevel").Value);
                        _expTable.Add(level, toNextLevel);
                    }
                }
                LoggerManager.Info(GetType().Name + ": Loaded " + _expTable.Count + " levels");
                LoggerManager.Info(GetType().Name + ": Max Player Level is: " + (_maxLevel - 1));
                LoggerManager.Info(GetType().Name + ": Max Pet Level is: " + (_maxPetLevel - 1));
            }
            catch (Exception ex)
            {
                LoggerManager.Error(GetType().Name + " " + ex.Message);
            }
        }
        public long GetExpForLevel(int level)
        {
            return _expTable[level];
        }
        
        public byte GetMaxLevel()
        {
            return _maxLevel;
        }
        
        public byte GetMaxPetLevel()
        {
            return _maxPetLevel;
        }
    }
}