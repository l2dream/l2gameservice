﻿using System;
using System.Collections.Generic;
using Core.Model.World.RegionData;
using L2Logger;

namespace Core.Model.World
{
    public sealed class WorldInit : WorldData
    {

        public WorldInit()
        {
            MAP_MIN_X = (TILE_X_MIN - 20) * TILE_SIZE;
            MAP_MAX_X = (TILE_X_MAX - 19) * TILE_SIZE;
            MAP_MIN_Y = (TILE_Y_MIN - 18) * TILE_SIZE;
            MAP_MAX_Y = (TILE_Y_MAX - 17) * TILE_SIZE;

            OFFSET_X = Math.Abs(MAP_MIN_X >> SHIFT_BY);
            OFFSET_Y = Math.Abs(MAP_MIN_Y >> SHIFT_BY);

            REGIONS_X = (MAP_MAX_X >> SHIFT_BY) + OFFSET_X;
            REGIONS_Y = (MAP_MAX_Y >> SHIFT_BY) + OFFSET_Y;
            Initialize();
        }

        private void Initialize()
        {
            _worldRegions = new WorldRegionData[REGIONS_X + 1,REGIONS_Y + 1];
            for (int x = 0; x <= REGIONS_X; x++)
            {
                for (int y = 0; y <= REGIONS_Y; y++)
                {
                    _worldRegions[x,y] = new WorldRegionData(x, y);
                }
            }

            //TODO
            // Set surrounding regions.
            for (int rx = 0; rx <= REGIONS_X; rx++)
            {
                for (int ry = 0; ry <= REGIONS_Y; ry++)
                {
                    List<WorldRegionData> surroundingRegions = new List<WorldRegionData>();
                    for (int sx = rx - 1; sx <= (rx + 1); sx++)
                    {
                        for (int sy = ry - 1; sy <= (ry + 1); sy++)
                        {
                            if (((sx >= 0) && (sx < REGIONS_X) && (sy >= 0) && (sy < REGIONS_Y)))
                            {
                                surroundingRegions.Add(_worldRegions[sx,sy]);
                            }
                        }
                    }
                    WorldRegionData[] regionArray = new WorldRegionData[surroundingRegions.Count];
                    _worldRegions[rx,ry].SetSurroundingRegions(surroundingRegions.ToArray());
                }
            }
            LoggerManager.Info("World: (" + REGIONS_X + "x" + REGIONS_Y + ") World Region Grid set up.");
        }
    }
}
