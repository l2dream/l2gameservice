﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Model.Actor;
using Core.Model.InventoryListener;
using Core.Model.InventoryListener.Data;
using Core.Model.Items;
using Core.Model.Items.Type;
using Core.Model.Player;
using L2Logger;

namespace Core.Model
{
    public abstract class Inventory : ItemContainer 
    {

        public const int PaperdollUnder = 0;
        public const int PaperdollLear = 1;
        public const int PaperdollRear = 2;
        public const int PaperdollNeck = 3;
        public const int PaperdollLfinger = 4;
        public const int PaperdollRfinger = 5;
        public const int PaperdollHead = 6;
        public const int PaperdollRhand = 7;
        public const int PaperdollLhand = 8;
        public const int PaperdollGloves = 9;
        public const int PaperdollChest = 10;
        public const int PaperdollLegs = 11;
        public const int PaperdollFeet = 12;
        public const int PaperdollBack = 13;
        public const int PaperdollLRHand = 14;
        public const int PaperdollFace = 15;
        public const int PaperdollHair = 16;
        public const int PaperdollDhair = 17;
	
        public const int PaperdollTotalslots = 18;

        // Speed percentage mods
        public const double MaxArmorWeight = 12000;
        
        private readonly ItemInstance[] _paperdoll;
        private readonly List<IPaperdollListener> _paperdollListeners;
        private int _wearedMask;
        
        protected Character Character { get; set; }

        protected Inventory(Character character)
        {
            Character = character;
            _paperdoll = new ItemInstance[0x12];
            _paperdollListeners = new List<IPaperdollListener>();
            AddPaperdollListener(new StatsListener(Character));
        }
        
        protected abstract ItemLocation GetEquipLocation();
        
        public ItemInstance GetPaperdollItem(int slot)
        {
            return _paperdoll[slot];
        }
        
        public int GetPaperdollObjectId(int slot)
        {
            ItemInstance item = _paperdoll[slot];
            if (item != null)
            {
                return item.ObjectId;
            }

            if (slot == PaperdollHair)
            {
                item = _paperdoll[PaperdollDhair];
                if (item != null)
                {
                    return item.ObjectId;
                }
            }
            return 0;
        }
        
        public int GetPaperdollItemId(int slot)
        {
            ItemInstance item = _paperdoll[slot];
            if (item != null)
            {
                return item.ItemId;
            }
            if (slot == PaperdollHair)
            {
                item = _paperdoll[PaperdollDhair];
                if (item != null)
                {
                    return item.ItemId;
                }
            }
            return 0;
        }
        
        public List<ItemInstance> EquipItemAndRecord(ItemInstance item)
        {
            ChangeRecorder recorder = NewRecorder();
            try
            {
                EquipItem(item);
            }
            catch (Exception ex)
            {
                LoggerManager.Error(ex.Message + nameof(Inventory));
            }
            finally
            {
                RemovePaperdollListener(recorder);
            }
            return recorder.GetChangedItems();
        }

        /// <summary>
        /// Equips item in slot of paperdoll
        /// </summary>
        /// <param name="item"></param>
        public void EquipItem(ItemInstance item)
        {
            if (!(GetOwner() is PlayerInstance))
            {
                return;
            }
            ItemSlotId targetSlot = item.Item.BodyPart;
            switch (targetSlot)
            {
                case ItemSlotId.SlotNone:
                    break;
                case ItemSlotId.SlotUnderwear:
                    SetPaperdollItem(PaperdollUnder, item);
                    break;
                case ItemSlotId.SlotREar:
                    break;
                case ItemSlotId.SlotLEar:
                    break;
                case ItemSlotId.SlotNeck:
                    SetPaperdollItem(PaperdollNeck, item);
                    break;
                case ItemSlotId.SlotRFinger:
                    break;
                case ItemSlotId.SlotLFinger:
                    break;
                case ItemSlotId.SlotHead:
                    SetPaperdollItem(PaperdollHead, item);
                    break;
                case ItemSlotId.SlotRHand:
                    if (_paperdoll[PaperdollLRHand] != null)
                    {
                        SetPaperdollItem(PaperdollLRHand, null);
                        SetPaperdollItem(PaperdollLhand, null);
                        SetPaperdollItem(PaperdollRhand, null);
                    }
                    else
                    {
                        SetPaperdollItem(PaperdollRhand, null);
                    }
                    SetPaperdollItem(PaperdollRhand, item);
                    break;
                case ItemSlotId.SlotLHand:
                    if (!(item.Item is EtcItem etcItem) || (etcItem.EtcItemType != EtcItemType.GetEtcItemTypeId("Arrow")))
                    {
                        ItemInstance old1 = SetPaperdollItem(PaperdollLRHand, null);
                        if (old1 != null)
                        {
                            SetPaperdollItem(PaperdollRhand, null);
                        }
                    }
                    SetPaperdollItem(PaperdollLhand, null);
                    SetPaperdollItem(PaperdollLhand, item);
                    break;
                case ItemSlotId.SlotGloves:
                    SetPaperdollItem(PaperdollGloves, item);
                    break;
                case ItemSlotId.SlotChest:
                    SetPaperdollItem(PaperdollChest, item);
                    break;
                case ItemSlotId.SlotLegs:
                    // handle full armor
                    ItemInstance chest = GetPaperdollItem(PaperdollChest);
                    if ((chest != null) && (chest.Item.BodyPart == ItemSlotId.SlotFullArmor))
                    {
                        SetPaperdollItem(PaperdollChest, null);
                    }
                    SetPaperdollItem(PaperdollLegs, null);
                    SetPaperdollItem(PaperdollLegs, item);
                    break;
                case ItemSlotId.SlotFeet:
                    SetPaperdollItem(PaperdollFeet, item);
                    break;
                case ItemSlotId.SlotBack:
                    SetPaperdollItem(PaperdollBack, item);
                    break;
                case ItemSlotId.SlotLRHand:
                    if (SetPaperdollItem(PaperdollLhand, null) != null)
                    {
                        // exchange 2h for 2h
                        SetPaperdollItem(PaperdollRhand, null);
                        SetPaperdollItem(PaperdollLhand, null);
                    }
                    else
                    {
                        SetPaperdollItem(PaperdollRhand, null);
                    }
                    SetPaperdollItem(PaperdollRhand, item);
                    SetPaperdollItem(PaperdollLRHand, item);
                    break;
                case ItemSlotId.SlotFullArmor:
                    break;
                case ItemSlotId.SlotHair:
                    break;
                case ItemSlotId.SlotWolf:
                    break;
                case ItemSlotId.SlotHatchLing:
                    break;
                case ItemSlotId.SlotStrider:
                    break;
                case ItemSlotId.SlotBabyPet:
                    break;
                case ItemSlotId.SlotFace:
                    break;
                case ItemSlotId.SlotDHair:
                    break;
                case ItemSlotId.SlotLFinger | ItemSlotId.SlotRFinger:
                    if (_paperdoll[PaperdollLfinger] == null)
                    {
                        SetPaperdollItem(PaperdollLfinger, item);
                    }
                    else if (_paperdoll[PaperdollRfinger] == null)
                    {
                        SetPaperdollItem(PaperdollRfinger, item);
                    }
                    else
                    {
                        SetPaperdollItem(PaperdollLfinger, null);
                        SetPaperdollItem(PaperdollLfinger, item);
                    }
                    break;
                case ItemSlotId.SlotREar | ItemSlotId.SlotLEar:
                    if (_paperdoll[PaperdollLear] == null)
                    {
                        SetPaperdollItem(PaperdollLear, item);
                    }
                    else if (_paperdoll[PaperdollRear] == null)
                    {
                        SetPaperdollItem(PaperdollRear, item);
                    }
                    else
                    {
                        SetPaperdollItem(PaperdollLear, null);
                        SetPaperdollItem(PaperdollLear, item);
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        
        private void UnEquipItemInBodySlot(int slot)
        {
            int pdollSlot = -1;
            switch (slot)
            {
                case (int)ItemSlotId.SlotLEar:
                {
                    pdollSlot = PaperdollLear;
                    break;
                }
                case (int)ItemSlotId.SlotREar:
                {
                    pdollSlot = PaperdollRear;
                    break;
                }
                case (int)ItemSlotId.SlotNeck:
                {
                    pdollSlot = PaperdollNeck;
                    break;
                }
                case (int)ItemSlotId.SlotRFinger:
                {
                    pdollSlot = PaperdollRfinger;
                    break;
                }
                case (int)ItemSlotId.SlotLFinger:
                {
                    pdollSlot = PaperdollLfinger;
                    break;
                }
                case (int)ItemSlotId.SlotHair:
                {
                    pdollSlot = PaperdollHair;
                    break;
                }
                case (int)ItemSlotId.SlotFace:
                {
                    pdollSlot = PaperdollFace;
                    break;
                }
                case (int)ItemSlotId.SlotDHair:
                {
                    SetPaperdollItem(PaperdollHair, null);
                    SetPaperdollItem(PaperdollFace, null); // this should be the same as in DHAIR
                    pdollSlot = PaperdollDhair;
                    break;
                }
                case (int)ItemSlotId.SlotHead:
                {
                    pdollSlot = PaperdollHead;
                    break;
                }
                case (int)ItemSlotId.SlotRHand:
                {
                    pdollSlot = PaperdollRhand;
                    break;
                }
                case (int)ItemSlotId.SlotLHand:
                {
                    pdollSlot = PaperdollLhand;
                    break;
                }
                case (int)ItemSlotId.SlotGloves:
                {
                    pdollSlot = PaperdollGloves;
                    break;
                }
                case (int)ItemSlotId.SlotChest:
                {
                    pdollSlot = PaperdollChest;
                    break;
                }
                case (int)ItemSlotId.SlotFullArmor:
                {
                    pdollSlot = PaperdollChest;
                    break;
                }
                case (int)ItemSlotId.SlotLegs:
                {
                    pdollSlot = PaperdollLegs;
                    break;
                }
                case (int)ItemSlotId.SlotBack:
                {
                    pdollSlot = PaperdollBack;
                    break;
                }
                case (int)ItemSlotId.SlotFeet:
                {
                    pdollSlot = PaperdollFeet;
                    break;
                }
                case (int)ItemSlotId.SlotUnderwear:
                {
                    pdollSlot = PaperdollUnder;
                    break;
                }
                case (int)ItemSlotId.SlotLRHand:
                {
                    SetPaperdollItem(PaperdollLhand, null);
                    SetPaperdollItem(PaperdollRhand, null); // this should be the same as in LRHAND
                    pdollSlot = PaperdollLRHand;
                    break;
                }
            }
            if (pdollSlot >= 0)
            {
                SetPaperdollItem(pdollSlot, null);
            }
        }

        public ItemInstance SetPaperdollItem(int slot, ItemInstance item)
        {
            ItemInstance old = _paperdoll[slot];
            if (old != item)
            {
                if (old != null)
                {
                    _paperdoll[slot] = null;
                    // Put old item from paperdoll slot to base location
                    old.SetLocation(GetBaseLocation());
                    old.LastChange = ItemInstance.Modified;
                    
                    // Get the mask for paperdoll
                    int mask = 0;
                    for (int i = 0; i < PaperdollLRHand; i++)
                    {
                        ItemInstance pi = _paperdoll[i];
                        if (pi != null)
                        {
                            mask |= pi.Item.GetItemMask();
                        }
                    }
                    _wearedMask = mask;
                    
                    _paperdollListeners.ForEach(listener =>
                    {
                        listener.NotifyUnequiped(slot, old);
                    });

                    old.ItemModel.UpdateDatabase();
                }
                // Add new item in slot of paperdoll
                if (item != null)
                {
                    _paperdoll[slot] = item;
                    item.SetLocation(GetEquipLocation(), slot);
                    item.LastChange = ItemInstance.Modified;
                    _wearedMask |= item.Item.GetItemMask();
                
                    _paperdollListeners.ForEach(listener =>
                    {
                        listener.NotifyEquiped(slot, item);
                    });

                    item.ItemModel.UpdateDatabase();
                }
            }
            return old;
        }
        
        /// <summary>
        /// Adds item to inventory for further adjustments and Equip it if necessary (itemlocation defined)
        /// </summary>
        /// <param name="item"></param>
        protected override void AddItem(ItemInstance item)
        {
            base.AddItem(item);
            if (item.IsEquipped())
            {
                EquipItem(item);
            }
        }
        public int GetWearedMask()
        {
            return _wearedMask;
        }

        /// <summary>
        /// Returns the item in the paperdoll Item slot
        /// </summary>
        /// <param name="slot"></param>
        /// <returns></returns>
        public ItemInstance GetPaperdollItemByItemId(int slot)
        {
            try
            {
                switch (slot)
                {
                    case 0x01:
                        return _paperdoll[0];
                    case 0x04:
                        return _paperdoll[1];
                    case 0x02:
                        return _paperdoll[2];
                    case 0x08:
                        return _paperdoll[3];
                    case 0x20:
                        return _paperdoll[4];
                    case 0x10:
                        return _paperdoll[5];
                    case 0x40:
                        return _paperdoll[6];
                    case 0x80:
                        return _paperdoll[7];
                    case 0x0100:
                        return _paperdoll[8];
                    case 0x0200:
                        return _paperdoll[9];
                    case 0x0400:
                        return _paperdoll[10];
                    case 0x0800:
                        return _paperdoll[11];
                    case 0x1000:
                        return _paperdoll[12];
                    case 0x2000:
                        return _paperdoll[13];
                    case 0x4000:
                        return _paperdoll[14];
                    case 0x040000:
                        return _paperdoll[15];
                    case 0x010000:
                        return _paperdoll[16];
                    case 0x080000:
                        return _paperdoll[17];
                }
            }
            catch (NullReferenceException ex)
            {
                LoggerManager.Info(GetType().Name + " " + ex.Message);
            }
            return _paperdoll[0];
        }
        
        public ChangeRecorder NewRecorder()
        {
            return new ChangeRecorder(this);
        }
        
        
        public void AddPaperdollListener(IPaperdollListener listener)
        {
            _paperdollListeners.Add(listener);
        }
	
        
        public void RemovePaperdollListener(IPaperdollListener listener)
        {
            _paperdollListeners.Remove(listener);
        }

        public List<ItemInstance> UnEquipItemInBodySlotAndRecord(int slot)
        {
            ChangeRecorder recorder = NewRecorder();
            try
            {
                UnEquipItemInBodySlot(slot);
                return recorder.GetChangedItems();
            }
            catch (Exception ex)
            {
                LoggerManager.Error(ex.Message);
            }
            finally
            {
                RemovePaperdollListener(recorder);
            }
            return new List<ItemInstance>();
        }
    }
}