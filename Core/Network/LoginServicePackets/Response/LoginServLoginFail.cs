﻿using System;
using System.Threading.Tasks;
using L2Logger;

namespace Core.Network.LoginServicePackets.Response
{
    public class LoginServLoginFail : PacketBase
    {
        private readonly LoginServiceController _controller;

        public LoginServLoginFail(IServiceProvider serviceProvider, Packet p, LoginServiceController controller) : base(serviceProvider)
        {
            _controller = controller;
        }

        public override async Task Execute()
        {
            LoggerManager.Info("TODO: Login Failed");
            await Task.FromResult(1);
        }
    }
}