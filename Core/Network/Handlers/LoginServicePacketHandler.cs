﻿using System;
using System.Collections.Concurrent;
using Core.Network.LoginServicePackets.Response;
using L2Logger;

namespace Core.Network.Handlers
{
    public class LoginServicePacketHandler
    {
        private readonly IServiceProvider _serviceProvider;
        private static readonly ConcurrentDictionary<byte, Type> LoginServerPackets = new ConcurrentDictionary<byte, Type>();
        public LoginServicePacketHandler(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            LoginServerPackets.TryAdd(0xA1, typeof(LoginServPingResponse));
            LoginServerPackets.TryAdd(0xA5, typeof(LoginServLoginFail));
            LoginServerPackets.TryAdd(0xA6, typeof(LoginServLoginOk));
            LoginServerPackets.TryAdd(0xA7, typeof(LoginServAcceptPlayer));
            LoginServerPackets.TryAdd(0xA8, typeof(LoginServKickAccount));
        }

        public void HandlePacket(Packet packet, LoginServiceController loginServiceController)
        {
            byte opCode = packet.FirstOpcode();
            LoggerManager.Info($"Received packet with Opcode:{opCode:X2}");
            
            PacketBase packetBase = null;
            
            if (LoginServerPackets.ContainsKey(opCode))
            {
                packetBase = (PacketBase)Activator.CreateInstance(LoginServerPackets[opCode], _serviceProvider, packet, loginServiceController);
            }

            if (packetBase == null)
            {
                throw new ArgumentNullException(nameof(packetBase), $"Packet with opcode: {opCode:X2} doesn't exist in the dictionary.");
            }
            packetBase.Execute();
        }
    }
}
