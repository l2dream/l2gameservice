﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Model;
using Core.Model.Player;
using Core.Model.World;
using L2Logger;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Network.Helper
{
    public class GameServiceHelper
    {
        private GameServiceController _gameServiceController;
        private PlayerInstance _player;
        private readonly List<int> _charSlotMapping = new List<int>();
        private readonly WorldInit _worldInit;
        
        public GameServiceHelper(GameServiceController gameServiceController)
        {
            _gameServiceController = gameServiceController;
            _worldInit = Initializer.ServiceProvider.GetService<WorldInit>();
        }
        
        public PlayerInstance GetCharacterBySlot(int slot)
        {
            int objId = GetObjectIdBySlot(slot);
            if (objId < 0)
            {
                return null;
            }
            
            PlayerInstance playerInstance = _worldInit.GetPlayer(objId);
            if (playerInstance != null)
            {
                
            }

            playerInstance = PlayerInstance.Load(objId);
            return playerInstance;
        }


        private int GetObjectIdBySlot(int slot)
        {
            if ((slot < 0) || (slot >= _charSlotMapping.Count))
            {
                LoggerManager.Info(this + " tried to delete Character in slot " + slot + " but no characters exits at that slot.");
                return -1;
            }
		
            int objectId = _charSlotMapping[slot];
            return objectId;
        }

        public void SetCharSelection(CharSelectInfoPackage[] characterPackages)
        {
            _charSlotMapping.Clear();
            foreach (var character in characterPackages)
            {
                _charSlotMapping.Add(character.ObjectId);
            }
        }
        
        public PlayerInstance CurrentPlayer
        {
            get => _player;
            set
            {
                _player = value;
                if (_player != null)
                {
                    _worldInit.StoreObject(_player);
                }
            }
        }
    }
}