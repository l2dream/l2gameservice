﻿using System;
using System.Threading.Tasks;

namespace Core.Network
{
    public abstract class PacketBase
    {
        private readonly IServiceProvider _serviceProvider;

        protected PacketBase(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public abstract Task Execute();
    }
}