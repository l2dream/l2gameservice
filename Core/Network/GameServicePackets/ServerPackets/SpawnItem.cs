﻿using Core.Model.Items;

namespace Core.Network.GameServicePackets.ServerPackets
{
    internal sealed class SpawnItem : ServerPacket
    {
        private readonly int _objectId;
        private readonly int _itemId;
        private readonly int _x;
        private readonly int _y;
        private readonly int _z;
        private readonly int _stackable;
        private readonly int _count;
        
        public SpawnItem(ItemInstance item)
        {
            _objectId = item.ObjectId;
            _itemId = item.ItemId;
            _x = item.GetX();
            _y = item.GetY();
            _z = item.GetZ();
            _stackable = item.IsStackable() ? 0x01 : 0x00;
            _count = item.Count;
        }
        public override void Write()
        {
            WriteByte(0x0b);
            WriteInt(_objectId);
            WriteInt(_itemId);
		
            WriteInt(_x);
            WriteInt(_y);
            WriteInt(_z);
            // only show item count if it is a stackable item
            WriteInt(_stackable);
            WriteInt(_count);
            WriteInt(0x00); // c2
        }
    }
}