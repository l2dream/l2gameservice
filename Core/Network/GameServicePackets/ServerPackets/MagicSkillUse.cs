﻿using Core.Model.Actor;

namespace Core.Network.GameServicePackets.ServerPackets
{
    public class MagicSkillUse : ServerPacket
    {
        private readonly int _targetId;
        private readonly int _skillId;
        private readonly int _skillLevel;
        private readonly int _hitTime;
        private readonly int _reuseDelay;
        private readonly int _objectId;
        private readonly int _x;
        private readonly int _y;
        private readonly int _z;
        
        public MagicSkillUse(Character character, Character target, int skillId, int skillLevel, int hitTime, int reuseDelay)
        {
            _objectId = character.ObjectId;
            _targetId = target.ObjectId;
            _skillId = skillId;
            _skillLevel = skillLevel;
            _hitTime = hitTime;
            _reuseDelay = reuseDelay;
            _x = character.GetX();
            _y = character.GetY();
            _z = character.GetZ();
        }
        public override void Write()
        {
            WriteByte(0x48);
            WriteInt(_objectId);
            WriteInt(_targetId);
            WriteInt(_skillId);
            WriteInt(_skillLevel);
            WriteInt(_hitTime);
            WriteInt(_reuseDelay);
            WriteInt(_x);
            WriteInt(_y);
            WriteInt(_z);
            WriteShort(0x00); // unknown loop but not AoE
            // for()
            // {
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
        }
    }
}