﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Helper;
using Core.Model.Player;
using Helpers;

namespace Core.Network.GameServicePackets.ServerPackets
{
    public class SkillCoolTime : ServerPacket
    {
        private readonly long _currentTime;
        private readonly IEnumerable<Timestamp> _reuseTimestamps;

        public SkillCoolTime(PlayerInstance player)
        {
            _currentTime = DateTimeHelper.CurrentUnixTimeMillis();
            _reuseTimestamps = player.PlayerSkillReuseTime().GetReuseTimeStamps();
        }
        
        public override void Write()
        {
            WriteByte(0xc1);
            WriteInt(_reuseTimestamps.Count());
            foreach (Timestamp ts in _reuseTimestamps)
            {
                WriteInt(ts.Skill.Id);
                WriteInt(ts.Skill.Level);
                WriteInt((int) ts.Reuse / 1000);
                WriteInt((int) Math.Max(ts.Stamp - _currentTime, 0) / 1000);
            }
        }
    }
}