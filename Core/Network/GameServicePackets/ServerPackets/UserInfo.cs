﻿using System;
using Core.Model;
using Core.Model.Player;

namespace Core.Network.GameServicePackets.ServerPackets
{
    public class UserInfo : ServerPacket
    {
        private PlayerInstance _playerInstance;
        private float _moveMultiplier;
        private int _runSpd;
        private int _walkSpd;
        private int _flyRunSpd;
        private int _flyWalkSpd;
        private int _relation;

        public UserInfo(PlayerInstance playerInstance)
        {
            _playerInstance = playerInstance;
            _moveMultiplier = playerInstance.Stat.GetMovementSpeedMultiplier();
            _runSpd = Convert.ToInt32(Math.Round(playerInstance.Stat.GetRunSpeed() / _moveMultiplier));
            _walkSpd = Convert.ToInt32(Math.Round(playerInstance.Stat.GetWalkSpeed() / _moveMultiplier));
            _flyRunSpd = 0;
            _flyWalkSpd = 0;
            _relation = 0;
        }
        
        public override void Write()
        {
            WriteByte(0x04);
            
            WriteInt(_playerInstance.GetX());
            WriteInt(_playerInstance.GetY());
            WriteInt(_playerInstance.GetZ());
            WriteInt(_playerInstance.Heading);
            WriteInt(_playerInstance.ObjectId);
            
            WriteString(_playerInstance.CharacterName);
            
            WriteInt((int) _playerInstance.PlayerModel().GetRace());
            WriteInt(_playerInstance.PlayerAppearance().Female? 1 : 0);
            WriteInt((int) _playerInstance.PlayerModel().GetClassId());
            WriteInt(_playerInstance.Stat.Level);
            WriteLong(_playerInstance.Stat.Exp);

            //stats
            WriteInt(_playerInstance.Stat.GetStr());
            WriteInt(_playerInstance.Stat.GetDex());
            WriteInt(_playerInstance.Stat.GetCon());
            WriteInt(_playerInstance.Stat.GetInt());
            WriteInt(_playerInstance.Stat.GetWit());
            WriteInt(_playerInstance.Stat.GetMen());
            
            WriteInt(_playerInstance.Stat.GetMaxHp());
            WriteInt(_playerInstance.Status.GetCurrentHp());
            WriteInt(_playerInstance.Stat.GetMaxMp());
            WriteInt(_playerInstance.Status.GetCurrentMp());
            WriteInt(_playerInstance.Stat.Sp);
            WriteInt(_playerInstance.PlayerInventory().GetCurrentLoad());
            WriteInt(_playerInstance.Stat.GetMaxLoad());

            WriteInt(_playerInstance.GetActiveWeaponItem() != null ? 40 : 20); // 20 no weapon, 40 weapon equipped
            
            //inventory
            for (byte id = 0; id < 17; id++)
	            WriteInt(_playerInstance.PlayerInventory().GetPaperdollObjectId(id));
           
            for (byte id = 0; id < 17; id++)
	           WriteInt(_playerInstance.PlayerInventory().GetPaperdollItemId(id));
           
            // c6 new h's
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteInt(0x00); // PAPERDOLL_RHAND
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteInt(0x00); //PAPERDOLL_LRHAND
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);    
            // end of c6 new h's
            
            WriteInt(_playerInstance.Stat.GetPAtk());
            WriteInt(_playerInstance.Stat.GetPAtkSpd());
            WriteInt(_playerInstance.Stat.GetPDef());
            WriteInt(_playerInstance.Stat.GetEvasionRate());
            WriteInt(_playerInstance.Stat.GetAccuracy());
            WriteInt(_playerInstance.Stat.GetCriticalHit());
            WriteInt(_playerInstance.Stat.GetMAtk());
		
            WriteInt(_playerInstance.Stat.GetMAtkSpd());
            WriteInt(_playerInstance.Stat.GetPAtkSpd());
		
            WriteInt(_playerInstance.Stat.GetMDef());
		
            WriteInt(0); // getPvpFlag 0-non-pvp 1-pvp = violett name
            WriteInt(0); //getKarma
		
            WriteInt(_runSpd); // base run speed
            WriteInt(_walkSpd); // base walk speed
            WriteInt(_runSpd); // swim run speed (calculated by getter)
            WriteInt(_walkSpd); // swim walk speed (calculated by getter)
            WriteInt(0);
            WriteInt(0);
            WriteInt(_flyRunSpd);
            WriteInt(_flyWalkSpd);
            WriteDouble(_playerInstance.Stat.GetMovementSpeedMultiplier()); // run speed multiplier
            
            //this is very mandatory option. To avoid player paralyzed when trying to physical attack
            WriteDouble(_playerInstance.Stat.GetAttackSpeedMultiplier()); // attack speed multiplier 

            WriteDouble(7); //getCollisionRadius
            WriteDouble(24); //getCollisionHeight
            
            WriteInt(_playerInstance.PlayerAppearance().HairStyle);
            WriteInt(_playerInstance.PlayerAppearance().HairColor);
            WriteInt(_playerInstance.PlayerAppearance().Face);
            WriteInt(0); // _playerInstance.isGM() builder level
            
            WriteString(_playerInstance.Stat.Title);
            
            WriteInt(0);//_player.ClanId
            WriteInt(0);//_player.ClanCrestId
            WriteInt(0);//_player.AllianceId
            WriteInt(0);//_player.AllianceCrestId

            WriteInt(_relation);
            
            WriteByte(0); // mount type
            WriteByte(0); //getPrivateStoreType
            WriteByte(0); //hasDwarvenCraft
            
            WriteInt(_playerInstance.Stat.PkKills); //_player.PkKills
            WriteInt(_playerInstance.Stat.PvpKills); //_player.PvpKills
            
            WriteShort(0);//_player.Cubics.Count
            
            WriteByte(0); //1-isInPartyMatchRoom
            
            WriteInt(0); //_player.AbnormalBitMask
            
            WriteByte(0x00);
            
            WriteInt(0);//_player.ClanPrivs
            
            WriteShort(0); // c2 recommendations remaining
            WriteShort(0); // c2 recommendations received
            WriteInt(0x00); // _player.getMountNpcId() > 0 ? _player.getMountNpcId() + 1000000 : 0
            WriteShort(80); //_player.getInventoryLimit()
            
            WriteInt((int)_playerInstance.Template.Stat.ClassId.Id);
            WriteInt(0x00); // special effects? circles around player...
            WriteInt(_playerInstance.Stat.GetMaxCp());
            WriteInt(_playerInstance.Status.GetCurrentCp());
            WriteByte(0); //_player.isMounted() ? 0 : _player.getEnchantEffect()
		
            WriteByte(0x00); // team circle around feet 1= Blue, 2 = red
		
            WriteInt(0); //_player.getClanCrestLargeId()
            WriteByte(0); // _player.isNoble() 0x01: symbol on char menu ctrl+I
            WriteByte(0); // 0x01: Hero Aura
		
            WriteByte(0); // _player.isFishing() Fishing Mode
            WriteInt(0); // fishing x
            WriteInt(0); // fishing y
            WriteInt(0); // fishing z
            WriteInt(_playerInstance.PlayerAppearance().NameColor);
		
            // new c5
            WriteByte(_playerInstance.Movement().IsRunning() ? 0x01 : 0x00); // changes the Speed display on Status Window
		
            WriteInt(0); // _player.getPledgeClass() changes the text above CP on Status Window
            WriteInt(0); //_player.getPledgeType()
		
            WriteInt(_playerInstance.PlayerAppearance().TitleColor);
		
            WriteInt(0x00); //_player.isCursedWeaponEquiped()
        }
    }
}