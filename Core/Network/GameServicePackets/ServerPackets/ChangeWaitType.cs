﻿using Core.Model.Actor;

namespace Core.Network.GameServicePackets.ServerPackets
{
    internal sealed class ChangeWaitType : ServerPacket
    {
        private readonly int _objectId;
        private readonly int _moveType;
        private readonly int _x;
        private readonly int _y;
        private readonly int _z;

        public ChangeWaitType(Character character, int newMoveType)
        {
            _objectId = character.ObjectId;
            _moveType = newMoveType;
            _x = character.GetX();
            _y = character.GetY();
            _z = character.GetZ();
        }
        public override void Write()
        {
            WriteByte(0x2F);
            WriteInt(_objectId);
            WriteInt(_moveType);
            WriteInt(_x);
            WriteInt(_y);
            WriteInt(_z);
        }
    }
}