﻿namespace Core.Network.GameServicePackets.ServerPackets
{
    public class AutoAttackStart : ServerPacket
    {
        private readonly int _targetObjId;
        public AutoAttackStart(int targetObjId)
        {
            _targetObjId = targetObjId;
        }
        public override void Write()
        {
            WriteByte(0x2b);
            WriteInt(_targetObjId);
        }
    }
}