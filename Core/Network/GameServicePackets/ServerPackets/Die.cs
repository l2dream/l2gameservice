﻿using Core.Model.Actor;

namespace Core.Network.GameServicePackets.ServerPackets
{
    public class Die : ServerPacket
    {
        private Character _character;
        private int _objectId;
        private bool _fake;
        public Die(Character character)
        {
            _character = character;
            _objectId = _character.ObjectId;
            _fake = !_character.Status.IsDead();
        }
        /// <summary>
        /// TODO
        /// </summary>
        public override void Write()
        {
            if (_fake)
            {
                return;
            }
            WriteByte(0x06);
            WriteInt(_objectId);
            // NOTE:
            // 6d 00 00 00 00 - to nearest village
            // 6d 01 00 00 00 - to hide away
            // 6d 02 00 00 00 - to castle
            // 6d 03 00 00 00 - to siege HQ
            // sweepable
            // 6d 04 00 00 00 - FIXED
            WriteInt(0x01); // 6d 00 00 00 00 - to nearest village
            
            WriteInt(0x00); // 6d 01 00 00 00 - to hide away
            WriteInt(0x00); // 6d 02 00 00 00 - to castle
            WriteInt(0x00); // 6d 03 00 00 00 - to siege HQ
            
            WriteInt(0x00); // sweepable (blue glow)
            WriteInt(0x00); // 6d 04 00 00 00 - to FIXED
        }
    }
}