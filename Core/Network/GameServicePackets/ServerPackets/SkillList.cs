﻿namespace Core.Network.GameServicePackets.ServerPackets
{
    public class SkillList : ServerPacket
    {
        private Skill[] _skills;

        readonly struct Skill
        {
            public int Id { get; }
            public int Level { get; }
            public bool Passive { get; }
		
            public Skill(int pId, int pLevel, bool pPassive)
            {
                Id = pId;
                Level = pLevel;
                Passive = pPassive;
            }
        }

        public SkillList()
        {
            _skills = new Skill[]{};
        }
        
        public void AddSkill(int id, int level, bool passive)
        {
            Skill sk = new Skill(id, level, passive);
            if ((_skills == null) || (_skills.Length == 0))
            {
                _skills = new Skill[]
                {
                    sk
                };
            }
            else
            {
                Skill[] ns = new Skill[_skills.Length + 1];
                bool added = false;
                int i = 0;
                foreach (Skill s in _skills)
                {
                    if ((sk.Id < s.Id) && !added)
                    {
                        ns[i] = sk;
                        i++;
                        ns[i] = s;
                        i++;
                        added = true;
                    }
                    else
                    {
                        ns[i] = s;
                        i++;
                    }
                }
                if (!added)
                {
                    ns[i] = sk;
                }
                _skills = ns;
            }
        }
        
        public override void Write()
        {
            WriteByte(0x58);
            WriteInt(_skills.Length);
		
            foreach (Skill temp in _skills)
            {
                WriteInt(temp.Passive ? 1 : 0);
                WriteInt(temp.Level);
                WriteInt(temp.Id);
                WriteByte(0x00); // c5
            }
        }
    }
}