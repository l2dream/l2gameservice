﻿using Core.Model.Items;

namespace Core.Network.GameServicePackets.ServerPackets
{
    internal sealed class GetItem : ServerPacket
    {
        private readonly ItemInstance _item;
        private readonly int _playerId;
        
        public GetItem(ItemInstance item, int playerId)
        {
            _item = item;
            _playerId = playerId;
        }

        public override void Write()
        {
            WriteByte(0x0d);
            WriteInt(_playerId);
            WriteInt(_item.ObjectId);
            WriteInt(_item.GetX());
            WriteInt(_item.GetY());
            WriteInt(_item.GetZ());
        }
    }
}