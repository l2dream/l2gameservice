﻿using Core.Model.Actor;

namespace Core.Network.GameServicePackets.ServerPackets
{
    public class ChangeMoveType : ServerPacket
    {
        private const int Walk = 0;
        private const int Run = 1;

        private readonly int _objectId;
        private readonly bool _running;

        public ChangeMoveType(Character character)
        {
            _objectId = character.ObjectId;
            _running = character.Movement().IsRunning();
        }
        
        public override void Write()
        {
            WriteByte(0x2e);
            WriteInt(_objectId);
            WriteInt(_running ? Run : Walk);
            WriteInt(0); // c2
        }
    }
}