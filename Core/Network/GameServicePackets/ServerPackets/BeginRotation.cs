﻿using Core.Model.Actor;

namespace Core.Network.GameServicePackets.ServerPackets
{
    internal sealed class BeginRotation : ServerPacket
    {
        private readonly int _objectId;
        private readonly int _degree;
        private readonly int _side;
        private readonly int _speed;
        public BeginRotation(Character character, int degree, int side, int speed)
        {
            _objectId = character.ObjectId;
            _degree = degree;
            _side = side;
            _speed = speed;
        }
        public override void Write()
        {
            WriteByte(0x62);
            WriteInt(_objectId);
            WriteInt(_degree);
            WriteInt(_side);
            if (_speed != 0)
            {
                WriteInt(_speed);
            }
        }
    }
}