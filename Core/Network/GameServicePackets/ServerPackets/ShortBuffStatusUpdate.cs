﻿namespace Core.Network.GameServicePackets.ServerPackets
{
    internal sealed class ShortBuffStatusUpdate : ServerPacket
    {
        private readonly int _skillId;
        private readonly int _skillLvl;
        private readonly int _duration;
        
        public ShortBuffStatusUpdate(int skillId, int skillLvl, int duration)
        {
            _skillId = skillId;
            _skillLvl = skillLvl;
            _duration = duration;
        }
        
        public override void Write()
        {
            WriteByte(0xF4);
            WriteInt(_skillId);
            WriteInt(_skillLvl);
            WriteInt(_duration);
        }
    }
}