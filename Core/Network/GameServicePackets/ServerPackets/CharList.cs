﻿namespace Core.Network.GameServicePackets.ServerPackets
{
    public class CharList : ServerPacket
    {
        private readonly GameServiceController _controller;
        private readonly int _sessionId;
        public CharList(GameServiceController controller, int sessionId)
        {
            _sessionId = sessionId;
            _controller = controller;
        }
        public override void Write()
        {
            WriteByte(0x13);
            WriteInt(0);
            
            
        }
    }
}