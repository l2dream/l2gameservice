﻿namespace Core.Network.GameServicePackets.ServerPackets
{
    internal sealed class ExAutoSoulShot : ServerPacket
    {
        private readonly int _itemId;
        private readonly int _type;

        public ExAutoSoulShot(int itemId, int type)
        {
            _itemId = itemId;
            _type = type;
        }
        public override void Write()
        {
            WriteByte(0xFE);
            WriteShort(0x12); // sub id
            WriteInt(_itemId);
            WriteInt(_type);
        }
    }
}