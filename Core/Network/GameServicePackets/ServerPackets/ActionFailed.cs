﻿namespace Core.Network.GameServicePackets.ServerPackets
{
    public class ActionFailed : ServerPacket
    {
        public override void Write()
        {
            WriteByte(0x25);
        }
    }
}