﻿using Core.Model.World;

namespace Core.Network.GameServicePackets.ServerPackets
{
    internal sealed class TeleportToLocation : ServerPacket
    {
        private readonly int _targetObjId;
        private readonly int _x;
        private readonly int _y;
        private readonly int _z;
        private readonly int _heading;
        
        public TeleportToLocation(WorldObject obj, int x, int y, int z)
        {
            _targetObjId = obj.ObjectId;
            _x = x;
            _y = y;
            _z = z;
            _heading = obj.WorldObjectPosition().Heading;
        }

        public TeleportToLocation(WorldObject obj, int x, int y, int z, int heading)
        {
            _targetObjId = obj.ObjectId;
            _x = x;
            _y = y;
            _z = z;
            _heading = heading;
        }
        
        public override void Write()
        {
            WriteByte(0x28);
            WriteInt(_targetObjId);
            WriteInt(_x);
            WriteInt(_y);
            WriteInt(_z);
            WriteInt(0x00); // isValidation ??
            WriteInt(_heading); // nYaw
        }
    }
}