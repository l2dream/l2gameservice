﻿using System.Collections.Generic;
using Core.Model.Items;
using Core.Model.Player;

namespace Core.Network.GameServicePackets.ServerPackets
{
    public class ItemList : ServerPacket
    {
        private readonly List<ItemInstance> _items;
        private readonly bool _showWindow;

        public ItemList(PlayerInstance player, bool showWindow)
        {
            _items = player.PlayerInventory().GetItems();
            _showWindow = showWindow;
        }
        public override void Write()
        {
            WriteByte(0x1b);
            WriteShort(_showWindow ? 0x01 : 0x00);
            WriteShort(_items.Count);
            foreach (ItemInstance temp in _items)
            {
                WriteShort((int)temp.Item.Type1);
                WriteInt(temp.ObjectId);
                WriteInt(temp.ItemId);
                WriteInt(temp.Count);
                WriteShort((int)temp.Item.Type2);
                WriteShort(temp.CustomType1);
                WriteShort(temp.IsEquipped() ? 0x01 : 0x00);
                WriteInt((int) temp.Item.BodyPart);
                WriteShort(temp.EnchantLevel);
                // race tickets
                WriteShort(temp.CustomType2);
                WriteInt(0);//WriteInt((temp.isAugmented()) ? temp.getAugmentation().getAugmentationId() : 0x00);
                WriteInt(temp.Mana);
            }
        }
    }
}