﻿using Core.Model.World;

namespace Core.Network.GameServicePackets.ServerPackets
{
    internal sealed class Revive : ServerPacket
    {
        private readonly int _objectId;
        public Revive(WorldObject obj)
        {
            _objectId = obj.ObjectId;
        }
        public override void Write()
        {
            WriteByte(0x07);
            WriteInt(_objectId);
        }
    }
}