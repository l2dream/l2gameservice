﻿using Core.Model.Actor;

namespace Core.Network.GameServicePackets.ServerPackets
{
    public class StopMove : ServerPacket
    {
        private int _objectId;
        private int _x;
        private int _y;
        private int _z;
        private int _heading;
        private Character _character;

        public StopMove(Character character) : this(character.ObjectId, character.GetX(), character.GetY(), character.GetZ(), character.Heading)
        {
            _character = character;
        }

        public StopMove(int objectId, int x, int y, int z, int heading)
        {
            _objectId = objectId;
            _x = x;
            _y = y;
            _z = z;
            _heading = heading;
        }
        
        public override void Write()
        {
            WriteByte(0x47);
            WriteInt(_objectId);
            WriteInt(_x);
            WriteInt(_y);
            WriteInt(_z);
            WriteInt(_heading);
        }
    }
}