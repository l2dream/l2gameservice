﻿using Core.Model;

namespace Core.Network.GameServicePackets.ServerPackets
{
    internal sealed class ShortCutRegister : ServerPacket
    {
        private readonly ShortCut _shortcut;

        public ShortCutRegister(ShortCut shortcut)
        {
            _shortcut = shortcut;
        }
        public override void Write()
        {
            WriteByte(0x44);
		
            WriteInt(_shortcut.Type);
            WriteInt(_shortcut.Slot + (_shortcut.Page * 12)); // C4 Client
            switch (_shortcut.Type)
            {
                case ShortCut.TypeItem: // 1
                {
                    WriteInt(_shortcut.Id);
                    break;
                }
                case ShortCut.TypeSkill: // 2
                {
                    WriteInt(_shortcut.Id);
                    WriteInt(_shortcut.Level);
                    WriteByte(0x00); // C5
                    break;
                }
                case ShortCut.TypeAction: // 3
                {
                    WriteInt(_shortcut.Id);
                    break;
                }
                case ShortCut.TypeMacro: // 4
                {
                    WriteInt(_shortcut.Id);
                    break;
                }
                case ShortCut.TypeRecipe: // 5
                {
                    WriteInt(_shortcut.Id);
                    break;
                }
                default:
                {
                    WriteInt(_shortcut.Id);
                    break;
                }
            }
            WriteInt(1); // ??
        }
    }
}