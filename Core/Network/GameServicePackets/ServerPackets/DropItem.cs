﻿using Core.Model.Items;

namespace Core.Network.GameServicePackets.ServerPackets
{
    internal sealed class DropItem : ServerPacket
    {
        private readonly ItemInstance _item;
        private readonly int _objectId;

        public DropItem(ItemInstance item, int playerObjId)
        {
            _item = item;
            _objectId = playerObjId;
        }
        public override void Write()
        {
            WriteByte(0x0c);
            WriteInt(_objectId);
            WriteInt(_item.ObjectId);
            WriteInt(_item.ItemId);
		
            WriteInt(_item.GetX());
            WriteInt(_item.GetY());
            WriteInt(_item.GetZ());
            // only show item count if it is a stackable item
            WriteInt(_item.IsStackable() ? 0x01 : 0x00);
            WriteInt(_item.Count);
		
            WriteInt(1); // unknown
        }
    }
}