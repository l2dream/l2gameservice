﻿using L2Logger;

namespace Core.Network.GameServicePackets.ServerPackets
{
    internal sealed class NpcHtmlMessage : ServerPacket
    {
        /** The _npc obj id. */
        private readonly int _npcObjId;
	
        /** The _html. */
        private string _html;
	
        /** The _html file. */
        private string _file;
	
        public NpcHtmlMessage(int npcObjId, string text)
        {
            _npcObjId = npcObjId;
            SetHtml(text);
        }
        
        public NpcHtmlMessage(int npcObjId)
        {
            _npcObjId = npcObjId;
        }

        public void SetHtml(string text)
        {
            if (text == null)
            {
                LoggerManager.Warn("Html is null! this will crash the client!");
                _html = "<html><body></body></html>";
                return;
            }
		
            if (text.Length > 8192)
            {
                LoggerManager.Warn("Html is too long! this will crash the client!");
                _html = "<html><body>Html was too long,<br>Try to use DB for this action</body></html>";
                return;
            }
            _html = text; // html code must not exceed 8192 bytes
        }
        
        public void SetFile(string path)
        {
            string content = Initializer.HtmlCacheService().GetHtm(path);
            if (content == null)
            {
                SetHtml("<html><body>My Text is missing:<br>" + path + "</body></html>");
                LoggerManager.Warn("missing html page " + path);
            }
            _file = path;
            SetHtml(content);
        }
        
        public string GetContent()
        {
            return _html;
        }
        
        public void Replace(string pattern, string value)
        {
            _html = _html.Replace(pattern, value);
        }
	
        public override void Write()
        {
            WriteByte(0x0f);
            WriteInt(_npcObjId);
            WriteString(_html);
            WriteInt(0x00);
        }
    }
}