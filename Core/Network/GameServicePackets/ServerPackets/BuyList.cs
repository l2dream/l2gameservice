﻿using System.Collections.Generic;
using Core.Model.Items;

namespace Core.Network.GameServicePackets.ServerPackets
{
    internal sealed class BuyList : ServerPacket
    {
        private readonly int _listId;
        private readonly List<ItemInstance> _list;
        private readonly int _money;
        
        public BuyList(List<ItemInstance> list, int listId, int currentMoney)
        {
            _listId = listId;
            _list = list;
            _money = currentMoney;
        }
        public override void Write()
        {
            WriteByte(0x11);
            WriteInt(_money); // current money
            WriteInt(_listId);
            WriteShort(_list.Count);
            
            _list.ForEach(i =>
            {
                if ((i.Count > 0) || (i.Count == -1))
                {
                    WriteShort((short)i.Item.Type1); // item type1
                    WriteInt(i.ObjectId);
                    WriteInt(i.ItemId);
                    if (i.Count < 0)
                    {
                        WriteInt(0x00); // max amount of items that a player can buy at a time (with this itemid)
                    }
                    else
                    {
                        WriteInt(i.Count);
                    }
                    WriteShort((short)i.Item.Type2); // item type2
                    WriteShort(0x00); // ?
				
                    if (i.Item.Type1 != Type1.Type1ItemQuestItemAdena)
                    {
                        WriteInt((int)i.Item.BodyPart); // rev 415 slot 0006-lr.ear 0008-neck 0030-lr.finger 0040-head 0080-?? 0100-l.hand 0200-gloves 0400-chest 0800-pants 1000-feet 2000-?? 4000-r.hand 8000-r.hand
                        WriteShort(i.EnchantLevel); // enchant level
                        WriteShort(0x00); // ?
                        WriteShort(0x00);
                    }
                    else
                    {
                        WriteInt(0x00); // rev 415 slot 0006-lr.ear 0008-neck 0030-lr.finger 0040-head 0080-?? 0100-l.hand 0200-gloves 0400-chest 0800-pants 1000-feet 2000-?? 4000-r.hand 8000-r.hand
                        WriteShort(0x00); // enchant level
                        WriteShort(0x00); // ?
                        WriteShort(0x00);
                    }
				
                    if ((i.ItemId >= 3960) && (i.ItemId <= 4026))
                    {
                        WriteInt((int) i.PriceSell + 0);
                    }
                    else
                    {
                        WriteInt((int) i.PriceSell + 0);
                    }
                }
            });
        }
    }
}