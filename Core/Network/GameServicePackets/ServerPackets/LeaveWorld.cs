﻿namespace Core.Network.GameServicePackets.ServerPackets
{
    public class LeaveWorld : ServerPacket
    {
        public override void Write()
        {
            WriteByte(0x7e);
        }
    }
}