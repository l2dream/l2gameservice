﻿namespace Core.Network.GameServicePackets.ServerPackets.CharacterPackets
{
    internal sealed class CharacterSay : ServerPacket
    {
        private readonly int _objectId;
        private readonly ChatType _chatType;
        private readonly string _charName;
        private readonly string _text;
        
        public CharacterSay(int objectId, ChatType chatType, string charName, string text)
        {
            _objectId = objectId;
            _chatType = chatType;
            _charName = charName;
            _text = text;
        }
        public override void Write()
        {
            WriteByte(0x4a);
            WriteInt(_objectId);
            WriteInt((byte)_chatType);
            WriteString(_charName);
            WriteString(_text);
        }
    }
}