﻿using System.Collections.Generic;
using Core.Model.Player.Template;

namespace Core.Network.GameServicePackets.ServerPackets.CharacterPackets
{
    public class CharTemplates : ServerPacket
    {
        private readonly IList<PlayerTemplate> _templates;
        public CharTemplates(IList<PlayerTemplate> playerTemplate)
        {
            _templates = playerTemplate;
        }
        public override void Write()
        {
            WriteByte(0x17);
            WriteInt(_templates.Count);

            foreach (PlayerTemplate t in _templates)
            {
                WriteInt((int)t.Stat.ClassId.ClassRace); //race id
                WriteInt((int)t.Stat.ClassId.Id);
                WriteInt(0x46);
                WriteInt(t.Stat.BaseStr);
                WriteInt(0x0a);
                WriteInt(0x46);
                WriteInt(t.Stat.BaseDex);
                WriteInt(0x0a);
                WriteInt(0x46);
                WriteInt(t.Stat.BaseCon);
                WriteInt(0x0a);
                WriteInt(0x46);
                WriteInt(t.Stat.BaseInt);
                WriteInt(0x0a);
                WriteInt(0x46);
                WriteInt(t.Stat.BaseWit);
                WriteInt(0x0a);
                WriteInt(0x46);
                WriteInt(t.Stat.BaseMen);
                WriteInt(0x0a);
            }
        }
    }
}