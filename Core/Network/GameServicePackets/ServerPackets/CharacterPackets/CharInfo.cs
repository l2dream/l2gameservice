﻿using System;
using Core.Model;
using Core.Model.Player;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Network.GameServicePackets.ServerPackets.CharacterPackets
{
    public class CharInfo : ServerPacket
    {
        private readonly PlayerInstance _playerInstance;
        private readonly int _x;
        private readonly int _y;
        private readonly int _z;
        private readonly int _heading;
        private readonly int _mAtkSpd;
        private readonly int _pAtkSpd;
        private readonly int _runSpd;
        private readonly int _walkSpd;
        private readonly int _flyRunSpd;
        private readonly int _flyWalkSpd;
        private readonly float _moveMultiplier;
        private readonly float _attackSpeedMultiplier;

        public CharInfo(PlayerInstance playerInstance)
        {
            _playerInstance = playerInstance;

            _x = playerInstance.GetX();
            _y = playerInstance.GetY();
            _z = playerInstance.GetZ();
            _heading = playerInstance.Stat.Heading;
            _mAtkSpd = playerInstance.Stat.GetMAtkSpd();
            _pAtkSpd = playerInstance.Stat.GetPAtkSpd();
            
            _attackSpeedMultiplier = playerInstance.Stat.GetAttackSpeedMultiplier();
            _moveMultiplier = _playerInstance.Stat.GetMovementSpeedMultiplier();
            _runSpd = Convert.ToInt32(Math.Round(playerInstance.Stat.GetRunSpeed() / _moveMultiplier));
            _walkSpd = Convert.ToInt32(Math.Round(playerInstance.Stat.GetWalkSpeed() / _moveMultiplier));
            _flyRunSpd = 0;
            _flyWalkSpd = 0;
        }
        
        public override void Write()
        {
            //PlayerInstance tmp = _playerInstance.GameServiceController.GameServiceHelper.CurrentPlayer;

            WriteByte(0x03);
            WriteInt(_x);
            WriteInt(_y);
            WriteInt(_z);
            WriteInt(0); //getBoat
            WriteInt(_playerInstance.ObjectId);
            WriteString(_playerInstance.CharacterName);
            WriteInt((int) _playerInstance.PlayerModel().GetRace());
            WriteInt(_playerInstance.PlayerAppearance().Female? 1 : 0);
            WriteInt((int) _playerInstance.PlayerModel().GetClassId());

            WriteInt(_playerInstance.PlayerInventory().GetPaperdollItemId(Inventory.PaperdollDhair));
            WriteInt(_playerInstance.PlayerInventory().GetPaperdollItemId(Inventory.PaperdollHead));
            WriteInt(_playerInstance.PlayerInventory().GetPaperdollItemId(Inventory.PaperdollRhand));
            WriteInt(_playerInstance.PlayerInventory().GetPaperdollItemId(Inventory.PaperdollLhand));
            WriteInt(_playerInstance.PlayerInventory().GetPaperdollItemId(Inventory.PaperdollGloves));
            WriteInt(_playerInstance.PlayerInventory().GetPaperdollItemId(Inventory.PaperdollChest));
            WriteInt(_playerInstance.PlayerInventory().GetPaperdollItemId(Inventory.PaperdollLegs));
            WriteInt(_playerInstance.PlayerInventory().GetPaperdollItemId(Inventory.PaperdollFeet));
            WriteInt(_playerInstance.PlayerInventory().GetPaperdollItemId(Inventory.PaperdollBack));
            WriteInt(_playerInstance.PlayerInventory().GetPaperdollItemId(Inventory.PaperdollLRHand));
            WriteInt(_playerInstance.PlayerInventory().GetPaperdollItemId(Inventory.PaperdollHair));
            WriteInt(_playerInstance.PlayerInventory().GetPaperdollItemId(Inventory.PaperdollFace));
            
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteInt(0);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteInt(0);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            WriteShort(0x00);
            
            WriteInt(0); // getPvpFlag 0-non-pvp 1-pvp = violett name
            WriteInt(0); //getKarma
            
            WriteInt(_mAtkSpd);
            WriteInt(_pAtkSpd);
            
            WriteInt(0); // getPvpFlag 0-non-pvp 1-pvp = violett name
            WriteInt(0); //getKarma
            
            WriteInt(_runSpd); // base run speed
            WriteInt(_walkSpd); // base walk speed
            WriteInt(_runSpd); // swim run speed (calculated by getter)
            WriteInt(_walkSpd); // swim walk speed (calculated by getter)
            WriteInt(_flyRunSpd);
            WriteInt(_flyWalkSpd);
            WriteInt(_flyRunSpd);
            WriteInt(_flyWalkSpd);
            WriteDouble(_moveMultiplier); // run speed multiplier
            
            //this is very mandatory option. To avoid player paralyzed when trying to physical attack
            WriteDouble(_attackSpeedMultiplier); // attack speed multiplier
            
            WriteDouble(_playerInstance.Template.Stat.CollisionRadius); //getCollisionRadius
            WriteDouble(_playerInstance.Template.Stat.CollisionHeight); //getCollisionHeight
			
            WriteInt(_playerInstance.PlayerAppearance().HairStyle);
            WriteInt(_playerInstance.PlayerAppearance().HairColor);
            WriteInt(_playerInstance.PlayerAppearance().Face);
			
            WriteString(_playerInstance.Stat.Title);
	
            WriteInt(0);//_player.ClanId
            WriteInt(0);//_player.ClanCrestId
            WriteInt(0);//_player.AllianceId
            WriteInt(0);//_player.AllianceCrestId
			// In UserInfo leader rights and siege flags, but here found nothing??
			// Therefore RelationChanged packet with that info is required
			WriteInt(0);
			
			WriteByte(_playerInstance.PlayerAction().IsSitting ? 0 : 1); // standing = 1 sitting = 0
			WriteByte(_playerInstance.Movement().IsRunning() ? 1 : 0); // running = 1 walking = 0
			WriteByte(0); //isInCombat
			WriteByte(_playerInstance.Status.IsAlikeDead() ? 1 : 0);
			
			WriteByte(0x00); // if the charinfo is written means receiver can see the char

			WriteByte(0); // 1 on strider 2 on wyvern 0 no mount
			WriteByte(0); // 1 - sellshop
			
			WriteShort(0);

			WriteByte(0);
			
			WriteInt(0); //_player.getAbnormalEffect()
			
			WriteByte(0); //getRecomLeft
			WriteShort(0); //getRecomHave  Blue value for name (0 = white, 255 = pure blue)
			WriteInt((int) _playerInstance.PlayerModel().GetClassId());
			
			WriteInt(_playerInstance.Stat.GetMaxCp());
			WriteInt((int) _playerInstance.Status.GetCurrentCp());
			WriteByte(0);
			
			WriteByte(0x00); // team circle around feet 1= Blue, 2 = red
			
			WriteInt(0);
			WriteByte(0); // isNoble Symbol on char menu ctrl+I
			WriteByte(0); // isHero  Hero Aura
			
			WriteByte(0); // 0x01: Fishing Mode (Cant be undone by setting back to 0)
			WriteInt(0);
			WriteInt(0);
			WriteInt(0);
			
			WriteInt(_playerInstance.PlayerAppearance().NameColor);
			
			WriteInt(_heading);
			
			WriteInt(0); //getPledgeClass
			WriteInt(0); //_player.getPledgeType())
			
			WriteInt(_playerInstance.PlayerAppearance().TitleColor);
			
			WriteInt(0x00); //isCursedWeaponEquiped
        }
    }
}