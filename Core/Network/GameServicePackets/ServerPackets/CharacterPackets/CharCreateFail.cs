﻿namespace Core.Network.GameServicePackets.ServerPackets.CharacterPackets
{
    public class CharCreateFail : ServerPacket
    {
        private readonly int _error;
        public const int REASON_CREATION_FAILED = 0x00;
        public const int REASON_TOO_MANY_CHARACTERS = 0x01;
        public const int REASON_NAME_ALREADY_EXISTS = 0x02;
        public const int REASON_16_ENG_CHARS = 0x03;
        public CharCreateFail(int errorCode)
        {
            _error = errorCode;
        }
        public override void Write()
        {
            WriteByte(0x1a);
            WriteInt(_error);
        }
    }
}