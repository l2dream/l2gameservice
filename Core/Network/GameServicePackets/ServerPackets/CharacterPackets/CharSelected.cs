﻿using Core.Model.Player;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Network.GameServicePackets.ServerPackets.CharacterPackets
{
    public class CharSelected : ServerPacket
    {
        private readonly PlayerInstance _player;
        private readonly int _sessionId;
        private readonly GameTimeController _gameTimeController;

        public CharSelected(PlayerInstance player, int sessionId)
        {
            _player = player;
            _sessionId = sessionId;
            _gameTimeController = Initializer.ServiceProvider.GetService<GameTimeController>();
        }
        
        public override void Write()
        {
            WriteByte(0x15);
		
            WriteString(_player.CharacterName);
            WriteInt(_player.ObjectId); // ??
            WriteString(_player.Stat.Title);
            WriteInt(_sessionId);
            WriteInt(_player.PlayerModel().ClanId);
            WriteInt(0x00); // ??
            WriteInt(_player.PlayerAppearance().Female ? 1 : 0);
            WriteInt((int) _player.PlayerModel().GetRace());
            WriteInt((int) _player.PlayerModel().GetClassId());
            WriteInt(0x01); // active ??
            WriteInt(_player.GetX());
            WriteInt(_player.GetY());
            WriteInt(_player.GetZ());
		
            WriteInt(_player.Status.GetCurrentHp());//_player.getCurrentHp()
            WriteInt(_player.Status.GetCurrentMp());//_player.getCurrentMp()
            WriteInt(_player.Stat.Sp);//_player.getSp()
            WriteLong(1);//_player.getExp()
            WriteInt(1);//_player.getLevel()
            WriteInt(0); // _player.getKarma()
            WriteInt(0x0); // ?
            WriteInt(_player.Stat.GetInt());//_player.getINT()
            WriteInt(_player.Stat.GetStr());//_player.getSTR()
            WriteInt(_player.Stat.GetCon());//_player.getCON()
            WriteInt(_player.Stat.GetMen());//_player.getMEN()
            WriteInt(_player.Stat.GetDex());//_player.getDEX()
            WriteInt(_player.Stat.GetWit());//_player.getWIT()
            for (int i = 0; i < 30; i++)
            {
                WriteInt(0x00);
            }
            WriteInt(0x00); // c3 work
            WriteInt(0x00); // c3 work
		
            // extra info
            WriteInt(_gameTimeController.GetGameTime());//WriteInt(GameTimeController.getInstance().getGameTime()); // in-game time
            
		
            WriteInt(0x00); //
		
            WriteInt(0x00); // c3
		
            WriteInt(0x00); // c3 InspectorBin
            WriteInt(0x00); // c3
            WriteInt(0x00); // c3
            WriteInt(0x00); // c3
		
            WriteInt(0x00); // c3 InspectorBin for 528 client
            WriteInt(0x00); // c3
            WriteInt(0x00); // c3
            WriteInt(0x00); // c3
            WriteInt(0x00); // c3
            WriteInt(0x00); // c3
            WriteInt(0x00); // c3
            WriteInt(0x00); // c3
        }
    }
}