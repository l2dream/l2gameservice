﻿namespace Core.Network.GameServicePackets.ServerPackets.CharacterPackets
{
    public enum ChatType : byte
    {
        General,
        Shout,
        Whisper,
        Party,
        CLan,
        Gm,
        PetitionPlayer,
        PetitionGm,
        Trade,
        Alliance,
        Announcement,
        Boat,
        Friend,
        MsnChat,
        PartyMatchRoom,
        PartyRoomCommander,
        PartyRoomAll,
        HeroVoice,
        CriticalAnnounce
    }
}