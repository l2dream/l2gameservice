﻿namespace Core.Network.GameServicePackets.ServerPackets.CharacterPackets
{
    public class CharacterCreateOk : ServerPacket
    {
        public override void Write()
        {
            WriteByte(0x19);
            WriteInt(0x01);
        }
    }
}