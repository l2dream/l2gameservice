﻿namespace Core.Network.GameServicePackets.ServerPackets
{
    public class TargetSelected : ServerPacket
    {
        private readonly int _objectId;
        private readonly int _targetObjId;
        private readonly int _x;
        private readonly int _y;
        private readonly int _z;
        
        public TargetSelected(int objectId, int targetId, int x, int y, int z)
        {
            _objectId = objectId;
            _targetObjId = targetId;
            _x = x;
            _y = y;
            _z = z;
        }

        public override void Write()
        {
            WriteByte(0x29);
            WriteInt(_objectId);
            WriteInt(_targetObjId);
            WriteInt(_x);
            WriteInt(_y);
            WriteInt(_z);
        }
    }
}