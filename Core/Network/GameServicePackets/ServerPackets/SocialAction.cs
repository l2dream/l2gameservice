﻿namespace Core.Network.GameServicePackets.ServerPackets
{
    public class SocialAction : ServerPacket
    {
        
        private readonly int _objectId;
        private readonly int _actionId;

        public SocialAction(int playerId, int actionId)
        {
            _objectId = playerId;
            _actionId = actionId;
        }
        public override void Write()
        {
            WriteByte(0x2d);
            WriteInt(_objectId);
            WriteInt(_actionId);
        }
    }
}