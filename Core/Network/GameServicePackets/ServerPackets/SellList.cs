﻿using System.Collections.Generic;
using Core.Model.Items;
using Core.Model.Player;

namespace Core.Network.GameServicePackets.ServerPackets
{
    internal sealed class SellList : ServerPacket
    {
        private readonly PlayerInstance _playerInstance;
        private int _money;
        private readonly List<ItemInstance> _sellList;

        public SellList(PlayerInstance playerInstance)
        {
            _playerInstance = playerInstance;
            _sellList = new List<ItemInstance>();

            InitList();
        }

        private void InitList()
        {
            _money = _playerInstance.PlayerInventory().GetAdena();
            var items =_playerInstance.PlayerInventory().GetItems();
            items.ForEach(item =>
            {
                if (!item.IsEquipped() && item.ItemId != 57 && item.Item.Sellable)
                {
                    _sellList.Add(item);
                }
            });
        }

        public override void Write()
        {
            WriteByte(0x10);
            WriteInt(_money);
            WriteInt(0x00);
            WriteShort(_sellList.Count);
            
            _sellList.ForEach(item =>
            {
                WriteShort((int)item.Item.Type1);
                WriteInt(item.ObjectId);
                WriteInt(item.ItemId);
                WriteInt(item.Count);
                WriteShort((int)item.Item.Type2);
                WriteShort(0x00);
                WriteInt((int)item.Item.BodyPart);
                WriteShort(item.EnchantLevel);
                WriteShort(0x00);
                WriteShort(0x00);
                WriteInt(item.Item.ReferencePrice / 2);
            });
        }
    }
}