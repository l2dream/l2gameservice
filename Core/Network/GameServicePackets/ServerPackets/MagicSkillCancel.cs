﻿namespace Core.Network.GameServicePackets.ServerPackets
{
    internal sealed class MagicSkillCancel : ServerPacket
    {
        private readonly int _objectId;

        public MagicSkillCancel(int objectId)
        {
            _objectId = objectId;
        }
        public override void Write()
        {
            WriteByte(0x49);
            WriteInt(_objectId);
        }
    }
}