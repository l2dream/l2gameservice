﻿using System.Collections.Generic;
using Core.Model.Items;

namespace Core.Network.GameServicePackets.ServerPackets
{
    public class InventoryUpdate : ServerPacket
    {
        private readonly List<ItemInfo> _items;

        public InventoryUpdate()
        {
            _items = new List<ItemInfo>();
        }
        
        public InventoryUpdate(List<ItemInfo> items)
        {
            _items = items;
        }
        
        public void AddItem(ItemInstance item)
        {
            if (item != null)
            {
                _items.Add(new ItemInfo(item));
            }
        }
        
        public void AddNewItem(ItemInstance item)
        {
            if (item != null)
            {
                _items.Add(new ItemInfo(item, 1));
            }
        }
        
        public void AddModifiedItem(ItemInstance item)
        {
            if (item != null)
            {
                _items.Add(new ItemInfo(item, 2));
            }
        }
        
        public void AddRemovedItem(ItemInstance item)
        {
            if (item != null)
            {
                _items.Add(new ItemInfo(item, 3));
            }
        }
        
        public void AddItems(List<ItemInstance> items)
        {
            if (items != null)
            {
                foreach (ItemInstance item in items)
                {
                    if (item != null)
                    {
                        _items.Add(new ItemInfo(item));
                    }
                }
            }
        }

        public override void Write()
        {
            WriteByte(0x27);
            WriteShort(_items.Count);

            foreach (var item in _items)
            {
                WriteShort(item.Change);
                WriteShort((int) item.Item.Type1);
                WriteInt(item.ObjectId);
                WriteInt(item.Item.ItemId);
                WriteInt(item.Count);
                WriteShort((int) item.Item.Type2);
                WriteShort(item.CustomType1);
                WriteShort(item.Equipped);
                WriteInt((int) item.Item.BodyPart);
                WriteShort(item.Enchant);
                WriteShort(item.CustomType2);
                WriteInt(item.AugmentationBonus);
                WriteInt(item.Mana);
            }
/*
            _items.ForEach(item =>
            {
                WriteShort(item.Change);
                WriteShort((int) item.Item.Type1);
                WriteInt(item.ObjectId);
                WriteInt(item.Item.ItemId);
                WriteInt(item.Count);
                WriteShort((int) item.Item.Type2);
                WriteShort(item.CustomType1);
                WriteShort(item.Equipped);
                WriteInt((int) item.Item.BodyPart);
                WriteShort(item.Enchant);
                WriteShort(item.CustomType2);
                WriteInt(item.AugmentationBonus);
                WriteInt(item.Mana);
            });
            */
        }
    }
}