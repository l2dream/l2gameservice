﻿using System.Collections.Generic;

namespace Core.Network.GameServicePackets.ServerPackets
{
    internal sealed class ExGetBossRecord : ServerPacket
    {
        private readonly IDictionary<int, int> _bossRecordInfo;
        private readonly int _ranking;
	
        /** The _total points. */
        private readonly int _totalPoints;
        
        public ExGetBossRecord(int ranking, int totalScore, IDictionary<int, int> list)
        {
            _ranking = ranking;
            _totalPoints = totalScore;
            _bossRecordInfo = list;
        }
        public override void Write()
        {
            WriteByte(0xFE);
            WriteShort(0x33);
            WriteInt(_ranking);
            WriteInt(_totalPoints);
            if (_bossRecordInfo == null)
            {
                WriteInt(0x00);
                WriteInt(0x00);
                WriteInt(0x00);
                WriteInt(0x00);
            }
        }
    }
}