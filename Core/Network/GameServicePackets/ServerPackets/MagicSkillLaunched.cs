﻿using System;
using Core.Model.Actor;
using Core.Model.World;
using L2Logger;

namespace Core.Network.GameServicePackets.ServerPackets
{
    internal sealed class MagicSkillLaunched : ServerPacket
    {
        private readonly int _objectId;
        private readonly int _skillId;
        private readonly int _skillLevel;
        private readonly int _numberOfTargets;
        private readonly WorldObject[] _targets;
        private readonly int _singleTargetId;

        public MagicSkillLaunched(Character character, int skillId, int skillLevel, WorldObject[] targets)
        {
            _objectId = character.ObjectId;
            _skillId = skillId;
            _skillLevel = skillLevel;
            if (targets != null)
            {
                _numberOfTargets = targets.Length;
                _targets = targets;
            }
            else
            {
                _numberOfTargets = 1;
                WorldObject[] objs =
                {
                    character
                };
                _targets = objs;
            }
            _singleTargetId = 0;
        }
        
        public MagicSkillLaunched(Character character, int skillId, int skillLevel)
        {
            _objectId = character.ObjectId;
            _skillId = skillId;
            _skillLevel = skillLevel;
            _numberOfTargets = 1;
            _singleTargetId = character.Target.GetTargetId();
        }
        
        public override void Write()
        {
            WriteByte(0x76);
            WriteInt(_objectId);
            WriteInt(_skillId);
            WriteInt(_skillLevel);
            WriteInt(_numberOfTargets); // also failed or not?
            if ((_singleTargetId != 0) || (_numberOfTargets == 0))
            {
                WriteInt(_singleTargetId);
            }
            else
            {
                foreach (WorldObject target in _targets)
                {
                    try
                    {
                        WriteInt(target.ObjectId);
                    }
                    catch (NullReferenceException e)
                    {
                        WriteInt(0); // untested
                        LoggerManager.Error(GetType().Name + " " + e.Message);
                    }
                }
            }
        }
    }
}