﻿using System.Collections.Generic;
using Helpers;

namespace Core.Network.GameServicePackets.ServerPackets
{
    public sealed class MagicEffectIcons : ServerPacket
    {
        private readonly IList<Effect> _effects;
        private readonly IList<Effect> _deBuffs;
	
        private class Effect
        {
            public int SkillId { get; }
            public int Level { get; }
            public  int Duration { get; }
		
            public Effect(int pSkillId, int pLevel, int pDuration)
            {
                SkillId = pSkillId;
                Level = pLevel;
                Duration = pDuration;
            }
        }
        
        public MagicEffectIcons()
        {
            _effects = new List<Effect>();
            _deBuffs = new List<Effect>();
        }

        
        
        public void AddEffect(int skillId, int level, int duration, bool debuff)
        {
            if ((skillId == 2031) || (skillId == 2032) || (skillId == 2037))
            {
                return;
            }
		
            if (debuff)
            {
                _deBuffs.Add(new Effect(skillId, level, duration));
            }
            else
            {
                _effects.Add(new Effect(skillId, level, duration));
            }
        }
        
        public override void Write()
        {
            WriteByte(0x7f);
		
            WriteShort(_effects.Count + _deBuffs.Count);
		
            foreach (Effect temp in _effects)
            {
                WriteInt(temp.SkillId);
                WriteShort(temp.Level);
			
                if (temp.Duration == -1)
                {
                    WriteInt(-1);
                }
                else
                {
                    WriteInt(temp.Duration / 1000);
                }
            }
		
            foreach (Effect temp in _deBuffs)
            {
                WriteInt(temp.SkillId);
                WriteShort(temp.Level);
			
                if (temp.Duration == -1)
                {
                    WriteInt(-1);
                }
                else
                {
                    WriteInt(temp.Duration / 1000);
                }
            }
        }
    }
}