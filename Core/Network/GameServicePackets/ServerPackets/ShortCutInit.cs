﻿using System.Collections.Generic;
using Core.Model;
using Core.Model.Player;

namespace Core.Network.GameServicePackets.ServerPackets
{
    internal sealed class ShortCutInit : ServerPacket
    {
        private readonly IList<ShortCut> _shortCuts;

        public ShortCutInit(PlayerInstance player)
        {
            _shortCuts = player.PlayerShortCut().GetAllShortCuts();
        }
        public override void Write()
        {
            WriteByte(0x45);
            WriteInt(_shortCuts.Count);
            foreach (var shortCut in _shortCuts)
            {
                WriteInt(shortCut.Type);
                WriteInt(shortCut.Slot + (shortCut.Page * 12));
                
                switch (shortCut.Type)
                {
                    case ShortCut.TypeItem: // 1
                    {
                        WriteInt(shortCut.Id);
                        WriteInt(0x01);
                        WriteInt(-1);
                        WriteInt(0x00);
                        WriteInt(0x00);
                        WriteShort(0x00);
                        WriteShort(0x00);
                        break;
                    }
                    case ShortCut.TypeSkill: // 2
                    {
                        WriteInt(shortCut.Id);
                        WriteInt(shortCut.Level);
                        WriteByte(0x00); // C5
                        WriteInt(0x01); // C6
                        break;
                    }
                    case ShortCut.TypeAction: // 3
                    {
                        WriteInt(shortCut.Id);
                        WriteInt(0x01); // C6
                        break;
                    }
                    case ShortCut.TypeMacro: // 4
                    {
                        WriteInt(shortCut.Id);
                        WriteInt(0x01); // C6
                        break;
                    }
                    case ShortCut.TypeRecipe: // 5
                    {
                        WriteInt(shortCut.Id);
                        WriteInt(0x01); // C6
                        break;
                    }
                    default:
                    {
                        WriteInt(shortCut.Id);
                        WriteInt(0x01); // C6
                        break;
                    }
                }
            }
        }
    }
}