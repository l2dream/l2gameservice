﻿namespace Core.Network.GameServicePackets.ServerPackets
{
    public class ExCursedWeaponList : ServerPacket
    {
        public override void Write()
        {
            WriteByte(0xfe);
            WriteShort(0x45);
            WriteInt(0);
        }
    }
}