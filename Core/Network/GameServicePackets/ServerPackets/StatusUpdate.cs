﻿using System.Collections.Generic;
using Core.Model.Player;

namespace Core.Network.GameServicePackets.ServerPackets
{
    public class StatusUpdate : ServerPacket
    {
        public const int Level = 0x01;
        public const int Exp = 0x02;
        public const int Str = 0x03;
        public const int Dex = 0x04;
        public const int Con = 0x05;
        public const int Int = 0x06;
        public const int Wit = 0x07;
        public const int Men = 0x08;
	
        public const int CurHp = 0x09;
        public const int MaxHp = 0x0a;
        public const int CurMp = 0x0b;
        public const int MaxMp = 0x0c;
	
        public const int Sp = 0x0d;
        public const int CurLoad = 0x0e;
        public const int MaxLoad = 0x0f;
	
        public const int PAtk = 0x11;
        public const int AtkSpd = 0x12;
        public const int PDef = 0x13;
        public const int Evasion = 0x14;
        public const int Accuracy = 0x15;
        public const int Critical = 0x16;
        public const int MAtk = 0x17;
        public const int CastSpd = 0x18;
        public const int MDef = 0x19;
        public const int PvpFlag = 0x1a;
        public const int Karma = 0x1b;
	
        public const int CurCp = 0x21;
        public const int MaxCp = 0x22;
	
        private PlayerInstance _actor;
	
        private List<Attribute> _attributes;
        public int _objectId;
        
        class Attribute
        {
	        // id values 09 - current health 0a - max health 0b - current mana 0c - max mana
	        public int Id;
	        public int Value;
		
	        public Attribute(int pId, int pValue)
	        {
		        Id = pId;
		        Value = pValue;
	        }
        }

        public StatusUpdate(PlayerInstance actor)
        {
	        _actor = actor;
        }

        public StatusUpdate(int objectId)
        {
	        _attributes = new List<Attribute>();
	        _objectId = objectId;
        }
        
        public void AddAttribute(int id, int level)
        {
	        _attributes.Add(new Attribute(id, level));
        }
        
        public override void Write()
        {
	        WriteByte(0x0e);
	        
	        if (_actor != null)
	        {
		        WriteInt(_actor.ObjectId);
		        WriteInt(28); // all the attributes
			
		        WriteInt(Level);
		        WriteInt(_actor.Stat.Level);
		        WriteInt(Exp);
		        WriteInt((int) _actor.Stat.Exp);
		        WriteInt(Str);
		        WriteInt(_actor.Stat.GetStr());
		        WriteInt(Dex);
		        WriteInt(_actor.Stat.GetDex());
		        WriteInt(Con);
		        WriteInt(_actor.Stat.GetCon());
		        WriteInt(Int);
		        WriteInt(_actor.Stat.GetInt());
		        WriteInt(Wit);
		        WriteInt(_actor.Stat.GetWit());
		        WriteInt(Men);
		        WriteInt(_actor.Stat.GetMen());
			
		        WriteInt(CurHp);
		        WriteInt((int) _actor.Status.GetCurrentHp());
		        WriteInt(MaxHp);
		        WriteInt(_actor.Stat.GetMaxHp());
		        WriteInt(CurMp);
		        WriteInt((int) _actor.Status.GetCurrentMp());
		        WriteInt(MaxMp);
		        WriteInt(_actor.Stat.GetMaxMp());
		        WriteInt(Sp);
		        WriteInt(_actor.Stat.Sp);
		        WriteInt(CurLoad);
		        WriteInt(_actor.Stat.CurrentLoad());
		        WriteInt(MaxLoad);
		        WriteInt(_actor.Stat.GetMaxLoad());
			
		        WriteInt(PAtk);
		        WriteInt(_actor.Stat.GetPAtk());
		        WriteInt(AtkSpd);
		        WriteInt(_actor.Stat.GetPAtkSpd());
		        WriteInt(PDef);
		        WriteInt(_actor.Stat.GetPDef());
		        WriteInt(Evasion);
		        WriteInt(_actor.Stat.GetEvasionRate());
		        WriteInt(Accuracy);
		        WriteInt(_actor.Stat.GetAccuracy());
		        WriteInt(Critical);
		        WriteInt(_actor.Stat.GetCriticalHit());
		        WriteInt(MAtk);
		        WriteInt(_actor.Stat.GetMAtk());
			
		        WriteInt(CastSpd);
		        WriteInt(_actor.Stat.GetMAtkSpd());
		        WriteInt(MDef);
		        WriteInt(_actor.Stat.GetMDef());
		        WriteInt(PvpFlag);
		        WriteInt(_actor.Stat.PvpKills);
		        WriteInt(Karma);
		        WriteInt(_actor.Stat.Karma);
		        WriteInt(CurCp);
		        WriteInt((int) _actor.Status.GetCurrentCp());
		        WriteInt(MaxCp);
		        WriteInt(_actor.Stat.GetMaxCp());
	        }
	        else
	        {
		        WriteInt(_objectId);
		        WriteInt(_attributes.Count);
			
		        foreach (var temp in _attributes)
		        {
			        WriteInt(temp.Id);
			        WriteInt(temp.Value);
		        }
	        }
        }
    }
}