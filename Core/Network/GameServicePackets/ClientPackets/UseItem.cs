﻿using System;
using System.Threading.Tasks;
using Core.Model.Items;
using Core.Model.Player;

namespace Core.Network.GameServicePackets.ClientPackets
{
    public class UseItem : PacketBase
    {
        private readonly int _objectId;
        private readonly PlayerInstance _playerInstance;
        
        public UseItem(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _objectId = packet.ReadInt();
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
        }

        public override async Task Execute()
        {
            ItemInstance item = GetItemByObjectId();
            await _playerInstance.PlayerUseItem().UseItemAsync(item);
        }

        private ItemInstance GetItemByObjectId()
        {
            return _playerInstance.PlayerInventory().GetItemByObjectId(_objectId);
        }
    }
}