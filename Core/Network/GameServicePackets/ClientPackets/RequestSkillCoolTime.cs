﻿using System;
using System.Threading.Tasks;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Network.GameServicePackets.ClientPackets
{
    public class RequestSkillCoolTime : PacketBase
    {
        private readonly GameServiceController _controller;
        
        public RequestSkillCoolTime(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _controller = controller;
        }

        public override async Task Execute()
        {
            PlayerInstance playerInstance = _controller.GameServiceHelper.CurrentPlayer;
            await _controller.SendPacketAsync(new SkillCoolTime(playerInstance));
        }
    }
}