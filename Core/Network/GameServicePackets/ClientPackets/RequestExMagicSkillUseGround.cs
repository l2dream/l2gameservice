﻿using System;
using System.Threading.Tasks;
using Core.Helper;
using Core.Model;
using Core.Model.Player;
using Core.Model.Skills;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Network.GameServicePackets.ClientPackets
{
    public class RequestExMagicSkillUseGround : PacketBase
    {
        private int _x;
        private int _y;
        private int _z;
        private readonly int _skillId;
        private bool _ctrlPressed;
        private bool _shiftPressed;
        private readonly SkillInit _skillInit;

        private readonly GameServiceController _controller;
        
        public RequestExMagicSkillUseGround(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _skillId = packet.ReadInt();
            _x = packet.ReadInt();
            _y = packet.ReadInt();
            _z = packet.ReadInt();
            _ctrlPressed = packet.ReadInt() != 0;
            _shiftPressed = packet.ReadByte() != 0;
            _controller = controller;
            _skillInit = Initializer.ServiceProvider.GetService<SkillInit>();
        }

        public override async Task Execute()
        {
            PlayerInstance playerInstance = _controller.GameServiceHelper.CurrentPlayer;
            // Get the level of the used skill
            int level = playerInstance.PlayerSkill().GetSkillLevel(_skillId);
            if (level <= 0)
            {
                await playerInstance.SendActionFailedPacketAsync();
                return;
            }

            Skill skill = _skillInit.GetSkill(_skillId, level);
            playerInstance.PlayerSkill().CurrentSkillWorldPosition = new Location(_x, _y, _z);
            playerInstance.Heading =
                CalculateRange.CalculateHeadingFrom(playerInstance.GetX(), playerInstance.GetY(), _x, _y);
            await playerInstance.PlayerSkillMagicUse().UseMagicAsync(skill, _ctrlPressed, _shiftPressed);
        }
    }
}