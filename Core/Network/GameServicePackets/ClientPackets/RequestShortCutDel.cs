﻿using System;
using System.Threading.Tasks;
using Core.Model.Player;

namespace Core.Network.GameServicePackets.ClientPackets
{
    internal sealed class RequestShortCutDel : PacketBase
    {
        private readonly int _slot;
        private readonly int _page;

        private readonly PlayerInstance _playerInstance;
        
        public RequestShortCutDel(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            int id = packet.ReadInt();
            _slot = id % 12;
            _page = id / 12;
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
        }

        public override async Task Execute()
        {
            await _playerInstance.PlayerShortCut().DeleteShortCutAsync(_slot, _page);
        }
    }
}