﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Model.Player;
using Core.Model.Player.Template;
using Core.Network.GameServicePackets.ServerPackets.CharacterPackets;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Network.GameServicePackets.ClientPackets.Character
{
    public class NewCharacter : PacketBase
    {
        private readonly GameServiceController _controller;
        private readonly IList<PlayerTemplate> _templates;
        private readonly PlayerTemplateInit _playerTemplateInit;
        public NewCharacter(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _playerTemplateInit = serviceProvider.GetService<PlayerTemplateInit>();
            _templates = new List<PlayerTemplate>
            {
                _playerTemplateInit.GetTemplateByClassId(ClassIds.HumanFighter),
                _playerTemplateInit.GetTemplateByClassId(ClassIds.HumanMystic),
                _playerTemplateInit.GetTemplateByClassId(ClassIds.ElvenFighter),
                _playerTemplateInit.GetTemplateByClassId(ClassIds.ElvenFighter),
                _playerTemplateInit.GetTemplateByClassId(ClassIds.ElvenMystic),
                _playerTemplateInit.GetTemplateByClassId(ClassIds.DarkFighter),
                _playerTemplateInit.GetTemplateByClassId(ClassIds.DarkMystic),
                _playerTemplateInit.GetTemplateByClassId(ClassIds.OrcFighter),
                _playerTemplateInit.GetTemplateByClassId(ClassIds.OrcMystic),
                _playerTemplateInit.GetTemplateByClassId(ClassIds.DwarvenFighter)
            };
            _controller = controller;
        }

        public override async Task Execute()
        {
            await _controller.SendPacketAsync(new CharTemplates(_templates));
        }
    }
}