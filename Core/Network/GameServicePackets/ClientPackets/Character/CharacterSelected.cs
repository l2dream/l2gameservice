﻿using System;
using System.Threading.Tasks;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets.CharacterPackets;
using L2Logger;

namespace Core.Network.GameServicePackets.ClientPackets.Character
{
    public class CharacterSelected : PacketBase 
    {
        private readonly GameServiceController _controller;
        private readonly IServiceProvider _serviceProvider;
        
        private readonly int _charSlot;
        private int _unk1; // new in C4
        private int _unk2;
        private int _unk3;
        private int _unk4;
        
        public CharacterSelected(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _controller = controller;
            _serviceProvider = serviceProvider;
            
            _charSlot = packet.ReadInt();
            _unk1 = packet.ReadShort();
            _unk2 = packet.ReadInt();
            _unk3 = packet.ReadInt();
            _unk4 = packet.ReadInt();
        }

        public override async Task Execute()
        {
            try
            {
                PlayerInstance player = _controller.GameServiceHelper.GetCharacterBySlot(_charSlot);
                _controller.GameServiceHelper.CurrentPlayer = player;
                player.Controller = _controller;
                await player.Status.SetCurrentHpDirectAsync(player.Status.CurrHp);
                await player.Status.SetCurrentCpDirectAsync(player.Status.CurrCp);
                player.Status.SetCurrentMpDirect(player.Status.CurrMp);
                await _controller.SendPacketAsync(new CharSelected(player, _controller.SessionKey.PlayOkId1));
            }
            catch (Exception ex)
            {
                LoggerManager.Error(ex.Message);
                throw;
            }
        }
    }
}