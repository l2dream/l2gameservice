﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Model;
using Core.Model.Items;
using Core.Model.Player;
using Core.Model.Player.Template;
using Core.Model.Skills;
using Core.Model.World;
using Core.Network.GameServicePackets.ServerPackets.CharacterPackets;
using DataBase.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Network.GameServicePackets.ClientPackets.Character
{
    public class CharacterCreate : PacketBase
    {
        private readonly GameServiceController _controller;
        private readonly IServiceProvider _serviceProvider;
        private readonly ICharacterRepository _characterRepository;
        private readonly PlayerTemplateInit _playerTemplateInit;
        private readonly SkillTreeInit _skillTreeInit;
        private readonly SkillInit _skillInit;
        
        private readonly string _characterName;
        private readonly int _race;
        private readonly int _sex;
        private readonly int _classId;
        private readonly int _hairStyle;
        private readonly int _hairColor;
        private readonly int _face;
        
        public CharacterCreate(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _controller = controller;
            _serviceProvider = serviceProvider;
            _characterRepository = serviceProvider.GetService<IUnitOfWork>()?.Characters;
            _playerTemplateInit = serviceProvider.GetService<PlayerTemplateInit>();
            
            _skillTreeInit = _serviceProvider.GetService<SkillTreeInit>();
            _skillInit = _serviceProvider.GetService<SkillInit>();
            
            _characterName = packet.ReadString();
            _race = packet.ReadInt();
            _sex = packet.ReadInt();
            _classId = packet.ReadInt();
            packet.ReadInt(); //INT
            packet.ReadInt(); //STR
            packet.ReadInt(); //CON
            packet.ReadInt(); //MEN
            packet.ReadInt(); //DEX
            packet.ReadInt(); //WIT
            _hairStyle = packet.ReadInt();
            _hairColor = packet.ReadInt();
            _face = (byte)packet.ReadInt();

        }

        public override async Task Execute()
        {
            if (_characterName.Length < 3 || _characterName.Length > 16)
            {
                await _controller.SendPacketAsync(new CharCreateFail(CharCreateFail.REASON_16_ENG_CHARS));
                return;
            }

            if (_characterRepository.IsCharacterExist(_characterName))
            {
                await _controller.SendPacketAsync(new CharCreateFail(CharCreateFail.REASON_NAME_ALREADY_EXISTS));
                return;
            }
            
            
            int objectId = _serviceProvider.GetService<IdFactory>().NextId(); //Object Id in the World
            var templateStat = _playerTemplateInit.GetTemplateByClassId(_classId);
            
            PlayerAppearance app = new PlayerAppearance((byte) _face, (byte) _hairColor, (byte) _hairStyle, _sex != 0);
            PlayerInstance playerInstance = new PlayerInstance(objectId, templateStat, _controller.AccountName, _characterName, app);
            playerInstance.Controller = _controller;
            await playerInstance.PlayerModel().CreateCharacter((int) templateStat.Stat.ClassId.Id);
            await _controller.SendPacketAsync(new CharacterCreateOk());
            await InitNewCharAsync(playerInstance, _controller);
        }

        private async Task InitNewCharAsync(PlayerInstance playerInstance, GameServiceController controller)
        {
            _serviceProvider.GetService<WorldInit>()?.StoreObject(playerInstance);
            var template = playerInstance.Template;
            
            // Shortcuts
            playerInstance.PlayerShortCut().RegisterShortCut(new ShortCut(0, 0, 3, 2, -1)); // Attack
            playerInstance.PlayerShortCut().RegisterShortCut(new ShortCut(3, 0, 3, 5, -1)); // Take
            playerInstance.PlayerShortCut().RegisterShortCut(new ShortCut(10, 0, 3, 0, -1)); // Sit
            foreach (ItemHolder item in template.Stat.Items)
            {
                ItemInstance itemInstance = playerInstance.PlayerInventory().AddItem(ItemAction.Init, item.Id, (int) item.Count, playerInstance, null);
                var test = itemInstance.ItemId;
                if (itemInstance.IsEquippable())
                {
                    playerInstance.PlayerInventory().EquipItemAndRecord(itemInstance);
                }
                
                if (itemInstance.ItemId == 5588)
                {
                    playerInstance.PlayerShortCut().RegisterShortCut(new ShortCut(11, 0, 1, itemInstance.ObjectId, -1)); // Tutorial Book shortcut
                }
            }
            
            List<SkillLearn> startSkills = _skillTreeInit.GetAvailableSkills(playerInstance, playerInstance.Template.Stat.ClassId);
            foreach (var startSkill in startSkills)
            {
                playerInstance.PlayerSkill().AddSkill(_skillInit.GetSkill(startSkill.Id, startSkill.Level), true);
                if ((startSkill.Id == 1001) || (startSkill.Id == 1177))
                {
                    playerInstance.PlayerShortCut().RegisterShortCut(new ShortCut(1, 0, 2, startSkill.Id, 1));
                }

                if (startSkill.Id == 1216)
                {
                    playerInstance.PlayerShortCut().RegisterShortCut(new ShortCut(10, 0, 2, startSkill.Id, 1));
                }
            }
            
            var characters = _characterRepository.GetCharactersByAccountNameAsync(_controller.AccountName);

            // Send char list
            CharSelectInfo info =
                new CharSelectInfo(controller.AccountName, controller.SessionKey.PlayOkId1, characters.Result);
            await controller.SendPacketAsync(info);
            controller.GameServiceHelper.SetCharSelection(info.CharacterPackages);
        }
    }
}