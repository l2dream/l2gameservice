﻿using System;
using System.Threading.Tasks;
using Core.Model;
using Core.Model.Actor.Data;
using Core.Model.Player;
using Core.Model.World.RegionData;
using L2Logger;

namespace Core.Network.GameServicePackets.ClientPackets
{
    internal sealed class RequestRestartPoint : PacketBase
    {
        private readonly int _requestedPointType;
        private readonly PlayerInstance _playerInstance;
        
        public RequestRestartPoint(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _requestedPointType = packet.ReadInt();
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
        }

        public override async Task Execute()
        {
            await new DeathTask(_playerInstance, _requestedPointType).Run();
        }

        private class DeathTask : IRunnable
        {
            private readonly int _requestedPointType;
            private readonly PlayerInstance _playerInstance;
            public DeathTask(PlayerInstance playerInstance, int requestedPointType)
            {
                _requestedPointType = requestedPointType;
                _playerInstance = playerInstance;
            }
            public async Task Run()
            {
                try
                {
                    Location loc = null;
                    switch (_requestedPointType)
                    {
                        case 1:
                            break;
                        case 2:
                            break;
                        default:
                            loc = Initializer.MapRegionService().GetTeleToLocation(_playerInstance, TeleportWhereType.Town);
                            break;
                    }
                    _playerInstance.Status.IsPendingRevive = true;
                    await _playerInstance.Teleport().TeleToLocationAsync(loc, true);
                }
                catch (Exception ex)
                {
                    LoggerManager.Error(GetType().Name + " " + ex.Message);
                }
            }
        }
    }
}