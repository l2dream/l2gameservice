﻿using System;
using System.Threading.Tasks;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Network.GameServicePackets.ClientPackets
{
    internal sealed class StartRotating : PacketBase
    {
        private readonly int _degree;
        private readonly int _side;
        private readonly PlayerInstance _playerInstance;
        
        public StartRotating(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _degree = packet.ReadInt();
            _side = packet.ReadInt();
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
        }

        public override async Task Execute()
        {
            await _playerInstance.SendBroadcastPacketAsync(new BeginRotation(_playerInstance, _degree, _side, 0));
        }
    }
}