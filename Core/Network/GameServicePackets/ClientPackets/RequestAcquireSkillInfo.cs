﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Core.Model.Npc;
using Core.Model.Npc.Type;
using Core.Model.Player;
using Core.Model.Skills;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Network.GameServicePackets.ClientPackets
{
    internal sealed class RequestAcquireSkillInfo : PacketBase
    {
        private readonly int _id;
        private readonly int _level;
        private readonly int _skillType;
        private readonly PlayerInstance _playerInstance;
        
        public RequestAcquireSkillInfo(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _id = packet.ReadInt();
            _level = packet.ReadInt();
            _skillType = packet.ReadInt();

            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
        }

        public override async Task Execute()
        {
            L2Trainer trainer = _playerInstance.LastTrainerNpc;
            if (trainer == null)
            {
                return;
            }

            if (!_playerInstance.Zone().IsInsideRadius(trainer, NpcInstance.InteractionDistance, false, false))
            {
                return;
            }

            Skill skill = Initializer.SkillService().GetSkill(_id, _level);
            await GetSkillType(skill);
        }

        private async Task GetSkillType(Skill skill)
        {
            AcquireSkillInfo acquireSkillInfo;
            switch (_skillType)
            {
                case 0:
                    acquireSkillInfo = BasicSkills(skill);
                    await _playerInstance.SendPacketAsync(acquireSkillInfo);
                    break;
                case 2:
                    //PledgeSkills();
                    break;
                default:
                    acquireSkillInfo = DefaultSkills(skill);
                    await _playerInstance.SendPacketAsync(acquireSkillInfo);
                    break;
            }
        }

        private AcquireSkillInfo BasicSkills(Skill skill)
        {
            var skillLearn = Initializer
                .SkillTreeService()
                .GetAvailableSkills(_playerInstance, _playerInstance.Template.Stat.ClassId)
                .FirstOrDefault(s => s.Id == _id && s.Level == _level);
            if (skillLearn == null)
                return new AcquireSkillInfo(0, 0, 0, 0);
            AcquireSkillInfo acquireSkillInfo = new AcquireSkillInfo(skill.Id, skill.Level, skillLearn.SpCost, 0);
            //check if skill require a book
            int spbId = -1;
            if (skill.Id == SkillStat.SkillDivineInspiration)
            {
                spbId = Initializer.SkillSpellBookService().GetBookForSkill(skill, _level);
            } 
            else if (skill.Level == 1)
            {
                spbId = Initializer.SkillSpellBookService().GetBookForSkill(skill);
            }
            if (spbId > -1)
            {
                acquireSkillInfo.AddRequirement(99, spbId, 1, 50);
            }
            return acquireSkillInfo;
        }

        private AcquireSkillInfo DefaultSkills(Skill skill)
        {
            var skillLearn = Initializer
                .SkillTreeService()
                .GetAvailableSkills(_playerInstance, _playerInstance.Template.Stat.ClassId)
                .FirstOrDefault(s => s.Id == _id && s.Level == _level);
            if (skillLearn == null) 
                return new AcquireSkillInfo(0, 0, 0, 0);
            
            AcquireSkillInfo acquireSkillInfo = new AcquireSkillInfo(skill.Id, skill.Level, skillLearn.SpCost, 1);
            int spbId = -1;
            if (skill.Id == SkillStat.SkillDivineInspiration)
            {
                spbId = Initializer.SkillSpellBookService().GetBookForSkill(skill, _level);
            } 
            else if (skill.Level == 1)
            {
                spbId = Initializer.SkillSpellBookService().GetBookForSkill(skill);
            }
            if (spbId > -1)
            {
                acquireSkillInfo.AddRequirement(99, spbId, 1, 50);
            }
            return acquireSkillInfo;
        }
    }
}