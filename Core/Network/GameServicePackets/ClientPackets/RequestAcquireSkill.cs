﻿using System;
using System.Threading.Tasks;
using Core.Model.Npc;
using Core.Model.Npc.Type;
using Core.Model.Player;
using Core.Model.Skills;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Network.GameServicePackets.ClientPackets
{
    internal sealed class RequestAcquireSkill : PacketBase
    {
        private readonly int _id;
        private readonly int _level;
        private readonly int _skillType;
        private readonly PlayerInstance _playerInstance;
        private readonly PlayerSkill _playerSkill;
        private readonly SkillInit _skillService;

        public RequestAcquireSkill(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _id = packet.ReadInt();
            _level = packet.ReadInt();
            _skillType = packet.ReadInt();
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
            _playerSkill = _playerInstance.PlayerSkill();
            _skillService = Initializer.SkillService();
        }

        public override async Task Execute()
        {
            L2Trainer trainer = _playerInstance.LastTrainerNpc;
            if (trainer == null)
            {
                return;
            }

            if (!_playerInstance.Zone().IsInsideRadius(trainer, NpcInstance.InteractionDistance, false, false))
            {
                return;
            }

            if (_playerSkill.GetSkillLevel(_id) >= _level)
            {
                // already knows the skill with this level
                return;
            }
            AddSkillToPlayer();

            await SendStatusUpdateAsync();
            await SendSpDecreasedAsync();
            await SendLearnedSkillAsync();
            await trainer.ShowSkillListAsync(_playerInstance, _playerInstance.Template.Stat.ClassId);

            await _playerSkill.SendSkillListAsync();
        }

        private async Task SendLearnedSkillAsync()
        {
            SystemMessage sm = new SystemMessage(SystemMessageId.LearnedSkillS1);
            sm.AddSkillName(_id, 1);
            await _playerInstance.SendPacketAsync(sm);
        }

        private async Task SendSpDecreasedAsync()
        {
            SystemMessage sp = new SystemMessage(SystemMessageId.SpDecreasedS1);
            sp.AddNumber(5);
            await _playerInstance.SendPacketAsync(sp);
        }

        private async Task SendStatusUpdateAsync()
        {
            StatusUpdate su = new StatusUpdate(_playerInstance.ObjectId);
            su.AddAttribute(StatusUpdate.Sp, (int) _playerInstance.Stat.Sp);
            await _playerInstance.SendPacketAsync(su);
        }

        private void AddSkillToPlayer()
        {
            Skill skill = _skillService.GetSkill(_id, _level);
            _playerSkill.AddSkill(skill, true);
        }
    }
}