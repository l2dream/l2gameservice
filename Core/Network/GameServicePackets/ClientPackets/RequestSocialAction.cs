﻿using System;
using System.Threading.Tasks;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Network.GameServicePackets.ClientPackets
{
    internal sealed class RequestSocialAction : PacketBase
    {
        private readonly int _actionId;
        private readonly PlayerInstance _playerInstance;
        public RequestSocialAction(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _actionId = packet.ReadInt();
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
        }

        public override async Task Execute()
        {
            //TODO add validation
            await _playerInstance.SendBroadcastPacketAsync(new SocialAction(_playerInstance.ObjectId, _actionId));
        }
    }
}