﻿using System;
using System.Threading.Tasks;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Network.GameServicePackets.ClientPackets
{
    public class RequestItemList : PacketBase
    {
        private readonly GameServiceController _controller;
        public RequestItemList(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _controller = controller;
        }

        public override async Task Execute()
        {
            PlayerInstance playerInstance = _controller.GameServiceHelper.CurrentPlayer;
            await _controller.SendPacketAsync(new ItemList(playerInstance, true));
        }
    }
}