﻿using System;
using System.Threading.Tasks;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Network.GameServicePackets.ClientPackets
{
    public class RequestShowMiniMap : PacketBase
    {
        private readonly PlayerInstance _playerInstance;
        public RequestShowMiniMap(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
        }

        public override async Task Execute()
        {
            await _playerInstance.SendPacketAsync(new ShowMiniMap(1665));
        }
    }
}