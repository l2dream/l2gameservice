﻿using System;
using System.Threading.Tasks;
using Core.Model;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Network.GameServicePackets.ClientPackets
{
    internal sealed class RequestShortCutReg : PacketBase
    {
        private readonly int _type;
        private readonly int _id;
        private readonly int _slot;
        private readonly int _page;
        private readonly PlayerInstance _playerInstance;
        private readonly PlayerShortCut _playerShortCut;

        public RequestShortCutReg(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _type = packet.ReadInt();
            int slot = packet.ReadInt();
            _id = packet.ReadInt();
            _slot = slot % 12;
            _page = slot / 12;
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
            _playerShortCut = _playerInstance.PlayerShortCut();
        }

        public override async Task Execute()
        {
            switch (_type)
            {
                case ShortCut.TypeItem: // item
                case ShortCut.TypeAction: // action
                case ShortCut.TypeMacro: // macro
                case ShortCut.TypeRecipe: // recipe
                {
                    ShortCut sc = new ShortCut(_slot, _page, _type, _id, -1);
                    await _playerInstance.SendPacketAsync(new ShortCutRegister(sc));
                    _playerShortCut.RegisterShortCut(sc);
                    break;
                }
                case ShortCut.TypeSkill: // skill
                {
                    int level = _playerInstance.PlayerSkill().GetSkillLevel(_id);
                    if (level > 0)
                    {
                        ShortCut sc = new ShortCut(_slot, _page, _type, _id, level);
                        await _playerInstance.SendPacketAsync(new ShortCutRegister(sc));
                        _playerShortCut.RegisterShortCut(sc);
                    }

                    break;
                }
            }
        }
    }
}