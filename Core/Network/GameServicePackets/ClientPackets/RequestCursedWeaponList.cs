﻿using System;
using System.Threading.Tasks;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Network.GameServicePackets.ClientPackets
{
    public class RequestCursedWeaponList : PacketBase
    {
        private readonly PlayerInstance _player;
        
        public RequestCursedWeaponList(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _player = controller.GameServiceHelper.CurrentPlayer;
        }

        public override async Task Execute()
        {
            await _player.SendPacketAsync(new ExCursedWeaponList());
        }
    }
}