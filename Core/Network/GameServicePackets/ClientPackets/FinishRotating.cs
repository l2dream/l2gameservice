﻿using System;
using System.Threading.Tasks;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Network.GameServicePackets.ClientPackets
{
    internal sealed class FinishRotating : PacketBase
    {
        private readonly int _degree;
        private readonly GameServiceController _controller;
        
        public FinishRotating(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _degree = packet.ReadInt();
            _controller = controller;
        }

        public override async Task Execute()
        {
            PlayerInstance playerInstance = _controller.GameServiceHelper.CurrentPlayer;
            await playerInstance.SendBroadcastPacketAsync(new StopRotation(playerInstance, _degree, 0));
        }
    }
}