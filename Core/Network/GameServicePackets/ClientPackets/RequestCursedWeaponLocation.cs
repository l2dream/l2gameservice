﻿using System;
using System.Threading.Tasks;

namespace Core.Network.GameServicePackets.ClientPackets
{
    /// <summary>
    /// TODO Not implemented yet
    /// </summary>
    public class RequestCursedWeaponLocation : PacketBase
    {
        public RequestCursedWeaponLocation(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
        }

        public override async Task Execute()
        {
            await Task.FromResult(1);
        }
    }
}