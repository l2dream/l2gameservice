﻿using System;
using System.Threading.Tasks;
using Core.Model.Items;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Network.GameServicePackets.ClientPackets
{
    public class RequestDestroyItem : PacketBase
    {
        private readonly int _objectId;
        private readonly int _count;
        private readonly PlayerInstance _playerInstance;
        private readonly PlayerInventory _playerInventory;

        public RequestDestroyItem(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _objectId = packet.ReadInt();
            _count = packet.ReadInt();
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
            _playerInventory = _playerInstance.PlayerInventory();
        }

        public override async Task Execute()
        {
            int count = _count;
            ItemInstance itemToRemove = _playerInventory.GetItemByObjectId(_objectId);
            if (_count > itemToRemove.Count)
            {
                count = itemToRemove.Count;
            }

            var removedItem = _playerInventory.DestroyItem(ItemAction.Destroy, _objectId, count, _playerInstance,
                    null);
            InventoryUpdate iu = new InventoryUpdate();
            if (removedItem.Count == 0)
            {
                iu.AddRemovedItem(removedItem);
            }
            else
            {
                iu.AddModifiedItem(removedItem);
            }

            await _playerInstance.SendPacketAsync(iu);
            StatusUpdate su = new StatusUpdate(_playerInstance.ObjectId);
            su.AddAttribute(StatusUpdate.CurLoad, _playerInstance.Stat.CurrentLoad());
            await _playerInstance.SendPacketAsync(su);
        }
    }
}