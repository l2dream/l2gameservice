﻿using System;
using System.Threading.Tasks;
using Core.AI;
using Core.Model;
using Core.Model.Player;

namespace Core.Network.GameServicePackets.ClientPackets
{
    internal sealed class CannotMoveAnymore : PacketBase
    {
        private readonly int _x;
        private readonly int _y;
        private readonly int _z;
        private readonly int _heading;
        private readonly GameServiceController _controller;
        
        public CannotMoveAnymore(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _x = packet.ReadInt();
            _y = packet.ReadInt();
            _z = packet.ReadInt();
            _heading = packet.ReadInt();
            _controller = controller;
        }

        public override async Task Execute()
        {
            await Task.Run(() =>
            {
                PlayerInstance player = _controller.GameServiceHelper.CurrentPlayer;
                player.AI?.NotifyEvent(CtrlEvent.EvtArrivedBlocked, new Location(_x, _y, _z, _heading));
            });
        }
    }
}