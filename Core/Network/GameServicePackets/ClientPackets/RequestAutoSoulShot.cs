﻿using System;
using System.Threading.Tasks;
using Core.Model.Items;
using Core.Model.Player;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Network.GameServicePackets.ClientPackets
{
    internal sealed class RequestAutoSoulShot : PacketBase
    {
        private readonly int _itemId;
        private readonly int _type; // 1 = on : 0 = off;
        private readonly PlayerInstance _playerInstance;
        private readonly PlayerSoulShot _playerSoulShot;
        private readonly PlayerInventory _playerInventory;
        private readonly PlayerMessage _playerMessage;
        
        public RequestAutoSoulShot(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _itemId = packet.ReadInt();
            _type = packet.ReadInt();
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
            _playerInventory = _playerInstance.PlayerInventory();
            _playerSoulShot = _playerInstance.PlayerSoulShot();
            _playerMessage = _playerInstance.PlayerMessage();
        }

        public override async Task Execute()
        {
            ItemInstance item = _playerInventory.GetItemByItemId(_itemId);
            if (item is null)
                return;
            switch (_type)
            {
                case 1:
                    await AddAutoSoulShotAsync(item);
                    break;
                case 0:
                    await RemoveAutoSoulShotAsync(item);
                    break;
            }
            await _playerInstance.SendPacketAsync(new ExAutoSoulShot(_itemId, _type));
        }

        private async Task AddAutoSoulShotAsync(ItemInstance item)
        {
            if (await IsNotValidUseAutoSoulShot(item)) return;
            _playerSoulShot.AddAutoSoulShot(_itemId);
            // start the auto SoulShot use
            await _playerMessage.SendMessageToPlayerAsync(item, SystemMessageId.UseOfS1WillBeAuto);
            await _playerSoulShot.RechargeAutoSoulShotAsync();
        }

        private Task<bool> IsNotValidUseAutoSoulShot(ItemInstance item)
        {
            bool result = (_playerInstance.GetActiveWeaponItem() == _playerInstance.FistsWeaponItem) ||
                          (item.Item.CrystalType != _playerInstance.GetActiveWeaponItem().CrystalType);
            return Task.FromResult(result);
        }

        private async Task RemoveAutoSoulShotAsync(ItemInstance item)
        {
            // cancel the auto SoulShot use
            _playerSoulShot.RemoveAutoSoulShot(_itemId);
            await _playerMessage.SendMessageToPlayerAsync(item, SystemMessageId.AutoUseOfS1Cancelled);
        }
    }
}