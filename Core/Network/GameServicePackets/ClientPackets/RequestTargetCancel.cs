﻿using System;
using System.Threading.Tasks;
using Core.Model.Player;

namespace Core.Network.GameServicePackets.ClientPackets
{
    public class RequestTargetCancel : PacketBase
    {
        private readonly int _unselect;
        private readonly PlayerInstance _playerInstance;
        
        public RequestTargetCancel(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
            _unselect = packet.ReadShort();
        }

        public override async Task Execute()
        {
            await Target().CancelTargetAsync(_unselect);
        }

        private PlayerTarget Target()
        {
            return (PlayerTarget)_playerInstance.Target;
        }
    }
}