﻿using System;
using System.Threading.Tasks;

namespace Core.Network.GameServicePackets.ClientPackets
{
    public class RequestPledgeInfo : PacketBase
    {
        private int clanId;
        
        public RequestPledgeInfo(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            clanId = packet.ReadInt();
        }

        public override async Task Execute()
        {
            await Task.FromResult(1);
        }
    }
}