﻿using System;
using System.Threading.Tasks;
using Core.Model.Player;
using Core.Model.World;

namespace Core.Network.GameServicePackets.ClientPackets
{
    public class ActionRequest : PacketBase
    {
        private readonly int _objectId;
        private int _originX;
        private int _originY;
        private int _originZ;
        private readonly int _actionId;
        private readonly PlayerInstance _playerInstance;
        
        public ActionRequest(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
            _objectId = packet.ReadInt(); // Target object Identifier
            _originX = packet.ReadInt();
            _originY = packet.ReadInt();
            _originZ = packet.ReadInt();
            _actionId = packet.ReadByte(); // Action identifier : 0-Simple click, 1-Shift click
        }

        public override async Task Execute()
        {
            var worldObject = GetWorldObject();
            await worldObject.OnActionAsync(_playerInstance);
        }

        private WorldObject GetWorldObject()
        {
            return _playerInstance.Target.GetTargetId() == _objectId
                ? _playerInstance.Target.GetTarget()
                : Initializer.WorldInit().FindObject(_objectId);
        }
    }
}