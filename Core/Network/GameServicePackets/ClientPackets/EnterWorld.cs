﻿using System;
using System.Threading.Tasks;
using Core.Model.Player;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Network.GameServicePackets.ClientPackets
{
    public class EnterWorld : PacketBase
    {
        private readonly PlayerInstance _playerInstance;
        
        public EnterWorld(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
        }

        public override async Task Execute()
        {
            _playerInstance.PlayerModel().SetOnlineStatus(true); // Set online status
            await _playerInstance.Movement().SetRunningAsync(); // running is default
            _playerInstance.PlayerAction().StandUp(); // standing is default

            await _playerInstance.SendActionFailedPacketAsync();
            _playerInstance.SpawnMe(_playerInstance.GetX(), _playerInstance.GetY(), _playerInstance.GetZ());
            await _playerInstance.SendPacketAsync(new EtcStatusUpdate(_playerInstance));
            await _playerInstance.SendPacketAsync(new UserInfo(_playerInstance));
            await _playerInstance.SendPacketAsync(new ClientSetTime()); // SetClientTime
            await _playerInstance.SendPacketAsync(new ItemList(_playerInstance, false));
            await _playerInstance.SendPacketAsync(new ShortCutInit(_playerInstance)); // SetClientTime
            await _playerInstance.SendPacketAsync(new SystemMessage(SystemMessageId.WelcomeToLineage));
        }
    }
}