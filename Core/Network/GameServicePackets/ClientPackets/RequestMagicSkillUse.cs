﻿using System;
using System.Threading.Tasks;
using Core.Model.Player;
using Core.Model.Skills;

namespace Core.Network.GameServicePackets.ClientPackets
{
    internal sealed class RequestMagicSkillUse : PacketBase
    {
        private readonly int _magicId;
        private readonly bool _ctrlPressed;
        private readonly bool _shiftPressed;
        private readonly PlayerInstance _playerInstance;
        private readonly SkillInit _skillInit;
        
        public RequestMagicSkillUse(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _magicId = packet.ReadInt(); // Identifier of the used skill
            _ctrlPressed = packet.ReadInt() != 0; // True if it's a ForceAttack : Ctrl pressed
            _shiftPressed = packet.ReadByte() != 0; // True if Shift pressed
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
            _skillInit = Initializer.SkillService();
        }

        public override async Task Execute()
        {
            int level = _playerInstance.PlayerSkill().GetSkillLevel(_magicId);
            Skill skill = _skillInit.GetSkill(_magicId, level);
            await _playerInstance.PlayerSkillMagicUse().UseMagicAsync(skill, _ctrlPressed, _shiftPressed);
        }
    }
}