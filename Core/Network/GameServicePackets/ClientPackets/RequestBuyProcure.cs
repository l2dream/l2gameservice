﻿using System;
using System.Threading.Tasks;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Network.GameServicePackets.ClientPackets
{
    public class RequestBuyProcure : PacketBase
    {
        private readonly PlayerInstance _playerInstance;
        
        public RequestBuyProcure(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
        }

        public override async Task Execute()
        {
            StatusUpdate su = new StatusUpdate(_playerInstance.ObjectId);
            su.AddAttribute(StatusUpdate.CurLoad, _playerInstance.Stat.CurrentLoad());
            await _playerInstance.SendPacketAsync(su);
        }
    }
}