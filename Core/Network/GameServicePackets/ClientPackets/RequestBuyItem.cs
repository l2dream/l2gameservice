﻿using System;
using System.Threading.Tasks;
using Core.Model.Items;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Network.GameServicePackets.ClientPackets
{
    internal sealed class RequestBuyItem : PacketBase
    {
        private int _listId;
        private int _count;
        private readonly int[] _items; // count*2
        private readonly PlayerInstance _playerInstance;
        private readonly PlayerInventory _playerInventory;

        public RequestBuyItem(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
            _listId = packet.ReadInt();
            _count = packet.ReadInt();
            _playerInventory = _playerInstance.PlayerInventory();

            _items = new int[_count * 2];
            InitItems(packet);
        }

        private void InitItems(Packet packet)
        {
            for (int i = 0; i < _count; i++)
            {
                int itemId = packet.ReadInt();
                _items[(i * 2) + 0] = itemId;
                long cnt = packet.ReadInt();
                if ((cnt > int.MaxValue) || (cnt < 0))
                {
                    _count = 0;
                    return;
                }
                _items[(i * 2) + 1] = (int) cnt;
            }
        }

        public override async Task Execute()
        {
            // Proceed the purchase
            for (int i = 0; i < _count; i++)
            {
                int itemId = _items[(i * 2) + 0];
                int count = _items[(i * 2) + 1];
                if (count < 0)
                {
                    count = 0;
                }

                // Add item to Inventory and adjust update packet
                _playerInventory.AddItem(ItemAction.Buy, itemId, count, _playerInstance,
                    _playerInstance.Target.GetTarget());
            }
            await _playerInstance.SendPacketAsync(new ItemList(_playerInstance, true));
        }
    }
}