﻿using System;
using System.Threading.Tasks;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Network.GameServicePackets.ClientPackets
{
    internal sealed class RequestGetBossRecord : PacketBase
    {
        private readonly PlayerInstance _playerInstance;
        public RequestGetBossRecord(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
        }

        public override async Task Execute()
        {
            await _playerInstance.SendPacketAsync(new ExGetBossRecord(0, 0, null));
            await _playerInstance.SendActionFailedPacketAsync();
        }
    }
}