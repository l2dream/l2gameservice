﻿using System;
using System.Threading.Tasks;
using Core.Model.Items;
using Core.Model.Npc.Type;
using Core.Model.Player;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Network.GameServicePackets.ClientPackets
{
    public class RequestSellItem : PacketBase
    {
        private readonly int _listId;
        private int _count;
        private int[] _items; // count*3
        private readonly PlayerInstance _playerInstance;
        private readonly PlayerInventory _playerInventory;

        public RequestSellItem(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _listId = packet.ReadInt();
            _count = packet.ReadInt();
            _items = PrepareItems(packet);
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
            _playerInventory = _playerInstance.PlayerInventory();
        }

        private int[] PrepareItems(Packet packet)
        {
            _items = new int[_count * 3];
            for (int i = 0; i < _count; i++)
            {
                int objectId = packet.ReadInt();
                _items[(i * 3) + 0] = objectId;
                int itemId = packet.ReadInt();
                _items[(i * 3) + 1] = itemId;
                long cnt = packet.ReadInt();
                if ((cnt > int.MaxValue) || (cnt <= 0))
                {
                    _count = 0;
                    return new[] {_count};
                }
                _items[(i * 3) + 2] = (int) cnt;
            }

            return _items;
        }

        public override async Task Execute()
        {
            L2Merchant merchant = (L2Merchant) _playerInstance.Target.GetTarget();
        
            if ((_listId > 1000000) && (merchant.NpcId != (_listId - 1000000)))
            {
                await _playerInstance.SendActionFailedPacketAsync();
                return;
            }

            long totalPrice = 0;
            // Proceed the sell
            for (int i = 0; i < _count; i++)
            {
                int objectId = _items[i * 3];
                int count = _items[(i * 3) + 2];
			
                // Check count
                if (count <= 0)
                {
                    await _playerInstance.SendPacketAsync(new SystemMessage(SystemMessageId.YouHaveExceededQuantityThatCanBeInputted));
                    return;
                }
			
                ItemInstance item = _playerInventory.GetItemByObjectId(objectId);
			
                // Check Item
                if ((item == null) || !item.Item.Sellable)
                {
                    continue;
                }
			
                long price = item.Item.ReferencePrice / 2;
                totalPrice += price * count;
			
                // Fix exploit during Sell
                if (((int.MaxValue / count) < price) || (totalPrice > int.MaxValue))
                {
                    // Util.handleIllegalPlayerAction(player, "Warning!! Character " + player.getName() + " of account " + player.getAccountName() + " tried to purchase over " + MAX_ADENA + " adena worth of goods.", Config.DEFAULT_PUNISH);
                    await _playerInstance.SendPacketAsync(new SystemMessage(SystemMessageId.YouHaveExceededQuantityThatCanBeInputted));
                    return;
                }
			
                // Check totalPrice
                if (totalPrice <= 0)
                {
                    await _playerInstance.SendPacketAsync(new SystemMessage(SystemMessageId.YouHaveExceededQuantityThatCanBeInputted));
                    return;
                }
                _playerInventory.DestroyItem(ItemAction.Sell, objectId, count, _playerInstance, null);
            }
            _playerInventory.AddAdena(ItemAction.Sell, (int) totalPrice, merchant, false);

            var html = Initializer.HtmlCacheService().GetHtm("/merchant/" + merchant.NpcId + "-sold.htm");
            if (html != null)
            {
                NpcHtmlMessage soldMsg = new NpcHtmlMessage(merchant.ObjectId);
                soldMsg.SetHtml(html.Replace("%objectId%", merchant.ObjectId.ToString()));
                await _playerInstance.SendPacketAsync(soldMsg);
            }
        }
    }
}