﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.AI;
using Core.Model.Items;
using Core.Model.Player;
using Core.Model.World;
using Core.Network.Enums;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Network.GameServicePackets.ClientPackets
{
    public class RequestUnEquipItem : PacketBase
    {
        private readonly int _slot;
        private readonly PlayerInstance _playerInstance;
        private readonly PlayerMessage _playerMessage;
        private readonly PlayerInventory _playerInventory;
        
        public RequestUnEquipItem(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _slot = packet.ReadInt();
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
            _playerMessage = _playerInstance.PlayerMessage();
            _playerInventory = _playerInstance.PlayerInventory();
        }

        public override async Task Execute()
        {
            ItemInstance item = _playerInventory.GetPaperdollItemByItemId(_slot);

            // Prevent player from unequipping items in special conditions
            if (_playerInstance.IsStunned || _playerInstance.IsConfused || _playerInstance.IsParalyzed || _playerInstance.IsSleeping ||
                _playerInstance.IsAlikeDead)
            {
                await _playerMessage.SendMessageAsync("Your status does not allow you to do that.");
                return;
            }

            if (_playerInstance.IsMoving && _playerInstance.Attack().IsAttackingNow() && ((_slot == (int) ItemSlotId.SlotLRHand) ||
                                                                        (_slot == (int) ItemSlotId.SlotLHand) ||
                                                                        (_slot == (int) ItemSlotId.SlotRHand)))
            {
                WorldObject target = _playerInstance.Target.GetTarget();
                _playerInstance.Target.RemoveTargetAsync();
                await _playerInstance.Movement().StopMoveAsync(null);
                _playerInstance.Target.SetTarget(target);
                _playerInstance.AI.SetIntention(CtrlIntention.AiIntentionAttack);
            }

            List<ItemInstance> unEquiped = _playerInventory.UnEquipItemInBodySlotAndRecord(_slot);

            // show the update in the inventory
            InventoryUpdate iu = new InventoryUpdate();
            unEquiped.ForEach(element =>
            {
                //player.CheckSSMatch(null, element); TODO SS
                iu.AddModifiedItem(element);
            });
            await _playerInstance.SendPacketAsync(iu);
            _playerInstance.SendBroadcastUserInfo();

            // this can be 0 if the user pressed the right mouse button twice very fast
            if (unEquiped.Count > 0)
            {
                SystemMessage sm;
                if (unEquiped[0].EnchantLevel > 0)
                {
                    sm = new SystemMessage(SystemMessageId.EquipmentS1S2Removed);
                    sm.AddNumber(unEquiped[0].EnchantLevel);
                    sm.AddItemName(unEquiped[0].ItemId);
                }
                else
                {
                    sm = new SystemMessage(SystemMessageId.S1Disarmed);
                    sm.AddItemName(unEquiped[0].ItemId);
                }

                await _playerInstance.SendPacketAsync(sm);
            }
        }
    }
}