﻿using System;
using System.Threading.Tasks;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;

namespace Core.Network.GameServicePackets.ClientPackets
{
    internal sealed class Appearing : PacketBase
    {
        private readonly PlayerInstance _playerInstance;
        public Appearing(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
        }
        public override async Task Execute()
        {
            if (_playerInstance.Status.IsTeleporting)
            {
                _playerInstance.OnTeleported();
            }
            await _playerInstance.SendPacketAsync(new UserInfo(_playerInstance));
        }
    }
}