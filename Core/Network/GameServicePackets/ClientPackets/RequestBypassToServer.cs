﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Core.Model.Npc;
using Core.Model.Player;
using Core.Model.World;

namespace Core.Network.GameServicePackets.ClientPackets
{
    internal sealed class RequestBypassToServer : PacketBase
    {
        private readonly PlayerInstance _playerInstance;
        private readonly string _command;
        
        public RequestBypassToServer(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
            _command = packet.ReadString();
        }

        public override async Task Execute()
        {
            if (_command.StartsWith("npc_"))
            {
                var id = Regex.Match(_command.Substring(4), @"\d+").Value;
                int objectId = Convert.ToInt32(id);
                WorldObject npcObject = Initializer.WorldInit().FindObject(objectId);
                await ((NpcInstance) npcObject).OnBypassFeedbackAsync(_playerInstance,
                    _command.Replace("npc_" + objectId + "_", ""));
            }
            else if (_command.StartsWith("player_help"))
            {

            }
        }
    }
}