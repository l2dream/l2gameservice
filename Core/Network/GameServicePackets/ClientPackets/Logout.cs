﻿using System;
using System.Threading.Tasks;
using Core.Model.Player;

namespace Core.Network.GameServicePackets.ClientPackets
{
    internal sealed class Logout : PacketBase
    {
        private readonly PlayerInstance _playerInstance;
        public Logout(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
        }

        public override async Task Execute()
        {
            await _playerInstance.PlayerEnter().DeleteMeAsync();
        }
    }
}