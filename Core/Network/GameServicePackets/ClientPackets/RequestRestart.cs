﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Core.Model.Player;
using Core.Network.GameServicePackets.ServerPackets;
using Core.Network.GameServicePackets.ServerPackets.CharacterPackets;
using DataBase.Interfaces;
using L2Logger;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Network.GameServicePackets.ClientPackets
{
    //TODO
    public class RequestRestart : PacketBase
    {
        private readonly GameServiceController _controller;
        private readonly ICharacterRepository _characterRepository;
        private readonly PlayerInstance _playerInstance;
        
        public RequestRestart(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            _controller = controller;
            _characterRepository = serviceProvider.GetService<IUnitOfWork>()?.Characters;
            _playerInstance = _controller.GameServiceHelper.CurrentPlayer;
        }

        public override async Task Execute()
        {
            try
            {
                await _playerInstance.PlayerEnter().DeleteMeAsync();
                _playerInstance.PlayerModel().Store();
            
                await _controller.SendPacketAsync(new RestartResponse(true));

                var characters = _characterRepository.GetCharactersByAccountNameAsync(_controller.AccountName);
                CharSelectInfo cl = new CharSelectInfo(_controller.AccountName, _controller.SessionKey.PlayOkId1, characters.Result);
                await _controller.SendPacketAsync(cl);
                _controller.GameServiceHelper.SetCharSelection(cl.CharacterPackages);
            }
            catch (Exception ex)
            {
                LoggerManager.Info(ex.Message);
            }
        }
    }
}