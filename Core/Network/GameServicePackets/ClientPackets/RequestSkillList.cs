﻿using System;
using System.Threading.Tasks;
using Core.Model.Player;

namespace Core.Network.GameServicePackets.ClientPackets
{
    public class RequestSkillList : PacketBase
    {
        private readonly PlayerSkill _playerSkill;

        public RequestSkillList(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            var playerInstance = controller.GameServiceHelper.CurrentPlayer;
            _playerSkill = playerInstance.PlayerSkill();
        }

        public override async Task Execute()
        {
            await _playerSkill.SendSkillListAsync();
        }
    }
}