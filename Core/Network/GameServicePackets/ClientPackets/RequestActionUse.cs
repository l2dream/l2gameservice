﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Model.Player;
using Core.Model.Player.ActionUse;
using Core.Model.Player.Handlers;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Network.GameServicePackets.ClientPackets
{
    internal sealed class RequestActionUse : PacketBase
    {
        private readonly int _actionId;
        private bool _ctrlPressed;
        private bool _shiftPressed;
        private readonly PlayerInstance _playerInstance;
        private readonly PlayerHandlerInit _playerHandler;

        public RequestActionUse(IServiceProvider serviceProvider, Packet packet, GameServiceController controller) : base(serviceProvider)
        {
            
            _actionId = packet.ReadInt();
            _ctrlPressed = packet.ReadInt() == 1;
            _shiftPressed = packet.ReadByte() == 1;
            _playerInstance = controller.GameServiceHelper.CurrentPlayer;
            _playerHandler = serviceProvider.GetService<PlayerHandlerInit>();
        }

        public override async Task Execute()
        {
            await _playerHandler.GetActionHandler(_actionId).
                DoActionAsync(_playerInstance);
        }
    }
}