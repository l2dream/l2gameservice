﻿using System;
using Core.Model.Skills;
using Helpers;

namespace Core.Helper
{
    public class Timestamp
    {
        public Skill Skill;
        public readonly long Reuse;
        public long Stamp;
        
        public Timestamp(Skill skill, long reuse)
        {
            Skill = skill;
            Reuse = reuse;
            Stamp = DateTimeHelper.CurrentUnixTimeMillis() + Reuse;
        }
	
        public Timestamp(Skill skill, long reuse, long stamp)
        {
            Skill = skill;
            Reuse = reuse;
            Stamp = stamp;
        }
        
        /// <summary>
        /// Gets the remaining time.
        /// </summary>
        /// <returns>the remaining time for this time stamp to expire</returns>
        public long GetRemaining()
        {
            if (Stamp == 0)
            {
                return 0;
            }
            long remainingTime = Math.Max(Stamp - DateTimeHelper.CurrentUnixTimeMillis(), 0);
            if (remainingTime == 0)
            {
                Stamp = 0;
            }
            return remainingTime;
        }
        
        /// <summary>
        /// Verifies if the reuse delay has passed.
        /// </summary>
        /// <returns>{@code true} if this time stamp has expired, {@code false} otherwise</returns>
        public bool HasNotPassed()
        {
            if (Stamp == 0)
            {
                return false;
            }
            bool hasNotPassed = DateTimeHelper.CurrentUnixTimeMillis() < Stamp;
            if (!hasNotPassed)
            {
                Stamp = 0;
            }
            return hasNotPassed;
        }
        
    }
}