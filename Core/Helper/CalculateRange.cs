﻿using System;
using Core.Model;
using Core.Model.Actor;
using Core.Model.Player;
using Core.Model.World;
using Helpers;

namespace Core.Helper
{
    internal static class CalculateRange
    {
        public static double CalculateDistance(int x1, int y1, int z1, int x2, int y2)
        {
            return CalculateDistance(x1, y1, 0, x2, y2, 0, false);
        }
        
        public static double CalculateDistance(int x1, int y1, int z1, int x2, int y2, int z2, bool includeZAxis)
        {
            double dx = (double) x1 - x2;
            double dy = (double) y1 - y2;
            if (includeZAxis)
            {
                double dz = z1 - z2;
                return Math.Sqrt((dx * dx) + (dy * dy) + (dz * dz));
            }
            return Math.Sqrt((dx * dx) + (dy * dy));
        }
        
        public static double CalculateDistance(WorldObject obj1, WorldObject obj2, bool includeZAxis)
        {
            if ((obj1 == null) || (obj2 == null))
            {
                return 1000000;
            }
            return CalculateDistance(obj1.WorldObjectPosition().GetX(), obj1.WorldObjectPosition().GetY(), obj1.WorldObjectPosition().GetZ(), obj2.WorldObjectPosition().GetX(), obj2.WorldObjectPosition().GetY(), obj2.WorldObjectPosition().GetZ(), includeZAxis);
        }
        
        public static bool CheckIfInRange(int range, WorldObject obj1, WorldObject obj2, bool includeZAxis)
        {
            if ((obj1 == null) || (obj2 == null))
            {
                return false;
            }
            if (range == -1)
            {
                return true; // not limited
            }
		
            float rad = 0;
            if (obj1 is Character character)
            {
                rad += character.Template.Stat.CollisionRadius;
            }
            if (obj2 is Character baseCharacter)
            {
                rad += baseCharacter.Template.Stat.CollisionRadius;
            }
		
            double dx = obj1.GetX() - obj2.GetX();
            double dy = obj1.GetY() - obj2.GetY();
            double d;
            if (includeZAxis)
            {
                double dz = obj1.GetZ() - obj2.GetZ();
                d = (dx * dx) + (dy * dy) + (dz * dz);
                return d <= ((range * range) + (2 * range * rad) + (rad * rad));
            }
            d = (dx * dx) + (dy * dy);
            return d <= ((range * range) + (2 * range * rad) + (rad * rad));
        }
        
        public static double ConvertHeadingToDegree(int heading)
        {
            if (heading == 0)
            {
                return 360D;
            }
            return (9.0D * heading) / 1610.0D; // = 360.0 * (heading / 64400.0)
        }
        
        public static int CalculateHeadingFrom(int obj1X, int obj1Y, int obj2X, int obj2Y)
        {
            return (int) ((Math.Atan2(obj1Y - obj2Y, obj1X - obj2X) * 10430.379999999999D) + 32768.0D);
        }
        
        public static int CalculateHeadingFrom(double dx, double dy)
        {
            double angleTarget = Math.Atan2(dy, dx).ToRadians();
            if (angleTarget < 0.0D)
            {
                angleTarget = 360.0D + angleTarget;
            }
            return (int) (angleTarget * 182.04444444399999D);
        }
        
        public static double GetDistanceSq(WorldObject worldObject, PlayerInstance playerInstance)
        {
            return GetDistanceSq(worldObject.GetX(), worldObject.GetY(), worldObject.GetZ(), playerInstance);
        }

        private static double GetDistanceSq(int x, int y, int z, PlayerInstance playerInstance)
        {
            double dx = x - playerInstance.GetX();
            double dy = y - playerInstance.GetY();
            double dz = z - playerInstance.GetZ();
            return (dx * dx) + (dy * dy) + (dz * dz);
        }
    }
}