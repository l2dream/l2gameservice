﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;
using Config;
using Core;
using Core.Model;
using Core.Network;
using Core.Network.Handlers;
using DataBase;
using L2Logger;

namespace L2GameService
{
    class Program
    {
        static async Task Main()
        {
            ClassLoggerConfigurator.ConfigureClassLogger($"{Assembly.GetExecutingAssembly().Location}.log");
            
            LoggerManager.Info("Starting GameService...");
            
            IServiceCollection services = new ServiceCollection();
            ConfigureServices(services);

            IServiceProvider serviceProvider = services.BuildServiceProvider();

            await Task.Factory.StartNew(serviceProvider.GetService<LoginServiceController>().StartAsync);
            await Task.Factory.StartNew(serviceProvider.GetService<GameService>().StartAsync);
            Process.GetCurrentProcess().WaitForExit();
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            ConfigDependencyBinder.Bind(services);
            DataBaseDependencyBinder.Bind(services);
            CoreDependencyBinder.Bind(services);
            
            services.AddSingleton<IdFactory>();
            services.AddSingleton<Initializer>();
            services.AddSingleton<LoginServicePacketHandler>();
            services.AddSingleton<LoginServiceController>();
            services.AddSingleton<GameServicePacketHandler>();
            services.AddSingleton<GameServiceController>();
            services.AddSingleton<ClientManager>();
            services.AddSingleton<GameService>();
        }
    }
}
