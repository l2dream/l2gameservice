﻿namespace LoginService.Enum
{
    internal enum LoginClientState
    {
        Connected,
        AuthedGG,
        AuthedLogin
    }
}
