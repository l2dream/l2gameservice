﻿using System;
using System.Threading.Tasks;

namespace LoginService.Network.ClientPackets
{
    internal abstract class LoginClientPacket
    {
        protected readonly IServiceProvider ServiceProvider;

        protected LoginClientPacket(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }

        public abstract Task ReadImpl();
    }
}
