﻿using System;
using System.Threading.Tasks;
using Config;
using DataBase.Entities;
using DataBase.Interfaces;
using LoginService.Enum;
using LoginService.Network.ServerPackets;
using Microsoft.Extensions.DependencyInjection;
using Security;

namespace LoginService.Network.ClientPackets
{
    internal class RequestAuthLogin : LoginClientPacket
    {
        private readonly LoginClient _client;
        private readonly IAccountRepository _accountRepository;
        private readonly LoginServerConfig _config;
        private readonly byte[] _raw;
        public RequestAuthLogin(IServiceProvider serviceProvider, Packet packet, LoginClient client) : base (serviceProvider)
        {
            _accountRepository = ServiceProvider.GetService<IUnitOfWork>()?.Accounts;
            _client = client;
            _raw = packet.ReadByteArray(128);
            _config = ServiceProvider.GetService<GameConfig>().LoginServerConfig;
        }
        public override async Task ReadImpl()
        {
            _client.State = LoginClientState.AuthedLogin;

            var login = _client.GetDecryptedLogin(_raw);
            var password = _client.GetDecryptedPassword(_raw);

            Account account = await _accountRepository.GetAccountByLoginAsync(login);
            
            if (account == null)
            {
                if (_config.AutoCreateAccount)
                {
                    account = await _accountRepository.CreateAccountAsync(login, password);
                }
                else
                {
                    await _client.SendPacketAsync(new LoginFail(LoginFailReason.ReasonUserOrPassWrong));
                    _client.Close();
                    return;
                }
            }
            else
            {
                if (!account.Password.Equals(L2Security.HashPassword(password))) {
                    await _client.SendPacketAsync(new LoginFail(LoginFailReason.ReasonUserOrPassWrong));
                    _client.Close();
                    return;
                }
            }
            
            _client.ActiveAccount = account;
            _client.State = LoginClientState.AuthedLogin;
            await _client.SendPacketAsync(new LoginOk(_client.SessionKey));
        }
    }
}
