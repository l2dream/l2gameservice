﻿using System;
using System.Threading.Tasks;
using LoginService.Network.ClientPackets;

namespace LoginService.Network.GameServerPackets
{
    class RequestPlayersOnline : LoginClientPacket
    {
        private readonly short _currentPlayers;
        private readonly GameServerClient _gameServerClient;

        public RequestPlayersOnline(IServiceProvider serviceProvider, Packet packet, GameServerClient gameServerClient)
            : base(serviceProvider)
        {
            _gameServerClient = gameServerClient;
            _currentPlayers = packet.ReadShort();
        }

        public override async Task ReadImpl()
        {
            await Task.Run(() => _gameServerClient.CurrentPlayers = _currentPlayers);
        }
    }
}
