L2InterludeServer is an open-source server emulator written in C# .Net Core.
=

This project was created with goal to learn C# .Net Core Framework and have fun.
Big thanks to the teams L2JMobius, L2JServer and L2dotNET for providing ideas and part of codes of their projects.

Current status of project is Developing.
The main goal is to get the basic functionality like L2JMobius/L2JServer.

Technical Specification:
---
- DB MariaDB
- Net Core Framework 3.1
- ORM Dapper 2.0.35
- Redis

What is done:
---
- Create character
- Moving character
- Day/Night
- Spawn mobs
- Moving mobs
- Player Attack mobs
- Player use basic Magic Skills
- Drop mobs
- PickUp drop
- Use Herbs
- Use Potions  
- Teleports
- Learn Skills
- Soul Shots
- Raid Bosses
- Weapon Armor Shop
- Grocery Shop

What is in progress:
---
- Player use Physical Skills
- Player Bow Attack
- Player Dual Sword Attack
- Player Long Sword Attack
- Player Pole Attack
- Implement Agro
- Mobs Magic Attack
- Reduce Adena

Slack
---
- https://l2dream.slack.com/

Links
---
- Client: https://drive.google.com/uc?id=1LcKCQTbRXJvteJcuvc_rnX8i2gT1fcHB&export=download
- System: https://mega.nz/#!t49wiKgZ!PzVAcxcg2o8gRkAiMjH7CUO6lKrBG27npg2JPL1uEq8

License
---
Distributed under the MPL-2.0 License