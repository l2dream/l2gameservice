﻿using System.ComponentModel.DataAnnotations.Schema;
using Dapper.Contrib.Extensions;
using Helpers;

namespace DataBase.Entities
{
    [Dapper.Contrib.Extensions.Table("accounts")]
    public class Account
    {
        [Key]
        public int AccountId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        
        [Column("lastactive")]
        public long LastActive { get; set; }
        
        [Column("lastServer")]
        public byte LastServer { get; set; }
        
        [Column("accessLevel")]
        public int AccessLevel { get; set; }

        public Account()
        {
            LastActive = DateTimeHelper.CurrentUnixTimeMillis();
            AccessLevel = 0;
            LastServer = 1;
        }
    }
}