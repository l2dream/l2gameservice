﻿using Dapper.FluentMap.Mapping;

namespace DataBase.Entities.Map
{
    public class CharacterMap : EntityMap<CharacterEntity>
    {
        public CharacterMap()
        {
            Map(i => i.AccountName).ToColumn("account_name");
            Map(i => i.ObjectId).ToColumn("obj_Id");
            Map(i => i.Name).ToColumn("char_name");
            Map(i => i.MaxHp).ToColumn("maxHp");
            Map(i => i.CurHp).ToColumn("curHp");
            Map(i => i.MaxCp).ToColumn("maxCp");
            Map(i => i.CurCp).ToColumn("curCp");
            Map(i => i.MaxMp).ToColumn("maxMp");
            Map(i => i.CurMp).ToColumn("curMp");
            Map(i => i.Accuracy).ToColumn("acc");
            Map(i => i.CriticalHit).ToColumn("crit");
            Map(i => i.EvasionRate).ToColumn("evasion");
            Map(i => i.MAtk).ToColumn("mAtk");
            Map(i => i.MDef).ToColumn("mDef");
            Map(i => i.MAtkSpd).ToColumn("mSpd");
            Map(i => i.PAtk).ToColumn("pAtk");
            Map(i => i.PDef).ToColumn("pDef");
            Map(i => i.PAtkSpd).ToColumn("pSpd");
            Map(i => i.RunSpeed).ToColumn("runSpd");
            Map(i => i.WalkSpeed).ToColumn("walkSpd");
            Map(i => i.Exp).ToColumn("exp");
            Map(i => i.ExpBeforeDeath).ToColumn("expBeforeDeath");
            Map(i => i.Str).ToColumn("str");
            Map(i => i.Con).ToColumn("con");
            Map(i => i.Dex).ToColumn("dex");
            Map(i => i.Int).ToColumn("_int");
            Map(i => i.Men).ToColumn("men");
            Map(i => i.HairStyle).ToColumn("hairStyle");
            Map(i => i.Heading).ToColumn("heading");
            Map(i => i.X).ToColumn("x");
            Map(i => i.Y).ToColumn("y");
            Map(i => i.Z).ToColumn("z");
            Map(i => i.MovementMultiplier).ToColumn("movement_multiplier");
            Map(i => i.AttackSpeedMultiplier).ToColumn("attack_speed_multiplier");
            Map(i => i.CollisionRadius).ToColumn("colRad");
            Map(i => i.CollisionHeight).ToColumn("colHeight");
            Map(i => i.PvpKills).ToColumn("pvpkills");
            Map(i => i.PkKills).ToColumn("pkkills");
            Map(i => i.ClanId).ToColumn("clanid");
            Map(i => i.MaxLoad).ToColumn("maxload");
            Map(i => i.ClassId).ToColumn("classid");
            Map(i => i.BaseClass).ToColumn("base_class");
            Map(i => i.DeleteTimer).ToColumn("deletetime");
            Map(i => i.CanCraft).ToColumn("cancraft");
            Map(i => i.AccessLevel).ToColumn("accesslevel");
            Map(i => i.Online).ToColumn("online");
            Map(i => i.OnlineTime).ToColumn("onlinetime");
            Map(i => i.ClanPrivileges).ToColumn("clan_privs");
            Map(i => i.WantsPeace).ToColumn("wantspeace");
            Map(i => i.In7SDungeon).ToColumn("isin7sdungeon");
            Map(i => i.PowerGrade).ToColumn("power_grade");
            Map(i => i.LastRecomDate).ToColumn("last_recom_date");
            Map(i => i.NameColor).ToColumn("name_color");
            Map(i => i.TitleColor).ToColumn("title_color");
            Map(i => i.Aio).ToColumn("aio");
            Map(i => i.AioEnd).ToColumn("aio_end");
        }
    }
}
