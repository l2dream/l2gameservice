﻿using Dapper.FluentMap.Mapping;

namespace DataBase.Entities.Map
{
    public class ItemMap : EntityMap<ItemEntity>
    {
        public ItemMap()
        {
            Map(i => i.OwnerId).ToColumn("owner_id");
            Map(i => i.ObjectId).ToColumn("object_id");
            Map(i => i.ItemId).ToColumn("item_id");
            Map(i => i.Count).ToColumn("count");
            Map(i => i.EnchantLevel).ToColumn("enchant_level");
            Map(i => i.Loc).ToColumn("loc");
            Map(i => i.LocData).ToColumn("loc_data");
            Map(i => i.PriceSell).ToColumn("price_sell");
            Map(i => i.PriceBuy).ToColumn("price_buy");
            Map(i => i.TimeOfUse).ToColumn("time_of_use");
            Map(i => i.CustomType1).ToColumn("custom_type1");
            Map(i => i.CustomType2).ToColumn("custom_type2");
            Map(i => i.ManaLeft).ToColumn("mana_left");
        }
    }
}