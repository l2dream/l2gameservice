﻿namespace DataBase.Entities
{
    public class ItemEntity
    {
        public int OwnerId { get; set; }
        public int ObjectId { get; set; }
        public int ItemId { get; set; }
        public int Count { get; set; }
        public int EnchantLevel { get; set; }
        public string Loc { get; set; }
        public int LocData { get; set; }
        public int PriceSell { get; set; }
        public int PriceBuy { get; set; }
        public int TimeOfUse { get; set; }
        public int CustomType1 { get; set; }
        public int CustomType2 { get; set; }
        public double ManaLeft { get; set; }
    }
}