﻿
namespace DataBase.Entities
{
    [Dapper.Contrib.Extensions.Table("characters")]
    public class CharacterEntity
    {
        public string AccountName { get; set; }
        public int ObjectId { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public int MaxHp { get; set; }
        public int CurHp { get; set; }
        public int MaxCp { get; set; }
        public int CurCp { get; set; }
        public int MaxMp { get; set; }
        public int CurMp { get; set; }
        public int Accuracy { get; set; }
        public int CriticalHit { get; set; }
        public int EvasionRate { get; set; }
        public int MAtk { get; set; }
        public int MDef { get; set; }
        public int MAtkSpd { get; set; }
        public int PAtk { get; set; }
        public int PDef { get; set; }
        public int PAtkSpd { get; set; }
        public int RunSpeed { get; set; }
        public int WalkSpeed { get; set; }
        public int Str { get; set; }
        public int Con { get; set; }
        public int Dex { get; set; }
        public int Int { get; set; }
        public int Men { get; set; }
        public int Wit { get; set; }
        public int Face { get; set; }
        public int HairStyle { get; set; }
        public int HairColor { get; set; }
        public int Sex { get; set; }
        public int Heading { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }
        public int MovementMultiplier { get; set; }
        public int AttackSpeedMultiplier { get; set; }
        public double CollisionRadius { get; set; }
        public double CollisionHeight { get; set; }
        public long Exp { get; set; }
        public long ExpBeforeDeath { get; set; }
        public long Sp { get; set; }
        public int Karma { get; set; }
        public int PvpKills { get; set; }
        public int PkKills { get; set; }
        public int ClanId { get; set; }
        public int MaxLoad { get; set; }
        public int Race { get; set; }
        public int ClassId { get; set; }
        public int DeleteTimer { get; set; }
        public int CanCraft { get; set; }
        public string Title { get; set; }
        public int AccessLevel { get; set; }
        public int Online { get; set; }
        public long OnlineTime { get; set; }
        public int In7SDungeon { get; set; }
        public int ClanPrivileges { get; set; }
        public int WantsPeace { get; set; }
        public int BaseClass { get; set; }
        public int Newbie { get; set; }
        public int Noble { get; set; }
        public int PowerGrade { get; set; }
        public long LastRecomDate { get; set; }
        public string NameColor { get; set; }
        public string TitleColor { get; set; }
        public int Aio { get; set; }
        public int AioEnd { get; set; }
    }
}