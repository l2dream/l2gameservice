﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataBase.Entities;

namespace DataBase.Interfaces
{
    public interface IItemRepository : IGenericRepository<ItemEntity>
    {
        Task<ItemEntity> CreateItemAsync(ItemEntity itemEntity);
        Task<ItemEntity> UpdateItemAsync(ItemEntity itemEntity);

        Task<List<ItemEntity>> GetInventoryItemsByOwnerId(int ownerId);
        Task<List<ItemEntity>> GetInventoryItemsByOwnerIdAndLocId(int ownerId, string baseLocation, string equipLocation);
    }
}