﻿using System.Threading.Tasks;
using DataBase.Entities;

namespace DataBase.Interfaces
{
    public interface IAccountRepository : IGenericRepository<Account>
    {
        Task<Account> GetAccountByLoginAsync(string login);
        Task<Account> CreateAccountAsync(string login, string password);
    }
}