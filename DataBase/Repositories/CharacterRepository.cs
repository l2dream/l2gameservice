﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DataBase.Entities;
using DataBase.Interfaces;
using L2Logger;
using Microsoft.Extensions.DependencyInjection;

namespace DataBase.Repositories
{
    public class CharacterRepository : ICharacterRepository
    {
        private readonly ConnectionFactory _connectionFactory;
        public CharacterRepository(IServiceProvider serviceProvider)
        {
            _connectionFactory = serviceProvider.GetService<ConnectionFactory>();
        }
        
        public Task<CharacterEntity> GetByIdAsync(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task<IReadOnlyList<CharacterEntity>> GetAllAsync()
        {
            throw new System.NotImplementedException();
        }

        public Task<int> AddAsync(CharacterEntity entity)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> UpdateAsync(CharacterEntity entity)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> DeleteAsync(int id)
        {
            throw new System.NotImplementedException();
        }

        public async Task<CharacterEntity> CreateCharacterAsync(CharacterEntity characterEntity)
        {
            try
            {
                using (var connection = _connectionFactory.GetDbConnection())
                {
                    connection.Open();
                    string sql =
                        "INSERT INTO characters (account_name,obj_Id,char_name,level,maxHp,curHp,maxCp,curCp,maxMp,curMp,acc,crit,evasion,mAtk,mDef,mSpd,pAtk,pDef,pSpd,runSpd,walkSpd,str,con,dex,_int,men,wit,face,hairStyle,hairColor,sex,x,y,z,movement_multiplier,attack_speed_multiplier,colRad,colHeight,exp,sp,karma,pvpkills,pkkills,clanid,maxload,race,classid,deletetime,cancraft,title,accesslevel,online,isin7sdungeon,clan_privs,wantspeace,base_class,newbie,nobless,power_grade,last_recom_date,name_color,title_color,aio,aio_end) values (@AccountName,@ObjectId,@Name,@Level,@MaxHp,@CurHp,@MaxCp,@CurCp,@MaxMp,@CurMp,@Accuracy,@CriticalHit,@EvasionRate,@MAtk,@MDef,@MAtkSpd,@PAtk,@PDef,@PAtkSpd,@RunSpeed,@WalkSpeed,@Str,@Con,@Dex,@Int,@Men,@Wit,@Face,@HairStyle,@HairColor,@Sex,@X,@Y,@Z,@MovementMultiplier,@AttackSpeedMultiplier,@CollisionRadius,@CollisionHeight,@Exp,@Sp,@Karma,@Pvpkills,@Pkkills,@Clanid,@Maxload,@Race,@Classid,@DeleteTimer,@Cancraft,@Title,@Accesslevel,@Online,@In7Sdungeon,@ClanPrivileges,@WantsPeace,@BaseClass,@Newbie,@Noble,@PowerGrade,@LastRecomDate,@NameColor,@TitleColor,@Aio,@AioEnd);";
                

                    await connection.ExecuteAsync(sql, characterEntity);
                
                    //var dd = await _connection.InsertAsync(character);
                    return characterEntity;
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Error(ex.Message);
                throw;                
            }
        }

        public async Task<CharacterEntity> UpdateCharacterAsync(CharacterEntity characterEntity)
        {
            try
            {
                using (var connection = _connectionFactory.GetDbConnection())
                {
                    connection.Open();
                    string sql =
                        "UPDATE characters SET level=@Level,maxHp=@MaxHp,curHp=@CurHp,maxCp=@MaxCp,curCp=@CurCp,maxMp=@MaxMp,curMp=@CurMp,exp=@Exp,sp=@Sp,x=@X,y=@Y,z=@Z WHERE obj_id=@ObjectId";
                    await connection.ExecuteAsync(sql, characterEntity);
                
                    //var dd = await _connection.InsertAsync(character);
                    return characterEntity;
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Error(ex.Message);
                throw;                
            }
        }

        public async Task<List<CharacterEntity>> GetCharactersByAccountNameAsync(string accountName)
        {
            try
            {
                using (var connection = _connectionFactory.GetDbConnection())
                {
                    connection.Open();
                    string sql = "SELECT * FROM characters WHERE account_name = @AccountName";
                
                    IEnumerable<CharacterEntity> characters = await connection.QueryAsync<CharacterEntity>(sql, new {AccountName = accountName});
                    return characters.ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Error(ex.Message);
                throw;
            }
        }

        public async Task<CharacterEntity> GetCharacterByObjectIdAsync(int objectId)
        {
            try
            {
                using (var connection = _connectionFactory.GetDbConnection())
                {
                    connection.Open();
                    string sql = "SELECT * FROM characters WHERE obj_Id = @ObjectId";
                    IEnumerable<CharacterEntity> characters = await connection.QueryAsync<CharacterEntity>(sql, new {ObjectId = objectId});
                    return characters.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Error(ex.Message);
                throw;
            }
        }

        public bool IsCharacterExist(string characterName)
        {
            try
            {
                using (var connection = _connectionFactory.GetDbConnection())
                {
                    connection.Open();
                    string sql = "SELECT EXISTS(SELECT char_name FROM characters WHERE char_name = @CharacterName)"; 
                    return connection.ExecuteScalar<bool>(sql, new {CharacterName = characterName});
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Error(ex.Message);
                throw;
            }
        }

        public int GetMaxObjectId()
        {
            try
            {
                using (var connection = _connectionFactory.GetDbConnection())
                {
                    connection.Open();
                    int maxItemId = connection.ExecuteScalar<int>("SELECT max(object_id) FROM items;");
                    int maxCharacterId = connection.ExecuteScalar<int>("SELECT max(obj_Id) FROM characters;");
                    return Math.Max(maxCharacterId, maxItemId);
                }
            }
            catch (Exception ex)
            {
                LoggerManager.Error(ex.Message);
                throw;
            }
        }
    }
}